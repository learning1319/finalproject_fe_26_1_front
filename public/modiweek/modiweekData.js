/*eslint-disable*/
  const weekImages = [
    { day: 'Sunday', image: '../../assets/modiweek/photo1.jpg', products: [{ id: 1, name: 'Product 1' }, { id: 2, name: 'Product 2' }] },
    { day: 'Monday', image: '/assets/modiweek/photo2.jpg', products: [{ id: 3, name: 'Product 3' }, { id: 4, name: 'Product 4' }] },
    { day: 'Tuesday', image: '../../assets/modiweek/photo3.jpg', products: [{ id: 5, name: 'Product 5' }, { id: 6, name: 'Product 6' }] },
    { day: 'Wednesday', image: '../../assets/modiweek/photo4.jpg', products: [{ id: 7, name: 'Product 7' }, { id: 8, name: 'Product 8' }] },
    { day: 'Thursday', image: '../../assets/modiweek/photo5.jpg', products: [{ id: 9, name: 'Product 9' }, { id: 10, name: 'Product 10' }] },
    { day: 'Friday', image: '../../assets/modiweek/photo6.jpg', products: [{ id: 11, name: 'Product 11' }, { id: 12, name: 'Product 12' }] },
    { day: 'Saturday', image: '../../assets/modiweek/photo7.jpg', products: [{ id: 13, name: 'Product 13' }, { id: 14, name: 'Product 14' }] },
  ];
  export default weekImages;