/* eslint-disable */
module.exports = {
    "setupFilesAfterEnv": ['<rootDir>/src/setupTests.js'],
    "testEnvironment": 'jsdom',
    "moduleNameMapper": {
        '\\.(css|less|scss)$': 'identity-obj-proxy',
        "^axios$": "axios/dist/node/axios.cjs"
    },
    "transform": {
        '^.+\\.(js|jsx)$': 'babel-jest',
    },
}