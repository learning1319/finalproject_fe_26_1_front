import PropTypes from 'prop-types';
import {
  Typography,
  List,
  ListItem,
  ListItemText,
  // useTheme,
} from '@mui/material';

const CareerOpportunities = () => (
  <>
    <Typography
      sx={{
        fontSize: {
          fontSize: {
            mobile: '14px',
            tablet: '16px',
          },
        },
        textAlign: 'left',
        fontWeight: '700',
        mb: 0,
        mt: 4,
      }}
    >
      Career Opportunities
    </Typography>
    <List>
      <ListItem
        sx={{
          pl: 0,
        }}
      >
        <ListItemText
          sx={{
            textAlign: 'justify',
          }}
          primary="Sales Associates: Our
                        sales associates are the face of Modimal.
                        If you have a passion for fashion and love
                        interacting with customers, this role is
                        perfect for you. As a sales associate,
                        you will provide exceptional customer service,
                        assist shoppers in finding their perfect outfits,
                        and help maintain our store's visual standards."
          primaryTypographyProps={{
            sx: {
              fontSize: {
                mobile: '14px',
                tablet: '16px',
              },
              pt: 0,
            },
          }}
        />
      </ListItem>
      <ListItem
        sx={{
          pl: 0,
        }}
      >
        <ListItemText
          sx={{
            textAlign: 'justify',
          }}
          primary="Visual Merchandisers:
                        Creativity and an eye for detail are key in this role.
                        Our visual merchandisers ensure that our
                        stores look stunning and on-brand, creating an
                        inviting shopping experience for our customers."
          primaryTypographyProps={{
            sx: {
              fontSize: {
                mobile: '14px',
                tablet: '16px',
              },
            },
          }}
        />
      </ListItem>
      <ListItem
        sx={{
          pl: 0,
          textAlign: 'justify',
        }}
      >
        <ListItemText
          primary="Buyers: Our buyers play
                        a crucial role in curating our collections.
                        If you have a keen sense of fashion trends
                        and excellent negotiation skills, consider
                        joining our buying team to help us source
                        the best products for our customers."
          primaryTypographyProps={{
            sx: {
              fontSize: {
                mobile: '14px',
                tablet: '16px',
              },
            },
          }}
        />
      </ListItem>
      <ListItem
        sx={{
          pl: 0,
        }}
      >
        <ListItemText
          sx={{
            textAlign: 'justify',
          }}
          primary="Marketing Specialists:
                        Help us spread the word about Modimal!
                        Our marketing specialists work on campaigns,
                        social media, and events to build brand
                        awareness and engage with our community."
          primaryTypographyProps={{
            sx: {
              fontSize: {
                mobile: '14px',
                tablet: '16px',
              },
            },
          }}
        />
      </ListItem>
    </List>
  </>
);
CareerOpportunities.propTypes = {
  theme: PropTypes.shape({
    typography: PropTypes.shape({
      mobile_bodyMD: PropTypes.shape({
        fontSize: PropTypes.string,
        fontWeight: PropTypes.number,
        lineHeight: PropTypes.string,
      }).isRequired,
      desktop_bodyMD: PropTypes.shape({
        fontSize: PropTypes.string,
        fontWeight: PropTypes.number,
        lineHeight: PropTypes.string,
      }).isRequired,
    }).isRequired,
    breakpoints: PropTypes.shape({
      up: PropTypes.func,
    }).isRequired,
  }).isRequired,
};

export default CareerOpportunities;
