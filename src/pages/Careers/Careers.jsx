import {
  Box,
  Container,
  Typography,
} from '@mui/material';
import { useEffect } from 'react';
import CareerOpportunities from './CareerOpportunities';

const Career = () => {
  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  }, []);
  return (
    <Box
      component="section"
      sx={{
        padding: '2rem 0',
        textAlign: 'center',
      }}
    >
      <Container
        sx={{
          minHeight: '54vh',
        }}
      >
        <Typography
          gutterBottom
          sx={{
            fontSize: {
              fontSize: {
                mobile: '14px',
                tablet: '16px',
              },
              mb: 1,
            },
            textAlign: 'left',
            fontWeight: '700',

          }}
        >
          Careers at Modimal
        </Typography>
        <Typography
          paragraph
          sx={{
            fontSize: {
              mobile: '14px',
              tablet: '16px',
            },
            textAlign: 'justify',
          }}
        >
          At Modimal, we
          believe in fostering a dynamic and inclusive
          work environment where creativity and innovation
          thrive. Our mission is to provide our customers
          with the latest fashion trends while delivering
          exceptional service. To achieve this, we are always
          on the lookout for talented individuals who share
          our passion for fashion and excellence.
        </Typography>
        <CareerOpportunities />
      </Container>
    </Box>
  );
};

export default Career;
