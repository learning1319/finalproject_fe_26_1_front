import React, { useEffect, useState } from 'react';
import {
  Box, Typography, TextField, FormControlLabel, Checkbox, Button, Grid, Container,
} from '@mui/material';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { styled } from '@mui/system';
import {
  useElements, CardCvcElement, CardNumberElement, CardExpiryElement,
} from '@stripe/react-stripe-js';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import { useDispatch, useSelector } from 'react-redux';
import { ReactComponent as VisaComponent } from '../../assets/svg/Visa/visa.svg';
import { ReactComponent as AmericanExpress } from '../../assets/svg/AmmericanExpress/americanEx.svg';
import { ReactComponent as MasterCard } from '../../assets/svg/MasterCard/MasterCard.svg';
import { ReactComponent as PayPal } from '../../assets/svg/PayPal/payPal.svg';
import { ReactComponent as Profil } from '../../assets/svg/profile.svg';
import { ReactComponent as Email } from '../../assets/svg/Email/email.svg';
import { ReactComponent as Flag } from '../../assets/svg/Flag/flag.svg';
import { ReactComponent as Home } from '../../assets/svg/Home/home.svg';
import { ReactComponent as City } from '../../assets/svg/City/city.svg';
import { ReactComponent as Zip } from '../../assets/svg/Zip/zip.svg';
import { ReactComponent as Phone } from '../../assets/svg/Phone/phone.svg';
import BASE_URL from '../../constants/constants.js';
import stripePromise from '../../utils/stripe.js';
import { getAddress } from '../../redux/slices/addressSlice.js';
import { addCard } from '../../redux/slices/paymentSlice.js';
import { addOrderedProductsAsync } from '../../redux/slices/cartSlice.js';

const CustomTextField = styled(TextField)({
  margin: '10px 0',
  width: '100%',
});

const validationSchema = Yup.object().shape({
  name: Yup.string()
    .required('Name is required')
    .matches(/^[\p{L}\s]+$/u, 'Name cannot contain numbers or special characters'),
  email: Yup.string()
    .email('Invalid email address')
    .required('Email is required'),
  country: Yup.string()
    .required('Country is required')
    .matches(/^[\p{L}\s]+$/u, 'Country cannot contain numbers or special characters'),
  addressLine1: Yup.string()
    .required('First address line is required'),
  city: Yup.string()
    .required('City is required')
    .matches(/^[\p{L}\s]+$/u, 'City cannot contain numbers or special characters'),
  zip: Yup.string()
    .required('ZIP is required')
    .matches(/^\d{2}-\d{3}$/, 'ZIP format must be **-***'),
  phone: Yup.string()
    .required('Phone number is required')
    .matches(/^\d{10}$/, 'Phone number must be exactly 10 digits'),
});

const PaymentForm = () => {
  const navigate = useNavigate();
  const [isDisabled, setIsDisabled] = useState(false);
  const products = useSelector((state) => state.cart.items);
  const isLogged = useSelector((state) => state.login.isLogged);
  const currentUser = useSelector((state) => state.login.user);
  const token = useSelector((state) => state.login.token);
  const address = useSelector((state) => state.address.address);
  const dispatch = useDispatch();
  const elements = useElements();
  const orderedProducts = useSelector(((state) => state.cart.orderedItems));

  const price = products?.reduce((total, item) => total + item.price, 0) || 1;

  const [initialValues, setInitialValues] = useState({
    name: '',
    email: '',
    company: '',
    addressLine1: '',
    addressLine2: '',
    zip: '',
    country: '',
    city: '',
    phone: '',
    defaultAddress: false,
    alternativeAddress: false,
  });

  useEffect(() => {
    const fetchAddress = async (userId, authToken) => {
      const response = await dispatch(getAddress(userId, authToken));
      if (response) {
        setInitialValues({
          name: response.firstName ? `${response.firstName} ${response.lastName}` : '',
          email: response.email || '',
          company: response.company || '',
          addressLine1: response.addressLine1 || '',
          addressLine2: response.addressLine2 || '',
          zip: response.zip || '',
          city: response.city || '',
          phone: response.phone || '',
          country: response.country || '',
          defaultAddress: true,
          alternativeAddress: false,
        });
      }
    };
    if (isLogged) { fetchAddress(currentUser._id, token); }
  }, [isLogged, currentUser._id, token, dispatch]);

  const handleSubmit = async (values) => {
    setIsDisabled(true);

    const cardNumberElement = elements.getElement(CardNumberElement);
    const cardExpiryElement = elements.getElement(CardExpiryElement);
    const cardCvcElement = elements.getElement(CardCvcElement);

    const { name, email } = values;

    const purchaseData = {
      userId: currentUser._id,
      name,
      email,
      currency: 'usd',
      shippingDetails: address,
      paymentDetails: {
        method: 'card',
        currency: 'usd',
        billingAddress: values,
      },
      totalPrice: price,
      products,
    };

    const response = await axios.post(`${BASE_URL}/purchase`, purchaseData, {
      headers: {
        Authorization: token,
      },
    });

    const { clientSecret } = response.data;
    const stripe = await stripePromise;

    if (!stripe || !elements) { return; }

    await addCard(currentUser._id, {
      cardNumber: cardNumberElement?.value,
      cardExpiryDate: cardExpiryElement?.value,
      cardCVV: cardCvcElement?.value,
    }, token, false);

    const { error, paymentIntent } = await stripe.confirmCardPayment(clientSecret, {
      payment_method: {
        card: cardNumberElement,
        billing_details: {
          name,
          email,
        },
      },
    });

    if (error) {
      navigate('/failed');
    } else if (paymentIntent.status === 'succeeded') {
      dispatch(addOrderedProductsAsync(orderedProducts));
      navigate('/success');
    }
  };

  return (
    <Container>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        enableReinitialize
        onSubmit={(values, { setTouched }) => {
          handleSubmit(values);
          setTouched({});
        }}
      >
        {({
          errors, touched, handleChange, values, setFieldValue, setTouched,
        }) => (
          <Form>
            <Box
              sx={{
                display: 'flex',
                flexDirection: {
                  small: 'column',
                  tablet: 'row',
                },
                gap: '40px',
                '& .MuiOutlinedInput-root': {
                  borderRadius: 0,
                  borderColor: '#606060',
                  p: 2,
                  '& input': {
                    fontSize: '14px',
                    fontWeight: 400,
                    px: 1,
                    py: 0,
                  },
                },
              }}
            >
              <Box sx={{ pt: 4 }}>
                <Typography sx={{ typography: { small: 'mobile_h4', tablet: 'desktop_h6' } }}>
                  Billing Address
                </Typography>
                <hr />
                <FormControlLabel
                  control={(
                    <Checkbox
                      name="defaultAddress"
                      checked={values.defaultAddress}
                      onChange={(e) => {
                        const { checked } = e.target;
                        setFieldValue('defaultAddress', checked);

                        if (checked) {
                          setFieldValue('name', initialValues.name);
                          setFieldValue('email', initialValues.email);
                          setFieldValue('country', initialValues.country);
                          setFieldValue('addressLine1', initialValues.addressLine1);
                          setFieldValue('addressLine2', initialValues.addressLine2);
                          setFieldValue('city', initialValues.city);
                          setFieldValue('zip', initialValues.zip);
                          setFieldValue('phone', initialValues.phone);

                          setTouched({
                            name: false,
                            email: false,
                            country: false,
                            addressLine1: false,
                            addressLine2: false,
                            city: false,
                            zip: false,
                            phone: false,
                          });
                        } else {
                          setFieldValue('name', '');
                          setFieldValue('email', '');
                          setFieldValue('country', '');
                          setFieldValue('addressLine1', '');
                          setFieldValue('addressLine2', '');
                          setFieldValue('city', '');
                          setFieldValue('zip', '');
                          setFieldValue('phone', '');
                        }
                      }}
                    />
                        )}
                  label="Default (Same As Billing Address)"
                  sx={{
                    '& .MuiFormControlLabel-label': {
                      typography: { small: 'mobile_bodyXS', tablet: 'desktop_bodyMD' },
                    },
                  }}
                />

                <FormControlLabel
                  control={(
                    <Checkbox
                      name="alternativeAddress"
                      checked={values.alternativeAddress}
                      onChange={handleChange}
                    />
                        )}
                  label="Add An Alternative Delivery Address"
                  sx={{
                    '& .MuiFormControlLabel-label': {
                      typography: { small: 'mobile_bodyXS', tablet: 'desktop_bodyMD' },
                    },
                  }}
                />

                <Field
                  name="name"
                  as={CustomTextField}
                  label="Name"
                  error={touched.name && !!errors.name}
                  helperText={touched.name && errors.name}
                  InputProps={{
                    startAdornment: <Profil width="16px" height="16px" position="start" />,
                  }}
                  onChange={(e) => {
                    setTouched({ ...touched, name: true });
                    handleChange(e);
                  }}
                />

                <Field
                  name="email"
                  as={CustomTextField}
                  label="Email"
                  error={touched.email && !!errors.email}
                  helperText={touched.email && errors.email}
                  InputProps={{
                    startAdornment: <Email position="start" />,
                  }}
                  onChange={(e) => {
                    setTouched({ ...touched, email: true });
                    handleChange(e);
                  }}
                />

                <Field
                  name="country"
                  as={CustomTextField}
                  label="Country"
                  error={touched.country && !!errors.country}
                  helperText={touched.country && errors.country}
                  InputProps={{
                    startAdornment: <Flag position="start" />,
                  }}
                  onChange={(e) => {
                    setTouched({ ...touched, country: true });
                    handleChange(e);
                  }}
                />

                <Field
                  name="addressLine1"
                  as={CustomTextField}
                  label="Address Line1"
                  error={touched.addressLine1 && !!errors.addressLine1}
                  helperText={touched.addressLine1 && errors.addressLine1}
                  InputProps={{
                    startAdornment: <Home position="start" />,
                  }}
                  onChange={(e) => {
                    setTouched({ ...touched, addressLine1: true });
                    handleChange(e);
                  }}
                />

                <Field
                  name="addressLine2"
                  as={CustomTextField}
                  label="Address Line2"
                  error={touched.addressLine2 && !!errors.addressLine2}
                  helperText={touched.addressLine2 && errors.addressLine2}
                  InputProps={{
                    startAdornment: <Home position="start" />,
                  }}
                  onChange={(e) => {
                    setTouched({ ...touched, addressLine2: true });
                    handleChange(e);
                  }}
                />

                <Field
                  name="city"
                  as={CustomTextField}
                  label="City / Suburb"
                  error={touched.city && !!errors.city}
                  helperText={touched.city && errors.city}
                  InputProps={{
                    startAdornment: <City position="start" />,
                  }}
                  onChange={(e) => {
                    setTouched({ ...touched, city: true });
                    handleChange(e);
                  }}
                />

                <Field
                  name="zip"
                  as={CustomTextField}
                  label="Zip / Postcode"
                  error={touched.zip && !!errors.zip}
                  helperText={touched.zip && errors.zip}
                  InputProps={{
                    startAdornment: <Zip position="start" />,
                  }}
                  onChange={(e) => {
                    setTouched({ ...touched, zip: true });
                    handleChange(e);
                  }}
                />

                <Field
                  name="phone"
                  as={CustomTextField}
                  label="Phone"
                  error={touched.phone && !!errors.phone}
                  helperText={touched.phone && errors.phone}
                  InputProps={{
                    startAdornment: <Phone position="start" />,
                  }}
                  onChange={(e) => {
                    setTouched({ ...touched, phone: true });
                    handleChange(e);
                  }}
                />
              </Box>

              <Box sx={{ pt: 4 }}>
                <Typography sx={{ typography: { small: 'mobile_h4', tablet: 'desktop_h6' } }}>
                  Payment
                </Typography>
                <hr />
                <Typography sx={{ typography: { small: 'mobile_bodyMD', tablet: 'desktop_bodyMD' } }}>
                  Please Choose Your Payment Method
                </Typography>
                <Box display="flex" justifyContent="space-around" my={2}>
                  <AmericanExpress />
                  <VisaComponent />
                  <MasterCard />
                  <PayPal />
                </Box>

                <Grid container sx={{ flexWrap: 'nowrap', alignItems: 'center', justifyContent: 'space-between' }}>
                  <Typography sx={{
                    textTransform: 'capitalize',
                    whiteSpace: 'nowrap',
                    typography: {
                      small: 'mobile_bodyMD',
                      tablet: 'desktop_bodyMD',
                    },
                  }}
                  >
                    Card Number*
                  </Typography>
                  <Grid sx={{ pl: 1, width: '70%' }}>
                    <Box sx={{
                      border: '1px solid #606060',
                      borderRadius: 0,
                      p: 2,
                    }}
                    >
                      <CardNumberElement options={{ style: { base: { fontSize: '14px' } } }} />
                    </Box>
                  </Grid>
                </Grid>

                <Grid
                  container
                  sx={{
                    flexWrap: 'nowrap', alignItems: 'center', mt: 2, justifyContent: 'space-between',
                  }}
                >
                  <Typography sx={{
                    textTransform: 'capitalize',
                    whiteSpace: 'nowrap',
                    typography: {
                      small: 'mobile_bodyMD',
                      tablet: 'desktop_bodyMD',
                    },
                  }}
                  >
                    Expiry Date*
                  </Typography>
                  <Grid sx={{ pl: 1, width: '60%' }}>
                    <Box sx={{
                      border: '1px solid #606060',
                      borderRadius: 0,
                      p: 2,
                    }}
                    >
                      <CardExpiryElement options={{ style: { base: { fontSize: '14px' } } }} />
                    </Box>
                  </Grid>
                </Grid>

                <Grid
                  container
                  sx={{
                    flexWrap: 'nowrap', alignItems: 'center', mt: 2, justifyContent: 'space-between',
                  }}
                >
                  <Typography sx={{
                    textTransform: 'capitalize',
                    whiteSpace: 'nowrap',
                    typography: {
                      small: 'mobile_bodyMD',
                      tablet: 'desktop_bodyMD',
                    },
                  }}
                  >
                    Security Code*
                  </Typography>
                  <Grid sx={{ pl: 1, width: '60%' }}>
                    <Box sx={{
                      border: '1px solid #606060',
                      borderRadius: 0,
                      p: 2,
                    }}
                    >
                      <CardCvcElement options={{ style: { base: { fontSize: '14px' } } }} />
                    </Box>
                  </Grid>
                </Grid>

                <Box sx={{ textAlign: 'center', pt: 4 }}>
                  <Button type="submit" variant="contained" disabled={isDisabled} color="primary" sx={{ typography: 'desktop_bodyMD', textTransform: 'capitalize', width: '100%' }}>
                    Pay And Place Order
                  </Button>
                </Box>

                <Typography sx={{ typography: { small: 'mobile_bodyXS', tablet: 'bodyXS' } }} display="block" py={2}>
                  By Clicking On Pay And Place Order You Agree To Make Your Purchase From
                  Gold-E. A Merchant Of Record For This Transaction,
                  Subject To Gold-E's Term Of Service
                  That Your Transaction Will Be Handled By Global-E
                  In Accordance With Global-E's Privacy Policy
                  And That Global-E Will Share Your Information
                  Excluding The Payment Details With Gold-E.
                </Typography>
              </Box>
            </Box>
          </Form>
        )}
      </Formik>
    </Container>
  );
};

export default PaymentForm;
