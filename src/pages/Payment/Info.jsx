import React from 'react';
import { IconButton, Box } from '@mui/material';
import InfoIcon from '@mui/icons-material/Info';
import Tooltip from '@mui/material/Tooltip';

const longText = `
 The security code, also known as the CVV (Card Verification Value) or CVC (Card Verification Code), is a 3- or 4-digit number printed on your credit card.
 Visa/MasterCard/Discover: The CVV is a 3-digit number located on the back of your card, usually near the signature strip.
  American Express: The CVV is a 4-digit number located on the front of your card, above the card number.`;

const SecurityCodeInfo = () => (
  <Box>
    <Tooltip title={longText}>
      <IconButton>
        <InfoIcon />
      </IconButton>
    </Tooltip>
  </Box>
);

export default SecurityCodeInfo;
