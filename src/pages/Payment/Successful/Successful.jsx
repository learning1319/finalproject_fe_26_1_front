import React from 'react';
import {
  Box, Typography, Container, Divider,
} from '@mui/material';
import { ReactComponent as CheckCircle } from '../../../assets/svg/CheckCircle/check-circle-outline.svg';

const PaymentSuccessful = () => (
  <Container sx={{
    py: {
      small: 3,
      tablet: 20,
    },
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  }}
  >
    <Box sx={{
      width: {
        small: '48px',
        tablet: '65px',
      },
      height: {
        small: '48px',
        tablet: '65px',
      },

    }}
    >
      <CheckCircle fill="#00966D" />
    </Box>
    <Typography variant="mobile_h1" color="success.main" sx={{ pt: 3 }}>
      Payment Successful
    </Typography>
    <Typography sx={{
      display: 'block',
      pt: 3,
      textAlign: 'center',
      typography: {
        small: 'mobile_bodyMD',
        tablet: 'desktop_sustainability',
      },
    }}
    >
      Thank You For Choosing Modimal. Your Order Will Be Generated Based On Your Delivery Request.
    </Typography>
    <Typography sx={{
      pt: 2,
      textAlign: 'center',
      typography: {
        small: 'mobile_bodyMD',
        tablet: 'desktop_sustainability',
      },
    }}
    >
      The Receipt Has Been Sent To Your Email
    </Typography>
    <Divider sx={{ pt: 5 }} />
    <Typography
      color="#404040"
      sx={{
        textAlign: 'center',
        typography: {
          small: 'desktop_overlineSM',
          tablet: 'desktop_bodyLG',
        },
      }}
    >
      Please Contact Us For Any Query
    </Typography>
    <Typography
      color="#404040"
      sx={{
        pt: 2,
        typography: {
          small: 'mobile_bodyXS',
          tablet: 'button_SM',
        },
      }}
    >
      +1(929)460-3208
    </Typography>
    <Typography
      color="#404040"
      sx={{
        pt: 2,
        typography: {
          small: 'mobile_bodyXS',
          tablet: 'button_SM',
        },
      }}
    >
      Or
    </Typography>
    <Typography
      color="#404040"
      sx={{
        pt: 2,
        typography: {
          small: 'mobile_bodyXS',
          tablet: 'button_SM',
        },
      }}
    >
      Hello @ Modimal.Com
    </Typography>
  </Container>
);

export default PaymentSuccessful;
