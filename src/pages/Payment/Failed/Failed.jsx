import React from 'react';
import {
  Box, Typography, Container, Button,
} from '@mui/material';
import { useNavigate } from 'react-router-dom';
import { ReactComponent as Back } from '../../../assets/svg/Back/back.svg';
import { ReactComponent as FailedIcon } from '../../../assets/svg/Failed/failed.svg';

const Failed = () => {
  const navigate = useNavigate();

  return (
    <Container sx={{
      py: {
        small: 3,
        tablet: 20,
      },
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
    }}
    >
      <Box sx={{
        width: {
          small: '48px',
          tablet: '65px',
        },
        height: {
          small: '48px',
          tablet: '65px',
        },

      }}
      >
        <FailedIcon fill="#C30000" />
      </Box>
      <Typography variant="mobile_h1" color="error.main" sx={{ pt: 3 }}>
        Sorry, Payment failed
      </Typography>
      <Typography sx={{
        display: 'block',
        textTransform: 'capitalize',
        pt: 3,
        textAlign: 'center',
        typography: {
          small: 'mobile_bodyMD',
          tablet: 'desktop_sustainability',
        },
      }}
      >
        Unfortunately. your order Cannot Be Completed.

      </Typography>
      <Typography sx={{
        textTransform: 'capitalize',
        pt: 1,
        textAlign: 'center',
        typography: {
          small: 'mobile_bodyMD',
          tablet: 'desktop_sustainability',
        },
      }}
      >
        Please ensure that the billing address you provided
        is the same one where your debit/credit card is registered.
      </Typography>
      <Typography sx={{
        textTransform: 'capitalize',
        pt: 1,
        textAlign: 'center',
        typography: {
          small: 'mobile_bodyMD',
          tablet: 'desktop_sustainability',
        },
      }}
      >
        Alternatively, please try a different payment method.
      </Typography>

      <Button
        onClick={() => { navigate('/payment'); }}
        type="submit"
        variant="contained"
        color="primary"
        sx={{
          textTransform: 'capitalize', mt: 6, maxWidth: '390px', width: '100%',
        }}
      >
        Retry
      </Button>
      <Button
        type="submit"
        variant="text"
        color="primary"
        onClick={() => { navigate('/cart'); }}
        startIcon={<Back />}
        sx={{
          textTransform: 'capitalize',
          mt: 3,
        }}
      >
        Back to My Orders
      </Button>

    </Container>
  );
};

export default Failed;
