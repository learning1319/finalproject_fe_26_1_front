import { Box, Container } from '@mui/material';
import ShippingForm from '../../components/Shiping/Shiping';
import Cart from '../Cart/Cart';

const ShippingInfo = () => {
  console.log('ShippingInfo component rendered');

  return (
    <Container>
      <Box
        sx={{
          display: 'flex',
          flexDirection: {
            small: 'column',
            tablet: 'row',
          },
          margin: 'auto',
          width: '100%',
          gap: 3,
        }}
      >
        <Box sx={{ flex: 1 }}>
          <ShippingForm />
        </Box>
        <Box sx={{ flex: 1 }}>
          <Cart isInSiping />
        </Box>
      </Box>
    </Container>
  );
};

export default ShippingInfo;
