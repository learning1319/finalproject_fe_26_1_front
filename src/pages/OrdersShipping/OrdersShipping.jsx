/* eslint-disable */
import React, { useEffect } from 'react';
import {
  Box,
  Container,
  Typography,
  useTheme,
} from '@mui/material';

import Section from './Section';

const OrdersShipping = () => {
  const theme = useTheme();
  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  }, []);
  return (
    <Box sx={{ bgcolor: theme.palette.background.paper, py: 4 }}>
      <Container>
        <Typography
          sx={{
            fontSize: {
              small: '12px',
              mobile: "17px",
              tablet: '19px',
            },
            fontWeight: '700',
            mt: {
              tablet: '3',
            },
          }}
        >
          Orders & Shipping
        </Typography>
        <Section title="Order Processing">
          At Modimal, we strive to process your orders as
          quickly and efficiently as possible.
          Once your order is placed, you will receive an
          order confirmation email detailing your purchase.
          Our team then works diligently to prepare your items for shipment.
          Typically, orders are processed within 1-2 business days.
          Please note that processing times may be extended during peak
          shopping periods or due to unforeseen circumstances.
        </Section>
        <Section title="Free Shipping">
          We offer free standard shipping on all
          domestic orders over a certain amount.
          This amount may vary during promotional periods,
          so be sure to check our website or sign up for our
          newsletter to stay informed about current offers.
        </Section>

        <Section title="International Shipping">
          Modimal is pleased to offer international shipping
          to many countries. International shipping costs and
          delivery times vary depending on the destination.
          Please note that international orders may be subject to customs duties,
          taxes, and fees, which are the responsibility of the customer.
          These charges vary by country and are not included in our shipping costs.
        </Section>

        <Section title="Order Tracking">
          Once your order has been shipped, you will receive a
          shipping confirmation email with a tracking number.
          You can use this tracking number to monitor the progress of your
          shipment on the carriers website.
          If you have any issues with tracking your order,
          please contact our customer service team for assistance.
        </Section>

        <Section title="Delivery Times">
          While we strive to deliver your order within the
          estimated time frame, please note that delivery times
          are not guaranteed and may be affected by factors beyond our control,
          such as weather conditions, carrier delays,
          and customs processing for international orders.
        </Section>

        <Section title="Shipping to APO/FPO Addresses">
          We are proud to support our military customers by
          shipping to APO/FPO addresses.
          Please note that delivery times to APO/FPO
          addresses may vary and can take longer than standard domestic shipping.
        </Section>

        <Section title="Lost or Damaged Packages">
          If your package is lost or arrives damaged, please contact
          our customer service team immediately. We will work with the carrier
          to resolve the issue and ensure you receive your order as quickly as possible.
        </Section>

        <Section title="Changes to Your Order">
          If you need to make changes to your order after it has been
          placed, please contact us as soon as possible.
          We will do our best to accommodate your request,
          but please note that once an order has been processed and shipped,
          changes cannot be made.
        </Section>

        <Section title="Contact Us">
          For any questions or concerns regarding
          your order or shipping, please feel free to contact our
          customer service team. We are here to help and ensure your
          shopping experience with Modimal is seamless and enjoyable.
        </Section>
      </Container>
    </Box>
  );
};

export default OrdersShipping;
