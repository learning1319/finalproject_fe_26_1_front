import React from 'react';
import {
  Box,
  Typography,
  useTheme,
}
  from '@mui/material';
import PropTypes from 'prop-types';

const Section = ({ title, children }) => {
  const theme = useTheme();
  return (
    <Box sx={{ mb: 4 }}>
      <Typography
        sx={{
          fontSize: {
            small: '12px',
            mobile: '15px',
            tablet: '16px',
          },
          mt: 3,
          mb: 2,
          fontWeight: '600',
        }}
      >
        {title}
      </Typography>
      <Typography
        sx={{
          ...theme.typography.desktop_bodyMD,
          mb: 2,
          textAlign: 'justify',
          [theme.breakpoints.down('tablet')]: {
            ...theme.typography.mobile_bodyMD,
          },
        }}
      >
        {children}
      </Typography>
    </Box>
  );
};
Section.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};
export default Section;
