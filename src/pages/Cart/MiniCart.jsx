import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Box } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import EmptyCart from '../../components/CartComponents/EmptyCart';
import OrderList from '../../components/CartComponents/OrderList';
import { fetchUserCartItems } from '../../redux/slices/cartSlice.js';
import modifyCartItems from '../../utils/modifyCartItems.js';

const MiniCart = ({ handleOpenCart, isOpenCart }) => {
  const dispatch = useDispatch();
  const [cart, setCart] = useState([]);
  const cartStrings = useSelector((state) => state.cart.processedItems || []);

  useEffect(() => {
    if (cartStrings.length === 0) {
      dispatch(fetchUserCartItems());
    }
  }, [dispatch]);

  useEffect(() => {
    const fetchCartItems = async () => {
      if (cartStrings && cartStrings.length > 0) {
        try {
          const processedItems = await modifyCartItems(cartStrings);
          console.log('Processed Items:', processedItems);
          setCart(processedItems);
        } catch (error) {
          console.error('Error modifying cart items:', error);
        }
      } else {
        console.log('Cart is empty, clearing cart state.');
        setCart([]);
      }
    };

    fetchCartItems();
  }, [cartStrings.join(',')]);
  // Only run this effect when cartStrings changes

  const cartLength = cart.length; // Simplify the cart length calculation

  return (
    <Box
      sx={{
        width: { small: '100%', middle: 420, tablet: 520 },
        position: 'absolute',
        top: {
          small: 0, middle: 55, tablet: 75, large: 90,
        },
        right: 0,
        backgroundColor: 'white',
        transition: 'height 0.3s ease-in-out',
        maxHeight: isOpenCart ? 'auto' : 0,
        overflow: 'hidden',
        zIndex: 11,
        marginRight: { bigScreen: '10%' },
        paddingTop: isOpenCart ? 1 : 0,
        paddingBottom: isOpenCart ? 3 : 0,
        display: 'flex',
        justifyContent: 'center',
      }}
    >
      {cartLength > 0 ? (
        <OrderList cartItems={cart} handleOpenCart={handleOpenCart} />
      ) : (
        <EmptyCart handleOpenCart={handleOpenCart} />
      )}
    </Box>
  );
};

MiniCart.propTypes = {
  isOpenCart: PropTypes.bool,
  handleOpenCart: PropTypes.func.isRequired,
};

export default MiniCart;
