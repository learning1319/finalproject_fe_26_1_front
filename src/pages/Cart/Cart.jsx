import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import {
  Box, Button, Typography, Container, Divider,
} from '@mui/material';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import CartItem from '../../components/CartComponents/CartItem';
import modifyCartItems from '../../utils/modifyCartItems.js';
import { setOrderedProducts, fetchUserCartItems } from '../../redux/slices/cartSlice.js';

const Cart = ({ isInSiping = false }) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [cart, setCart] = useState([]);
  const cartStrings = useSelector((state) => state.cart.processedItems || []);
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);
  const [totalQuantityItems, setTotalQuantityItems] = useState();

  useEffect(() => {
    if (cartStrings.length === 0) {
      dispatch(fetchUserCartItems());
    }
  }, [dispatch]);

  useEffect(() => {
    const fetchCartItems = async () => {
      if (cartStrings && cartStrings.length > 0) {
        try {
          const processedItems = await modifyCartItems(cartStrings);
          setCart(processedItems);
          const totalQuantityReduce = processedItems
            .reduce((total, item) => total + Number(item.quantity), 0);
          setTotalQuantityItems(totalQuantityReduce);
        } catch (error) {
          console.error('Error modifying cart items:', error);
        }
      } else {
        setCart([]);
      }
    };

    fetchCartItems();
  }, [cartStrings.join(',')]);

  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  }, []);

  const subtotal = cart.reduce((total, item) => total + item.price * item.quantity, 0);
  const tax = subtotal * 0.10;
  const shipping = 0;
  const totalOrders = subtotal + tax + shipping;

  const handleNextClick = () => {
    dispatch(setOrderedProducts(cart));
    navigate('/info');
  };
  useEffect(() => {
    setIsButtonDisabled(!cartStrings.length === 0);
  }, [cartStrings]);

  return (
    <Container
      sx={{
        width: '100%',
        height: '100%',
        backgroundColor: 'white',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'space-between',
        mt: { small: 6, tablet: 4 },
        px: isInSiping ? 0 : 'auto',
      }}
    >
      <Box>
        <Box sx={{
          background: 'white',
          mt: '-40px',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'space-between',
        }}
        >
          { !isInSiping && (
          <Box
            sx={{
              position: 'relative',
              textAlign: { small: 'left', tablet: 'right' },
              marginBottom: 2,
            }}
          >
            <Box
              display="flex"
              gap={3}
              alignItems="center"
              justifyContent={{ small: 'left', tablet: 'flex-end' }}
            >
              <Typography
                sx={{
                  typography:
                                        {
                                          small: 'mobile_h1',
                                          tablet: 'desktop_h3',
                                        },
                }}
              >
                Your Cart
              </Typography>
            </Box>
          </Box>
          )}
          <Typography
            variant="h6"
            sx={{
              fontSize: isInSiping ? '24px' : '20px',
              typography: {
                small: 'button_SM',
                tablet: 'desktop_sustainability',
              },
              textAlign: { small: 'left', tablet: 'right' },
            }}
          >
            Order summary
          </Typography>
          <Divider sx={{ mt: isInSiping ? 3 : 2, mb: 3 }} />
          <Box
            sx={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: { small: 'center', tablet: 'flex-start' },
              flexDirection: isInSiping
                ? 'column'
                : { small: 'column', tablet: 'row' },
              gap: { tablet: 7, small: 2 },
            }}
          >
            <Box mt={2} sx={{ width: '100%' }}>
              {cart.map((item) => (
                <CartItem key={item.cartID} item={item} isInSiping={isInSiping} />
              ))}
            </Box>
          </Box>

          <Box
            display="flex"
            flexDirection="column"
            sx={{
              pt: 2,
              width: '100%',
            }}
          >
            {/* Subtotal */}
            <Box
              sx={{
                display: 'flex',
                justifyContent: { small: 'flex-end', tablet: 'space-between' },
                gap: 2,
                marginBottom: 1,
              }}
            >
              <Typography
                variant="body1"
                sx={{
                  typography: {
                    small: 'button_SM',
                    tablet: 'desktop_sustainability',
                  },
                }}
              >
                Subtotal
                {` (${parseInt(totalQuantityItems, 10)})`}
              </Typography>
              <Typography
                variant="body1"
                sx={{
                  typography: {
                    small: 'button_SM',
                    tablet: 'desktop_sustainability',
                  },
                }}
              >
                $
                {subtotal.toFixed(2)}
              </Typography>
            </Box>

            {/* Tax */}
            <Box
              sx={{
                display: 'flex',
                justifyContent: { small: 'flex-end', tablet: 'space-between' },
                gap: 2,
                marginBottom: 1,
              }}
            >
              <Typography
                variant="body1"
                sx={{
                  typography: {
                    small: 'button_SM',
                    tablet: 'desktop_sustainability',
                  },
                }}
              >
                Tax
              </Typography>
              <Typography
                variant="body1"
                sx={{
                  typography: {
                    small: 'button_SM',
                    tablet: 'desktop_sustainability',
                  },
                }}
              >
                $
                {tax.toFixed(2)}
              </Typography>
            </Box>

            {/* Shipping */}
            <Box
              sx={{
                display: 'flex',
                justifyContent: { small: 'flex-end', tablet: 'space-between' },
                gap: 2,
                marginBottom: 1,
              }}
            >
              <Typography
                variant="body1"
                sx={{
                  typography: {
                    small: 'button_SM',
                    tablet: 'desktop_sustainability',
                  },
                }}
              >
                Shipping
              </Typography>
              <Typography
                variant="body1"
                sx={{
                  typography: {
                    small: 'button_SM',
                    tablet: 'desktop_sustainability',
                  },
                }}
              >
                {shipping === 0 ? 'Free' : `$${shipping.toFixed(2)}`}
              </Typography>
            </Box>

            <Divider
              sx={{ mb: { small: 3, tablet: 3 }, mt: { small: 5, tablet: 6 } }}
            />

            {/* Total Orders */}
            <Box
              sx={{
                display: 'flex',
                justifyContent: 'space-between',
                marginBottom: '16px',
              }}
            >
              <Typography
                variant="h6"
                sx={{
                  typography: {
                    small: 'desktop_overlineSM',
                    tablet: 'desktop_sustainability',
                  },
                }}
              >
                Total Orders:
              </Typography>
              <Typography
                variant="h6"
                sx={{
                  typography: {
                    small: 'desktop_overlineSM',
                    tablet: 'desktop_sustainability',
                  },
                }}
              >
                $
                {totalOrders.toFixed(2)}
              </Typography>
            </Box>

            {/* Notice */}
            <Typography
              variant="body2"
              sx={{
                color: '#202020',
                marginBottom: '16px',
                typography: {
                  small: 'mobile_overline',
                  tablet: 'desktop_overlineSM',
                },
              }}
            >
              The total amount you pay includes all applicable customs duties &
              taxes. We guarantee no additional charges on delivery.
            </Typography>

            {/* Button */}
            { !isInSiping && (
            <Button
              sx={{
                alignSelf: 'end',
                width: { small: '152px', tablet: '184px' },
                padding: 1,
                textTransform: 'capitalize',
              }}
              variant="contained"
              onClick={handleNextClick}
              disabled={isButtonDisabled}
            >
              Next
            </Button>
            )}
          </Box>
        </Box>
      </Box>
    </Container>
  );
};

Cart.propTypes = {
  isInSiping: PropTypes.bool,
};

export default Cart;
