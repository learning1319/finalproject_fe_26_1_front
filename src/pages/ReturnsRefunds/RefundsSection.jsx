/* eslint-disable */
import PropTypes from 'prop-types';
import {
  Typography,
  List,
  ListItem,
  ListItemIcon,
} from '@mui/material';
import CheckCircle from '../../assets/svg/CheckCircle/CheckCircle';

const RefundsSection = ({ theme }) => (
  <>
    <Typography
      gutterBottom
      sx={{
        ...theme.typography.mobile_h4,
        [theme.breakpoints.up('tablet')]: theme.typography.desktop_h5,
        mb: 2,
      }}
    >
      Refunds
    </Typography>
    <Typography
      paragraph
      sx={{
        ...theme.typography.mobile_bodyMD,
        [theme.breakpoints.up('tablet')]: theme.typography.desktop_bodyMD,
        mt: 3,
        mb: 6,
        textAlign: 'justify',
      }}
    >
      Inspection
      : Once your return
      is received and inspected, we will
      notify you of the approval or rejection of your refund.
      Approval
      : If approved,
      your refund will be processed, and a credit will
      automatically be applied to your original method
      of payment within a certain number of days.
      Partial Refunds
      : There are
      certain situations where only partial refunds are granted:
    </Typography>
    <List sx={{
      ...theme.typography.mobile_bodyMD,
      [theme.breakpoints.up('tablet')]: theme.typography.desktop_bodyMD,
    }}
    >
      <ListItem
        sx={{
          textAlign: 'justify',
          mb: 3,
        }}
      >
        <ListItemIcon>
          <CheckCircle />
        </ListItemIcon>
        Any item not in its original condition,
        damaged, or missing parts for reasons
        not due to our error.
      </ListItem>
      <ListItem
        sx={{
          textAlign: 'justify',
          mb: 3,
        }}
      >
        <ListItemIcon>
          <CheckCircle />
        </ListItemIcon>
        Any item that is returned more
        than 30 days after delivery.
      </ListItem>
    </List>
    <Typography
      paragraph
      sx={{
        ...theme.typography.mobile_bodyMD,
        [theme.breakpoints.up('tablet')]: theme.typography.desktop_bodyMD,
        mb: 4,
        textAlign: 'justify',
      }}
    >
      Late or Missing Refunds
      : If you have not
      received a refund yet, first check your bank account again.
      Then contact your credit card company, as it may take some
      time before your refund is officially posted. If you have
      done all of this and still have not
      received your refund, please contact us.
    </Typography>
  </>
);

RefundsSection.propTypes = {
  theme: PropTypes.shape({
    typography: PropTypes.shape({
      mobile_h4: PropTypes.shape({
        fontSize: PropTypes.string,
        fontWeight: PropTypes.number,
        lineHeight: PropTypes.string,
      }).isRequired,
      desktop_h5: PropTypes.shape({
        fontSize: PropTypes.string,
        fontWeight: PropTypes.number,
        lineHeight: PropTypes.string,
      }).isRequired,
      mobile_bodyMD: PropTypes.shape({
        fontSize: PropTypes.string,
        fontWeight: PropTypes.number,
        lineHeight: PropTypes.string,
      }).isRequired,
      desktop_bodyMD: PropTypes.shape({
        fontSize: PropTypes.string,
        fontWeight: PropTypes.number,
        lineHeight: PropTypes.string,
      }).isRequired,
    }).isRequired,
    breakpoints: PropTypes.shape({
      up: PropTypes.func,
    }).isRequired,
  }).isRequired,
};
export default RefundsSection;
