/* eslint-disable */
import {
  Container,
  Box,
} from '@mui/material';
import { useEffect } from 'react';
import { useTheme } from '@mui/material/styles';
import ReturnsSection from './ReturnsSection';
import RefundsSection from './RefundsSection';
import ExchangesSection from './ExchangesSection';
import NonReturnableItemsSection from './NonReturnableItemsSection';

const ReturnsRefunds = () => {
  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  }, []);
  const theme = useTheme();

  return (
    <Container>
      <Box sx={{ my: 4 }}>
        <ReturnsSection theme={theme} />
        <RefundsSection theme={theme} />
        <ExchangesSection theme={theme} />
        <NonReturnableItemsSection theme={theme} />
      </Box>
    </Container>
  );
};

export default ReturnsRefunds;
