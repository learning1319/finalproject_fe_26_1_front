/* eslint-disable */
import PropTypes from 'prop-types';
import { Typography } from '@mui/material';

const NonReturnableItemsSection = ({ theme }) => (
  <>
    <Typography
      gutterBottom
      sx={{
        ...theme.typography.mobile_h4,
        [theme.breakpoints.up('tablet')]: theme.typography.desktop_h5,
        mb: 3,
      }}
    >
      Non-Returnable Items
    </Typography>
    <Typography
      paragraph
      sx={{
        ...theme.typography.mobile_bodyMD,
        [theme.breakpoints.up('tablet')]: theme.typography.desktop_bodyMD,
        mb: 3,
        textAlign: 'justify',
      }}
    >
      Gift Cards
      : Gift
      cards are non-returnable and non-refundable.
      Final Sale Items
      : Items
      marked as final sale are not eligible for return, refund, or exchange.
    </Typography>
    <Typography
      paragraph
      sx={{
        ...theme.typography.mobile_bodyMD,
        [theme.breakpoints.up('tablet')]: theme.typography.desktop_bodyMD,
        mb: 10,
        textAlign: 'justify',
      }}
    >
      We are committed to making your
      shopping experience with Modimal pleasant and stress-free.
      If you have any questions about our returns and refunds policy,
      please contact our customer service team.
      We are here to assist you.
    </Typography>
  </>
);

NonReturnableItemsSection.propTypes = {
  theme: PropTypes.shape({
    typography: PropTypes.shape({
      mobile_h4: PropTypes.shape({
        fontSize: PropTypes.string,
        fontWeight: PropTypes.number,
        lineHeight: PropTypes.string,
      }).isRequired,
      desktop_h5: PropTypes.shape({
        fontSize: PropTypes.string,
        fontWeight: PropTypes.number,
        lineHeight: PropTypes.string,
      }).isRequired,
      mobile_bodyMD: PropTypes.shape({
        fontSize: PropTypes.string,
        fontWeight: PropTypes.number,
        lineHeight: PropTypes.string,
      }).isRequired,
      desktop_bodyMD: PropTypes.shape({
        fontSize: PropTypes.string,
        fontWeight: PropTypes.number,
        lineHeight: PropTypes.string,
      }).isRequired,
    }).isRequired,
    breakpoints: PropTypes.shape({
      up: PropTypes.func,
    }).isRequired,
  }).isRequired,
};

export default NonReturnableItemsSection;
