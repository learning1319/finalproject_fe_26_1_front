/* eslint-disable */
import PropTypes from 'prop-types';
import { Typography } from '@mui/material';

const ExchangesSection = ({ theme }) => (
  <>
    <Typography
      paragraph
      sx={{
        ...theme.typography.mobile_bodyMD,
        [theme.breakpoints.up('tablet')]: theme.typography.desktop_bodyMD,
        mb: 4,
        textAlign: 'justify',
      }}
    >
      Defective or Damaged Items
      :
      We only replace items if they are defective or
      damaged. If you need to exchange it for the same item,
      contact us.
      Process
      : For exchanges,
      follow the return process to send the item back.
      Once the return is received and inspected,
      we will send out the replacement item.
    </Typography>
  </>
);

ExchangesSection.propTypes = {
  theme: PropTypes.shape({
    typography: PropTypes.shape({
      mobile_h4: PropTypes.shape({
        fontSize: PropTypes.string,
        fontWeight: PropTypes.number,
        lineHeight: PropTypes.string,
      }).isRequired,
      desktop_h5: PropTypes.shape({
        fontSize: PropTypes.string,
        fontWeight: PropTypes.number,
        lineHeight: PropTypes.string,
      }).isRequired,
      mobile_bodyMD: PropTypes.shape({
        fontSize: PropTypes.string,
        fontWeight: PropTypes.number,
        lineHeight: PropTypes.string,
      }).isRequired,
      desktop_bodyMD: PropTypes.shape({
        fontSize: PropTypes.string,
        fontWeight: PropTypes.number,
        lineHeight: PropTypes.string,
      }).isRequired,
    }).isRequired,
    breakpoints: PropTypes.shape({
      up: PropTypes.func,
    }).isRequired,
  }).isRequired,
};

export default ExchangesSection;
