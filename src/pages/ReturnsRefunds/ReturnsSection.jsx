/* eslint-disable */
import PropTypes from 'prop-types';
import { Typography } from '@mui/material';

const ReturnsSection = ({ theme }) => (
  <>
    <Typography
      gutterBottom
      sx={{
        ...theme.typography.mobile_h4,
        [theme.breakpoints.up('tablet')]: theme.typography.desktop_h5,
        mb: 3,
      }}
    >
      Returns
    </Typography>
    <Typography
      paragraph
      sx={{
        ...theme.typography.mobile_bodyMD,
        [theme.breakpoints.up('tablet')]: theme.typography.desktop_bodyMD,
        mt: 3,
        mb: 6,
        textAlign: 'justify',
      }}
    >
      Eligibility
      : To be eligible for a return,
      your item must be unused, unworn,
      and in the same condition that you received it.
      It must also be in the original packaging with all tags attached.
      Time Frame
      : You have 30 days from the date
      of delivery to initiate a return.
      Items returned after this period will not be accepted.
      Shipping
      : You will be
      responsible for the cost of returning the
      item unless the return is due to a defect or
      error on our part. We recommend using a trackable
      shipping service or purchasing shipping insurance.
      We cannot guarantee that we will receive your returned item.
    </Typography>
  </>
);

ReturnsSection.propTypes = {
  theme: PropTypes.shape({
    typography: PropTypes.shape({
      mobile_h4: PropTypes.shape({
        fontSize: PropTypes.string,
        fontWeight: PropTypes.number,
        lineHeight: PropTypes.string,
      }).isRequired,
      desktop_h5: PropTypes.shape({
        fontSize: PropTypes.string,
        fontWeight: PropTypes.number,
        lineHeight: PropTypes.string,
      }).isRequired,
      mobile_bodyMD: PropTypes.shape({
        fontSize: PropTypes.string,
        fontWeight: PropTypes.number,
        lineHeight: PropTypes.string,
      }).isRequired,
      desktop_bodyMD: PropTypes.shape({
        fontSize: PropTypes.string,
        fontWeight: PropTypes.number,
        lineHeight: PropTypes.string,
      }).isRequired,
    }).isRequired,
    breakpoints: PropTypes.shape({
      up: PropTypes.func,
    }).isRequired,
  }).isRequired,
};

export default ReturnsSection;
