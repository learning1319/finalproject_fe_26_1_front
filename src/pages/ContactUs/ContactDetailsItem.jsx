/* eslint-disable */
import PropTypes from 'prop-types';
import {
  Typography,
  useTheme,
  Link,
} from '@mui/material';

const ContactDetailsItem = ({ label, value, href }) => {
  const theme = useTheme();
  return (
    <Typography
      sx={{
        fontWeight: 400,
        fontSize: 16,
        lineHeight: '28.8px',
        mb: 1,
        [theme.breakpoints.down('tablet')]: {
          fontSize: 14,
          lineHeight: '19.6px',
        },
      }}
    >
      {label}
      {' '}
      {href ? <Link href={href} color={theme.palette.primary.main}>{value}</Link> : value}
    </Typography>

  );
};

ContactDetailsItem.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  href: PropTypes.string,
};

ContactDetailsItem.defaultProps = {
  href: '',
};

export default ContactDetailsItem;