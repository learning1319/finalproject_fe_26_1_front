import {
  Box,
  Container,
  useTheme,
  useMediaQuery,
} from '@mui/material';
import { useEffect } from 'react';
import ContactUsSection from '../../components/ContactUs/ContactUsSection';
import ContactForm from '../../components/ContactUs/ContactForms';
import ContactFormsMobil from '../../components/ContactUs/Mobil/ContactFormsMobil';

const ContactUs = () => {
  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  }, []);
  const theme = useTheme();
  const isDesktop = useMediaQuery(theme.breakpoints.up('large'));
  return (
    <Box sx={{ bgcolor: theme.palette.background.paper, py: 4 }}>
      <Container>
        <ContactUsSection
          title="Contact Us"
        />
        <Box bgcolor="#eff1ef" padding={1}>
          <ContactUsSection
            content="We always love hearing from our customers! Please do not hesitate to contact us should you have any questions regarding our products and sizing recommendations or inquiries about your current order."
          />
          <ContactUsSection
            content="Contact our Customer Care team through the contact form below, email us at hello@modimal.com ."
          />
          <ContactUsSection
            content="We will aim to respond to you within 1-2 business days."
          />
        </Box>
      </Container>

      {isDesktop && (
        <ContactForm />

      )}

      {!isDesktop && (
        <ContactFormsMobil />

      )}

    </Box>
  );
};

export default ContactUs;
