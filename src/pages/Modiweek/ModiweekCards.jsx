import React, { useRef, useState } from 'react';
import PropTypes from 'prop-types';
import {
  Box,
  Container,
  Card,
  Typography,
  CardActionArea,
  CardContent,
  CardMedia,
  useMediaQuery,
  Grid,
} from '@mui/material';
import { useNavigate, useLocation } from 'react-router-dom';
import IconButtonNext from '../../components/Buttons/IconButtonNext/IconButtonNext';
import IconButtonPrev from '../../components/Buttons/IconButtonPrev/IconButtonPrev';
import theme from '../../themeStyle';

const ModiweekCards = ({ banners, home = false }) => {
  const isMobile = useMediaQuery(theme.breakpoints.down('tablet'));
  const [currentIndex, setCurrentIndex] = useState(0);
  const containerRef = useRef(null);
  const navigate = useNavigate();
  const location = useLocation();

  // Визначення, чи слід показувати Container
  const showContainer = location.pathname === '/'; // Замініть '/your-path' на ваш шлях

  const handleCardClick = (day) => {
    navigate(`/modiweek?day=${day}`);
  };

  const handlePrev = () => {
    if (containerRef.current) {
      containerRef.current.scrollBy({
        left: -150, // Прокручуємо на 150 пікселів назад
        behavior: 'smooth',
      });
    }
  };

  const handleNext = () => {
    if (containerRef.current) {
      containerRef.current.scrollBy({
        left: 150, // Прокручуємо на 150 пікселів вперед
        behavior: 'smooth',
      });
    }
  };

  const handleSwipeStart = (event) => {
    const touchStartX = event.changedTouches[0].clientX;
    containerRef.current.touchStartX = touchStartX;
  };

  const handleSwipeEnd = (event) => {
    const touchEndX = event.changedTouches[0].clientX;
    const { touchStartX } = containerRef.current;

    if (touchStartX - touchEndX > 50 && currentIndex < banners.length - (isMobile ? 2 : 1)) {
      setCurrentIndex((prevIndex) => prevIndex + 1);
    } else if (touchEndX - touchStartX > 50 && currentIndex > 0) {
      setCurrentIndex((prevIndex) => prevIndex - 1);
    }
  };

  const content = (
    <Box
      sx={{
        position: 'relative',
        width: '100%',
        maxWidth: '1200px',
        m: 'auto',
        overflow: 'hidden',
      }}
    >
      {home && (
        <Typography
          sx={{
            typography: {
              desktop: 'desktop_logoName',
              tablet: 'mobile_logoName',
              small: 'mobile_h2',
            },
            textAlign: 'left',
            marginBottom: '35px',
            marginTop: '60px',
          }}
        >
          Modiweek
        </Typography>
      )}
      {!isMobile && <IconButtonPrev onClick={handlePrev} />}
      <Grid
        container
        wrap="nowrap"
        spacing={3}
        ref={containerRef}
        sx={{
          overflowX: 'auto',
          scrollBehavior: 'smooth',
          '&::-webkit-scrollbar': { display: 'none' },
        }}
        onTouchStart={isMobile ? handleSwipeStart : undefined}
        onTouchEnd={isMobile ? handleSwipeEnd : undefined}
      >
        {banners.slice(currentIndex, currentIndex + (isMobile ? 2 : banners.length)).map((day) => (
          <Grid item key={day.name}>
            <Card
              sx={{
                minWidth: isMobile ? 'calc(50% - 15px)' : '288px',
                flex: '0 0 auto',
              }}
              onClick={() => handleCardClick(day.name)}
            >
              <CardActionArea>
                <CardMedia
                  component="img"
                  height="490px"
                  image={day.imageUrl}
                  alt={day.name}
                />
                <CardContent>
                  <Typography
                    sx={{
                      fontWeight: 'bold',
                      fontSize: { mobile: '14px', desktop: '16px' },
                    }}
                    component="div"
                  >
                    {day.name}
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
          </Grid>
        ))}
      </Grid>
      {!isMobile && <IconButtonNext onClick={handleNext} />}
      {isMobile && (
        <Box sx={{ display: 'flex', justifyContent: 'center', mt: 2 }}>
          {Array(7).fill().map((item, index) => (
            <Box
              key={Math.random()}
              sx={{
                width: currentIndex === index ? '15px' : '10px',
                height: currentIndex === index ? '15px' : '10px',
                borderRadius: '50%',
                backgroundColor: currentIndex === index ? '#adadad' : '#868686',
                transition: 'background-color 0.3s',
                margin: '0 5px',
              }}
            />
          ))}
        </Box>
      )}
    </Box>
  );

  return showContainer ? (
    <Container
      sx={{
        display: 'flex',
        justifyContent: 'center',
        width: '100%',
        mb: 6,
      }}
    >
      {content}
    </Container>
  ) : (
    content
  );
};

ModiweekCards.propTypes = {
  home: PropTypes.bool,
  banners: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      imageUrl: PropTypes.string.isRequired,
      id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    }),
  ).isRequired,
};

export default ModiweekCards;
