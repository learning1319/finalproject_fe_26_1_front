import { Box, Typography, useMediaQuery } from '@mui/material';
import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import { fetchBanners } from '../../redux/slices/modiweekSlice.js';
import theme from '../../themeStyle';
import ProductCard from '../../components/ProductCard/ProductCard';
import ModiweekCards from './ModiweekCards';
import getProductById from '../../api/getProductById.js';

const Modiweek = () => {
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const day = queryParams.get('day');
  const dispatch = useDispatch();
  const banners = useSelector((state) => state.banners.items);
  const [selectedDay, setSelectedDay] = useState('');
  const [selectedDayData, setSelectedDayData] = useState(null);
  const [products, setProducts] = useState([]);
  const isMobile = useMediaQuery(theme.breakpoints.down('tablet'));
  const getTodayDayOfWeek = () => {
    const daysOfWeek = [
      'Sunday',
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday',
    ];
    const today = new Date();
    const dayOfWeek = today.getDay();
    return daysOfWeek[dayOfWeek];
  };
  console.log(banners);
  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  }, [location]);

  useEffect(() => {
    dispatch(fetchBanners('Modiweek'));
  }, [dispatch]);

  useEffect(() => {
    if (day) {
      setSelectedDay(day);
    } else {
      const todayDayOfWeek = getTodayDayOfWeek();
      setSelectedDay(todayDayOfWeek);
    }
  }, [day]);

  useEffect(() => {
    if (banners.length > 0) {
      const dayDataNew = banners.find((dayData) => dayData.name === selectedDay);
      setSelectedDayData(dayDataNew);
    }
  }, [selectedDay, banners]);

  useEffect(() => {
    const fetchProducts = async () => {
      if (selectedDayData && selectedDayData.category) {
        const productsData = await Promise.all(
          selectedDayData.category
            .filter((product) => product.id) // Фільтруємо тільки продукти з id
            .map(async (product) => {
              const productData = await getProductById(product.id);
              return productData.data[0];
            }),
        );
        console.log(productsData);
        setProducts(productsData); // Додаємо тільки знайдені продукти
      }
    };

    fetchProducts();
  }, [selectedDayData]);

  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        maxWidth: '1200px',
        margin: 'auto',
        boxSizing: 'border-box',
        gap: '30px',
        mt: 4,
        pl: {
          desktop: 0,
          tablet: 4,
        },
        pr: {
          desktop: 0,
          tablet: 2,
        },
      }}
    >
      <Typography
        sx={{
          fontSize: { desktop: '32px', mobile: '24px' },
          fontWeight: 'bold',
          ml: { mobile: 4, desktop: 0 },
        }}
      >
        {selectedDay}
      </Typography>
      {selectedDayData && (
        <Box
          sx={{
            display: 'flex',
            gap: '70px',
            flexDirection: isMobile ? 'column' : 'row',
          }}
        >
          <Box
            component="img"
            src={selectedDayData.imageUrl}
            alt={`${selectedDay} image`}
            sx={{
              height: '750px',
              objectFit: 'cover',
              flex: '0 0 40%',
            }}
          />
          <Box
            sx={{
              ml: { mobile: 2, desktop: 0 },
              mr: { mobile: 2, desktop: 0 },
              flex: 1,
              display: 'flex',
              flexDirection: 'column',
              margin: 'auto',
            }}
          >
            <Typography sx={{ fontSize: '20px', fontWeight: 'bold' }}>
              Shop The Look
            </Typography>
            <Typography>
              {products.length}
              {products.length > 1 ? ' items' : ' item'}
            </Typography>
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                flexWrap: 'wrap',
                gap: '15px',
                mt: 5,
              }}
            >
              {products.map((product) => (
                <ProductCard
                  key={product.id}
                  product={product}
                />
              ))}
            </Box>
          </Box>
        </Box>
      )}
      <ModiweekCards banners={banners} />
    </Box>
  );
};

export default Modiweek;
