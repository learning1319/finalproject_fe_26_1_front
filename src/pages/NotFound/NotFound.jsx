import React from 'react';
import {
  Container, Typography, Button, Box,
} from '@mui/material';
import { useNavigate } from 'react-router-dom';

const NotFoundPage = () => {
  const navigate = useNavigate();
  const handleGoHome = () => {
    navigate('/');
  };
  return (
    <Container component="main" maxWidth="md" style={{ textAlign: 'center', marginTop: '50px' }}>
      <Box>
        <Typography variant="h1" component="h1" color="primary">
          404
        </Typography>
        <Typography variant="h4" component="h2" color="textSecondary" gutterBottom>
          Page Not Found
        </Typography>
        <Typography variant="body1" color="textSecondary">
          Sorry, the page you are looking for does not exist or has been moved.
        </Typography>
        <Button variant="contained" color="primary" onClick={handleGoHome} style={{ marginTop: '20px' }}>
          Go to Homepage
        </Button>
      </Box>
    </Container>
  );
};
export default NotFoundPage;
