import React from 'react';
import PropTypes from 'prop-types';
import {
  Modal, Box, Typography, Button, useTheme,
} from '@mui/material';
import Leaf from '../../assets/svg/Leaf/Leaf';

const UserProfileModal = ({
  open, onClose, message, onCancel, onConfirm, confirmText,
}) => {
  const theme = useTheme();

  return (
    <Modal
      open={open}
      onClose={onClose}
      aria-labelledby="user-profile-modal"
      aria-describedby="user-profile-description"
    >
      <Box
        sx={{
          maxWidth: '550px',
          backgroundColor: 'white',
          maxHeight: '300px',
          borderRadius: '8px',
          boxShadow: 24,
          p: 5,
          border: `2px solid ${theme.palette.primary.main}`,
          position: 'absolute',
          top: '40%',
          left: '50%',
          transform: 'translate(-50%, -50%)',
        }}
      >
        <Box
          sx={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <Box sx={{ pb: 3 }}>
            <Leaf
              width={80}
              height={80}
              currentColor={theme.palette.primary.main}
            />
          </Box>
          <Typography
            id="user-profile-description"
            sx={{
              textAlign: 'center',
              typography: {
                desktop: 'mobile_h1',
                tablet: 'mobile_h2',
                small: 'mobile_h2',
              },
              pr: 2,
              width: '450px',
            }}
          >
            {message}
          </Typography>
        </Box>
        <Box
          sx={{
            display: 'flex',
            justifyContent: 'space-between',
            mt: 5,
            px: 7,
          }}
        >
          <Button onClick={onCancel} sx={{ color: 'error.main', fontSize: '18px', mr: 4 }}>Cancel</Button>
          <Button onClick={onConfirm} sx={{ fontSize: '18px' }}>
            {confirmText}
          </Button>
        </Box>
      </Box>
    </Modal>
  );
};

UserProfileModal.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  message: PropTypes.string.isRequired,
  onConfirm: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  confirmText: PropTypes.string,
};

export default UserProfileModal;
