import React from 'react';
import {
  Modal, Box, Typography, Button,
} from '@mui/material';
import PropTypes from 'prop-types';

const CustomUserModal = ({ open, onClose, message }) => (
  <Modal
    open={open}
    onClose={onClose}
  >
    <Box sx={{
      position: 'absolute',
      top: '40%',
      left: '50%',
      transform: 'translate(-50%, -50%)',
      maxWidth: 400,
      width: '100%',
      backgroundColor: 'white',
      borderRadius: '8px',
      boxShadow: 24,
      p: 6,
      textAlign: 'center',
    }}
    >
      <Typography variant="h6" sx={{ mb: 6 }}>
        {message}
      </Typography>
      <Button
        onClick={onClose}
        variant="contained"
        color="primary"
      >
        OK
      </Button>
    </Box>
  </Modal>
);

CustomUserModal.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  message: PropTypes.string.isRequired,
};

export default CustomUserModal;
