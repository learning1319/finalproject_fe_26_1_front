import axios from 'axios';
import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  Box, TextField, IconButton, InputAdornment, Button, Typography,
} from '@mui/material';
import {
  Field, Formik, Form, ErrorMessage,
} from 'formik';
import { Visibility, VisibilityOff } from '@mui/icons-material';
import passwordFieldsSchema from '../../../utils/passwordFieldsSchema.js';
import BASE_URL from '../../../constants/constants.js';
import { openPasswordWindow, closePasswordWindow } from '../../../redux/slices/modalSlice.js';
import CustomUserModal from '../CustomUserModal';

const PasswordFields = () => {
  const dispatch = useDispatch();
  const currentUser = useSelector((state) => (state.login.isLogged ? state.login.user : null));
  const token = useSelector((state) => (state.login.isLogged ? state.login.token : null));
  const showPasswordWindow = useSelector((state) => state.modal.showPasswordWindow);
  const [showPassword, setShowPassword] = useState({
    currentPassword: false,
    newPassword: false,
    confirmPassword: false,
  });

  const handleClickShowPassword = (field) => {
    setShowPassword((prev) => ({ ...prev, [field]: !prev[field] }));
  };

  const handleSubmitPassword = (values, { setSubmitting, resetForm }) => {
    axios.put(`${BASE_URL}/customers/password`, {
      currentPassword: values.currentPassword,
      newPassword: values.newPassword,
      _id: currentUser._id,
    }, {
      headers: {
        Authorization: token,
      },
    })
      .then((response) => {
        console.log(`${response} password was changed`);
        setSubmitting(false);
        resetForm();
        dispatch(openPasswordWindow());
      })
      .catch(() => {
        setSubmitting(false);
      });
  };

  const initialValues = {
    currentPassword: currentUser?.password || '',
    newPassword: '',
    confirmPassword: '',
  };

  return (
    <>
      <Formik
        initialValues={initialValues}
        validationSchema={passwordFieldsSchema}
        onSubmit={handleSubmitPassword}
        enableReinitialize
      >
        {({ isSubmitting, touched, errors }) => (
          <Form>
            <Box sx={{
              display: 'flex',
              flexDirection: 'column',
              gap: 2,
              p: 4,
              maxWidth: '400px',
              m: 'auto',
            }}
            >
              <Typography
                sx={{
                  typography: {
                    desktop: 'desktop_logoName',
                    tablet: 'mobile_logoName',
                    small: 'mobile_h2',
                  },
                  textAlign: 'center',
                  mb: 10,
                }}
              >
                Password Setting
              </Typography>
              <Box>
                <Field
                  name="currentPassword"
                  as={TextField}
                  label="Current Password"
                  variant="standard"
                  type={showPassword.currentPassword ? 'text' : 'password'}
                  fullWidth
                  sx={{ mb: 3 }}
                  helperText={<ErrorMessage name="currentPassword" />}
                  error={touched.currentPassword && Boolean(errors.currentPassword)}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          onClick={() => handleClickShowPassword('currentPassword')}
                          edge="end"
                        >
                          {showPassword.currentPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
              </Box>
              <Box>
                <Field
                  name="newPassword"
                  as={TextField}
                  label="New Password"
                  variant="standard"
                  type={showPassword.newPassword ? 'text' : 'password'}
                  fullWidth
                  sx={{ mb: 3 }}
                  helperText={<ErrorMessage name="newPassword" />}
                  error={touched.newPassword && Boolean(errors.newPassword)}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          onClick={() => handleClickShowPassword('newPassword')}
                          edge="end"
                        >
                          {showPassword.newPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
              </Box>
              <Box>
                <Field
                  name="confirmPassword"
                  as={TextField}
                  label="Confirm Password"
                  variant="standard"
                  type={showPassword.confirmPassword ? 'text' : 'password'}
                  fullWidth
                  sx={{ mb: 3 }}
                  helperText={<ErrorMessage name="confirmPassword" />}
                  error={touched.confirmPassword && Boolean(errors.confirmPassword)}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          onClick={() => handleClickShowPassword('confirmPassword')}
                          edge="end"
                        >
                          {showPassword.confirmPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
              </Box>
              <Box sx={{
                width: '100%', display: 'flex', justifyContent: 'center', mt: 6, mb: 6,
              }}
              >
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  disabled={isSubmitting}
                  sx={{ minWidth: '200px' }}
                >
                  Save
                </Button>
              </Box>
            </Box>
          </Form>
        )}
      </Formik>

      <CustomUserModal
        open={showPasswordWindow}
        onClose={() => dispatch(closePasswordWindow())}
        message="Password updated successfully!"
      />
    </>
  );
};

export default PasswordFields;
