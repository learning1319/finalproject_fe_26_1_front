import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  Formik, Form, Field, ErrorMessage,
} from 'formik';
import {
  TextField, Box, Typography, Button,
} from '@mui/material';
import axios from 'axios';
import getUserData from '../../../api/getUserData.js';
import BASE_URL from '../../../constants/constants.js';
import UserInfoSchema from '../../../utils/userInfoSchema.js';
import { openUserInfoWindow, closeUserInfoWindow } from '../../../redux/slices/modalSlice.js';
import CustomUserModal from '../CustomUserModal';

const UserInfo = () => {
  const dispatch = useDispatch();
  const [initialValues, setInitialValues] = useState({
    firstName: '',
    lastName: '',
    email: '',
    birthDate: '',
    phoneNumber: '',
  });

  const token = useSelector((state) => state.login.token);
  const currentUser = useSelector((state) => (state.login.isLogged ? state.login.user : null));
  const showUserInfoWindow = useSelector((state) => state.modal.showUserInfoWindow);

  useEffect(() => {
    const fetchUserData = async () => {
      try {
        const userData = await getUserData(token);
        setInitialValues({
          firstName: userData.firstName || '',
          lastName: userData.lastName || '',
          email: userData.email || '',
          birthDate: userData.birthDate || '',
          phoneNumber: userData.phoneNumber || '',
        });
      } catch (error) {
        console.error('Error fetching user data:', error);
      }
    };

    if (token) {
      fetchUserData();
    }
  }, [token]);

  const handleSubmit = async (values) => {
    try {
      const payload = {
        email: values.email,
        firstName: values.firstName,
        lastName: values.lastName,
        birthDate: values.birthDate,
        phoneNumber: values.phoneNumber,
        _id: currentUser._id,
      };
      await axios.put(`${BASE_URL}/customers`, payload, {
        headers: { Authorization: token },
      });
      dispatch(openUserInfoWindow());
    } catch (error) {
      console.error('Error updating user information:', error);
    }
  };

  return (
    <>
      <Formik
        initialValues={initialValues}
        validationSchema={UserInfoSchema}
        onSubmit={handleSubmit}
        enableReinitialize
      >
        {({ isSubmitting, touched, errors }) => (
          <Form>
            <Box sx={{
              display: 'flex',
              flexDirection: 'column',
              gap: 2,
              p: 4,
              maxWidth: '400px',
              m: 'auto',
            }}
            >
              <Typography
                sx={{
                  typography: {
                    desktop: 'desktop_logoName',
                    tablet: 'desktop_logoName',
                    small: 'mobile_h2',
                  },
                  textAlign: 'center',
                  mb: 6,
                }}
              >
                User Information
              </Typography>
              <Box sx={{ display: 'flex' }}>
                <Field
                  name="firstName"
                  as={TextField}
                  label="First Name"
                  variant="standard"
                  fullWidth
                  sx={{ mb: 3 }}
                  helperText={<ErrorMessage name="firstName" />}
                  error={touched.firstName && Boolean(errors.firstName)}
                />
              </Box>
              <Box sx={{ display: 'flex' }}>
                <Field
                  name="lastName"
                  as={TextField}
                  label="Last Name"
                  variant="standard"
                  fullWidth
                  sx={{ mb: 3 }}
                  helperText={<ErrorMessage name="lastName" />}
                  error={touched.lastName && Boolean(errors.lastName)}
                />
              </Box>
              <Box sx={{ display: 'flex' }}>
                <Field
                  name="birthDate"
                  as={TextField}
                  label="Birth Date"
                  variant="standard"
                  fullWidth
                  type="date"
                  InputLabelProps={{ shrink: true }}
                  sx={{ mb: 3 }}
                  helperText={<ErrorMessage name="birthDate" />}
                  error={touched.birthDate && Boolean(errors.birthDate)}
                />
              </Box>
              <Box sx={{ display: 'flex' }}>
                <Field
                  name="email"
                  as={TextField}
                  label="Email"
                  variant="standard"
                  fullWidth
                  sx={{ mb: 3 }}
                  helperText={<ErrorMessage name="email" />}
                  error={touched.email && Boolean(errors.email)}
                />
              </Box>
              <Box sx={{ display: 'flex' }}>
                <Field
                  name="phoneNumber"
                  as={TextField}
                  label="Phone Number"
                  variant="standard"
                  fullWidth
                  sx={{ mb: 3 }}
                  helperText={<ErrorMessage name="phoneNumber" />}
                  error={touched.phoneNumber && Boolean(errors.phoneNumber)}
                />
              </Box>
              <Box sx={{
                width: '100%', display: 'flex', justifyContent: 'center', mt: 4, mb: 2,
              }}
              >
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  disabled={isSubmitting}
                  sx={{ width: '200px' }}
                >
                  Save
                </Button>
              </Box>
            </Box>
          </Form>
        )}
      </Formik>
      <CustomUserModal
        open={showUserInfoWindow}
        onClose={() => dispatch(closeUserInfoWindow())}
        message="Your personal information has been successfully updated"
      />
    </>
  );
};

export default UserInfo;
