import React, { useState, useEffect } from 'react';
import {
  Box, Button, Typography, TextField, Modal, Grid, useMediaQuery,
} from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import AddCardIcon from '@mui/icons-material/AddCard';
import { addCard, getCardInfo, removeCard } from '../../../redux/slices/paymentSlice.js';
import theme from '../../../themeStyle';

const PaymentCard = () => {
  const [card, setCard] = useState(null);
  const [open, setOpen] = useState(false);
  const isSmallScreen = useMediaQuery('(max-width:600px)');

  const initialCard = {
    cardNumber: '',
    cardExpiryDate: '',
    cardCVV: '',
  };

  const initialErrors = {
    cardNumber: '',
    cardExpiryDate: '',
    cardCVV: '',
  };

  const [newCard, setNewCard] = useState(initialCard);
  const [errors, setErrors] = useState(initialErrors);
  const [touched, setTouched] = useState({});

  const dispatch = useDispatch();
  const token = useSelector((state) => state.login.token);
  const userId = useSelector((state) => state.login.user._id);

  useEffect(() => {
    const fetchUserCard = async () => {
      try {
        const response = await dispatch(getCardInfo(userId, token));
        const fetchedCard = response.data.paymentDetails;
        setCard(fetchedCard);
      } catch (error) {
        console.error('Error fetching user card:', error);
      }
    };
    fetchUserCard();
  }, [dispatch, token, userId]);

  const handleOpen = () => setOpen(true);
  const handleClose = () => {
    setOpen(false);
    setNewCard(initialCard);
    setErrors(initialErrors);
    setTouched({});
  };

  const validateField = (field, value) => {
    let error = '';

    if (field === 'cardNumber') {
      if (!/^\d{4} \d{4} \d{4} \d{4}$/.test(value)) {
        error = 'Invalid card number';
      }
      if (!value) {
        error = 'Card number is required';
      }
    }

    if (field === 'cardExpiryDate') {
      const [month, year] = value.split('/').map(Number);
      const currentYear = new Date().getFullYear() % 100;
      const currentMonth = new Date().getMonth() + 1;

      if (!/^(0[1-9]|1[0-2])\/\d{2}$/.test(value)) {
        error = 'Invalid expiry date format';
      } else if (year < currentYear || (year === currentYear && month < currentMonth)) {
        error = 'Card is expired';
      }

      if (!value) {
        error = 'Expiry date is required';
      }
    }

    if (field === 'cardCVV') {
      if (!/^\d{3}$/.test(value)) {
        error = 'Invalid CVV';
      }
      if (!value) {
        error = 'CVV is required';
      }
    }

    setErrors((prevErrors) => ({ ...prevErrors, [field]: error }));
  };

  const handleInputChange = (field) => (event) => {
    let { value } = event.target;

    if (field === 'cardNumber') {
      let formattedValue = value.replace(/\D/g, '').slice(0, 16);
      formattedValue = formattedValue.replace(/(\d{4})(?=\d)/g, '$1 ');
      value = formattedValue;
    }

    if (field === 'cardExpiryDate') {
      let formattedValue = value.replace(/\D/g, '');
      if (formattedValue.length > 2) {
        formattedValue = `${formattedValue.slice(0, 2)}/${formattedValue.slice(2)}`;
      }
      if (formattedValue.length > 5) {
        formattedValue = formattedValue.slice(0, 5);
      }
      value = formattedValue;
    }

    if (field === 'cardCVV') {
      value = value.replace(/\D/g, '').slice(0, 3);
    }

    setNewCard({ ...newCard, [field]: value });
    validateField(field, value);
  };

  const handleBlur = (field) => () => {
    setTouched((prevTouched) => ({ ...prevTouched, [field]: true }));
    validateField(field, newCard[field]);
  };

  const handleAddCard = async () => {
    if (card) return;

    try {
      const isValid = Object.values(errors).every((error) => !error);
      const isTouched = Object.values(touched).length === 3;

      if (isValid && isTouched) {
        const response = await dispatch(addCard(userId, newCard, token, true));
        setCard(response.data.paymentDetails);
        setNewCard(initialCard);
        setOpen(false);
      } else {
        console.log('Incomplete or invalid card information');
      }
    } catch (error) {
      console.error('Error adding new card:', error);
    }
  };

  const handleRemoveCard = async () => {
    if (!card) return;

    try {
      await dispatch(removeCard(userId, token, card?.cardNumber));
      setCard(null);
    } catch (error) {
      console.error('Error removing card:', error);
    }
  };

  return (
    <Box sx={{
      display: 'flex', flexDirection: 'column', gap: 2, paddingLeft: 2, width: '100%', maxWidth: 600, margin: 'auto',
    }}
    >
      {card ? (
        <Box
          sx={{
            background: theme.palette.success.main,
            color: 'white',
            padding: 2,
            borderRadius: 2,
            boxShadow: '0px 4px 20px rgba(0, 0, 0, 0.1)',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-between',
            height: 150,
            width: '100%',
            maxWidth: 300,
            margin: 'auto',
            fontFamily: theme.typography.fontFamily,
            position: 'relative',
          }}
        >
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}
          >
            <Button
              onClick={handleRemoveCard}
              sx={{
                color: 'white',
                backgroundColor: theme.palette.primary.main,
                '&:hover': {
                  backgroundColor: theme.palette.error.main,
                },
                borderRadius: '20px',
                padding: '8px 16px',
                boxShadow: '0px 4px 6px rgba(0, 0, 0, 0.1)',
              }}
            >
              <Typography variant="body2">Delete Card</Typography>
            </Button>
          </Box>

          <Box sx={{ marginTop: 'auto' }}>
            <Typography
              sx={{
                letterSpacing: '2px',
                fontSize: '1.2rem',
                fontWeight: 'bold',
                mb: 1,
              }}
            >
              {card.cardNumber.replace(/\d{4}(?= \d{4})/g, '****')}
            </Typography>

            <Box sx={{ display: 'flex', justifyContent: 'space-between', mt: 1 }}>
              <Typography variant="body2">
                Exp:
                {' '}
                {card.cardExpiryDate || 'MM/YY'}
              </Typography>
              <Typography variant="body2">
                CVV:
                {' '}
                {card.cardCVV || '***'}
              </Typography>
            </Box>
          </Box>
        </Box>
      ) : (
        <Box
          sx={{
            display: 'flex', flexDirection: 'column', alignItems: 'center', gap: 2, py: 2, width: '80%',
          }}
        >
          <Typography sx={{ typography: 'mobile_h2', margin: 'auto' }}>No card added yet</Typography>
          <Box
            sx={{
              border: '1px dashed #ccc',
              padding: 2,
              borderRadius: 2,
              display: 'flex',
              alignItems: 'center',
              cursor: 'pointer',
              mb: 2,
              width: '100%',
              margin: 'auto',
            }}
            onClick={handleOpen}
          >
            <Typography variant="h6" sx={{ mr: 1, mt: 1 }}>
              <AddCardIcon />
            </Typography>
            <Typography>Add a new card</Typography>
          </Box>
        </Box>
      )}
      <Modal open={open} onClose={handleClose}>
        <Box
          sx={{
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            bgcolor: 'background.paper',
            padding: 4,
            borderRadius: 2,
            width: isSmallScreen ? '70%' : 'auto',
            maxWidth: '500px',
          }}
        >
          <Typography variant="h6" gutterBottom>Add New Card</Typography>
          <TextField
            label="Card Number"
            fullWidth
            margin="normal"
            value={newCard.cardNumber}
            onChange={handleInputChange('cardNumber')}
            onBlur={handleBlur('cardNumber')}
            placeholder="1234 5678 1234 5678"
            InputProps={{
              inputProps: {
                maxLength: 19,
              },
            }}
            error={touched.cardNumber && Boolean(errors.cardNumber)}
            helperText={touched.cardNumber && errors.cardNumber}
          />
          <Grid
            sx={{
              display: 'flex',
              flexDirection: isSmallScreen ? 'column' : 'row',
              justifyContent: 'space-between',
              gap: 2,
            }}
          >
            <TextField
              label="Expiry Date"
              sx={{ width: isSmallScreen ? '100%' : '48%' }}
              margin="normal"
              value={newCard.cardExpiryDate}
              onChange={handleInputChange('cardExpiryDate')}
              onBlur={handleBlur('cardExpiryDate')}
              placeholder="MM/YY"
              InputProps={{
                inputProps: {
                  pattern: '\\d{2}/\\d{2}',
                },
              }}
              error={touched.cardExpiryDate && Boolean(errors.cardExpiryDate)}
              helperText={touched.cardExpiryDate && errors.cardExpiryDate}
            />
            <TextField
              label="CVV"
              sx={{ width: isSmallScreen ? '100%' : '55%' }}
              margin="normal"
              value={newCard.cardCVV}
              onChange={handleInputChange('cardCVV')}
              onBlur={handleBlur('cardCVV')}
              placeholder="123"
              InputProps={{
                inputProps: {
                  maxLength: 3,
                },
              }}
              error={touched.cardCVV && Boolean(errors.cardCVV)}
              helperText={touched.cardCVV && errors.cardCVV}
            />
          </Grid>
          <Button variant="contained" color="primary" onClick={handleAddCard}>
            Save
          </Button>
        </Box>
      </Modal>
    </Box>
  );
};
export default PaymentCard;
