import React from 'react';
import PropTypes from 'prop-types';
import {
  Box, Typography, Modal, TextField, Button, Grid,
} from '@mui/material';
import { Formik, Form, Field } from 'formik';
import userAddressSchema from '../../../../utils/userAddressSchema.js';

const AddressForm = ({
  open, handleClose, handleSubmit, initialValues, title,
}) => (
  <Modal open={open} onClose={handleClose}>
    <Box sx={{
      position: 'absolute',
      top: '40%',
      left: '50%',
      transform: 'translate(-50%, -50%)',
      bgcolor: 'background.paper',
      p: 6,
      borderRadius: 2,
      maxWidth: '600px',
      width: '100%',
    }}
    >
      <Typography variant="h6" gutterBottom sx={{ mb: 4, textAlign: 'center' }}>{title}</Typography>
      <Formik
        initialValues={initialValues}
        validationSchema={userAddressSchema}
        onSubmit={(values, { setSubmitting }) => {
          handleSubmit(values);
          setSubmitting(false);
        }}
      >
        {({ isSubmitting, errors, touched }) => (
          <Form>
            <Grid container spacing={3} px={2}>
              <Grid item small={12} middle={6}>
                <Field
                  as={TextField}
                  label="First Name"
                  name="firstName"
                  variant="standard"
                  fullWidth
                  error={touched.firstName && !!errors.firstName}
                  helperText={touched.firstName && errors.firstName}
                />
              </Grid>
              <Grid item small={12} middle={6}>
                <Field
                  as={TextField}
                  label="Last Name"
                  name="lastName"
                  variant="standard"
                  fullWidth
                  error={touched.lastName && !!errors.lastName}
                  helperText={touched.lastName && errors.lastName}
                />
              </Grid>
              <Grid item small={12} middle={6}>
                <Field
                  as={TextField}
                  label="Country"
                  name="country"
                  variant="standard"
                  fullWidth
                  error={touched.city && !!errors.city}
                  helperText={touched.city && errors.city}
                />
              </Grid>
              <Grid item small={12} middle={6}>
                <Field
                  as={TextField}
                  label="City/Region"
                  name="city"
                  variant="standard"
                  fullWidth
                  error={touched.city && !!errors.city}
                  helperText={touched.city && errors.city}
                />
              </Grid>
              <Grid item small={12} middle={6}>
                <Field
                  as={TextField}
                  label="Street"
                  name="addressLine1"
                  variant="standard"
                  fullWidth
                  error={touched.addressLine1 && !!errors.addressLine1}
                  helperText={touched.addressLine1 && errors.addressLine1}
                />
              </Grid>
              <Grid item small={12} middle={6}>
                <Field
                  as={TextField}
                  label="Apartment"
                  name="addressLine2"
                  variant="standard"
                  fullWidth
                  error={touched.addressLine2 && !!errors.addressLine2}
                  helperText={touched.addressLine2 && errors.addressLine2}
                />
              </Grid>
              <Grid item small={12} middle={6}>
                <Field
                  as={TextField}
                  label="Postal Code"
                  name="zip"
                  variant="standard"
                  fullWidth
                  error={touched.zip && !!errors.zip}
                  helperText={touched.zip && errors.zip}
                />
              </Grid>
              <Grid item small={12} middle={6}>
                <Field
                  as={TextField}
                  label="Phone Number"
                  name="phone"
                  variant="standard"
                  fullWidth
                  error={touched.phone && !!errors.phone}
                  helperText={touched.phone && errors.phone}
                />
              </Grid>
              <Grid item small={12} middle={6}>
                <Field
                  as={TextField}
                  label="Email"
                  name="email"
                  variant="standard"
                  fullWidth
                  error={touched.email && !!errors.email}
                  helperText={touched.email && errors.email}
                />
              </Grid>
            </Grid>
            <Box sx={{ mt: 6, textAlign: 'center' }}>
              <Button variant="contained" color="primary" type="submit" disabled={isSubmitting}>Save</Button>
            </Box>
          </Form>
        )}
      </Formik>
    </Box>
  </Modal>
);

AddressForm.propTypes = {
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  initialValues: PropTypes.shape({
    email: PropTypes.string.isRequired,
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    country: PropTypes.string.isRequired,
    city: PropTypes.string.isRequired,
    addressLine1: PropTypes.string.isRequired,
    addressLine2: PropTypes.string,
    zip: PropTypes.string.isRequired,
    phone: PropTypes.string.isRequired,
  }).isRequired,
  title: PropTypes.string.isRequired,
};

export default AddressForm;
