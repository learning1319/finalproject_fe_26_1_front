import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Box, Typography } from '@mui/material';
import { getAddress, addAddress, updateAddress } from '../../../../redux/slices/addressSlice.js';
import CurrentAddress from './CurrentUserAddress';
import AddressForm from './AddressForm';
import CustomUserModal from '../../CustomUserModal';

const UserAddress = () => {
  const [open, setOpen] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [modalMessage, setModalMessage] = useState('');
  const [formValues, setFormValues] = useState(null);

  const dispatch = useDispatch();
  const token = useSelector((state) => state.login.token);
  const userId = useSelector((state) => state.login.user._id);
  const address = useSelector((state) => state.address.address);

  const handleOpenAdd = () => {
    setIsEditing(false);
    setFormValues({
      email: '',
      firstName: '',
      lastName: '',
      country: '',
      city: '',
      addressLine1: '',
      addressLine2: '',
      zip: '',
      phone: '',
    });
    setOpen(true);
  };

  useEffect(() => {
    if (userId && token) {
      dispatch(getAddress(userId, token));
    }
  }, [dispatch, userId, token, handleOpenAdd]);

  const handleOpenEdit = () => {
    setIsEditing(true);
    setFormValues(address);
    setOpen(true);
  };

  const handleClose = () => setOpen(false);

  const handleAddAddress = (values) => {
    dispatch(addAddress(
      userId,
      values.email,
      values.firstName,
      values.lastName,
      values.country,
      values.city,
      values.addressLine1,
      values.addressLine2,
      values.zip,
      values.phone,
      token,
      true,
    )).then(() => {
      setModalMessage('You have successfully added a new address.');
      setOpen(false);
    });
  };

  const handleUpdateAddress = (values) => {
    dispatch(updateAddress(
      userId,
      values.email,
      values.firstName,
      values.lastName,
      values.country,
      values.city,
      values.addressLine1,
      values.addressLine2,
      values.zip,
      values.phone,
      token,
    )).then(() => {
      setModalMessage('You have successfully updated your address.');
      setOpen(false);
    });
  };

  return (
    <Box sx={{
      display: 'flex',
      flexDirection: 'column',
      gap: 2,
      pl: 2,
      width: '100%',
      maxWidth: 600,
      margin: 'auto',
      minHeight: '45vh',
      overflow: 'hidden',
      boxSizing: 'border-box',
    }}
    >
      {address.userId ? (
        <>
          <CurrentAddress address={address} />
          <Box
            sx={{
              border: '1px dashed #ccc',
              padding: 2,
              borderRadius: 2,
              display: 'flex',
              alignItems: 'center',
              cursor: 'pointer',
              mb: 2,
            }}
            onClick={handleOpenEdit}
          >
            <Typography variant="h6" sx={{ mr: 1 }}>+</Typography>
            <Typography>Update current address</Typography>
          </Box>
        </>
      ) : (
        <>
          <Box sx={{
            display: 'flex', justifyContent: 'center', width: '100%', py: 2,
          }}
          >
            <Typography sx={{ typography: 'mobile_h2' }}>No address added yet</Typography>
          </Box>
          <Box
            sx={{
              border: '1px dashed #ccc',
              padding: 2,
              borderRadius: 2,
              display: 'flex',
              alignItems: 'center',
              cursor: 'pointer',
              mb: 2,
            }}
            onClick={handleOpenAdd}
          >
            <Typography variant="h6" sx={{ mr: 1 }}>+</Typography>
            <Typography>Add a new address</Typography>
          </Box>
        </>
      )}

      {open && (
        <AddressForm
          open={open}
          handleClose={handleClose}
          handleSubmit={isEditing ? handleUpdateAddress : handleAddAddress}
          initialValues={formValues}
          title={isEditing ? 'Update Address' : 'Add New Address'}
        />
      )}

      <CustomUserModal
        open={!!modalMessage}
        onClose={() => setModalMessage('')}
        message={modalMessage}
      />
    </Box>
  );
};

export default UserAddress;
