import React from 'react';
import PropTypes from 'prop-types';
import { Box, Typography, Grid } from '@mui/material';
import { useTheme } from '@mui/material/styles';

const CurrentUserAddress = ({ address }) => {
  const theme = useTheme();

  return (
    <Box
      sx={{
        border: `1px solid ${theme.palette.primary.main}`,
        padding: 2,
        borderRadius: 2,
        mb: 2,
      }}
    >
      <Typography sx={{ typography: 'mobile_h2' }}>
        Current Address:
      </Typography>
      <Grid container spacing={2} mt={1}>
        <Grid item small={12} middle={6}>
          <Typography>
            First Name:
            {' '}
            <i>{address.firstName}</i>
          </Typography>
        </Grid>
        <Grid item small={12} middle={6}>
          <Typography>
            Last Name:
            {' '}
            <i>{address.lastName}</i>
          </Typography>
        </Grid>
        <Grid item small={12} middle={6}>
          <Typography>
            Country:
            {' '}
            <i>{address.country}</i>
          </Typography>
        </Grid>
        <Grid item small={12} middle={6}>
          <Typography>
            City:
            {' '}
            <i>{address.city}</i>
          </Typography>
        </Grid>
        <Grid item small={12} middle={6}>
          <Typography>
            Street:
            {' '}
            <i>{address.addressLine1}</i>
          </Typography>
        </Grid>
        <Grid item small={12} middle={6}>
          <Typography>
            Apartment:
            {' '}
            <i>{address.addressLine2}</i>
          </Typography>
        </Grid>
        <Grid item small={12} middle={6}>
          <Typography>
            Postal Code:
            {' '}
            <i>{address.zip}</i>
          </Typography>
        </Grid>
        <Grid item small={12} middle={6}>
          <Typography>
            Phone Number:
            {' '}
            <i>{address.phone}</i>
          </Typography>
        </Grid>
        <Grid item small={12} middle={6}>
          <Typography>
            Email:
            {' '}
            <i>{address.email}</i>
          </Typography>
        </Grid>
      </Grid>
    </Box>
  );
};

CurrentUserAddress.propTypes = {
  address: PropTypes.shape({
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    addressLine1: PropTypes.string.isRequired,
    addressLine2: PropTypes.string.isRequired,
    city: PropTypes.string.isRequired,
    country: PropTypes.string.isRequired,
    zip: PropTypes.string.isRequired,
    phone: PropTypes.string.isRequired,
  }).isRequired,
};

export default CurrentUserAddress;
