import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import {
  Container, Tabs, Tab, Box, styled, useMediaQuery, useTheme, Typography,
} from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import {
  loggedOut,
} from '../../redux/slices/loginSlice.js';
import {
  openLogoutWindow,
  closeLogoutWindow,
  openDeleteAccountWindow,
  closeDeleteAccountWindow,
} from '../../redux/slices/modalSlice.js';
import UserInfo from './ProfileMenu/UserInfo';
import UserAddress from './ProfileMenu/UserAdress/UserAdress';
import UserWishlist from './ProfileMenu/UserWishlist';
import PaymentCard from './ProfileMenu/PaymentCard';
import Password from './ProfileMenu/PasswordFields';
import BASE_URL from '../../constants/constants.js';
import UserProfileModal from './UserProfileModal';
import Leaf from '../../assets/svg/Leaf/Leaf';

const StyledVerticalTab = styled(Tab)(({ theme }) => ({
  textAlign: 'left',
  alignItems: 'flex-start',
  textTransform: 'none',
  fontSize: '1.2rem',
  padding: theme.spacing(3, 2),
  width: '100%',
  '&.Mui-selected': {
    fontWeight: 'bold',
    backgroundColor: theme.palette.primary.light,
    '&::before': {
      content: '""',
      position: 'absolute',
      right: 0,
      top: 0,
      width: '4px',
      height: '100%',
      backgroundColor: theme.palette.primary.main,
    },
  },
}));

const StyledHorizontalTab = styled(Tab)(({ theme }) => ({
  textAlign: 'center',
  textTransform: 'none',
  fontSize: '1rem',
  padding: theme.spacing(2, 1),
  minWidth: '120px',
  '&.Mui-selected': {
    fontWeight: 'bold',
    color: theme.palette.primary.main,
  },
}));

const UserProfile = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { showLogoutWindow, showDeleteAccountWindow } = useSelector((state) => state.modal);
  const [value, setValue] = useState(0);
  const [showMessage, setShowMessage] = useState(false);
  const token = useSelector((state) => (state.login.isLogged ? state.login.token : null));
  const currentUser = useSelector((state) => (state.login.isLogged ? state.login.user : null));

  const theme = useTheme();
  const isLarge = useMediaQuery(theme.breakpoints.down('large'));

  const handleLogout = () => {
    dispatch(loggedOut());
    dispatch(closeLogoutWindow());
    navigate('/');
  };

  const handleDeleteAccount = async () => {
    await axios.delete(`${BASE_URL}/customers/delete-account`, {
      data: { _id: currentUser._id },
      headers: { Authorization: token },
    })
      .then(() => {
        dispatch(closeDeleteAccountWindow());
        dispatch(loggedOut());
        navigate('/');
      })
      .catch((error) => {
        console.log(error.response.data.message);
        dispatch(closeDeleteAccountWindow());
      });
  };

  const handleCloseLogoutWindow = () => {
    dispatch(closeLogoutWindow());
    setShowMessage(true);
  };

  const handleCloseDeleteAccountWindow = () => {
    dispatch(closeDeleteAccountWindow());
    setShowMessage(true);
  };

  const handleChange = (event, newValue) => {
    setValue(newValue);
    setShowMessage(false);

    if (newValue === 5) {
      dispatch(openDeleteAccountWindow());
    } else if (newValue === 6) {
      dispatch(openLogoutWindow());
    }
  };

  return (
    <Container sx={{
      display: 'flex', mt: 4, height: '100%', flexDirection: isLarge ? 'column' : 'row',
    }}
    >
      {isLarge ? (
        <Box sx={{
          width: '100%', overflowX: 'auto', whiteSpace: 'nowrap',
        }}
        >
          <Tabs
            value={value}
            onChange={handleChange}
            variant="scrollable"
            scrollButtons="auto"
            aria-label="User Profile Tabs"
            sx={{ mb: 2 }}
          >
            <StyledHorizontalTab label="User Profile" />
            <StyledHorizontalTab label="Address" />
            <StyledHorizontalTab label="Wishlist" />
            <StyledHorizontalTab label="Payment Card" />
            <StyledHorizontalTab label="Password" />
            <StyledHorizontalTab label="Delete Account" sx={{ color: 'error.main' }} />
            <StyledHorizontalTab label="Logout" />
          </Tabs>
        </Box>
      ) : (
        <Box sx={{
          width: '350px', height: 'fit-content', position: 'sticky', top: theme.spacing(2), alignSelf: 'flex-start',
        }}
        >
          <Tabs
            orientation="vertical"
            value={value}
            onChange={handleChange}
            aria-label="User Profile Tabs"
            sx={{ borderRight: 1, borderColor: 'divider' }}
          >
            <StyledVerticalTab label="User Profile" />
            <StyledVerticalTab label="Address" />
            <StyledVerticalTab label="Wishlist" />
            <StyledVerticalTab label="Payment Card" />
            <StyledVerticalTab label="Password" />
            <StyledVerticalTab label="Delete Account" sx={{ color: 'error.main', mt: 6 }} />
            <StyledVerticalTab label="Logout" />
          </Tabs>
        </Box>
      )}
      <Box sx={{
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        overflowY: 'auto',
        mt: isLarge ? 4 : 0,
        minHeight: '45vh',
      }}
      >
        <Box sx={{ flex: 1 }}>
          {value === 0 && <UserInfo />}
          {value === 1 && <UserAddress />}
          {value === 2 && <UserWishlist />}
          {value === 3 && <PaymentCard />}
          {value === 4 && <Password />}
          {showMessage && (
          <Box sx={{
            display: 'flex', alignItems: 'center', justifyContent: 'center',
          }}
          >
            <Box sx={{ pb: 4 }}>
              <Leaf
                width={80}
                height={80}
                currentColor={theme.palette.primary.main}
              />
            </Box>
            <Typography variant="mobile_h1">Thank you for staying with us.</Typography>
          </Box>
          )}
        </Box>
      </Box>

      <UserProfileModal
        open={showLogoutWindow}
        onClose={handleCloseLogoutWindow}
        message="Are you sure you want to logout?"
        onCancel={handleCloseLogoutWindow}
        onConfirm={handleLogout}
        confirmText="Logout"
      />

      <UserProfileModal
        open={showDeleteAccountWindow}
        onClose={handleCloseDeleteAccountWindow}
        message="Are you sure you want to delete your account?"
        onCancel={handleCloseDeleteAccountWindow}
        onConfirm={handleDeleteAccount}
        confirmText="Delete"
      />
    </Container>
  );
};

export default UserProfile;
