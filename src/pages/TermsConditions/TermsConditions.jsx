import {
  Container,
  Box,
  Typography,
} from '@mui/material';
import { useEffect } from 'react';

const TermsConditions = () => {
  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  }, []);
  const titleStyle = {
    fontSize: {
      mobile: '18px',
      tablet: '20px',
    },
    mb: 2,
  };

  return (
    <Container
      sx={{
        minHeight: '46vh',
      }}
    >
      <Box sx={{ my: 2 }}>
        <Typography
          variant="h4"
          component="h1"
          gutterBottom
          sx={{
            fontSize: {
              small: '16px',
              mobile: '18px',
              tablet: '20px',
            },
            fontWeight: '700',
            mb: 4,
          }}
        >
          Modimal Terms & Conditions
        </Typography>
        <Typography sx={{ textAlign: 'justify', mb: 4 }}>
          Welcome to Modimal, your trusted online clothing store. By accessing
          and using our website, you agree to comply with and be bound by the
          following terms and conditions. Please read these terms carefully
          before making any purchases.
        </Typography>
        <Typography
          variant="h5"
          sx={{
            fontSize: {
              small: '16px',
              mobile: '18px',
              tablet: '20px',
            },
            fontWeight: '700',
            mb: 4,
            mt: 2,
          }}
        >
          General Conditions
        </Typography>
        <Typography
          sx={{
            textAlign: 'justify',
            titleStyle,
            mb: 2,
          }}
        >
          Eligibility: To make a purchase on our website,
          you must be at least 18 years old or have the
          permission and supervision of a parent or guardian.
        </Typography>
        <Typography
          sx={{
            mb: 2,
            textAlign: 'justify',
          }}
        >
          Acceptance of Orders: All orders are subject to
          acceptance by Modimal. We reserve the right to
          refuse or cancel any order for any reason at our discretion.
        </Typography>
        <Typography
          sx={{
            titleStyle,
            mb: 4,
            textAlign: 'justify',
          }}
        >
          Product Descriptions: We strive to provide
          accurate descriptions of our products. However,
          we do not warrant that the descriptions are complete,
          current, or error-free. If a product offered by
          Modimal is not as described, your sole remedy is
          to return it in unused condition.
        </Typography>
      </Box>
    </Container>
  );
};

export default TermsConditions;
