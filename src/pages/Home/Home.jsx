/* eslint-disable */
import Bestsellers from "../../components/Bestsellers/Bestsellers";
import MainBannerNewIn from "../../components/MainBannerNewIn/MainBannerNewIn";
import MainBannerSustainability from "../../components/MainBannerSustainability/MainBannerSustainability";
import Collection from "../../components/Collection/Collection";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import BASE_URL from "../../constants/constants";
import { useEffect } from "react";
import { loggedIn, loggedOut } from "../../redux/slices/loginSlice";
import ModiweekCards from "../Modiweek/ModiweekCards";
import { fetchBanners } from "../../redux/slices/modiweekSlice";

const Home = () => {
  const dispatch = useDispatch();

  // Needed for reading cookies info.
  useEffect(() => {
    const fetchUserInfo = async () => {
      try {
        const response = await axios.get(`${BASE_URL}/cookies`, {
          withCredentials: true,
        });
        if (response) {
          dispatch(loggedIn(response.data));
        } else {
          console.log("No cookies were set");
        }
      } catch (error) {
        console.log("Cookies are empty");
      }
    };

    fetchUserInfo();
  }, [dispatch]);

  const banners = useSelector((state) => state.banners.items);

  useEffect(() => {
    dispatch(fetchBanners("Modiweek"));
  }, [dispatch]);

  return (
    <>
      <MainBannerNewIn />
      <Bestsellers />
      <Collection />
      <ModiweekCards banners={banners} home={true} />
      <MainBannerSustainability />
    </>
  );
};

export default Home;
