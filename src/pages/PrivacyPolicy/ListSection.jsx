import React from 'react';
import { List } from '@mui/material';
import PropTypes from 'prop-types';

const ListSection = ({ children }) => (
  <List>
    {children}
  </List>
);

ListSection.propTypes = {
  children: PropTypes.node.isRequired,
};

export default ListSection;
