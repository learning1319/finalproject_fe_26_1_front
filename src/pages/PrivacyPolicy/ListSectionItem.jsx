import React from 'react';
import { ListItem, ListItemIcon, ListItemText } from '@mui/material';
import PropTypes from 'prop-types';

const ListSectionItem = ({ icon, text }) => (
  <ListItem>
    <ListItemIcon>{icon}</ListItemIcon>
    <ListItemText
      primary={text}
    />
  </ListItem>
);

ListSectionItem.propTypes = {
  icon: PropTypes.node.isRequired,
  text: PropTypes.string.isRequired,
};

export default ListSectionItem;
