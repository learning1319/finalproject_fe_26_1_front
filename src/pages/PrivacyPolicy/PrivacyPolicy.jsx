import React, { useEffect } from 'react';
import {
  Container,
  Box,
} from '@mui/material';

import Section from './Section';
import ListSection from './ListSection';
import ListSectionItem from './ListSectionItem';
import Information from '../../assets/svg/Information/Information';
import CheckCircle from '../../assets/svg/CheckCircle/CheckCircle';
import Lock from '../../assets/svg/Lock/Lock';
import Cookie from '../../assets/svg/Cookie/Cookie';

const PrivacyPolicy = () => {
  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  }, []);
  return (
    <Container>
      <Box sx={{ my: 4 }}>
        <Section title="1. Information We Collect" />
        <ListSection>
          <ListSectionItem
            icon={<Information />}
            text="Personal Information"
          />
          <ListSectionItem
            icon={<Information />}
            text="Usage Data"
          />
          <ListSectionItem
            icon={<Cookie />}
            text="Cookies and Tracking Technologies"
          />
        </ListSection>
        <Section title="2. How We Use Your Information" />
        <ListSection>
          <ListSectionItem icon={<CheckCircle />} text="Processing and fulfilling your orders." />
          <ListSectionItem icon={<CheckCircle />} text="Providing customer support and responding to your inquiries." />
          <ListSectionItem
            icon={<CheckCircle />}
            text="Sending you updates about your order,
          promotional offers,
          and other news about Modimal."
          />
          <ListSectionItem icon={<CheckCircle />} text="Improving our website, products, and services." />
          <ListSectionItem icon={<CheckCircle />} text="Analyzing user behavior and preferences to enhance the shopping experience." />
          <ListSectionItem icon={<CheckCircle />} text="Complying with legal obligations and protecting our legal rights." />
        </ListSection>
        <Section title="3. How We Share Your Information" />
        <ListSection>
          <ListSectionItem
            icon={<Lock />}
            text="Service Providers:
          Third-party service providers who perform services on our
          behalf, such as payment processing, shipping, and marketing."
          />
          <ListSectionItem
            icon={<Lock />}
            text="Business Transfers:
          If we are involved in a merger, acquisition, or sale of assets,
          your information may be transferred as part of that transaction."
          />
          <ListSectionItem
            icon={<Lock />}
            text="Legal Requirements:
          When required by law or to protect our rights,
           we may disclose your information to authorities or other parties."
          />
        </ListSection>
        <Section title="4. Your Choices" />
        <ListSection>
          <ListSectionItem
            icon={<CheckCircle />}
            text="Access and Correction:
          You can access and update your personal information
          by logging into your account, or contacting us directly."
          />
          <ListSectionItem
            icon={<CheckCircle />}
            text="Opt-Out: You can opt-out of receiving
          promotional emails from us,
          by following the unsubscribe instructions in those emails."
          />
          <ListSectionItem
            icon={<Cookie />}
            text="Cookies: You
          can manage cookies through your browser settings."
          />
        </ListSection>
      </Box>
    </Container>
  );
};

export default PrivacyPolicy;
