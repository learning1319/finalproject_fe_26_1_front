import React from 'react';
import { Typography } from '@mui/material';
import PropTypes from 'prop-types';

const Section = ({ title, content, variant }) => (
  <>
    {title
      && (
      <Typography
        sx={{
          fontSize: {
            small: '16px',
            mobile: '14px',
            tablet: '16px',
          },
          mb: 4,
          mt: 4,
          fontWeight: '700',
        }}
        variant={variant}
        gutterBottom
      >
        {title}
      </Typography>
      )}
    {content
      && (
      <Typography
        paragraph
      >
        {content}
      </Typography>
      )}
  </>
);

Section.propTypes = {
  title: PropTypes.string,
  content: PropTypes.string,
  variant: PropTypes.string,
};

Section.defaultProps = {
  title: '',
  content: '',
  variant: 'body1',
};

export default Section;
