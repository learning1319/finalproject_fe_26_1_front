/* eslint-disable */
import PropTypes from 'prop-types';
import { Typography } from '@mui/material';

const Address = ({ theme }) => (
  <Typography
    paragraph
    sx={{
      ...theme.typography.mobile_bodyMD,
      [theme.breakpoints.up('tablet')]: theme.typography.desktop_bodyMD,
    }}
  >
    Address:
    {' '}
    22 Khreshchatyk St, Kyiv, Ukraine, 01001
  </Typography>
);

Address.propTypes = {
  theme: PropTypes.shape({
    typography: PropTypes.shape({
      mobile_bodyMD: PropTypes.shape({
        fontSize: PropTypes.string,
        fontWeight: PropTypes.number,
        lineHeight: PropTypes.string,
      }).isRequired,
      desktop_bodyMD: PropTypes.shape({
        fontSize: PropTypes.string,
        fontWeight: PropTypes.number,
        lineHeight: PropTypes.string,
      }).isRequired,
    }).isRequired,
    breakpoints: PropTypes.shape({
      up: PropTypes.func,
    }).isRequired,
  }).isRequired,
};

export default Address;
