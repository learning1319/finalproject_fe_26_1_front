/* eslint-disable */
import PropTypes from 'prop-types';
import { Typography } from '@mui/material';

const Hours = ({ theme }) => (
  <Typography
    paragraph
    sx={{
      ...theme.typography.mobile_bodyMD,
      [theme.breakpoints.up('tablet')]: theme.typography.desktop_bodyMD,
    }}
  >
    <strong>Hours:</strong>
    {' '}
    Monday to Friday: 10:00 AM - 7:00 PM | Saturday: 10:00 AM - 6:00 PM | Sunday: Closed
  </Typography>
);

Hours.propTypes = {
  theme: PropTypes.shape({
    typography: PropTypes.shape({
      mobile_bodyMD: PropTypes.shape({
        fontSize: PropTypes.string,
        fontWeight: PropTypes.number,
        lineHeight: PropTypes.string,
      }).isRequired,
      desktop_bodyMD: PropTypes.shape({
        fontSize: PropTypes.string,
        fontWeight: PropTypes.number,
        lineHeight: PropTypes.string,
      }).isRequired,
    }).isRequired,
    breakpoints: PropTypes.shape({
      up: PropTypes.func,
    }).isRequired,
  }).isRequired,
};

export default Hours;
