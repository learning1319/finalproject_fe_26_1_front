import PropTypes from 'prop-types';
import {
  Box,
  Container,
  Typography,
  useTheme,
  Paper,
  CircularProgress,
} from '@mui/material';
import { useEffect, useState } from 'react';
import Address from './Address';
import Hours from './Hours';
import ContactDetailsItem from '../ContactUs/ContactDetailsItem';

const VisitUs = () => {
  const [mapLoaded, setMapLoaded] = useState(false);

  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  }, []);

  const theme = useTheme();

  return (
    <Box
      component="section"
      sx={{
        padding: '2rem 0',
        textAlign: 'center',
      }}
    >
      <Container>
        <Typography
          gutterBottom
          sx={{
            ...theme.typography.mobile_h2,
            [theme.breakpoints.up('tablet')]: theme.typography.desktop_h3,
            marginBottom: '1rem',
          }}
        >
          Visit Us
        </Typography>
        <Address theme={theme} />
        <Hours theme={theme} />
        <ContactDetailsItem
          label="Email:"
          value="info@modimal.com"
          href="mailto:info@modimal.com"
        />
        <ContactDetailsItem
          label="Phone:"
          value="+3 (044) 111-1111"
        />
      </Container>
      <Paper
        elevation={3}
        sx={{
          width: {
            small: '100%',
            mobile: '100%',
            middle: '80%',
            tablet: '70%',
            large: '60%',
            desktop: '50%',
          },
          height: {
            small: '300px',
            mobile: '300px',
            middle: '400px',
            tablet: '400px',
            large: '500px',
            desktop: '600px',
          },
          ml: 'auto',
          mr: 'auto',
          mt: 4,
          position: 'relative',
        }}
      >
        {!mapLoaded && (
          <Box
            sx={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              position: 'absolute',
              top: 0,
              left: 0,
              width: '100%',
              height: '100%',
              backgroundColor: 'rgba(255, 255, 255, 0.8)',
              zIndex: 10,
            }}
          >
            <CircularProgress />
          </Box>
        )}
        <iframe
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2540.530058443822!2d30.520575876097876!3d50.44985348740739!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40d4ce56884f4f6f%3A0xcd06ffbc5e9276c0!2z0YPQuy4g0JrRgNC10YnQsNGC0LjQuiwgMjIsINCa0LjQtdCyLCDQo9C60YDQsNC40L3QsCwgMDIwMDA!5e0!3m2!1sru!2spl!4v1723229802312!5m2!1sru!2spl"
          width="100%"
          height="100%"
          style={{ border: 0 }}
          allowFullScreen=""
          loading="lazy"
          referrerPolicy="no-referrer-when-downgrade"
          title="Google Map"
          onLoad={() => setMapLoaded(true)}
        />
      </Paper>
    </Box>
  );
};

VisitUs.propTypes = {
  theme: PropTypes.shape({
    palette: PropTypes.shape({
      primary: PropTypes.shape({
        light: PropTypes.string,
      }),
    }),
    typography: PropTypes.shape({
      mobile_h2: PropTypes.shape({
        fontSize: PropTypes.number,
        fontWeight: PropTypes.number,
        lineHeight: PropTypes.string,
      }),
      desktop_h3: PropTypes.shape({
        fontSize: PropTypes.number,
        fontWeight: PropTypes.number,
        lineHeight: PropTypes.string,
      }),
      mobile_bodyMD: PropTypes.shape({
        fontSize: PropTypes.number,
        fontWeight: PropTypes.number,
        lineHeight: PropTypes.string,
      }),
      desktop_bodyMD: PropTypes.shape({
        fontSize: PropTypes.number,
        fontWeight: PropTypes.number,
        lineHeight: PropTypes.string,
      }),
    }),
    breakpoints: PropTypes.shape({
      up: PropTypes.func,
    }).isRequired,
  }).isRequired,
};

export default VisitUs;
