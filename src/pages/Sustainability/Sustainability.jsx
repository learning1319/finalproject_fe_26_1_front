import {
  Container,
  Box,
  Typography,
} from '@mui/material';
import { useEffect } from 'react';
import SustainabilityText from './SustainabilityText';

const Sustainability = () => {
  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  }, []);
  return (
    <Container>
      <Box
        sx={{
          textAlign: 'center',
          mt: 4,
        }}
      >
        <Typography variant="desktop_h3">
          Sustainability at Modimal
        </Typography>
        <SustainabilityText />
      </Box>
    </Container>
  );
};

export default Sustainability;
