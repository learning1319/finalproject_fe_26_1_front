/* eslint-disable */
import React from 'react';
import {
  Container,
  Box,
  List,
  ListItem,
  ListItemText,
  Typography,
} from '@mui/material';

const items = [
  {
    title: 'Eco-Friendly Materials',
    text: 'We prioritize using sustainable materials such as organic cotton, recycled polyester, and eco-friendly dyes in our clothing. These materials not only reduce environmental impact but also ensure that our products are safe and gentle on your skin.',
  },
  {
    title: 'Ethical Manufacturing',
    text: 'Our garments are crafted in factories that uphold the highest standards of ethical labor practices. We ensure fair wages, safe working conditions, and no child labor, reflecting our dedication to human rights and dignity.',
  },
  {
    title: 'Waste Reduction',
    text: 'We are committed to minimizing waste in our production processes. Through innovative design and manufacturing techniques, we reduce fabric waste and strive for zero-waste operations. Any surplus materials are repurposed or recycled whenever possible.',
  },
  {
    title: 'Sustainable Packaging',
    text: 'Our packaging is designed with the planet in mind. We use recyclable, biodegradable, and compostable materials to minimize our carbon footprint and encourage our customers to join us in our sustainability efforts by recycling and reusing our packaging.',
  },
  {
    title: 'Carbon Footprint',
    text: 'We continuously work on reducing our carbon footprint by optimizing our supply chain, improving energy efficiency in our operations, and investing in renewable energy sources. We aim to offset our carbon emissions and contribute to a healthier planet.',
  },
  {
    title: 'Circular Fashion',
    text: 'Modimal is a proud supporter of the circular fashion movement. We encourage customers to return their used Modimal clothing for recycling or resale, extending the lifecycle of our products and reducing textile waste.',
  },
  {
    title: 'Community Engagement',
    text: 'We believe in giving back to the community and supporting environmental causes. A portion of our profits is dedicated to environmental conservation projects and organizations that work towards a sustainable future.',
  },
  {
    title: 'Transparency',
    text: 'We are committed to transparency in our sustainability journey. We regularly publish updates on our progress and challenges, ensuring that our customers are informed and can hold us accountable.',
  },
];

const SustainabilityText = () => (
  <Container
    sx={{
      minWidth: 0,
    }}
  >
    <Box>
      <Typography
        sx={{
          pl: 0,
          textAlign: 'start',
          mt: 4,
        }}
        variant="desktop_bodyLG"
        gutterBottom
      >
        At Modimal, we believe that fashion and sustainability can
        go hand in hand. Our commitment to the environment and ethical
        practices is woven into the very fabric of our brand.
        Here is how we are making a difference:
      </Typography>
      <List>
        {items.map((item) => (
          <ListItem key={item.title}>
            <ListItemText
              sx={{
                display: 'flex',
                flexDirection: 'column',
                gap: 1,
              }}
              primary={<Typography variant="desktop_h5">{item.title}</Typography>}
              secondary={<Typography variant="desktop_bodyMD">{item.text}</Typography>}
            />
          </ListItem>
        ))}
      </List>
      <Typography
        sx={{
          mb: 1,
        }}
        variant="desktop_bodyLG"
        gutterBottom
      >
        Join us in our mission to create a more sustainable
        fashion industry. Together, we can make a positive
        impact on the planet while looking stylish and feeling great.
        Thank you for supporting Modimal and our commitment to sustainability.
      </Typography>
    </Box>
  </Container>
);

export default SustainabilityText;
