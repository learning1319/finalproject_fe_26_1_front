import { Routes, Route, Navigate } from 'react-router-dom';
import { useTheme } from '@mui/material';
import { useSelector } from 'react-redux';
import Home from './pages/Home/Home';
import Cart from './pages/Cart/Cart';
import Favorites from './pages/Favorites/Favorites';
import Login from './pages/Login/Login';
import Registration from './pages/Registration/Registration';
import Modiweek from './pages/Modiweek/Modiweek';
import UserProfile from './pages/UserProfile/UserProfile';
import TermsConditions from './pages/TermsConditions/TermsConditions';
import PrivacyPolicy from './pages/PrivacyPolicy/PrivacyPolicy';
import OrdersShipping from './pages/OrdersShipping/OrdersShipping';
import ReturnsRefunds from './pages/ReturnsRefunds/ReturnsRefunds';
import ContactUs from './pages/ContactUs/ContactUs';
import Careers from './pages/Careers/Careers';
import VisitUs from './pages/VisitUs/VisitUs';
import Layout from './Layout';
import PaymentForm from './pages/Payment/PaymentForm';
import Product from './pages/Product/Product';
import NotFoundPage from './pages/NotFound/NotFound';
import Successful from './pages/Payment/Successful/Successful';
import Failed from './pages/Payment/Failed/Failed';
import CollectionFilter from './components/Filter/CollectionFilter';
import CollectionNewIn from './components/Filter/CollectionNewIn';
import CollectionPlusSize from './components/Filter/CollectionPlusSize';
import HeaderSustainability from './components/HeaderSustainability/HeaderSustainability';
import SustainabilityMaterials from './components/HeaderSustainability/HeaderSustainabilityComponents/SustainabilityMaterials';
import ShippingInfo from './pages/ShippingInfo/ShippingInfo';

const AppRouter = () => {
  const theme = useTheme();
  const userIsLogged = useSelector((state) => state.login.isLogged);
  return (
    <Routes>
      <Route path="/" element={<Layout />}>
        <Route index path="/" element={<Home />} />
        <Route path="wishList" element={<Favorites />} />
        <Route path="success" element={<Successful />} />
        <Route path="failed" element={<Failed />} />
        <Route path="cart" element={<Cart />} />
        <Route path="info" element={<ShippingInfo />} />
        <Route path="login" element={<Login />} />
        <Route path="registration" element={<Registration />} />
        <Route path="collection" element={<CollectionFilter />} />
        <Route path="newIn" element={<CollectionNewIn />} />
        <Route path="modiweek/:day?" element={<Modiweek />} />
        <Route path="plusSize" element={<CollectionPlusSize />} />
        <Route path="termsConditions" element={<TermsConditions />} />
        <Route path="policy" element={<PrivacyPolicy />} />
        <Route path="sustainability" element={<HeaderSustainability />} />
        <Route path="sustainability/materials" element={<SustainabilityMaterials />} />
        <Route path="orderShipping" element={<OrdersShipping />} />
        <Route path="returnsRefunds" element={<ReturnsRefunds />} />
        <Route path="contactUs" element={<ContactUs />} />
        <Route path="careers" element={<Careers />} />
        <Route path="payment" element={<PaymentForm />} />
        <Route path="/:id" element={<Product />} />
        <Route path="visitUs" element={<VisitUs theme={theme} />} />
        <Route
          path="userProfile/*"
          element={userIsLogged ? <UserProfile /> : <Navigate to="/login" />}
        />
        <Route path="*" element={<NotFoundPage />} />
      </Route>
    </Routes>
  );
};
export default AppRouter;
