import React from 'react';
import ReactDOM from 'react-dom/client';
import { ErrorBoundary } from 'react-error-boundary';
import { BrowserRouter } from 'react-router-dom';
import { ThemeProvider } from '@mui/material';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { Elements } from '@stripe/react-stripe-js';
import theme from './themeStyle';
import App from './App';
import { store, persistor } from './store.js';
import stripePromise from './utils/stripe.js';
import NotFoundPage from './pages/NotFound/NotFound';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Elements stripe={stripePromise}>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <ThemeProvider theme={theme}>
            <ErrorBoundary fallback={<NotFoundPage />}>
              <BrowserRouter>
                <App />
              </BrowserRouter>
            </ErrorBoundary>
          </ThemeProvider>
        </PersistGate>
      </Provider>
    </Elements>
  </React.StrictMode>,
);
