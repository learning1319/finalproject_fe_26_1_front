import { configureStore, combineReducers } from '@reduxjs/toolkit';
import {
  persistStore,
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import modalReducer from './redux/slices/modalSlice.js';
import favoriteReducer from './redux/slices/favoriteSlice.js';
import cartReducer from './redux/slices/cartSlice.js';
import loginReducer from './redux/slices/loginSlice.js';
import usersReducer from './redux/slices/authSlice.js';
import currentUserReducer from './redux/slices/currentUserSlice.js';
import filterReducer from './redux/slices/filterSlice.js';
import bannersReducer from './redux/slices/modiweekSlice.js';
import addressReducer from './redux/slices/addressSlice.js';
import paymentReducer from './redux/slices/paymentSlice.js';

const persistConfig = {
  key: 'root',
  storage,
  whitelist: [
    'modal',
    'auth',
    'login',
    'favorites',
    'cart',
    'currentUser',
    'filters',
    'address',
    'payment',
  ],
};

const rootReducer = combineReducers({
  modal: modalReducer,
  login: loginReducer,
  favorites: favoriteReducer,
  cart: cartReducer,
  auth: usersReducer,
  currentUser: currentUserReducer,
  filters: filterReducer,
  banners: bannersReducer,
  address: addressReducer,
  payment: paymentReducer,
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({
    serializableCheck: {
      ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
    },
  }),
});

const persistor = persistStore(store);

export { store, persistor };
