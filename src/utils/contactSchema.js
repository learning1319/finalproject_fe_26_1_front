import * as Yup from 'yup';

const contactSchema = Yup.object({
  fullName: Yup.string()
    .required('First Name is required')
    .min(2, 'Too short!')
    .max(25, 'Too long!')
    .matches(/^[\p{L} -]+$/u, 'Name must contain only letters')
    .test('not-only-spaces', 'Name cannot consist of spaces only', (value) => value && value.trim().length > 0),
  email: Yup.string().email('Invalid email address').required('Email is required'),
  message: Yup.string().required('Message is required'),
  privacyPolicy: Yup.boolean().oneOf([true], 'You have to submit our Privacy and Policy'),
});

export default contactSchema;
