import * as Yup from 'yup';

const registrationSchema = Yup.object().shape({
  email: Yup.string()
    .required('Email is required')
    .email('Invalid email format'),
  password: Yup.string()
    .required('Password is required')
    .min(8, 'Password is too short!')
    .max(30, 'Password is too long!'),
  firstName: Yup.string()
    .required('First Name is required')
    .min(2, 'Too short!')
    .max(25, 'Too long!')
    .matches(/^[\p{L} -]+$/u, 'Name must contain only letters')
    .test('not-only-spaces', 'Name cannot consist of spaces only', (value) => value && value.trim().length > 0),
  lastName: Yup.string()
    .required('Last Name is required')
    .min(2, 'Too short!')
    .max(25, 'Too long!')
    .matches(/^[\p{L} -]+$/u, 'Surname must contain only letters')
    .test('not-only-spaces', 'Last Name cannot consist of spaces only', (value) => value && value.trim().length > 0),
});
export default registrationSchema;
