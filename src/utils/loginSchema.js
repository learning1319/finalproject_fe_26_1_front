import * as Yup from 'yup';

const loginSchema = Yup.object().shape({
  email: Yup.string()
    .required('Email is required')
    .email('Invalid email format'),
  password: Yup.string()
    .required('Password is required')
    .min(8, 'Password is too short!')
    .max(30, 'Password is too long!'),
});

export default loginSchema;
