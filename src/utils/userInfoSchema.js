import * as Yup from 'yup';

const UserInfoSchema = Yup.object().shape({
  birthDate: Yup.date()
    .required('Birth Date is required'),
  phoneNumber: Yup.string()
    .required('Phone Number is required'),
  email: Yup.string()
    .required('Email is required')
    .email('Invalid email format'),
  firstName: Yup.string()
    .required('First Name is required')
    .min(2, 'Too short!')
    .max(25, 'Too long!')
    .matches(/^[\p{L} ]+$/u, 'Name must contain only letters')
    .test('not-only-spaces', 'Name cannot consist of spaces only', (value) => value && value.trim().length > 0),
  lastName: Yup.string()
    .required('Last Name is required')
    .min(2, 'Too short!')
    .max(25, 'Too long!')
    .matches(/^[\p{L} ]+$/u, 'Surname must contain only letters')
    .test('not-only-spaces', 'Last Name cannot consist of spaces only', (value) => value && value.trim().length > 0),
});

export default UserInfoSchema;
