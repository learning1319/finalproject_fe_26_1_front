import getProductById from '../api/getProductById.js';

const modifyCartItems = async (cartItems) => Promise.all(
  cartItems.map(async (item) => {
    const [id, color, size, quantity] = item.split('/');
    const cartItemObject = await getProductById(id);
    const product = cartItemObject.data[0];
    const selectedColorObj = product.products.find(
      (prod) => prod.color === color,
    );
    if (selectedColorObj) {
      const selectedSizeObj = selectedColorObj.sizes.find(
        (sizeObj) => sizeObj.size === size,
      );
      product.cartID = item;
      product.quantity = quantity;
      product.products = [
        {
          ...selectedColorObj,
          sizes: selectedSizeObj ? [selectedSizeObj] : [],
        },
      ];
    } else {
      product.products = [];
    }
    return product;
  }),
);

export default modifyCartItems;
