import * as Yup from 'yup';

const userAddressSchema = Yup.object().shape({
  country: Yup.string()
    .required('Country is required')
    .matches(/^[A-Za-z\s]+$/, 'Country cannot contain numbers or special symbols'),
  email: Yup.string()
    .email('Invalid email address')
    .required('Email is required'),
  firstName: Yup.string()
    .required('First Name is required')
    .matches(/^[A-Za-z\s]+$/, 'First Name cannot contain numbers or special symbols'),
  lastName: Yup.string()
    .required('Last Name is required')
    .matches(/^[A-Za-z\s]+$/, 'Last Name cannot contain numbers or special symbols'),
  addressLine1: Yup.string()
    .matches(
      /^[a-zA-Z0-9\s/-]*$/,
      'Street can only contain letters, numbers, spaces, hyphens (-), and slashes (/)',
    )
    .required('Street is required'),
  addressLine2: Yup.string()
    .matches(/^[0-9]*$/, 'Apartment can only contain numbers')
    .required('Apartment/Unit is required'),
  zip: Yup.string()
    .required('Postal Code is required')
    .matches(/^\d{2}-\d{3}$/, 'Postal Code must be in the format **-***'),
  city: Yup.string()
    .required('City is required')
    .matches(/^[A-Za-z\s]+$/, 'City cannot contain numbers or special symbols'),
  phone: Yup.string()
    .required('Phone Number is required')
    .matches(/^\d{10}$/, 'Phone number must be exactly 10 digits'),

});
export default userAddressSchema;
