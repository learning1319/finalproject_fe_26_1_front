const hoverEffect = (theme) => ({
  '&:hover': {
    color: theme.palette.primary.main,
    transform: 'scale(1.1)',
    transition: 'color 0.3s, transform 0.3s',
  },
});

export default hoverEffect;
