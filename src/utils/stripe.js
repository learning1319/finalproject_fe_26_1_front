import { loadStripe } from '@stripe/stripe-js';

const stripePromise = loadStripe('pk_test_51PhYfrJNRXJ9Q5DorJ3KKwphTh2SVhKHM5e23cOpyiS7KCDzod6sWT4uooUWsv6TtJPSNVI2hqr1W7mpq14Li7vk000CJnVl7D');

export default stripePromise;
