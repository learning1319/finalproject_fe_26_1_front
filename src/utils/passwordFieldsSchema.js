import * as Yup from 'yup';

const passwordFieldsSchema = Yup.object({
  currentPassword: Yup.string().required('Current Password is required'),
  newPassword: Yup.string()
    .required('New Password is required')
    .min(8, 'Password is too short!')
    .max(30, 'Password is too long!'),
  confirmPassword: Yup.string()
    .oneOf([Yup.ref('newPassword'), null], 'Passwords must match')
    .required('Confirm Password is required'),
});

export default passwordFieldsSchema;
