import { useLocation, Link as RouterLink } from 'react-router-dom';
import { Typography, Breadcrumbs, Container } from '@mui/material';
import Link from '@mui/material/Link';
import theme from '../../themeStyle';
import segmentDisplayNames from '../../assets/breadcrumbsNames.js';

const Breadcrumb = () => {
  const location = useLocation();
  const pathnames = location.pathname.split('/').filter((el) => el);
  const isHomePage = location.pathname === '/';
  const typography = {
    desktop: 'desktop_bodyLG',
    tablet: 'tablet_bodyMD',
    small: 'mobile_bodySM',
  };

  return (
    !isHomePage && (
      <Container
        role="presentation"
        sx={{
          height: '55px',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'flex-start',
          my: { small: 2, desktop: 3 },
        }}
      >
        <Breadcrumbs aria-label="breadcrumb">
          <Link underline="hover" color="inherit" component={RouterLink} to="/">
            <Typography
              color={theme.palette.secondary.light}
              sx={{
                typography,
              }}
            >
              Home
            </Typography>
          </Link>
          {pathnames.map((value, index) => {
            const to = `/${pathnames.slice(0, index + 1).join('/')}`;
            const displayName = segmentDisplayNames[value]
            || value.charAt(0).toUpperCase() + value.slice(1);

            return index === pathnames.length - 1 ? (
              <Typography
                key={to}
                color={theme.palette.secondary.dark}
                sx={{
                  typography,
                }}
              >
                {displayName}
              </Typography>
            ) : (
              <Link
                key={to}
                underline="hover"
                component={RouterLink}
                to={to}
              >
                <Typography
                  sx={{
                    typography,
                  }}
                  color={theme.palette.secondary.light}
                >
                  {displayName}
                </Typography>
              </Link>
            );
          })}
        </Breadcrumbs>
      </Container>
    )
  );
};

export default Breadcrumb;
