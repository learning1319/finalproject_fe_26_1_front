import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { useTheme, Button } from '@mui/material';
import { addCartItemAsync } from '../../../redux/slices/cartSlice.js';

const AddToCartButton = ({ product, selectedColor, selectedSize }) => {
  const [isInCart, setIsInCart] = useState(false);
  const theme = useTheme();
  const dispatch = useDispatch();
  const { items: productsInCart } = useSelector((state) => state.cart);

  // Check if the necessary fields are present
  const isProductInfoComplete = product?.id && selectedColor && selectedSize;
  const cartID = isProductInfoComplete ? `${product.id}/${selectedColor}/${selectedSize}/1` : null;

  useEffect(() => {
    console.log('Products in Cart:', productsInCart);

    if (Array.isArray(productsInCart) && cartID) {
      const productInCart = productsInCart.some((item) => item === cartID);
      setIsInCart(productInCart);
    } else {
      setIsInCart(false);
    }
  }, [productsInCart, cartID]);

  const handleAddToCart = () => {
    if (!isProductInfoComplete) {
      console.warn('Product info is incomplete. Cannot add to cart.');
      return;
    }

    if (isInCart) {
      console.log('Product is already in the cart');
      return; // Prevent duplicate addition
    }

    console.log('Adding to Cart with ID:', cartID);
    dispatch(addCartItemAsync(cartID));
  };

  const isButtonDisabled = !isProductInfoComplete;

  return (
    <Button
      variant="contained"
      color={isInCart ? 'secondary' : 'primary'}
      sx={{
        width: '100%',
        mt: 3,
        backgroundColor: isInCart ? '#FFFFFF' : theme.palette.primary.main,
        color: isInCart ? '#000000' : '#FFFFFF',
        '&:hover': {
          backgroundColor: isInCart ? '#FFFFFF' : theme.palette.primary.dark,
        },
      }}
      onClick={handleAddToCart}
      disabled={isButtonDisabled}
    >
      {isInCart ? 'Added to Cart' : `Add to Cart $${product.price}`}
    </Button>
  );
};

AddToCartButton.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
  }).isRequired,
  selectedColor: PropTypes.string.isRequired,
  selectedSize: PropTypes.string.isRequired,
};

export default AddToCartButton;
