import React, { useState } from 'react';
import { Box } from '@mui/material';
import PropTypes from 'prop-types';

const ColorsButtons = ({ colors, initialColor, onColorChange }) => {
  const [selectedValue, setSelectedValue] = useState(initialColor);

  const handleChange = (color) => {
    setSelectedValue(color);
    onColorChange(color);
  };

  const handleKeyDown = (event, color) => {
    if (event.key === 'Enter' || event.key === ' ') {
      event.preventDefault();
      handleChange(color);
    }
  };

  return (
    <Box sx={{ display: 'flex', gap: '8px' }}>
      {colors.map((color) => (
        <div
          key={color}
          style={{
            width: '24px',
            height: '24px',
            borderRadius: '50%',
            backgroundColor: color,
            boxShadow: color === 'white' ? '0px 0px 3px rgba(0, 0, 0, 1)' : 'none',
            border: selectedValue === color ? '2px solid black' : 'none',
            cursor: 'pointer',
          }}
          onClick={() => handleChange(color)}
          onKeyDown={(event) => handleKeyDown(event, color)}
          role="button"
          tabIndex={0}
          aria-label={`Select color ${color}`}
        />
      ))}
    </Box>
  );
};

ColorsButtons.propTypes = {
  colors: PropTypes.arrayOf(PropTypes.string).isRequired,
  onColorChange: PropTypes.func.isRequired,
  initialColor: PropTypes.string.isRequired,
};

export default ColorsButtons;
