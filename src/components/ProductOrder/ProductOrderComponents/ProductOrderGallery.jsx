import React, { useState, useEffect } from 'react';
import { Box, useMediaQuery } from '@mui/material';
import PropTypes from 'prop-types';
import theme from '../../../themeStyle';

const ProductOrderGallery = ({ images, mainImage }) => {
  const [selectedImage, setSelectedImage] = useState(mainImage || images[0]);
  const [currentIndex, setCurrentIndex] = useState(0);
  const [swipeStart, setSwipeStart] = useState(0);
  const isMobile = useMediaQuery(theme.breakpoints.down('desktop'));

  useEffect(() => {
    if (images.length > 0) {
      setSelectedImage(mainImage);
    }
  }, [mainImage]);

  const handleSwipeStart = (e) => {
    setSwipeStart(e.touches[0].clientX);
  };

  const handleSwipeEnd = (e) => {
    const swipeEnd = e.changedTouches[0].clientX;
    if (swipeStart - swipeEnd > 50) {
      if (currentIndex < images.length - 1) {
        setCurrentIndex(currentIndex + 1);
      }
    } else if (swipeEnd - swipeStart > 50) {
      if (currentIndex > 0) {
        setCurrentIndex(currentIndex - 1);
      }
    }
  };

  useEffect(() => {
    if (mainImage) {
      setSelectedImage(mainImage);
    } else setSelectedImage(images[currentIndex]);
  }, [currentIndex, images, mainImage]);

  return (
    <Box
      sx={{
        display: 'flex',
        maxHeight: '512px',
        mt: 2,
      }}
    >
      <Box
        sx={{
          display: 'flex',
          maxWidth: '160px',
          flexDirection: 'column',
          gap: '13px',
          mr: 3,
          overflowY: 'auto',
          alignItems: 'center',
          '&::-webkit-scrollbar': {
            width: '10px',
          },
          '&::-webkit-scrollbar-thumb': {
            backgroundColor: '#888',
          },
          '&::-webkit-scrollbar-thumb:hover': {
            backgroundColor: '#555',
          },
        }}
      >
        {images.map((image, index) => (
          <Box
            key={image}
            sx={{
              cursor: 'pointer',
              width: '127px',
              mr: 2,
              opacity: currentIndex === index ? 1 : 0.5,
              transition: 'opacity 0.3s',
              '&:hover': {
                opacity: 1,
              },
            }}
            onClick={() => setCurrentIndex(index)}
          >
            <Box
              component="img"
              src={image}
              alt={`Thumbnail ${index}`}
              sx={{
                width: '100%',
                height: '170px',
                objectFit: 'cover',
              }}
            />
          </Box>
        ))}
      </Box>

      <Box
        sx={{
          flexGrow: 1,
          flexDirection: isMobile ? 'column' : 'row',
          position: 'relative',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        }}
        onTouchStart={isMobile ? handleSwipeStart : undefined}
        onTouchEnd={isMobile ? handleSwipeEnd : undefined}
      >
        <Box
          component="img"
          src={selectedImage}
          alt="Selected"
          sx={{
            maxWidth: '400px',
            height: '512px',
            objectFit: 'cover',
          }}
        />

        {isMobile && (
          <Box
            sx={{
              width: '100%',
              display: 'flex',
              mt: 2,
              mb: 2,
              justifyContent: 'center',
              gap: '5px',
              position: 'relative',
              bottom: '0',
              alignItems: 'center',
            }}
          >
            {images.map((image, index) => (
              <Box
                key={image}
                sx={{
                  width: currentIndex === index ? '15px' : '10px',
                  height: currentIndex === index ? '15px' : '10px',
                  borderRadius: '50%',
                  backgroundColor: currentIndex === index ? '#adadad' : '#868686',
                  transition: 'background-color 0.3s',
                }}
              />
            ))}
          </Box>
        )}
      </Box>
    </Box>
  );
};

ProductOrderGallery.propTypes = {
  images: PropTypes.arrayOf(PropTypes.string).isRequired,
  mainImage: PropTypes.string.isRequired,
};

export default ProductOrderGallery;
