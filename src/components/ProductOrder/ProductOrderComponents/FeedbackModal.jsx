import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import axios from 'axios';
import {
  Box,
  Button,
  TextField,
  Typography,
  Snackbar,
  Alert,
  Rating,
} from '@mui/material';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import {
  Formik, Form, Field, ErrorMessage,
} from 'formik';
import * as Yup from 'yup';
import CustomModal from './CustomModal';
import fetchUserData from '../../../api/getUserData.js';
import BASE_URL from '../../../constants/constants.js';

const FeedbackModal = ({
  handleCloseFeedbackModal, isFeedbackModalOpen, productObject, getFeedbacks,
}) => {
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');
  const [snackbarSeverity, setSnackbarSeverity] = useState('success');
  const { id, name } = productObject;
  const isLogged = useSelector((state) => state.login.isLogged);
  const token = useSelector((state) => state.login.token);
  const navigate = useNavigate();
  const [user, setUser] = useState({});

  useEffect(() => {
    const fetchUser = async () => {
      try {
        const currentUser = await fetchUserData(token);
        setUser(currentUser);
      } catch (error) {
        // setSnackbarMessage('Error fetching user data');
        // setSnackbarSeverity('error');
        // setSnackbarOpen(true);
      }
    };
    fetchUser();
  }, [token]);

  const postComment = async (feedbackData) => {
    try {
      const response = await axios.post(`${BASE_URL}/comments`, feedbackData, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      });
      setSnackbarMessage('Thank you for your feedback!');
      setSnackbarSeverity('success');
      setSnackbarOpen(true);
      console.log('Feedback submitted successfully:', response.data);
    } catch (error) {
      setSnackbarMessage('Error submitting feedback');
      setSnackbarSeverity('error');
      setSnackbarOpen(true);
      console.error('Error submitting feedback:', error);
    } finally {
      handleCloseFeedbackModal();
      getFeedbacks();
    }
  };

  const handleLoginRedirect = () => {
    navigate('/login');
  };

  const handleCloseSnackbar = () => {
    setSnackbarOpen(false);
  };

  const validationSchema = Yup.object().shape({
    feedback: Yup.string()
      .required('Describe your experience with Modimal'),
    rate: Yup.number()
      .required('Rating is required')
      .min(1, `Please rate ${name}`),
  });

  return (
    <>
      <CustomModal
        open={isFeedbackModalOpen}
        handleClose={handleCloseFeedbackModal}
        title="Your feedback"
        content={(
          <Box sx={{
            height: '100%', width: '100%', display: 'flex', flexDirection: 'column',
          }}
          >
            {isLogged ? (
              <Formik
                initialValues={{ feedback: '', rate: 0 }}
                validationSchema={validationSchema}
                onSubmit={(values, { resetForm }) => {
                  const feedbackData = {
                    userId: user._id,
                    productId: id,
                    rate: values.rate,
                    content: values.feedback,
                    customerName: user.firstName,
                    customerSurname: user.lastName,
                  };

                  postComment(feedbackData);
                  resetForm();
                }}
              >
                {({ setFieldValue, values, isSubmitting }) => (
                  <Form style={{
                    display: 'flex',
                    flexDirection: 'column',
                    height: '100%',
                    width: '100%',
                  }}
                  >
                    <Box sx={{
                      display: 'flex', flexDirection: 'column', gap: 2, flexGrow: 1,
                    }}
                    >
                      <Field name="rate">
                        {({ field }) => (
                          <>
                            <Rating
                              // eslint-disable-next-line react/jsx-props-no-spreading
                              {...field}
                              name="rate"
                              value={values.rate}
                              onChange={(event, newValue) => {
                                setFieldValue('rate', newValue);
                              }}
                            />
                            <ErrorMessage name="rate">
                              {(msg) => (
                                <Typography variant="body2" color="error">
                                  {msg}
                                </Typography>
                              )}
                            </ErrorMessage>
                          </>
                        )}
                      </Field>

                      <Field name="feedback">
                        {({ field }) => (
                          <>
                            <TextField
                              // eslint-disable-next-line react/jsx-props-no-spreading
                              {...field}
                              label="Leave Your Feedback"
                              multiline
                              rows={6}
                              variant="outlined"
                              fullWidth
                              sx={{
                                flexGrow: 1,
                              }}
                            />
                            <ErrorMessage name="feedback">
                              {(msg) => (
                                <Typography variant="body2" color="error">
                                  {msg}
                                </Typography>
                              )}
                            </ErrorMessage>
                          </>
                        )}
                      </Field>
                    </Box>
                    <Button
                      type="submit"
                      variant="contained"
                      color="primary"
                      sx={{
                        mt: 3, maxWidth: 300, width: '100%', alignSelf: 'center',
                      }}
                      disabled={isSubmitting}
                    >
                      Send
                    </Button>
                  </Form>
                )}
              </Formik>
            ) : (
              <Box
                sx={{
                  textAlign: 'center',
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                  flexGrow: 1,
                }}
              >
                <Typography variant="body1" sx={{ my: 3 }}>
                  Please log in to leave feedback.
                </Typography>
                <Button
                  onClick={handleLoginRedirect}
                  variant="contained"
                  color="primary"
                >
                  Log In
                </Button>
              </Box>
            )}
          </Box>
        )}
      />
      <Snackbar
        open={snackbarOpen}
        autoHideDuration={4000}
        onClose={handleCloseSnackbar}
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
      >
        <Alert onClose={handleCloseSnackbar} severity={snackbarSeverity} sx={{ width: '100%' }}>
          {snackbarMessage}
        </Alert>
      </Snackbar>
    </>
  );
};

FeedbackModal.propTypes = {
  handleCloseFeedbackModal: PropTypes.func.isRequired,
  getFeedbacks: PropTypes.func.isRequired,
  isFeedbackModalOpen: PropTypes.bool.isRequired,
  productObject: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  }).isRequired,
};

export default FeedbackModal;
