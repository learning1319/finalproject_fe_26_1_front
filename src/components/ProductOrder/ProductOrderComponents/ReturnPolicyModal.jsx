import React from 'react';
import PropTypes from 'prop-types';
import { Typography, Box, Button } from '@mui/material';
import { Link as RouterLink } from 'react-router-dom';
import CustomModal from './CustomModal';

const ReturnPolicyModal = ({ open, handleClose }) => (
  <CustomModal
    open={open}
    handleClose={handleClose}
    title="EASY RETURN"
    content={(
      <Box>
        <Typography>
          We offer a hassle-free return policy for all our
          products. You can return any item
          within 30 days of purchase, provided it is unused,
          in its original packaging, and with
          all tags attached.
          Refunds will be processed within 7-10 business days after the item is received.
        </Typography>
        <Button
          component={RouterLink}
          to="/returnsRefunds"
          sx={{
            mt: 3,
            display: 'flex',
            justifyContent: 'center',
          }}
          variant="contained"
          color="primary"
          onClick={handleClose}
        >
          See more information
        </Button>
      </Box>
    )}
  />
);

ReturnPolicyModal.propTypes = {
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
};

export default ReturnPolicyModal;
