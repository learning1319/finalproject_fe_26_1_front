import { Box, Typography } from '@mui/material';
import PropTypes from 'prop-types';
import CustomModal from './CustomModal';

const SizeGuideModal = ({ open, handleClose }) => (
  <CustomModal
    open={open}
    handleClose={handleClose}
    title="Discover your size"
    content={(
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'flex-start',
          justifyContent: 'center',
          textAlign: 'justify',
        }}
      >
        <Box sx={{ mt: 2 }}>
          <Typography
            sx={{
              mb: 1,
              fontWeight: '700',
              mt: 2,
              textAlign: 'justify',
              width: '100%',
            }}
          >
            S (Small)
          </Typography>
          <Box>
            <Typography>- Bust: 32-34 inches (81-86 cm)</Typography>
            <Typography>- Waist: 24-26 inches (61-66 cm)</Typography>
            <Typography>- Hips: 34-36 inches (86-91 cm)</Typography>
          </Box>
        </Box>

        <Box sx={{ mt: 2 }}>
          <Typography
            sx={{
              mb: 1,
              fontWeight: '700',
            }}
          >
            M (Medium)
          </Typography>
          <Box>
            <Typography>- Bust: 36-38 inches (91-97 cm)</Typography>
            <Typography>- Waist: 28-30 inches (71-76 cm)</Typography>
            <Typography>- Hips: 38-40 inches (97-102 cm)</Typography>
          </Box>
        </Box>

        <Box sx={{ mt: 2 }}>
          <Typography
            sx={{
              mb: 1,
              fontWeight: '700',
            }}
          >
            L (Large)
          </Typography>
          <Box>
            <Typography>- Bust: 40-42 inches (102-107 cm)</Typography>
            <Typography>- Waist: 32-34 inches (81-86 cm)</Typography>
            <Typography>- Hips: 42-44 inches (107-112 cm)</Typography>
          </Box>
        </Box>

        <Box sx={{ mt: 2 }}>
          <Typography
            sx={{
              mb: 1,
              fontWeight: '700',
            }}
          >
            1X
          </Typography>
          <Box>
            <Typography>- Bust: 44-46 inches (112-117 cm)</Typography>
            <Typography>- Waist: 38-40 inches (97-102 cm)</Typography>
            <Typography>- Hips: 46-48 inches (117-122 cm)</Typography>
          </Box>
        </Box>

        <Box sx={{ mt: 2 }}>
          <Typography
            sx={{
              mb: 1,
              fontWeight: '700',
            }}
          >
            2X
          </Typography>
          <Box>
            <Typography>- Bust: 48-50 inches (122-127 cm)</Typography>
            <Typography>- Waist: 42-44 inches (107-112 cm)</Typography>
            <Typography>- Hips: 50-52 inches (127-132 cm)</Typography>
          </Box>
        </Box>

        <Box sx={{ mt: 2 }}>
          <Typography
            sx={{
              mb: 1,
              fontWeight: '700',
            }}
          >
            3X
          </Typography>
          <Box>
            <Typography>- Bust: 52-54 inches (132-137 cm)</Typography>
            <Typography>- Waist: 46-48 inches (117-122 cm)</Typography>
            <Typography>- Hips: 54-56 inches (137-142 cm)</Typography>
          </Box>
        </Box>
      </Box>
            )}
  />
);

SizeGuideModal.propTypes = {
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
};

export default SizeGuideModal;
