import PropTypes from 'prop-types';
import {
  Box,
  Button,
} from '@mui/material';
import theme from '../../../themeStyle';

const FeedbackButton = ({ handleOpenFeedbackModal, feedback = [], scrollToFeedback }) => (
  <Box
    sx={{
      display: 'flex',
      alignItems: {
        middle: 'center',
      },
      justifyContent: {
        middle: 'space-between',
      },
      flexDirection: {
        mobile: 'column',
        middle: 'row',
      },
      mt: 2,
      gap: 3,
    }}
  >
    <Button
      onClick={handleOpenFeedbackModal}
      sx={{
        width: '100%',
        borderRadius: 1,
        border: 'none',
        backgroundColor: theme.palette.primary.dark,
        color: theme.palette.secondary.white,
        '&:hover': {
          backgroundColor: theme.palette.primary.main,
        },
      }}
      variant="outlined"
    >
      Leave your feedback
    </Button>
    { feedback.length > 0 && (
    <Button
      onClick={scrollToFeedback}
      sx={{
        width: '100%',
        borderRadius: 1,
        border: 'none',
        backgroundColor: theme.palette.primary.dark,
        color: theme.palette.secondary.white,
        '&:hover': {
          backgroundColor: theme.palette.primary.main,
        },
      }}
      variant="outlined"
    >
      See All Feedbacks
    </Button>
    )}
  </Box>
);

FeedbackButton.propTypes = {
  handleOpenFeedbackModal: PropTypes.func.isRequired,
  scrollToFeedback: PropTypes.func.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  feedback: PropTypes.array,
};

export default FeedbackButton;
