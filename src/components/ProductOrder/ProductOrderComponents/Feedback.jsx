import { forwardRef } from 'react';
import {
  Card,
  CardContent,
  Typography,
  Avatar,
  Grid,
  Rating,
  Box,
} from '@mui/material';
import PropTypes from 'prop-types';
import theme from '../../../themeStyle';

const Feedback = forwardRef(({ feedback }, ref) => (
  <Box ref={ref} sx={{ my: 5, mx: 0 }}>
    <Typography
      sx={{
        fontWeight: 'bold',
        fontSize: {
          mobile: '20px',
          tablet: '24px',
          desktop: '32px',
        },
        mb: 2,
      }}
    >
      {feedback.lenght > 1 ? 'Customer Feedbacks' : 'Customer Feedback'}
    </Typography>
    <Box
      sx={{
        maxHeight: { small: 500, tablet: 300 },
        overflowY: feedback.length >= 3 ? 'scroll' : 'visible',
        '&::-webkit-scrollbar': {
          width: 2,
        },
        '&::-webkit-scrollbar-track': {
          backgroundColor: theme.palette.secondary.gray,
        },
        '&::-webkit-scrollbar-thumb': {
          backgroundColor: theme.palette.primary.main,
          borderRadius: '4px',
        },
      }}
    >
      {feedback.map((item, index) => (
        <Card
              // eslint-disable-next-line react/no-array-index-key
          key={index}
          sx={{
            my: 2,
            padding: '16px',
            border: `1px solid ${theme.palette.primary.light}`,
            borderRadius: 1,
            width: 'calc(100% - 32px)',
            marginLeft: 'auto',
            marginRight: 'auto',
            boxSizing: 'border-box',
          }}
        >
          <Grid container spacing={2} alignItems="center">
            <Grid item>
              <Avatar sx={{ bgcolor: theme.palette.primary.main }}>
                {item.customerName.charAt(0)}
                {item.customerSurname.charAt(0)}
              </Avatar>
            </Grid>
            <Grid item xs>
              <Typography
                sx={{
                  fontSize: {
                    mobile: '16px',
                    tablet: '20px',
                    desktop: '20px',
                  },
                }}
              >
                {item.customerName}
                {' '}
                {item.customerSurname}
              </Typography>
              <Rating value={item.rate} readOnly />
            </Grid>
          </Grid>
          <CardContent>
            <Typography variant="body2" color="textSecondary" sx={{ textAlign: 'justify' }}>
              {item.content}
            </Typography>
          </CardContent>
        </Card>
      ))}
    </Box>
  </Box>
));

Feedback.propTypes = {
  feedback: PropTypes.arrayOf(
    PropTypes.shape({
      userId: PropTypes.string.isRequired,
      productId: PropTypes.string.isRequired,
      rate: PropTypes.number.isRequired,
      content: PropTypes.string.isRequired,
      customerName: PropTypes.string.isRequired,
      customerSurname: PropTypes.string.isRequired,
      __v: PropTypes.number,
    }),
  ).isRequired,
};

export default Feedback;
