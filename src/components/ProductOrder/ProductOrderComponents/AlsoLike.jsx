import { useState, useEffect } from 'react';
import {
  Box,
  Typography,
  useMediaQuery,
} from '@mui/material';
import theme from '../../../themeStyle';
import ProductCard from '../../ProductCard/ProductCard';
import getYouMayAlsoLikeProducts from '../../../api/getYouMayAlsoLikeProducts.js';

const AlsoLike = () => {
  const [products, setProducts] = useState([]);
  const isMobile = useMediaQuery(theme.breakpoints.down('tablet'));
  const displayedProducts = (products || []).slice(0, isMobile ? 2 : 3);

  useEffect(() => {
    const fetchProducts = async () => {
      try {
        const data = await getYouMayAlsoLikeProducts();
        console.log(data);
        setProducts(data);
      } catch (error) {
        console.error('Fetch error:', error);
      }
    };

    fetchProducts();
  }, []);

  return (
    <Box sx={{ flexGrow: 1, my: 4 }}>
      <Typography
        sx={{
          fontWeight: 'bold',
          fontSize: {
            mobile: '20px',
            tablet: '24px',
            desktop: '32px',
          },
          mb: 2,
        }}
      >
        You May Also Like
      </Typography>
      <Box
        sx={{
          display: 'flex',
          gap: 3,
          justifyContent: 'space-between',
        }}
      >
        {displayedProducts.map((product) => (
          <Box
            key={product.id}
            sx={{
              flex: `1 1 ${isMobile ? 'calc(50% - 1.5rem)' : 'calc(33.333% - 2rem)'}`,
              maxWidth: isMobile ? 'calc(50% - 1.5rem)' : 'calc(33.333% - 2rem)',
              boxSizing: 'border-box',
            }}
          >
            <ProductCard product={product} />
          </Box>
        ))}
      </Box>
    </Box>
  );
};

export default AlsoLike;
