import {
  Select,
  MenuItem,
  FormControl,
} from '@mui/material';
import PropTypes from 'prop-types';

const SizeSelector = ({ sizes, selectedSize, onSizeChange }) => {
  const handleChange = (event) => {
    const newSize = event.target.value;
    onSizeChange(newSize);
  };

  return (
    <FormControl fullWidth>
      <Select
        labelId="size-selector-label"
        id="size-selector"
        value={selectedSize}
        onChange={handleChange}
        displayEmpty
        sx={{
          fontWeight: 'bold',
          fontSize: '16px',
          position: 'relative',
          '& .MuiSelect-select': {
            display: 'flex',
            alignItems: 'center',
          },
        }}
        MenuProps={{
          PaperProps: {
            sx: {
              minWidth: '160px',
              maxWidth: '300px',
              position: 'absolute',
              top: '100%',
              left: 0,
              transform: 'translateY(8px)',
            },
          },
        }}
      >
        <MenuItem
          value=""
          disabled
        >
          Size
        </MenuItem>
        {sizes.map((sizeObj) => (
          <MenuItem
            key={sizeObj.size}
            value={sizeObj.size}
            sx={{
              fontWeight: selectedSize === sizeObj.size ? 'bold' : 'normal',
              fontSize: '16px',
              position: 'relative',
            }}
          >
            {sizeObj.size}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

SizeSelector.propTypes = {
  sizes: PropTypes.arrayOf(
    PropTypes.shape({
      size: PropTypes.string.isRequired,
      quantity: PropTypes.number.isRequired,
      inCart: PropTypes.bool.isRequired,
    }),
  ).isRequired,
  selectedSize: PropTypes.string.isRequired,
  onSizeChange: PropTypes.func.isRequired,
};

export default SizeSelector;
