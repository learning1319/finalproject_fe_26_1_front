import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { useTheme, Box, Typography } from '@mui/material';
import FavoriteIcon from '../../../assets/svg/FavoriteIcon/FavoriteIcon';
import {
  addFavorite,
  removeFavorite,
  saveFavorite,
  deleteFavorite,
} from '../../../redux/slices/favoriteSlice.js';

const FavoriteButton = ({ product, showText = false, position = true }) => {
  const [isFavorite, setIsFavorite] = useState(false);
  const theme = useTheme();
  const dispatch = useDispatch();
  const { isLogged, user, token } = useSelector((state) => state.login);
  const favorites = useSelector((state) => state.favorites.items);

  useEffect(() => {
    if (favorites) {
      setIsFavorite(favorites.some((fav) => fav && fav.id === product.id));
    }
  }, [favorites, product.id]);

  const handleFavoriteAction = (action) => {
    if (isLogged && user) {
      const { _id } = user;
      console.log(_id);
      console.log(token);
      dispatch(action === 'add' ? saveFavorite(product, _id, token) : deleteFavorite(product.id, _id, token));
    } else {
      dispatch(action === 'add' ? addFavorite(product) : removeFavorite(product.id));
    }
  };

  const toggleFavorite = () => {
    handleFavoriteAction(isFavorite ? 'remove' : 'add');
    setIsFavorite(!isFavorite);
  };

  return (
    <Box
      className="favorite-icon-container"
      sx={{
        display: 'flex',
        alignItems: 'center',
        cursor: 'pointer',
        position: !position ? 'absolute' : 'static',
        top: {
          desktop: '1.5rem',
          tablet: '1rem',
          small: '0.5rem',
        },
        right: {
          desktop: '1.5rem',
          tablet: '1rem',
          small: '0.5rem',
        },
      }}
      onClick={toggleFavorite}
      onKeyDown={(e) => {
        if (e.key === 'Enter') {
          toggleFavorite();
        }
      }}
      role="button"
      tabIndex={0}
    >
      <FavoriteIcon fill={isFavorite ? theme.palette.error.main : '#FFF'} stroke={isFavorite ? theme.palette.error.main : '#0C0C0C'} />
      {showText && (
        <Typography
          sx={{
            marginLeft: '2px',
            fontSize: {
              small: '10px',
              mobile: '13px',
              middle: '16px',
            },
          }}
        >
          {isFavorite ? 'Added To Wish List' : 'Add To Wish List'}
        </Typography>
      )}
    </Box>
  );
};

FavoriteButton.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.string.isRequired,
  }).isRequired,
  showText: PropTypes.bool,
  position: PropTypes.bool,
};

export default FavoriteButton;
