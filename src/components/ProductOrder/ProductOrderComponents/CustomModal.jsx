import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  Modal,
  Box,
  Typography,
  IconButton,
} from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';

const titleStyles = {
  fontSize: {
    small: '14px',
    mobile: '20px',
  },
  fontWeight: '700',
  textAlign: 'justify',
};
const contentStyles = {
  fontSize: {
    small: '12px',
    mobile: '14px',
    tablet: '16px',
    large: '18px',
  },
  textAlign: 'justify',
  mt: 2,
  width: '100%',
};

const CustomModal = ({
  open,
  handleClose,
  title,
  content,

}) => {
  useEffect(() => {
    if (open) {
      document.body.style.overflow = 'hidden';
    } else {
      document.body.style.overflow = '';
    }
    return () => {
      document.body.style.overflow = '';
    };
  }, [open]);

  return (
    <Modal
      open={open}
      onClose={handleClose}
      sx={{
        backdropFilter: 'blur(5px)',
        '& .MuiBackdrop-root': {
          backdropFilter: 'blur(5px)',
        },
      }}
    >
      <Box
        sx={{
          position: 'absolute',
          top: '50%',
          left: '50%',
          transform: 'translate(-50%, -50%)',
          width: {
            small: '90%',
            mobile: '82%',
            middle: '53%',
            tablet: '35%',
            large: '39%',
            desktop: '28%',
          },
          maxHeight: '33vh',
          overflow: 'auto',
          borderRadius: '8px',
          '&::-webkit-scrollbar': {
            width: '8px',
          },
          '&::-webkit-scrollbar-track': {
            backgroundColor: '#f1f1f1',
            borderRadius: '8px',
          },
          '&::-webkit-scrollbar-thumb': {
            backgroundColor: '#888',
            borderRadius: '8px',
          },
          '&::-webkit-scrollbar-thumb:hover': {
            backgroundColor: '#555',
          },
          bgcolor: 'background.paper',
          boxShadow: 'none',
          p: 4,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          outline: 'none',
          '&:focus': {
            outline: 'none',
          },
        }}
      >
        <IconButton
          onClick={handleClose}
          sx={{
            position: 'absolute',
            top: '10px',
            right: '10px',
          }}
        >
          <CloseIcon />
        </IconButton>
        <Typography sx={titleStyles}>{title}</Typography>
        <Box sx={contentStyles}>{content}</Box>
      </Box>
    </Modal>
  );
};

CustomModal.propTypes = {
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.node.isRequired,
  content: PropTypes.node.isRequired,
};

export default CustomModal;
