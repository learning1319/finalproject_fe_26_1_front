import { useState, useEffect, useRef } from 'react';

import {
  Box,
  Typography,
  useTheme,
  CircularProgress,
} from '@mui/material';
import { useNavigate, useLocation, useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import ProductOrderGallery from './ProductOrderComponents/ProductOrderGallery';
import ColorsButtons from './ProductOrderComponents/ColorsButtons';
import SizeSelector from './ProductOrderComponents/SizeSelector';
import Shiping from '../../assets/svg/ProductOrder/Shipig';
import AddToCartButton from './ProductOrderComponents/AddToCartButton';
import AlsoLike from './ProductOrderComponents/AlsoLike';
import FavoriteButton from './ProductOrderComponents/FavoriteButton';
import getProductById from '../../api/getProductById.js';
import Leaf from '../../assets/svg/Leaf/Leaf';
import CustomModal from './ProductOrderComponents/CustomModal';
import SizeGuideModal from './ProductOrderComponents/SizeGuideModal';
import ReturnPolicyModal from './ProductOrderComponents/ReturnPolicyModal';
import FeedbackButton from './ProductOrderComponents/FeedbackButton';
import FeedbackModal from './ProductOrderComponents/FeedbackModal';
import Feedback from './ProductOrderComponents/Feedback';
import fetchFeedback from '../../api/getFeedbacks.js';

const ProductOrder = () => {
  const [productObject, setProductObject] = useState(null);
  const [selectedColor, setSelectedColor] = useState('');
  const [selectedSizes, setSelectedSizes] = useState([]);
  const [selectedSize, setSelectedSize] = useState('');
  const [selectedImage, setSelectedImage] = useState('');
  const [allImages, setAllImages] = useState([]);
  const [isSizeModalOpen, setIsSizeModalOpen] = useState(false);
  const [isReturnModalOpen, setIsReturnModalOpen] = useState(false);
  const [isFeedbackModalOpen, setIsFeedbackModalOpen] = useState(false);
  const [feedback, setFeedback] = useState([]);
  const token = useSelector((state) => state.login.token);
  const feedbackRef = useRef(null);

  const navigate = useNavigate();
  const { id } = useParams();
  const location = useLocation();
  const theme = useTheme();
  const queryParams = new URLSearchParams(location.search);
  const currentColor = queryParams.get('color');
  const currentSize = queryParams.get('size');

  useEffect(() => {
    const fetchProduct = async () => {
      try {
        const productData = await getProductById(id);
        if (
          !productData
                    || !productData.data
                    || productData.data.length === 0
        ) {
          console.error(`Product with ID ${id} not found`);
          return;
        }

        const newProductObject = productData.data[0];
        setProductObject(newProductObject);

        const allProductImages = newProductObject.products.flatMap((item) => (Array
          .isArray(item.image) ? item.image : [item.image]));
        setAllImages(allProductImages);

        const initialProduct = newProductObject.products.find(
          (item) => item.color === (currentColor || newProductObject.products[0].color),
        );
        console.log(currentColor, initialProduct);
        setSelectedColor(currentColor || initialProduct.color);
        setSelectedSizes(
          initialProduct.sizes.filter((size) => size.quantity > 0),
        );
        setSelectedImage(
          Array.isArray(initialProduct.image)
            ? initialProduct.image[0]
            : initialProduct.image,
        );
        setSelectedSize(currentSize || '');

        const data = await fetchFeedback(token, id);
        setFeedback(data);
      } catch (error) {
        console.error('Error fetching product:', error);
      }
    };
    fetchProduct();
  }, [id]);

  const getFeedbacks = async () => {
    const data = await fetchFeedback(token, id);
    setFeedback(data);
  };

  useEffect(() => {
    getFeedbacks();
  }, [id]);

  useEffect(() => {
    if (productObject) {
      let updateNeeded = false;

      if (selectedColor && selectedColor !== currentColor) {
        queryParams.set('color', selectedColor);
        updateNeeded = true;
      }

      if (selectedSize && selectedSize !== currentSize) {
        queryParams.set('size', selectedSize);
        updateNeeded = true;
      }

      if (updateNeeded) {
        navigate(`${location.pathname}?${queryParams.toString()}`, {
          replace: true,
        });
      }
    }
  }, [
    navigate,
    location.pathname,
    location.search,
    productObject,
    selectedColor,
    selectedSize,
  ]);

  const handleColorChange = (color) => {
    if (color !== selectedColor) {
      setSelectedColor(color);
      const selectedProduct = productObject.products.find(
        (item) => item.color === color,
      );
      if (!selectedProduct) {
        console.error(`Product with color ${color} not found`);
        return;
      }
      const selectedHandlImage = Array.isArray(selectedProduct.image)
        ? selectedProduct.image[0]
        : selectedProduct.image;
      setSelectedImage(selectedHandlImage);
      setSelectedSizes(
        selectedProduct.sizes.filter((size) => size.quantity > 0),
      );
      setSelectedSize('');
    }
  };
  console.log(productObject);
  useEffect(() => {
    if (currentColor) {
      setSelectedColor(currentColor);
    }
  }, [currentColor]);

  const handleSizeChange = (size) => {
    if (size !== selectedSize) {
      setSelectedSize(size);
    }
  };

  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  }, [location.pathname]);

  const handleOpenSizeModal = () => {
    setIsSizeModalOpen(true);
  };

  const handleCloseSizeModal = () => {
    setIsSizeModalOpen(false);
  };

  const handleOpenReturnModal = () => {
    setIsReturnModalOpen(true);
  };

  const handleCloseReturnModal = () => {
    setIsReturnModalOpen(false);
  };

  const handleOpenFeedbackModal = () => {
    setIsFeedbackModalOpen(true);
  };

  const handleCloseFeedbackModal = () => {
    setIsFeedbackModalOpen(false);
  };
  if (!productObject) {
    return (
      <Box
        sx={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          minHeight: '50vh',
        }}
      >
        <CircularProgress />
      </Box>
    );
  }

  const colors = productObject.products.map((item) => item.color);

  const scrollToFeedback = () => {
    if (feedbackRef.current) {
      const rect = feedbackRef.current.getBoundingClientRect();
      const scrollTop = window.scrollY || document.documentElement.scrollTop;
      const offsetTop = rect.top + scrollTop;
      window.scrollTo({
        top: offsetTop - 130,
        behavior: 'smooth',
      });
    }
  };

  return (
    <Box
      sx={{
        display: 'flex',
        maxWidth: '1200px',
        margin: 'auto',
        boxSizing: 'border-box',
        flexDirection: 'column',
        gap: '10px',
        pl: {
          mobile: 2,
          desktop: 2,
        },
        pr: {
          mobile: 2,
          desktop: 0,
        },
      }}
    >
      <Box
        sx={{
          display: 'flex',
          flexDirection: {
            mobile: 'column',
            desktop: 'row',
          },
        }}
      >
        <Box
          sx={{
            flex: 1,
            [theme.breakpoints.up('mobile')]: { mr: 2 },
            marginRight: { tablet: '16px' },
          }}
        >
          <ProductOrderGallery images={allImages} mainImage={selectedImage} />
        </Box>
        <Box
          sx={{
            flex: 1,
            [theme.breakpoints.up('tablet')]: { mt: 2 },
          }}
        >
          <Typography
            sx={{
              fontWeight: 'bold',
              fontSize: {
                [theme.breakpoints.up('mobile')]: '20px',
                tablet: '24px',
                desktop: '32px',
              },
            }}
          >
            {productObject.name}
          </Typography>
          <Typography
            sx={{
              mt: 4,
              textAlign: 'justify',
              fontSize: {
                [theme.breakpoints.up('mobile')]: '14px',
                [theme.breakpoints.up('tablet')]: '16px',
                desktop: '18px',
              },
            }}
          >
            {productObject.description}
          </Typography>
          <FeedbackButton
            scrollToFeedback={scrollToFeedback}
            feedback={feedback}
            handleOpenFeedbackModal={handleOpenFeedbackModal}
          />
          <Typography
            sx={{
              mt: 4,
              mb: 3,
            }}
          >
            Colors
          </Typography>
          <ColorsButtons
            colors={colors}
            initialColor={currentColor}
            onColorChange={handleColorChange}
          />
          <Typography
            sx={{
              fontSize: '14px',
              color: '#868686',
              mt: 4,
              mb: 2,
              display: 'flex',
              justifyContent: 'flex-end',
              alignItems: 'baseline',
              cursor: 'pointer',
            }}
            onClick={handleOpenSizeModal}
          >
            Size Guide
            <Leaf currentColor="green" />
          </Typography>
          <SizeGuideModal
            open={isSizeModalOpen}
            handleClose={handleCloseSizeModal}
          />
          <SizeSelector
            sizes={selectedSizes}
            selectedSize={selectedSize}
            onSizeChange={handleSizeChange}
          />
          <AddToCartButton
            product={productObject}
            selectedColor={selectedColor}
            selectedSize={selectedSize}
            showText
            position
          />
          <Box
            sx={{
              fontSize: '14px',
              color: '#868686',
              mt: 4,
              display: 'flex',
              flexDirection: {
                [theme.breakpoints.up('mobile')]: 'column',
                [theme.breakpoints.up('tablet')]: 'row',
              },
              justifyContent: 'space-between',
              alignItems: {
                [theme.breakpoints.up('mobile')]: 'center',
                [theme.breakpoints.up('tablet')]: 'flex-start',
              },
            }}
          >
            <Box
              sx={{
                display: 'flex',
                alignItems: 'center',
                gap: '15px',
                order: { [theme.breakpoints.up('mobile')]: 2, [theme.breakpoints.up('tablet')]: 1 },
              }}
            >
              <Shiping />
              <Typography
                onClick={handleOpenReturnModal}
                sx={{
                  cursor: 'pointer',
                  fontSize: {
                    small: '10px',
                    mobile: '13px',
                    middle: '16px',
                  },
                }}
              >
                Easy Return
              </Typography>
              <ReturnPolicyModal
                open={isReturnModalOpen}
                handleClose={handleCloseReturnModal}
              />
            </Box>

            <Box
              sx={{
                display: 'flex',
                flexDirection: 'row',
                gap: '5px',
                order: { mobile: 1, [theme.breakpoints.up('tablet')]: 2 },
              }}
            >
              <FavoriteButton product={productObject} showText />
            </Box>
          </Box>
        </Box>
      </Box>
      {feedback.length > 0 && <Feedback ref={feedbackRef} feedback={feedback} />}
      <AlsoLike />
      <CustomModal
        open={isSizeModalOpen}
        handleClose={handleCloseSizeModal}
        title="Discover your size"
        content={(
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              justifyContent: 'center',
              textAlign: 'justify',
            }}
          >
            <Box sx={{ mt: 2 }}>
              <Typography
                sx={{
                  mb: 1,
                  fontWeight: '700',
                  mt: 2,
                  textAlign: 'justify',
                  width: '100%',
                }}
              >
                S (Small)
              </Typography>
              <Box>
                <Typography>- Bust: 32-34 inches (81-86 cm)</Typography>
                <Typography>- Waist: 24-26 inches (61-66 cm)</Typography>
                <Typography>- Hips: 34-36 inches (86-91 cm)</Typography>
              </Box>
            </Box>

            <Box sx={{ mt: 2 }}>
              <Typography
                sx={{
                  mb: 1,
                  fontWeight: '700',
                }}
              >
                M (Medium)
              </Typography>
              <Box>
                <Typography>- Bust: 36-38 inches (91-97 cm)</Typography>
                <Typography>- Waist: 28-30 inches (71-76 cm)</Typography>
                <Typography>- Hips: 38-40 inches (97-102 cm)</Typography>
              </Box>
            </Box>

            <Box sx={{ mt: 2 }}>
              <Typography
                sx={{
                  mb: 1,
                  fontWeight: '700',
                }}
              >
                L (Large)
              </Typography>
              <Box>
                <Typography>- Bust: 40-42 inches (102-107 cm)</Typography>
                <Typography>- Waist: 32-34 inches (81-86 cm)</Typography>
                <Typography>- Hips: 42-44 inches (107-112 cm)</Typography>
              </Box>
            </Box>

            <Box sx={{ mt: 2 }}>
              <Typography
                sx={{
                  mb: 1,
                  fontWeight: '700',
                }}
              >
                1X
              </Typography>
              <Box>
                <Typography>- Bust: 44-46 inches (112-117 cm)</Typography>
                <Typography>- Waist: 38-40 inches (97-102 cm)</Typography>
                <Typography>- Hips: 46-48 inches (117-122 cm)</Typography>
              </Box>
            </Box>

            <Box sx={{ mt: 2 }}>
              <Typography
                sx={{
                  mb: 1,
                  fontWeight: '700',
                }}
              >
                2X
              </Typography>
              <Box>
                <Typography>- Bust: 48-50 inches (122-127 cm)</Typography>
                <Typography>- Waist: 42-44 inches (107-112 cm)</Typography>
                <Typography>- Hips: 50-52 inches (127-132 cm)</Typography>
              </Box>
            </Box>

            <Box sx={{ mt: 2 }}>
              <Typography
                sx={{
                  mb: 1,
                  fontWeight: '700',
                }}
              >
                3X
              </Typography>
              <Box>
                <Typography>- Bust: 52-54 inches (132-137 cm)</Typography>
                <Typography>- Waist: 46-48 inches (117-122 cm)</Typography>
                <Typography>- Hips: 54-56 inches (137-142 cm)</Typography>
              </Box>
            </Box>
          </Box>
                )}
      />
      {/* <Typography
        onClick={handleOpenReturnModal}
        sx={{
          cursor: 'pointer',
          fontSize: {
            small: '10px',
            mobile: '13px',
            middle: '16px',
          },
        }}
      >
        Easy Return
      </Typography>
      <ReturnPolicyModal
        open={isReturnModalOpen}
        handleClose={handleCloseReturnModal}
      /> */}

      <FeedbackModal
        isFeedbackModalOpen={isFeedbackModalOpen}
        handleCloseFeedbackModal={handleCloseFeedbackModal}
        productObject={productObject}
        getFeedbacks={getFeedbacks}
      />
    </Box>
  );
};

export default ProductOrder;
