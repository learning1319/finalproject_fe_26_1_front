import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { styled } from '@mui/material/styles';
import theme from '../../themeStyle';

const StyledLink = styled(Link)(() => ({
  textDecoration: 'none',
  color: theme.palette.secondary.light,
  position: 'relative',
  display: 'inline-block',
  padding: '8px 0',
  fontSize: '16px',
  transition: 'color 0.3s ease',
  '&::after': {
    content: '""',
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: '0%',
    height: '2px',
    backgroundColor: theme.palette.secondary.light,
    transform: 'scaleX(0)',
    transformOrigin: 'bottom left',
    transition: 'transform 0.3s ease-in-out, width 0.3s ease-in-out',
  },
  '&:hover': {
    color: '#000',
  },
  '&:hover::after': {
    width: '100%',
    transform: 'scaleX(1)',
  },
  [theme.breakpoints.up('small')]: {
    '&:hover::after': {
      width: '100px',
    },
  },
  [theme.breakpoints.up('desktop')]: {
    '&:hover::after': {
      width: '100%',
    },
  },
}));

const CustomLink = ({ children, to, onClick }) => (
  <StyledLink
    to={to}
    onClick={onClick}
  >
    {children}
  </StyledLink>
);

CustomLink.propTypes = {
  to: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.shape({
      pathname: PropTypes.string,
      search: PropTypes.string,
      hash: PropTypes.string,
    }),
  ]).isRequired,
  children: PropTypes.node.isRequired,
  onClick: PropTypes.func,
};

export default CustomLink;
