/* eslint-disable */
import { describe, expect, test } from '@jest/globals';
import { render, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from '../../store';

import RegistrationAuthentication from './RegistrationAuthentication';

describe('RegistrationAuthentication', () => {
  test('registration component exists', () => {
    const { el } = render(<BrowserRouter>
      <Provider store={store}>
        <RegistrationAuthentication />
      </Provider>
                          </BrowserRouter>);
    const l = screen.getByText('Register Now');
    expect(l).toBeInTheDocument();
    expect(el).toMatchSnapshot();
  });
});
