import { describe, expect, test } from '@jest/globals';
import { render, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from '../../store.js';

import RegistrationAuthentication from './RegistrationAuthentication';

describe('RegistrationAuthentication', () => {
  test('registration component exists', () => {
    const { container } = render(
      <BrowserRouter>
        <Provider store={store}>
          <RegistrationAuthentication />
        </Provider>
      </BrowserRouter>,
    );
    const registerNowText = screen.getByText('Register Now');
    expect(registerNowText).toBeInTheDocument();
    expect(container).toMatchSnapshot();
  });
});
