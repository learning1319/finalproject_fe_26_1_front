import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, Link } from 'react-router-dom';
import { Typography, useMediaQuery } from '@mui/material';
import Authentication from '../Authentication/Authentication';
import ModalWindow from '../Modal/Modal';
import { openEmailWindow, closeEmailWindow } from '../../redux/slices/modalSlice.js';
import { addUser, authenticateUser, removeUser } from '../../redux/slices/authSlice.js';
import { addCurrentUser, removeCurrentUser } from '../../redux/slices/currentUserSlice.js';
import { loggedIn } from '../../redux/slices/loginSlice.js';
import registerCustomer from '../../api/registerCustomer.js';

function RegistrationAuthentication() {
  const [showPassword, setShowPassword] = useState(true);
  const [user, setUser] = useState({});
  const isOpen = useSelector((state) => state.modal.showEmailWindow);
  const name = useSelector((state) => state.currentUser.currentUser);
  const dispatch = useDispatch();
  const matches = useMediaQuery('(max-width: 500px)');
  const message = `We’ve Sent An Email To ${name} To Verify Your Email Address And Activate Your Account. The Link In The Email Will Expire In 24 Hours.`;
  const title = matches ? 'Verify Your Email' : 'Verify Your Email Address';
  const navigate = useNavigate();

  async function handleClose() {
    await dispatch(closeEmailWindow());
    await dispatch(removeUser(name));
    await dispatch(removeCurrentUser());
    await dispatch(authenticateUser(name));
    await dispatch(removeCurrentUser());
    await dispatch(closeEmailWindow());
    await dispatch(loggedIn(user));
    navigate('/');
  }

  const onRegisterSubmit = async (firstName, lastName, password, email) => {
    const payload = {
      firstName,
      lastName,
      email,
      password,
    };

    try {
      const registrationResult = await registerCustomer(payload);
      const { token } = registrationResult;

      dispatch(addUser({ email: payload.email, isAuthenticated: false }));
      dispatch(addCurrentUser(payload.email));
      dispatch(openEmailWindow());

      setUser({
        user: {
          _id: registrationResult.customer._id,
          loginOrEmail: email,
          password,
        },
        token: `Bearer ${token}`,
      });

      return registrationResult.success;
    } catch (err) {
      console.log(err);
      return false;
    }
  };

  return (
    <>
      <Authentication
        buttonText="Register Now"
        isLogin={false}
        onLoginSubmit={() => { console.log('It is not login'); }}
        setShowPassword={setShowPassword}
        showPassword={showPassword}
        title="Create Account"
        isRegister
        onRegisterSubmit={onRegisterSubmit}
      />

      <ModalWindow
        title={title}
        handleClose={handleClose}
        open={isOpen}
        main={(
          <Typography sx={{
            textAlign: 'center',
            width: {
              small: 255,
              mobile: 255,
              middle: 320,
              tablet: 420,
              large: 520,
              desktop: 620,
            },
            mt: 5,
            fontSize: {
              small: '11px',
              mobile: '11px',
              middle: '13px',
              tablet: '15px',
              large: '18px',
              desktop: '18px',
            },
          }}
          >
            {message}
          </Typography>
                  )}
        footer={(
          <Typography sx={{
            textAlign: 'center',
            width: {
              small: 255,
              mobile: 255,
              middle: 310,
              tablet: 410,
              large: 510,
              desktop: 610,
            },
            mt: 4,
            fontSize: {
              small: '11px',
              mobile: '11px',
              middle: '13px',
              tablet: '15px',
              large: '18px',
              desktop: '18px',
            },
          }}
          >
            <Link
              to=""
              style={{
                color: '#5A6D57',
                textDecoration: 'none',
              }}
              onClick={async () => {
                await dispatch(authenticateUser(name));
                await dispatch(removeCurrentUser());
                await dispatch(closeEmailWindow());
                await dispatch(loggedIn(user));
                await navigate('/userProfile');
              }}
            >
              Click Here
              {' '}
            </Link>
            If You Did Not Receive An Email Or Would Like To Change
            The Email Address You Registered With
          </Typography>
                  )}
      />
    </>
  );
}

export default RegistrationAuthentication;
