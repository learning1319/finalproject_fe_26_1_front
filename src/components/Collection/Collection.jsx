import {
  Container,
  Box,
  ImageList,
  ImageListItem,
  ImageListItemBar,
  Typography,
  useMediaQuery,
  Button,
} from '@mui/material';
import { ArrowForward } from '@mui/icons-material';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import theme from '../../themeStyle';
import { mobileBanners, tabletBanners, desktopBanners } from '../../assets/banners.js';
import { setCategory } from '../../redux/slices/filterSlice.js';

function Collection() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const isMobile = useMediaQuery(theme.breakpoints.down('middle'));
  const isTablet = useMediaQuery(theme.breakpoints.between('middle', 'large'));
  const isDesktop = useMediaQuery(theme.breakpoints.up('large'));

  const getTypographyVariantSeeAll = () => {
    if (isMobile) return 'mobile_bodyMD';
    if (isTablet) return 'desktop_bodyMD';
    if (isDesktop) return 'desktop_bodyLG';
    return 'body1';
  };

  const handleImageClick = (category) => {
    const modifiedCategory = category.charAt(0).toLowerCase() + category.slice(1);
    dispatch(setCategory(modifiedCategory));
    navigate(`/collection?category=${modifiedCategory}`);
  };

  const getBanners = () => {
    switch (true) {
      case isMobile:
        return mobileBanners;
      case isTablet:
        return tabletBanners;
      case isDesktop:
        return desktopBanners;
      default:
        return mobileBanners;
    }
  };

  const getObjectPosition = () => {
    if (isMobile) return 'top';
    if (isTablet) return '50% 20%';
    if (isDesktop) return 'center';
    return 'center';
  };

  const getCols = () => {
    if (isMobile) return 2;
    if (isTablet) return 3;
    if (isDesktop) return 3;
    return 2;
  };

  return (
    <Container sx={{ mb: 7 }}>
      <Box display="flex" justifyContent="space-between" alignItems="center" mb={4}>
        <Typography
          sx={{
            typography: {
              desktop: 'desktop_logoName',
              tablet: 'desktop_logoName',
              small: 'mobile_logoName',
            },
          }}
        >
          Collection
        </Typography>
        <Button
          variant="text"
          endIcon={<ArrowForward />}
          onClick={() => navigate('/collection')}
          sx={{
            textTransform: 'capitalize',
            color: theme.palette.secondary.dark,
          }}
        >
          <Typography variant={getTypographyVariantSeeAll()}>
            See All
          </Typography>
        </Button>
      </Box>
      <Box
        sx={{
          mt: 4,
          width: '100%',
          height: 'fit-content',
          overflow: 'unset',
          '&::-webkit-scrollbar': {
            width: '4px',
          },
          '&::-webkit-scrollbar-track': {
            background: 'transparent',
          },
          '&::-webkit-scrollbar-thumb': {
            background: theme.palette.primary.light,
            borderRadius: '4px',
          },
          '&::-webkit-scrollbar-thumb:hover': {
            background: theme.palette.primary.dark,
          },
        }}
      >
        <ImageList variant="standard" cols={getCols()} gap={16}>
          {getBanners().map((item) => (
            <ImageListItem
              key={item.id}
              size="small"
              onClick={() => handleImageClick(item.name)}
              sx={{
                height: item.height,
                overflow: 'hidden',
                cursor: 'pointer',
                position: 'relative',
                '&:hover img': {
                  transform: 'scale(1.1)',
                },
                '& img': {
                  transition: 'transform 0.5s ease-in-out',
                },
              }}
            >
              <img
                srcSet={`${item.imageUrl}?w=248&fit=crop&auto=format&dpr=2 2x`}
                src={`${item.imageUrl}?w=248&fit=crop&auto=format`}
                alt={item.name}
                loading="lazy"
                style={{
                  height: item.height,
                  objectFit: 'cover',
                  objectPosition: getObjectPosition(),
                  width: '100%',
                }}
              />
              {!isDesktop && (
                <ImageListItemBar
                  title={item.name}
                  position="below"
                  sx={{
                    typography: 'button_SM',
                    color: theme.palette.secondary.dark,
                    p: '0 16px',
                    textTransform: 'capitalize',
                    zIndex: 4,
                    backgroundColor: theme.palette.secondary.white,
                  }}
                />
              )}
              {isDesktop && (
                <Box
                  sx={{
                    position: 'absolute',
                    bottom: 20,
                    right: 40,
                    backgroundColor: 'white',
                    borderRadius: 0,
                    color: theme.palette.secondary.dark,
                    padding: '8px 40px',
                    typography: 'button_SM',
                    textTransform: 'capitalize',
                  }}
                >
                  {item.name}
                </Box>
              )}
            </ImageListItem>
          ))}
        </ImageList>
      </Box>
    </Container>
  );
}

export default Collection;
