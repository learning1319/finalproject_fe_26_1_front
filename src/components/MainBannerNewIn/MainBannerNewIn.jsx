import { useEffect, useState } from 'react';
import {
  Box,
  Button,
  Typography,
  Container,
  useMediaQuery,
  ImageList,
  ImageListItem,
  CardMedia,
} from '@mui/material';
import { useNavigate } from 'react-router-dom';
import { useTheme } from '@mui/material/styles';
import getBannersByCategory from '../../api/getBannersByCategory.js';

function MainBannerNewIn() {
  const navigate = useNavigate();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('tablet'));
  const [banners, setBanners] = useState([]);
  const [imagesLoaded, setImagesLoaded] = useState(false);
  const cols = isMobile ? 1 : banners.length;

  const handleButtonClick = () => {
    navigate('/newIn');
  };

  useEffect(() => {
    const fetchBanner = async () => {
      try {
        const data = await getBannersByCategory('mainBanner');
        setBanners(data.reverse());
      } catch (err) {
        console.log(err);
      } finally {
        setImagesLoaded(true);
      }
    };
    fetchBanner();
  }, []);

  return (
    <Container
      variant="fullWidth"
      sx={{
        position: 'relative',
        width: '100%',
        maxHeight: { small: '500px', tablet: '600px' },
        minHeight: { small: '356px', tablet: '500px', desktop: '555px' },
        overflow: 'hidden',
        marginTop: '30px',
      }}
    >
      {!imagesLoaded ? (
        <Box
          sx={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            height: { small: '500px', tablet: '600px' },
            minHeight: { small: '356px', tablet: '500px', desktop: '555px' },
            width: '100%',
            backgroundColor: theme.palette.primary.gray,
          }}
        />
      ) : (
        <ImageList
          sx={{
            width: '100%',
            display: isMobile ? 'block' : 'flex',
            overflow: 'hidden',
          }}
          cols={cols}
        >
          {banners.slice(0, isMobile ? 1 : banners.length).map((item) => (
            <ImageListItem
              key={item.id}
              sx={{
                flexBasis: isMobile ? '100%' : `${100 / cols}%`,
                height: '100%',
                alignItems: 'center',
                justifyContent: 'center',
                position: 'relative',
              }}
            >
              <CardMedia
                component="img"
                image={item.image}
                alt={item.name}
                sx={{
                  height: '100%',
                  width: '100%',
                  objectFit: 'cover',
                }}
              />
            </ImageListItem>
          ))}
        </ImageList>
      )}
      <Container
        sx={{
          display: 'flex',
          alignItems: 'flex-end',
          position: 'absolute',
          bottom: {
            desktop: '100px',
            large: '90px',
            tablet: '60px',
            middle: '40px',
            small: '20px',
          },
          left: 0,
          right: 0,
          justifyContent: 'flex-start',
          height: 'auto',
          zIndex: 1,
          boxSizing: 'border-box',
        }}
      >
        <Box
          sx={{
            color: theme.palette.secondary.dark,
            whiteSpace: 'normal',
            maxWidth: {
              desktop: '295px',
              tablet: '230px',
              small: '168px',
            },
            display: 'flex',
            flexDirection: 'column',
            gap: '30px',
          }}
        >
          <Typography
            sx={{
              typography: {
                desktop: 'desktop_banner',
                tablet: 'tablet_banner',
                small: 'mobile_banner',
              },
            }}
          >
            Elegance In Simplicity, Earth&apos;s Harmony
          </Typography>

          <Button
            variant="contained"
            onClick={handleButtonClick}
            sx={{
              typography: {
                desktop: 'desktop_bodyMD',
                tablet: 'mobile_bodyLG',
                small: 'mobile_bodyMD',
              },
              textTransform: 'capitalize',
              backgroundColor: theme.palette.secondary.white,
              color: theme.palette.secondary.dark,
              maxWidth: {
                desktop: '184px',
                large: '150px',
                tablet: '130px',
                middle: '110px',
                small: '90px',
              },
              '&:hover': {
                backgroundColor: theme.palette.primary.gray,
              },
            }}
          >
            New In
          </Button>
        </Box>
      </Container>
    </Container>
  );
}

export default MainBannerNewIn;
