/* eslint-disable */
import {
  FormControl,
  OutlinedInput,
  InputLabel,
  FormHelperText,
  InputAdornment,
  useMediaQuery,
} from '@mui/material';
import { useField } from 'formik';
import PropTypes from 'prop-types';
import theme from '../../../themeStyle';
import ShowHidePassword from '../InputAdornment/ShowHidePassword';

const Input = (props) => {
  const {
    label,
    type,
    adornment = false,
    showPassword = false,
    setShowPassword = () => {},
  } = props;

  const [field, meta] = useField(props);
  const isDesktop = useMediaQuery(theme.breakpoints.up('desktop'));

  const {
    name, value, onChange, onBlur,
  } = field;
  const { touched, error } = meta;

  return (
    <FormControl
      sx={{
        width: '100%',
        maxWidth: '500px',
        mb: { small: 1, middle: 2 },
        borderRadius: 0,
        backgroundColor: 'transparent',
        '& .MuiOutlinedInput-root': {
          borderRadius: 0,
          '& input': {
            fontSize: '12px',
            fontWeight: 400,
          },
        },
        '& fieldset': {
          borderColor: theme.palette.primary.main,
        },
      }}
      variant="outlined"
      size={!isDesktop ? 'small' : 'medium'}
      error={touched && Boolean(error)}
    >
      <InputLabel
        htmlFor={`outlined-adornment-${type}`}
        sx={{
          fontSize: { small: '12px', desktop: '16px' },
        }}
      >
        {label}
      </InputLabel>
      <OutlinedInput
        id={`outlined-adornment-${type}`}
        autoComplete="off"
        name={name}
        value={value}
        onChange={onChange}
        onBlur={onBlur}
        type={type}
        label={label}
        sx={{
          '&.Mui-focused .MuiOutlinedInput-notchedOutline': {
            borderWidth: '1px',
          },
        }}
        endAdornment={
          adornment && (
            <InputAdornment position="end">
              {(type === 'password' || type === 'text') && (
                <ShowHidePassword
                  showPassword={showPassword}
                  setShowPassword={setShowPassword}
                />
              )}
            </InputAdornment>
          )
        }
      />
      {touched && error && (
        <FormHelperText sx={{ fontSize: '9px' }}>{meta.error}</FormHelperText>
      )}
    </FormControl>
  );
};

Input.propTypes = {
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  adornment: PropTypes.bool,
  showPassword: PropTypes.bool,
  setShowPassword: PropTypes.func,
};

export default Input;
