/* eslint-disable */
import PropTypes from "prop-types";
import { IconButton } from "@mui/material";
import Visibility from "../../../assets/svg/Visibility/Visibility";
import VisibilityOff from "../../../assets/svg/VisibilityOff/VisibilityOff";

const ShowHidePassword = ({ showPassword, setShowPassword }) => {
  const handleClickShowPassword = () => setShowPassword((prev) => !prev);
  return (
    <IconButton
      aria-label="toggle password visibility"
      onClick={handleClickShowPassword}
      edge="end"
    >
      {!showPassword ? (
        <VisibilityOff />
      ) : (
        <Visibility />
      )}
    </IconButton>
  );
};

ShowHidePassword.propTypes = {
  showPassword: PropTypes.bool.isRequired,
  setShowPassword: PropTypes.func.isRequired,
};

export default ShowHidePassword;
