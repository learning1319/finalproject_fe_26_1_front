/* eslint-disable */
import React from 'react';
import { Box, Button, Typography, IconButton } from '@mui/material';
import { ReactComponent as CrossIcon } from '../../assets/svg/cross.svg';
import { NavLink } from 'react-router-dom';

const EmptyCart = ({ handleOpenCart }) => {
  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        pb: 15,
        pt: 1,
        px: 2.5,
        bgcolor: 'background.paper',
        position: 'relative',
        maxWidth: 392,
      }}
    >
      <IconButton
        onClick={handleOpenCart}
        sx={{
          position: 'absolute',
          top: 8,
          right: 8,
          '@media (max-width: 600px)': {
            left: 8,
            right: 'auto'
          }
        }}
      >
        <CrossIcon />
      </IconButton>

      <Typography sx={{ mt: 8, typography: "mobile_h3" }} component="p">
        Your Shopping Bag Is Empty
      </Typography>
      <Typography sx={{ mt: 3, typography: " mobile_bodySM", textAlign: "center" }} component="div">
        Discover Modimal
        <Typography sx={{ typography: " mobile_bodySM" }} component="p" >
          And Add Products To Your Bag
        </Typography>
      </Typography>


      <Box sx={{ mt: 10, display: "flex", alignItems: "center", flexDirection: "column", gap: 3, }}>
        <Button
          component={NavLink}
          to="collection"
          onClick={handleOpenCart}
          variant="contained"
          color="primary"
          sx={{ width: 184, bgcolor: 'primary.main', typography: "button_SM", textTransform: 'none' }}
        >
          Collection
        </Button>
        <Button
          component={NavLink}
          onClick={handleOpenCart}
          to="newIn"
          variant="contained"
          color="primary"
          sx={{ width: 184, bgcolor: 'primary.main', typography: "button_SM", textTransform: 'none' }}
        >
          New In
        </Button>
        <Button
          component={NavLink}
          onClick={handleOpenCart}
          to="plusSize"
          variant="contained"
          color="primary"
          sx={{ width: 184, bgcolor: 'primary.main', typography: "button_SM", textTransform: 'none' }}
        >
          Plus Size
        </Button>
      </Box>
    </Box>
  );
};

export default EmptyCart;