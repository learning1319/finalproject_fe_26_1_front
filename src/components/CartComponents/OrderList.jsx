/*eslint-disable*/
import React from 'react';
import {
  Box, Typography, Button, IconButton,
} from '@mui/material';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import CartItem from './CartItem';
import { ReactComponent as CrossIcon } from '../../assets/svg/cross.svg';

const OrderList = ({ cartItems, handleOpenCart }) => {
  return (
  <Box
    sx={{
      maxHeight: 550,
      overflowY: 'auto',
      minWidth: { small: '100%', tablet: 500 },
      '&::-webkit-scrollbar': {
        display: 'none',
      },
      '&': {
        scrollbarWidth: 'none',
        scrollbarColor: 'transparent transparent',
      },
    }}
  >
    <IconButton
      onClick={handleOpenCart}
      sx={{
        display: { middle: 'none' },
        position: 'absolute',
        top: 8,
        right: 8,
        '@media (max-width: 600px)': {
          left: 8,
          right: 'auto',
        },
      }}
    >
      <CrossIcon />
    </IconButton>
    <Typography variant="h6" sx={{ textAlign: 'center', typography: { small: 'mobile_h3', tablet: 'mobile_h2' }, py: 1 }}>
      Your Cart
    </Typography>
    <Box sx={{ display: 'flex', flexDirection: 'column' }} mt={2} px={2} pb={5}>
      {cartItems.map((item) => (
        <CartItem key={item.cartID} item={item} isMiniCart /> // Ensure onRemove is passed
      ))}
      <Button
        onClick={handleOpenCart}
        component={NavLink}
        to="cart"
        variant="contained"
        sx={{
          mt: 2, mb: 6, textTransform: 'none', padding: 1, width: { small: 185, tablet: 392 }, alignSelf: 'center',
        }}
      >
        Check Out
      </Button>
    </Box>

  </Box>
);}

OrderList.propTypes = {
  cartItems: PropTypes.arrayOf(
    PropTypes.shape({
      cartID: PropTypes.string.isRequired,
    }),
  ).isRequired,
  handleOpenCart: PropTypes.func.isRequired,
  handleRemoveItem: PropTypes.func.isRequired, // Add this to prop types
};

export default OrderList;
