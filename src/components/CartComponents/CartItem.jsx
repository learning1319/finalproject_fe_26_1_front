import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  Box,
  Avatar,
  IconButton,
  Typography,
  Button,
  useMediaQuery,
} from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { ReactComponent as CrossIcon } from '../../assets/svg/cross.svg';
import {
  updateCartItemQuantity,
  removeCartItemAsync,
} from '../../redux/slices/cartSlice.js';
import theme from '../../themeStyle';

// eslint-disable-next-line react/prop-types, no-unused-vars
const CartItem = ({ item, isInSiping, isMiniCart }) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [quantity, setQuantity] = useState(Number(item.quantity));
  const maxQuantity = Number(item.products[0].sizes[0].quantity);
  const [id, color, size] = item.cartID.split('/');
  const totalPrice = (item.price * quantity).toFixed(2);
  const isMobile = useMediaQuery(theme.breakpoints.down('middle'));

  const handleUpdateQuantity = (newQuantity) => {
    const updatedItemString = `${id}/${color}/${size}/${newQuantity}`;
    dispatch(updateCartItemQuantity({
      oldItemString: item.cartID,
      newItemString: updatedItemString,
    }));
  };

  const handleIncrement = () => {
    if (quantity < maxQuantity) {
      setQuantity((prevQuantity) => {
        const newQuantity = prevQuantity + 1;
        handleUpdateQuantity(newQuantity);
        return newQuantity;
      });
    }
  };

  const handleDecrement = () => {
    if (quantity > 1) {
      setQuantity((prevQuantity) => {
        const newQuantity = prevQuantity - 1;
        handleUpdateQuantity(newQuantity);
        return newQuantity;
      });
    }
  };

  const handleRemoveItem = () => {
    dispatch(removeCartItemAsync(item.cartID));
  };

  const handleBackToProduct = () => {
    navigate(`/${id}?color=${color}&size=${size}`);
  };

  return (
    <Box
      display="flex"
      sx={{
        mb: 2,
        p: 1,
        cursor: 'pointer',
        height: { small: 115, tablet: 163 },
        border: `1px solid ${theme.palette.primary.main}`,
        position: 'relative',
        '&:hover .back-to-product': {
          display: 'flex',
        },
      }}
    >
      <Avatar
        onClick={handleBackToProduct}
        variant="square"
        src={Array.isArray(item.products[0].image) ? item
          .products[0].image[0] : item.products[0].image}
        alt={item.name}
        sx={{ width: { small: 110, tablet: 142 }, height: '100%' }}
      />
      <Box width="100%" px={1}>
        <Box height="auto">
          <Box
            display="flex"
            justifyContent="space-between"
            alignItems="center"
          >
            <Typography
              sx={{
                typography: {
                  small: 'desktop_overlineSM',
                  tablet: 'mobile_h3',
                },
                maxWidth: { small: '50px', mobile: '75px', middle: 'unset' },
                whiteSpace: 'nowrap',
                overflow: 'hidden',
                textOverflow: 'ellipsis',
              }}
            >
              {item.name}
            </Typography>
            <Box sx={{
              display: 'flex',
            }}
            >
              {!isMiniCart && (
                <IconButton onClick={handleBackToProduct}>
                  <ArrowBackIcon />
                </IconButton>
              )}
              <IconButton onClick={handleRemoveItem}>
                <CrossIcon />
              </IconButton>
            </Box>
          </Box>

          <Typography
            sx={{
              pt: { small: 0, tablet: 1 },
              typography: {
                small: 'desktop_captionMD',
                tablet: 'desktop_bodyMD',
              },
            }}
            variant="body2"
          >
            Size:
            {` ${item.products[0].sizes[0].size}`}
          </Typography>
          <Typography
            sx={{
              pt: { small: 0, tablet: 1 },
              typography: {
                small: 'desktop_captionMD',
                tablet: 'desktop_bodyMD',
              },
            }}
            variant="body2"
          >
            Color:
            {` ${item.products[0].color}`}
          </Typography>

          <Box
            display="flex"
            justifyContent="space-between"
            alignItems="center"
            sx={{ pt: { small: 0, tablet: 1 } }}
          >
            { !isMobile && (
            <>
              <Box
                sx={{
                  display: 'flex',
                  alignItems: 'center',
                  backgroundColor: '#D3D8CF',
                  borderRadius: '4px',
                  width: '68px',
                  height: '25px',
                }}
              >
                <Button
                  onClick={handleDecrement}
                  sx={{
                    minWidth: '24px',
                    padding: 0,
                    color: '#404E3E',
                    height: '100%',
                    backgroundColor: '#D3D8CF',
                    borderRadius: 0,
                  }}
                >
                  <RemoveIcon sx={{ fontSize: 'small' }} />
                </Button>
                <Box
                  sx={{
                    flexGrow: 1,
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: '100%',
                    backgroundColor: '#D3D8CF',
                  }}
                >
                  <Typography
                    sx={{
                      fontSize: '14px',
                      color: '#404E3E',
                    }}
                  >
                    {quantity}
                  </Typography>
                </Box>
                <Button
                  onClick={handleIncrement}
                  sx={{
                    minWidth: '24px',
                    padding: 0,
                    color: '#404E3E',
                    height: '100%',
                    backgroundColor: '#D3D8CF',
                    borderRadius: 0,
                  }}
                  disabled={quantity >= maxQuantity}
                >
                  <AddIcon sx={{ fontSize: 'small' }} />
                </Button>
              </Box>
              <Typography sx={{ pt: 1, typography: 'mobile_h3' }} variant="h6">
                $
                {totalPrice}
              </Typography>
            </>
            )}
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

CartItem.propTypes = {
  item: PropTypes.shape({
    cartID: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    quantity: PropTypes.number.isRequired,
    products: PropTypes.arrayOf(
      PropTypes.shape({
        image: PropTypes.string.isRequired,
        color: PropTypes.string.isRequired,
        sizes: PropTypes.arrayOf(
          PropTypes.shape({
            size: PropTypes.string.isRequired,
            quantity: PropTypes.number.isRequired,
          }),
        ).isRequired,
      }),
    ).isRequired,
  }).isRequired,
};

export default CartItem;
