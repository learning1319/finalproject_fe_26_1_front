import PropTypes from 'prop-types';
import { Box } from '@mui/material';

function ColorCircle({ colorName }) {
  return (
    <Box
      component="span"
      sx={{
        width: 16,
        height: 16,
        borderRadius: '50%',
        backgroundColor: colorName,
        display: 'inline-block',
        marginRight: 1,
        boxShadow: colorName === 'white' ? '0px 0px 3px rgba(0, 0, 0, 0.3)' : 'none',
      }}
    />
  );
}

ColorCircle.propTypes = {
  colorName: PropTypes.string.isRequired,
};

export default ColorCircle;
