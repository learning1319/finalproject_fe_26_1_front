import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useNavigate } from 'react-router-dom';
import { Card, Dialog, DialogContent } from '@mui/material';
import ProductCardImage from './ProductCardParts/ProductCardImage';
import ProductCardContent from './ProductCardParts/ProductCardContent';

function ProductCard({ product }) {
  const navigate = useNavigate();
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(false);
  }, []);

  const handleCardClick = () => {
    navigate(`/${product.id}`);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Card
      sx={{
        maxWidth: 370,
        maxHeight: 540,
        display: 'flex',
        flexDirection: 'column',
        boxShadow: '0px 0px 10px rgba(0, 0, 0, 0.1)',
        position: 'relative',
        overflow: 'hidden',
        '&:hover .view-details-btn': {
          display: 'block',
        },
        '&:hover .zoom-icon': {
          display: 'flex',
        },

      }}
    >
      <ProductCardImage
        product={product}
        onCardClick={handleCardClick}
        onImageClick={handleOpen}
      />
      <ProductCardContent product={product} loading={loading} />

      <Dialog open={open} onClose={handleClose} maxWidth="md">
        <DialogContent
          sx={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <img
            src={product.image}
            alt={product.name}
            style={{
              maxWidth: '100%',
              maxHeight: '80vh',
              objectFit: 'contain',
            }}
          />
        </DialogContent>
      </Dialog>
    </Card>
  );
}

ProductCard.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    category: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    new: PropTypes.bool,
    products: PropTypes.arrayOf(
      PropTypes.shape({
        color: PropTypes.string.isRequired,
      }),
    ),
  }).isRequired,
};

export default ProductCard;
