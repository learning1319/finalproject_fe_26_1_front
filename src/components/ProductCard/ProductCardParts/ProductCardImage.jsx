import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  Box,
  CardMedia,
  IconButton,
  Skeleton,
  Button,
  useMediaQuery,
} from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import { Link } from 'react-router-dom';
import FavoriteButton from '../../ProductOrder/ProductOrderComponents/FavoriteButton';
import theme from '../../../themeStyle';

function ProductCardImage({ product, onCardClick, onImageClick }) {
  const [imageLoaded, setImageLoaded] = useState(false);
  const isMobile = useMediaQuery(theme.breakpoints.down('desktop'));

  return (
    <Box
      sx={{
        position: 'relative',
        backgroundColor: 'grey.200',
        height: {
          desktop: 428,
          large: 380,
          tablet: 330,
          middle: 280,
          small: 213,
        },
      }}
    >
      {!imageLoaded && (
        <Skeleton
          variant="rectangular"
          width="100%"
          height="100%"
          sx={{ backgroundColor: 'grey.300' }}
        />
      )}
      <CardMedia
        component="img"
        image={product.image}
        alt={product.name}
        onLoad={() => setImageLoaded(true)}
        sx={{
          display: imageLoaded ? 'block' : 'none',
          height: '100%',
        }}
      />
      {product.new && (
        <Box
          sx={{
            position: 'absolute',
            top: '1.5rem',
            left: '1.5rem',
            backgroundColor: 'white',
            color: 'black',
            width: '86px',
            height: '32px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          New
        </Box>
      )}
      <FavoriteButton product={product} showText={false} position={false} />
      <IconButton
        aria-label="zoom"
        className="zoom-icon"
        onClick={onImageClick}
        sx={{
          position: 'absolute',
          top: '50%',
          left: '50%',
          transform: 'translate(-50%, -50%)',
          fontSize: '2.5rem',
          display: 'none',
        }}
      >
        <SearchIcon fontSize="inherit" />
      </IconButton>
      <Link to={`/${product.id}`} style={{ textDecoration: 'none' }}>
        <Button
          className="view-details-btn"
          variant="contained"
          onClick={onCardClick}
          sx={{
            position: 'absolute',
            bottom: '1rem',
            left: '50%',
            transform: 'translateX(-50%)',
            width: '90%',
            display: isMobile ? 'block' : 'none',
          }}
        >
          View
        </Button>
      </Link>
    </Box>
  );
}

ProductCardImage.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    category: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    new: PropTypes.bool,
    products: PropTypes.arrayOf(
      PropTypes.shape({
        color: PropTypes.string.isRequired,
      }),
    ),
  }).isRequired,
  onCardClick: PropTypes.func.isRequired,
  onImageClick: PropTypes.func.isRequired,
};

export default ProductCardImage;
