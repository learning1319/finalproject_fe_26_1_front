import React from 'react';
import PropTypes from 'prop-types';
import {
  Box,
  CardContent,
  Typography,
  Skeleton,
} from '@mui/material';

function ProductCardContent({ product, loading }) {
  return (
    <CardContent>
      {loading ? (
        <>
          <Skeleton width="80%" />
          <Skeleton width="60%" />
          <Skeleton width="40%" />
        </>
      ) : (
        <>
          <Typography
            gutterBottom
            sx={{
              textAlign: 'left',
              overflow: 'hidden',
              whiteSpace: 'nowrap',
              textOverflow: 'ellipsis',
              width: '100%',
              typography: {
                desktop: 'desktop_h6',
                tablet: 'mobile_h4',
                small: 'mobile_h4',
              },
            }}
          >
            {product.name}
          </Typography>
          <Box
            sx={{
              display: 'flex',
              flexDirection: {
                desktop: 'row',
                tablet: 'column',
                small: 'column',
              },
              justifyContent: {
                desktop: 'space-between',
              },
              width: '100%',
              alignItems: {
                tablet: 'flex-start',
                small: 'flex-start',
              },
            }}
          >
            { product.category !== 'plusSize' && (
            <Typography
              sx={{
                textTransform: 'capitalize',
                typography: {
                  desktop: 'desktop_bodyMD',
                  tablet: 'tablet_bodyMD',
                  small: 'mobile_bodyMD',
                },
              }}
            >
              {product.category}
            </Typography>
            )}
            <Typography
              sx={{
                typography: {
                  desktop: 'desktop_h6',
                  tablet: 'mobile_h4',
                  small: 'mobile_h4',
                },
                textAlign: {
                  tablet: 'right',
                  small: 'right',
                },
                width: {
                  tablet: '100%',
                  small: '100%',
                },
              }}
            >
              $
              {product.price}
            </Typography>
          </Box>
          <Box sx={{ display: 'flex', gap: 1, mt: 1 }}>
            {product.products?.map((variant) => (
              <Box
                key={variant.color}
                sx={{
                  width: '24px',
                  height: '24px',
                  borderRadius: '50%',
                  backgroundColor: variant.color,
                  boxShadow: variant.color === 'white' ? '0px 0px 3px rgba(0, 0, 0, 1)' : 'none',
                }}
                title={variant.color}
              />
            ))}
          </Box>
        </>
      )}
    </CardContent>
  );
}

ProductCardContent.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    category: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    new: PropTypes.bool,
    products: PropTypes.arrayOf(
      PropTypes.shape({
        color: PropTypes.string.isRequired,
      }),
    ),
  }).isRequired,
  loading: PropTypes.bool.isRequired,
};

export default ProductCardContent;
