import React, { useState, useEffect } from 'react';
import {
  Typography, Grid, Box, Container, useMediaQuery, useTheme, Skeleton,
} from '@mui/material';
import IconButtonNext from '../Buttons/IconButtonNext/IconButtonNext';
import IconButtonPrev from '../Buttons/IconButtonPrev/IconButtonPrev';
import ProductCard from '../ProductCard/ProductCard';
import getBestSellerProducts from '../../api/getBestSellerProducts.js';

const Bestsellers = () => {
  const [products, setProducts] = useState([]);
  const [visibleProducts, setVisibleProducts] = useState([]);
  const [currentIndex, setCurrentIndex] = useState(0);
  const [loading, setLoading] = useState(true);

  const theme = useTheme();
  const isTabletOrSmaller = useMediaQuery(theme.breakpoints.down('tablet'));

  const itemsPerPage = isTabletOrSmaller ? 2 : 3;

  useEffect(() => {
    const fetchProducts = async () => {
      try {
        const productsData = await getBestSellerProducts();
        setProducts(productsData);
        setVisibleProducts(productsData.slice(0, itemsPerPage));
      } catch (error) {
        console.error('Fetch error:', error);
      } finally {
        setLoading(false);
      }
    };

    fetchProducts();
  }, [itemsPerPage]);

  useEffect(() => {
    if (products.length > 0) {
      const newVisibleProducts = [
        ...products.slice(currentIndex, currentIndex + itemsPerPage),
        ...products.slice(0, Math.max(0, (currentIndex + itemsPerPage) - products.length)),
      ];
      setVisibleProducts(newVisibleProducts);
    }
  }, [currentIndex, products, itemsPerPage]);

  const handlePrev = () => {
    setCurrentIndex((prevIndex) => {
      const newIndex = (prevIndex - 1 + products.length) % products.length;
      return newIndex;
    });
  };

  const handleNext = () => {
    setCurrentIndex((prevIndex) => (prevIndex + 1) % products.length);
  };

  return (
    <Container
      sx={{
        display: 'flex', justifyContent: 'center', width: '100%', mb: 6,
      }}
    >
      <Box sx={{ p: 0, position: 'relative', width: '100%' }}>
        <Typography
          sx={{
            typography: {
              desktop: 'desktop_logoName',
              tablet: 'mobile_logoName',
              small: 'mobile_h2',
            },
            textAlign: 'left',
            mb: 6,
            mt: 8,
          }}
        >
          Best Sellers
        </Typography>
        <Box>
          <IconButtonPrev onClick={handlePrev} />
          <Grid container wrap="nowrap" spacing={3}>
            {loading ? (
              Array.from(new Array(itemsPerPage)).map(() => (
                <Grid
                  item
                  small={12}
                  mobile={6}
                  middle={6}
                  tablet={4}
                  large={4}
                  desktop={4}
                  key={Math.random().toString(36).substr(2, 9)}
                >
                  <Skeleton
                    variant="rectangular"
                    width="100%"
                    height="auto"
                    sx={{ aspectRatio: '1 / 1.4' }}
                  />
                </Grid>
              ))
            ) : (
              visibleProducts.map((product) => (
                product ? (
                  <Grid
                    item
                    small={12}
                    mobile={6}
                    middle={6}
                    tablet={4}
                    large={4}
                    desktop={4}
                    key={product.id}
                  >
                    <ProductCard product={product} key={product.id} />
                  </Grid>
                ) : null
              ))
            )}
          </Grid>
          <IconButtonNext onClick={handleNext} />
        </Box>
      </Box>
    </Container>
  );
};

export default Bestsellers;
