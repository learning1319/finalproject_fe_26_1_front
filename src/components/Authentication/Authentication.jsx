import Box from '@mui/material/Box';
import { Formik, Form } from 'formik';
import {
  Typography,
  Container,
  Snackbar,
  Alert,
} from '@mui/material';
import Button from '@mui/material/Button';
import PropTypes from 'prop-types';
import { useNavigate } from 'react-router-dom';
import { useEffect, useState } from 'react';
import Input from '../Inputs/Input/Input';
import registrationSchema from '../../utils/registrationSchema.js';
import loginSchema from '../../utils/loginSchema.js';
import AuthRedirect from './AuthRedirect';
import getBannersById from '../../api/getBannersById.js';

function Authentication({
  showPassword,
  setShowPassword,
  title,
  buttonText,
  isLogin,
  isRegister,
  onRegisterSubmit,
  onLoginSubmit,
}) {
  const [banner, setBanner] = useState(null);

  useEffect(() => {
    const fetchBanner = async () => {
      try {
        const data = await getBannersById('bannerLogin', 'bannerLogin');
        setBanner(data[0]);
      } catch (err) {
        console.log(err);
      }
    };
    fetchBanner();
  }, []);

  let initialValues = {};
  let sch = {};

  if (isRegister) {
    initialValues = {
      firstName: '',
      lastName: '',
      email: '',
      password: '',
    };
    sch = registrationSchema;
  }

  if (isLogin) {
    initialValues = {
      email: '',
      password: '',
    };
    sch = loginSchema;
  }

  const navigate = useNavigate();
  const [openSnackbar, setOpenSnackbar] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');
  const handleSnackbarClose = () => {
    setOpenSnackbar(false);
  };

  return (
    <Container variant="fullWidth" sx={{ mb: 6 }}>
      <Box
        sx={{
          width: '100%',
          height: {
            small: '35vh',
            middle: '45vh',
            tablet: '60vh',
            desktop: '80vh',
          },
          backgroundImage: `url(${banner?.imageUrl})`,
          backgroundSize: 'cover',
          backgroundPosition: {
            small: '50% 60%',
            middle: '50% 50%',
            tablet: '50% 100%',
          },
          mt: 2,
        }}
      />
      <Container variant="formContainer" sx={{ mt: 2 }}>
        <Formik
          initialValues={initialValues}
          onSubmit={async (values, { resetForm }) => {
            if (isLogin) {
              const success = await onLoginSubmit(values.email, values.password);
              console.log(success);

              if (success) {
                navigate('/');
              } else {
                setSnackbarMessage('Login unsuccessful. Please try again.');
                setOpenSnackbar(true);
              }
            }
            if (isRegister) {
              const success = await onRegisterSubmit(
                values.firstName,
                values.lastName,
                values.password,
                values.email,
              );
              console.log(success);
              if (success) {
                console.log('success');
              } else {
                setSnackbarMessage('User already exists. Please, choose other email.');
                setOpenSnackbar(true);
              }
            }
            resetForm();
          }}
          validationSchema={sch}
        >
          {() => (
            <Form>
              <Typography
                variant="mobile_h3"
                component="h3"
                sx={{
                  textTransform: 'capitalize',
                  mb: { small: 2, middle: 3, desktop: 4 },
                  fontSize: {
                    middle: '18px',
                    tablet: '20px',
                    large: '22px',
                    desktop: '26px',
                  },
                }}
              >
                {title}
              </Typography>

              {isRegister && <Input label="First Name" type="text" name="firstName" data-testid="first" />}
              {isRegister && <Input label="Last Name" type="text" name="lastName" />}

              <Input name="email" label="Email" type="email" />
              <Input
                name="password"
                label="Password"
                type={showPassword ? 'password' : 'text'}
                adornment
                showPassword={showPassword}
                setShowPassword={setShowPassword}
              />
              <Button
                type="submit"
                variant="contained"
                size="large"
                sx={{
                  width: '100%',
                  maxWidth: '500px',
                  textTransform: 'none',
                  mt: 2,
                  fontSize: '16px',
                }}
              >
                {buttonText}
              </Button>

              {isLogin && <AuthRedirect isLogin={isLogin} isRegister={false} />}
              {isRegister && <AuthRedirect isRegister={isRegister} isLogin={false} />}

              <Snackbar
                open={openSnackbar}
                autoHideDuration={6000}
                onClose={handleSnackbarClose}
                anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
              >
                <Alert
                  onClose={handleSnackbarClose}
                  severity="error"
                  sx={{ width: '100%' }}
                >
                  {snackbarMessage}
                </Alert>
              </Snackbar>

            </Form>
          )}
        </Formik>

      </Container>
    </Container>
  );
}

Authentication.propTypes = {
  showPassword: PropTypes.bool.isRequired,
  setShowPassword: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  buttonText: PropTypes.string.isRequired,
  isLogin: PropTypes.bool.isRequired,
  isRegister: PropTypes.bool.isRequired,
  onLoginSubmit: PropTypes.func.isRequired,
  onRegisterSubmit: PropTypes.func.isRequired,
};

export default Authentication;
