import { describe, expect, test } from '@jest/globals';
import { screen, render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import userEvent from '@testing-library/user-event';
import Authentication from './Authentication';
import { store } from '../../store.js';
import Registration from '../../pages/Registration/Registration';
import Login from '../../pages/Login/Login';

describe('authentication', () => {
  test('input fields for login are in the document', () => {
    render(<Authentication isRegister />);
    const firstName = screen.getAllByText('First Name');
    const lastName = screen.getAllByText('Last Name');
    const email = screen.getAllByText('Email');
    const password = screen.getAllByText('Password');
    expect(firstName[0]).toBeInTheDocument();
    expect(lastName[0]).toBeInTheDocument();
    expect(email[0]).toBeInTheDocument();
    expect(password[0]).toBeInTheDocument();
  });
});

describe('authenticationRedirect', () => {
  test('redirects to login page', async () => {
    render(
      <Router>
        <Provider store={store}>
          <Registration />
        </Provider>
      </Router>,
    );
    expect(screen.getByText('Register Now')).toBeInTheDocument();
    const link = screen.getByText('Log In');
    userEvent.click(link);
    expect(screen.getByText('Log In')).toBeInTheDocument();
  });

  test('redirects to registration page', async () => {
    render(
      <Router>
        <Provider store={store}>
          <Login />
          <Registration />
        </Provider>
      </Router>,
    );
    expect(screen.getByText('create an account')).toBeInTheDocument();
    const link = screen.getByText('create an account');
    userEvent.click(link);
    await expect(screen.getByText('Register Now')).toBeInTheDocument();
  });
});
