import Button from '@mui/material/Button';
import Apple from '../../../assets/svg/Apple/Apple';

const AppleSignInButton = () => {
  const handleAuthClick = () => {
    // window.location.href = 'http://localhost:3000/login';
    console.log('Apple login');
  };
  return (
    <Button onClick={handleAuthClick} style={{ padding: 0 }}>
      <Apple />
    </Button>
  );
};
export default AppleSignInButton;
