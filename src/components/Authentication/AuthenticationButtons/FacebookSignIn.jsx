import { Button } from '@mui/material';
import FacebookLogin from '../../../assets/svg/Facebook/FacebookLogin';

const FacebookSignInButton = () => {
  const handleAuthClick = () => {
    // window.location.href = 'http://localhost:3000/login';
    console.log('Facebook login');
  };

  return (
    <Button onClick={handleAuthClick} style={{ padding: 0 }}>
      <FacebookLogin />
    </Button>
  );
};

export default FacebookSignInButton;
