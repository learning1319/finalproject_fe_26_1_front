import { Button } from '@mui/material';
import Google from '../../../assets/svg/Google/Google';

const GoogleSignInButton = () => {
  const handleAuthClick = () => {
    window.location.href = 'http://localhost:4000/auth/google';
  };
  return (
    <Button onClick={handleAuthClick} style={{ padding: 0 }}>
      <Google />
    </Button>
  );
};

export default GoogleSignInButton;
