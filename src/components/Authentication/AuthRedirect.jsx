import PropTypes from 'prop-types';
import { Typography, Box } from '@mui/material';
import { Link } from 'react-router-dom';
import GoogleSignInButton from './AuthenticationButtons/GoogleSignIn';

const textStyle = {
  mb: 2,
  mt: 3,
  textTransform: 'capitalize',
  display: 'flex',
  gap: 1,
  justifyContent: 'center',
};

const AuthRedirect = ({ isLogin, isRegister }) => (
  <>
    <Typography
      variant="mobile_bodySM"
      component="p"
      sx={textStyle}
    >
      {isLogin && 'New to modimal?'}
      {isLogin && <Link to="/registration" style={{ color: '#5A6D57', textDecoration: 'none' }}>create an account</Link>}
      {isRegister && 'Already Have An Account?'}
      {isRegister && <Link to="/login" style={{ color: '#5A6D57', textDecoration: 'none' }}>Log In</Link>}
    </Typography>

    <Box sx={{
      display: 'flex', flexDirection: 'column', margin: 'auto',
    }}
    >
      <Typography
        variant="mobile_bodySM"
        component="p"
        sx={textStyle}
      >
        Or
      </Typography>
      <Box sx={{
        mb: 2,
        mt: 2,
        ml: 'auto',
        mr: 'auto',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
      }}
      >
        <GoogleSignInButton />
      </Box>
    </Box>

    {isRegister && (
      <Typography
        variant="mobile_bodySM"
        component="div"
        sx={{
          mb: 2,
          mt: 4,
          ml: 'auto',
          mr: 'auto',
          textTransform: 'capitalize',
          textAlign: 'center',
          maxWidth: '360px',
          fontSize: {
            small: '10px',
            mobile: '10px',
            middle: '12px',
            tablet: '12px',
            large: '12px',
            desktop: '12px',
          },
        }}
      >
        <p>
          By Clicking Register Now You Agree To
          {' '}
          <Link style={{ color: '#5A6D57', textDecoration: 'none' }} to="/termsConditions">
            Terms & Conditions
          </Link>
          {' '}
          And
          {' '}
          <Link
            style={{
              color: '#5A6D57',
              textDecoration: 'none',
            }}
            to="/policy"
          >
            Privacy Policy
          </Link>
        </p>
      </Typography>
    )}
  </>
);

AuthRedirect.propTypes = {
  isLogin: PropTypes.bool.isRequired,
  isRegister: PropTypes.bool.isRequired,
};

export default AuthRedirect;
