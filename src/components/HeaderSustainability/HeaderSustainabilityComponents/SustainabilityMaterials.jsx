import { useState, useEffect } from 'react';
import {
  Container,
  Typography,
  Box,
  useMediaQuery,
  CardMedia,
  ImageList,
} from '@mui/material';
import { useTheme } from '@mui/material/styles';
import getBannersById from '../../../api/getBannersById.js';

const SustainabilityMaterials = () => {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('middle'));
  const isMiddle = useMediaQuery(theme.breakpoints.only('middle'));
  const isTablet = useMediaQuery(theme.breakpoints.up('tablet'));
  const [cotton, setCotton] = useState(null);
  const [wool, setWool] = useState(null);
  const [linen, setLinen] = useState(null);
  const [silk, setSilk] = useState(null);

  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  }, []);

  useEffect(() => {
    const loadCotton = async () => {
      try {
        console.log('loadCotton 1');
        const response = await getBannersById('sustainability', 'cotton');
        console.log('loadCotton 2');
        setCotton(response[0]);
      } catch (err) {
        console.error('Error loading banners:', err);
      }
    };
    const loadWool = async () => {
      try {
        const response = await getBannersById('sustainability', 'wool');
        setWool(response[0]);
      } catch (err) {
        console.error('Error loading banners:', err);
      }
    };
    const loadLinen = async () => {
      try {
        const response = await getBannersById('sustainability', 'linen');
        setLinen(response[0]);
      } catch (err) {
        console.error('Error loading banners:', err);
      }
    };
    const loadSilk = async () => {
      try {
        const response = await getBannersById('sustainability', 'silk');
        setSilk(response[0]);
      } catch (err) {
        console.error('Error loading banners:', err);
      }
    };

    loadCotton();
    loadWool();
    loadLinen();
    loadSilk();
  }, []);

  console.log('cotton', cotton);

  const titleStyle = {
    fontSize: {
      mobile: theme.typography.mobile_h4.fontSize,
      tablet: '15px',
      large: '16px',
      desktop: '19px',
    },
    display: 'block',
    mb: 1,
    mt: 2,
  };

  const paragraphStyle = {
    textAlign: 'justify',
    fontSize: {
      mobile: theme.typography.mobile_bodyMD.fontSize,
      tablet: '16px',
    },
    lineHeight: {
      tablet: '27px',
    },
  };

  const renderImageWithText = (image, text) => (
    <Box key={image.id} sx={{ mb: 4 }}>
      <CardMedia
        component="img"
        src={image.imageUrl}
        alt={text}
        sx={{
          width: '100%', height: 'auto', mt: 5, mb: 5,
        }}
      />
      <Typography variant="body2" sx={{ mt: 1, textAlign: 'justify' }}>
        {text}
      </Typography>
    </Box>
  );

  return (
    <Container maxWidth="md">
      <Box sx={{ mb: 4 }}>
        <Typography variant="mobile_h4" sx={titleStyle}>
          Sustainably sourced materials
        </Typography>
      </Box>
      <Typography variant="mobile_bodyMD" sx={paragraphStyle} paragraph>
        At Modimal, we believe in investing in the now to design for the future. That
        is why we are committed to sourcing quality materials that will have less
        impact on the environment. So far in 2022, 92% of the base fabrics in our
        collection are more sustainably sourced. Our goal is to use only 100%
        sustainably sourced materials by 2025. There are five kinds of fabrics in
        our collections that are Organic and responsibly sourced, and we highlight
        these so you can make considered choices when you shop.
      </Typography>

      {isMobile || isMiddle ? (
        <Box>
          {cotton && renderImageWithText(cotton, 'We source certified organic cotton, which is grown without the use of pesticides or synthetic fertilizers and requires less irrigation as it relies mainly on rainwater. Avoiding harmful pesticides preserves soil biodiversity and protects the health of surrounding communities. Our organic cotton fabrics are made using organic cotton yarns that are certified by the Global Organic Textile Standard (GOTS).')}
          {wool && renderImageWithText(wool, 'Wool is a natural fiber with added performance attributes such as temperature regulation, durability, and natural water repellency. Considered a circular product by nature, wool can be recycled or biodegraded easily. Animal welfare is extremely important to us, and therefore we only source mulesing-free wool from producers that follow humane and eco-friendly processes aligned with our animal welfare guidelines.')}
          {linen && renderImageWithText(linen, 'Found throughout our collections, linen is a sustainable fiber made from the flax plant. Flax is naturally pest resistant that requires less pesticides, water and energy to produce compared to cotton and polyester. Flax aids in sequestering carbon into the soil, which removes carbon dioxide from the atmosphere and is beneficial for improving soil health.')}
          {silk && renderImageWithText(silk, 'Organic silk is a more responsible alternative to making conventional silk through traditional methods. The silkworms are fed mulberry tree leaves from organic agriculture that uses no pesticides or harmful chemicals and resulting in a lustrous fabric that is gentle on both you and environment. This responsibly sourced material epitomizes our dedication to creating exquisite clothing with a conscience.')}
        </Box>
      ) : (
        <ImageList
          variant="masonry"
          cols={2}
          gap={isTablet ? 35 : 8}
          sx={{
            '& .MuiImageListItem-root': {
              mb: isTablet ? '35px' : '8px',
            },
          }}
        >
          {cotton && renderImageWithText(cotton, 'We source certified organic cotton, which is grown without the use of pesticides or synthetic fertilizers and requires less irrigation as it relies mainly on rainwater. Avoiding harmful pesticides preserves soil biodiversity and protects the health of surrounding communities. Our organic cotton fabrics are made using organic cotton yarns that are certified by the Global Organic Textile Standard (GOTS).')}
          {wool && renderImageWithText(wool, 'Wool is a natural fiber with added performance attributes such as temperature regulation, durability, and natural water repellency. Considered a circular product by nature, wool can be recycled or biodegraded easily. Animal welfare is extremely important to us, and therefore we only source mulesing-free wool from producers that follow humane and eco-friendly processes aligned with our animal welfare guidelines.')}
          {linen && renderImageWithText(linen, 'Found throughout our collections, linen is a sustainable fiber made from the flax plant. Flax is naturally pest resistant that requires less pesticides, water and energy to produce compared to cotton and polyester. Flax aids in sequestering carbon into the soil, which removes carbon dioxide from the atmosphere and is beneficial for improving soil health.')}
          {silk && renderImageWithText(silk, 'Organic silk is a more responsible alternative to making conventional silk through traditional methods. The silkworms are fed mulberry tree leaves from organic agriculture that uses no pesticides or harmful chemicals and resulting in a lustrous fabric that is gentle on both you and environment. This responsibly sourced material epitomizes our dedication to creating exquisite clothing with a conscience.')}
        </ImageList>
      )}
      <Box
        sx={{
          mt: 3,
          mb: 4,
        }}
      >
        <Typography
          variant="mobile_bodyMD"
          sx={paragraphStyle}
          paragraph
        >
          We Are Continually Exploring More Sustainable Alternatives That Offer The
          Same Quality And Performance. We Will Soon Add New Fabrics In To Our
          Collections Which Are Recycling And Repurposing. By Giving A New Life To
          Leftover Fabrics Through Recycling And Repurposing, We Can Reduce Our Demand
          On The Planets Limited Natural Resources. Recycled Fabrics Are Made Using
          The Waste From Both The Pre- And Post-Consumer Stage Of A Products Life.
        </Typography>
      </Box>
    </Container>
  );
};

export default SustainabilityMaterials;
