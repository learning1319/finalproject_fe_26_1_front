import { useState } from 'react';
import PropTypes from 'prop-types';
import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Typography,
  Box,
} from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

const MissionItem = ({ title, content, isAccordion }) => {
  const [expanded, setExpanded] = useState(false);
  const handleChange = () => {
    setExpanded(!expanded);
  };
  if (isAccordion) {
    return (

      <Accordion
        expanded={expanded}
        onChange={handleChange}
        sx={{
          textAlign: 'justify',
          mb: {
            mobile: 1,
            large: 2,
          },
        }}
      >
        <AccordionSummary
          expandIcon={(
            <ExpandMoreIcon
              sx={{
                color: expanded ? (theme) => theme.palette.primary.dark : 'inherit',
              }}
            />
          )}
        >
          <Typography
            variant="mobile_h4"
            sx={{
              fontSize: { mobile: '12px' },
              fontWeight: { mobile: '800' },
              color: expanded ? (theme) => theme.palette.primary.dark : 'inherit',
            }}
          >
            {title}
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography
            variant="mobile_bodySM"
            sx={{ fontSize: { mobile: '12px', tablet: '14px' } }}
          >
            {content}
          </Typography>
        </AccordionDetails>
      </Accordion>
    );
  }

  return (

    <Box flexBasis="48%" mb={2}>
      <Box mb={2}>
        <Typography
          variant="desktop_bodyLG"
          sx={{
            fontWeight: '700',
            fontSize: {
              tablet: '15px',
              large: '16px',
              desktop: '18px',
            },
            mb: 2,
          }}
        >
          {title}
        </Typography>
        <Typography
          variant="desktop_bodyMD"
          paragraph
          sx={{
            mt: 2,
          }}
        >
          {content}
        </Typography>
      </Box>
    </Box>
  );
};

MissionItem.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  isAccordion: PropTypes.bool,
};

export default MissionItem;
