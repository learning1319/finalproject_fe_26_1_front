import React, { useState, useEffect } from 'react';
import {
  Container,
  Typography,
  Box,
  Button,
  useMediaQuery,
  CardMedia,
  ImageList,
  ImageListItem,
} from '@mui/material';
import { Link } from 'react-router-dom';
import { useTheme } from '@mui/material/styles';
import MissionItem from './MissionItem';
import ModalContent from './ModalContent';
import getBannersById from '../../../api/getBannersById.js';

const SustainabilityMission = () => {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('middle'));
  const isMiddle = useMediaQuery(theme.breakpoints.only('middle'));
  const [open, setOpen] = useState(false);
  const [modalTitle, setModalTitle] = useState('');
  const [modalContent, setModalContent] = useState('');
  const [mainBanner, setMainBanner] = useState(null);
  const [secondBanner, setSecondBanner] = useState(null);
  const [thirdBanner, setThirdBanner] = useState(null);
  const [fourthBanner, setFourthBanner] = useState(null);
  const [fifthBanner, setFifthBanner] = useState(null);
  const [loading, setLoading] = useState(true);

  const missionData = [
    {
      title: 'Minimalism',
      content: 'We believe less is more. Our thoughtfully designed pieces embrace minimalism ensuring that garment becomes a versatile and timeless addition to your wardrobe. By choosing quality over quantity, we encourage conscious consumption.',
    },
    {
      title: 'Ethical',
      content: 'Every stitch tells a story. Our garments are meticulously crafted by skilled artisans who share our values of ethical and fair labor practices. This dedication to craftsmanship not only ensures exceptional quality but also supports a network of talented individuals.',
    },
    {
      title: 'Eco-Friendly Materials',
      content: 'We are dedicated to reducing our environmental impact. Our clothing is made using sustainable materials, carefully sourced to minimize harm to the planet. From organic fabrics to innovative recycled materials, we aim to leave a lighter footprint.',
    },
    {
      title: 'Circular',
      content: "Embracing the circular economy, we design with longevity in mind. Our pieces are intended to be treasured for years, encouraging a shift away from disposable fashion. When you invest in our clothing, you're investing in a more sustainable future.",
    },
    {
      title: 'Transparency',
      content: "We value openness and transparency. We're on a journey to continuously improve our practices, and we're committed to sharing our progress with you. From sourcing to production, we want you to know the story behind each piece you wear. We are updating all information every six months.",
    },
    {
      title: 'Community And Empowerment',
      content: 'Our brand is a part of a community that shares a vision for a better world. Through collaborations and initiatives, we aim to inspire and empower individuals to make conscious choices and contribute to positive change.',
    },
  ];
  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  }, []);
  useEffect(() => {
    const loadMainBanner = async () => {
      try {
        const response = await getBannersById('sustainability', 'mainBanner');
        console.log(response[0]);
        setMainBanner(response[0]);
        setLoading(false);
      } catch (err) {
        console.error('Error loading banners:', err);
      }
    };
    const loadSecondBanner = async () => {
      try {
        const response = await getBannersById('sustainability', 'secondBanner');
        console.log(response[0]);
        setSecondBanner(response[0]);
      } catch (err) {
        console.error('Error loading banners:', err);
      }
    };
    const loadThirdBanner = async () => {
      try {
        const response = await getBannersById('sustainability', 'thirdBanner');
        console.log(response[0]);
        setThirdBanner(response[0]);
      } catch (err) {
        console.error('Error loading banners:', err);
      }
    };
    const loadFourthBanner = async () => {
      try {
        const response = await getBannersById('sustainability', 'fourthBanner');
        console.log(response[0]);
        setFourthBanner(response[0]);
      } catch (err) {
        console.error('Error loading banners:', err);
      }
    };
    const loadFifthBanner = async () => {
      try {
        const response = await getBannersById('sustainability', 'fifthBanner');
        console.log(response[0]);
        setFifthBanner(response[0]);
      } catch (err) {
        console.error('Error loading banners:', err);
      }
    };

    loadMainBanner();
    loadSecondBanner();
    loadThirdBanner();
    loadFourthBanner();
    loadFifthBanner();
  }, []);

  const handleOpen = (title, content) => {
    setModalTitle(title);
    setModalContent(content);
    setOpen(true);
  };

  const handleClose = () => setOpen(false);

  const TypographyStyles = {
    fontSize: {
      small: '8px',
      mobile: '10px',
      middle: '13px',
    },
    color: 'white',
  };

  const ButtonStyles = {
    backgroundColor: '#5A6D57',
    mt: 2,
    '&:hover': {
      backgroundColor: '#748C70',
    },
    width: '100%',
  };

  return (
    <>
      <Container variant="fullWidth">
        {loading ? (
          <Box
            sx={{
              width: '100%',
              height: { small: '25vh', tablet: '35vh', desktop: '45vh' },
              backgroundColor: theme.palette.primary.gray,
              mb: 6,
            }}
          />
        ) : (
          mainBanner && (
            <CardMedia
              component="img"
              key={mainBanner.id}
              src={mainBanner.imageUrl}
              alt="Sustainability"
              sx={{
                width: '100%',
                height: 'auto',
                mb: 6,
              }}
            />
          )
        )}
      </Container>
      <Container maxWidth="md">
        <Typography
          variant="mobile_h4"
          sx={{
            fontSize: {
              mobile: theme.typography.mobile_h4.fontSize,
              tablet: '18px',
              large: '22px',
              desktop: '26px',
            },
          }}
        >
          Sustainability At Modimal
        </Typography>
        <Typography
          variant="mobile_bodyMD"
          sx={{
            textAlign: 'justify',
            fontSize: {
              mobile: theme.typography.mobile_bodyMD.fontSize,
              tablet: '16px',
            },
            lineHeight: {
              tablet: '27px',
            },
            mt: 5,
            mb: 6,
          }}
          paragraph
        >
          At Modimal, sustainability is at the heart
          of everything we do. Our brand identity,
          characterized by its simplicity and elegance,
          is a reflection of our commitment to a more sustainable future.
        </Typography>
        <Box sx={{
          mb: 6,
        }}
        >
          <Typography
            variant="mobile_h4"
            sx={{
              fontSize: {
                mobile: theme.typography.mobile_h4.fontSize,
                tablet: '16px',
                large: '19px',
                desktop: '21px',
              },
              fontWeight: { tablet: '700' },
            }}
          >
            Our Mission, The Modimal Six:
          </Typography>
        </Box>

        {isMobile || isMiddle ? (
          <>
            {missionData.map((item) => (
              <MissionItem
                key={item.title}
                title={item.title}
                content={item.content}
                isAccordion
              />
            ))}
          </>
        ) : (
          <Box
            display="flex"
            flexWrap="wrap"
            justifyContent="space-between"
            textAlign="justify"
          >
            {missionData.map((item) => (
              <MissionItem
                key={item.title}
                title={item.title}
                content={item.content}
                isAccordion={false}
              />
            ))}
          </Box>
        )}

        <Typography
          variant="mobile_bodyMD"
          sx={{
            textAlign: 'justify',
            fontSize: {
              mobile: theme.typography.mobile_bodyMD.fontSize,
              tablet: '16px',
            },
            mt: { mobile: '20px', large: '46px' },
            mb: 5,
            lineHeight: { tablet: '27px' },
          }}
          paragraph
        >
          Guided by our core missions, we intertwine
          sustainability into every thread of our brand,
          from thoughtfully sourced materials and innovative
          manufacturing process to nurturing product longevity
          and embracing eco-friendly packaging – all harmonizing
          to create a more meaningful and responsible approach
          to fashion.
        </Typography>

        <ImageList variant="masonry" cols={2} gap={8}>
          {secondBanner && (
            <ImageListItem key={secondBanner.id}>
              <CardMedia
                component="img"
                src={secondBanner.imageUrl}
                alt="Sustainability"
                sx={{ width: '100%', height: 'auto' }}
              />
              <Button
                onClick={() => handleOpen(
                  'Production Process',
                  'We take pride in how carefully and responsibly we approach every stage of our clothing production. From selecting materials to quality control, every step of our process is aimed at creating an exceptional product that meets the highest standards',
                )}
                style={{ width: '100%' }}
                sx={ButtonStyles}
              >
                <Typography
                  align="center"
                  sx={TypographyStyles}
                >
                  Processing
                </Typography>
              </Button>
            </ImageListItem>
          )}
          {thirdBanner && (
            <ImageListItem key={thirdBanner.id}>
              <CardMedia
                component="img"
                src={thirdBanner.imageUrl}
                alt="Sustainability"
                sx={{ width: '100%', height: 'auto' }}
              />
              <Link to="/sustainability/materials" style={{ textDecoration: 'none' }}>
                <Button
                  style={{ width: '100%' }}
                  sx={ButtonStyles}
                >
                  <Typography
                    align="center"
                    sx={TypographyStyles}
                  >
                    Materials
                  </Typography>
                </Button>
              </Link>
            </ImageListItem>
          )}
          {fourthBanner && (
            <ImageListItem key={fourthBanner.id}>
              <CardMedia
                component="img"
                src={fourthBanner.imageUrl}
                alt="Sustainability"
                sx={{ width: '100%', height: 'auto' }}
              />
              <Button
                onClick={() => handleOpen(
                  'Eco-Friendly Packaging',
                  'All our packaging is designed with sustainability and functionality in mind. We use recycled and recyclable materials to minimize our environmental footprint and make every purchase not only enjoyable but also responsible.',
                )}
                style={{ width: '100%' }}
                sx={ButtonStyles}
              >
                <Typography
                  align="center"
                  sx={TypographyStyles}
                >
                  Packaging
                </Typography>
              </Button>
            </ImageListItem>
          )}
          {fifthBanner && (
            <ImageListItem key={fifthBanner.id}>
              <CardMedia
                component="img"
                src={fifthBanner.imageUrl}
                alt="Sustainability"
                sx={{ width: '100%', height: 'auto' }}
              />
              <Button
                onClick={() => handleOpen(
                  'Product Care',
                  'Follow the care label instructions: Always check the care label on your clothing before washing. Use the recommended wash cycle and water temperature.Using a dryer: If a dryer is necessary, choose a delicate setting and avoid overheating, which can damage the fabric and cause shrinkage.Use garment bags: For long-term storage or to protect delicate clothing, use garment bags to prevent dirt and damage.',
                )}
                style={{ width: '100%' }}
                sx={ButtonStyles}
              >
                <Typography
                  align="center"
                  sx={TypographyStyles}
                >
                  Product Caring
                </Typography>
              </Button>
            </ImageListItem>
          )}
        </ImageList>

        <ModalContent
          open={open}
          handleClose={handleClose}
          title={modalTitle}
          content={modalContent}
        />

        <Typography
          variant="mobile_bodyMD"
          sx={{
            textAlign: 'justify',
            fontSize: {
              mobile: theme.typography.mobile_bodyMD.fontSize,
              tablet: '16px',
            },
            pl: 1,
            lineHeight: { tablet: '27px' },
            mt: 5,
            mb: 9,
          }}
          paragraph
        >
          With every step, our quest for sustainability is fortified
          by our trusted suppliers, united in our shared dedication
          to ethical craftsmanship and a more conscious future.
          With Modimal, you are not just wearing fashion – you are
          making a statement. A statement that elegance and
          sustainability can coexist, shaping a more responsible
          and beautiful future for us all.
        </Typography>
      </Container>
    </>
  );
};

export default SustainabilityMission;
