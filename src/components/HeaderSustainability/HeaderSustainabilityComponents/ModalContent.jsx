import PropTypes from 'prop-types';
import {
  Modal,
  Box,
  Typography,
} from '@mui/material';
import Cross from '../../../assets/svg/Cross/Cross';

const ModalContent = ({
  open, handleClose, title, content,
}) => (
  <Modal
    open={open}
    onClose={handleClose}
    aria-labelledby="modal-title"
    aria-describedby="modal-description"
    sx={{
      backdropFilter: 'blur(5px)',
      '& .MuiBackdrop-root': {
        backdropFilter: 'blur(5px)',
      },
    }}
  >
    <Box
      sx={{
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: {
          small: '84%',
          mobile: '82%',
          middle: '53%',
          tablet: '35%',
          large: '39%',
          desktop: '27%',
        },
        bgcolor: 'background.paper',
        boxShadow: 24,
        p: 4,
      }}
    >
      <Box
        onClick={handleClose}
        sx={{
          position: 'absolute',
          top: 8,
          right: 8,
          cursor: 'pointer',
        }}
      >
        <Cross />
      </Box>
      <Typography
        id="modal-title"
        component="h2"
        sx={{
          fontSize: {
            small: '11px',
            mobile: '15px',
            tablet: '19px',
          },
          fontWeight: '700',
        }}
      >
        {title}
      </Typography>
      <Typography
        id="modal-description"
        sx={{
          mt: 2,
          textAlign: 'justify',
          fontSize: {
            small: '11px',
            mobile: '13px',
            tablet: '17px',
          },
        }}
      >
        {content}
      </Typography>
    </Box>
  </Modal>
);
ModalContent.propTypes = {
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
};

export default ModalContent;
