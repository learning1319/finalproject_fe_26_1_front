import PropTypes from 'prop-types';
import { Box } from '@mui/material';

const Banner = ({ banner, sx }) => (
  <Box
    key={banner.id}
    sx={{
      padding: '5px',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      overflow: 'hidden',
      height: 'auto',
      ...sx,
    }}
  >
    <img
      src={banner.imageUrl}
      alt={banner.name}
      loading="lazy"
      style={{
        width: '100%',
        height: '100%',
        objectFit: 'cover',
        objectPosition: 'center',
      }}
    />
  </Box>
);

Banner.propTypes = {
  banner: PropTypes.shape({
    id: PropTypes.string.isRequired,
    imageUrl: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  }).isRequired,
  sx: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.object),
    PropTypes.object,
  ]),
};

export default Banner;
