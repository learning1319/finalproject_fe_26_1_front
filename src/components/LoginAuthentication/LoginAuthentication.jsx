import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import Authentication from '../Authentication/Authentication';
import { loggedIn } from '../../redux/slices/loginSlice.js';
import loginCustomer from '../../api/loginCustomer.js';

const LoginAuthentication = () => {
  const [showPassword, setShowPassword] = useState(true);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const onLoginSubmit = async (email, password) => {
    const userData = {
      loginOrEmail: email,
      password,
    };

    const response = await loginCustomer(userData);

    if (response.data.success) {
      dispatch(loggedIn({
        token: response.data.token,
        user: response.data.user,
      }));
      navigate('/');
      return true;
    }
    return false;
  };

  return (
    <Authentication
      isRegister={false}
      onRegisterSubmit={() => {
        console.log('It is not register, developer!!!');
      }}
      showPassword={showPassword}
      setShowPassword={setShowPassword}
      title="Log in"
      buttonText="Log in"
      isLogin
      onLoginSubmit={onLoginSubmit}
    />
  );
};

export default LoginAuthentication;
