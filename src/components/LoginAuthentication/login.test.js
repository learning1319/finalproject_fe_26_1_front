/* eslint-disable */
import { describe, test, expect } from '@jest/globals';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import { store } from '../../store.js'; 
import LoginAuthentication from './LoginAuthentication';

describe('LoginAuthentication', () => {
  test('login component exists', () => {
    const { el } = render(
      <BrowserRouter>
        <Provider store={store}>
          <LoginAuthentication />
        </Provider>
      </BrowserRouter>,
    );
    const l = screen.getByText('create an account');
    expect(l).toBeInTheDocument();
    expect(el).toMatchSnapshot();
  });
});
