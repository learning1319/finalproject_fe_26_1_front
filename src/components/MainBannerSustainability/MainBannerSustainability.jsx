import React, { useState } from 'react';
import {
  Box, Button, Typography, Container,
} from '@mui/material';
import { useNavigate } from 'react-router-dom';
import { useTheme } from '@mui/material/styles';
import { mainBannerSustainability } from '../../assets/banners.js';

function MainBannerSustainability() {
  const [bannerLoaded, setBannerLoaded] = useState(false);
  const navigate = useNavigate();
  const theme = useTheme();

  const handleButtonClick = () => {
    navigate('/sustainability');
  };

  return (
    <Container
      variant="fullWidth"
      sx={{
        position: 'relative',
        width: '100%',
        maxHeight: '526px',
        minHeight: '408px',
        overflow: 'hidden',
        marginTop: '30px',
      }}
    >
      {!bannerLoaded && (
        <Box
          sx={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            height: { small: '500px', tablet: '600px' },
            minHeight: { small: '356px', tablet: '500px', desktop: '555px' },
            width: '100%',
            backgroundColor: theme.palette.primary.gray,
          }}
        />
      )}
      {mainBannerSustainability && (
        <Box
          component="img"
          src={mainBannerSustainability[0].imageUrl}
          alt={mainBannerSustainability[0].name}
          sx={{
            width: '100%',
            height: '100%',
            objectFit: 'cover',
            position: 'absolute',
            top: 0,
            left: 0,
            zIndex: bannerLoaded ? -1 : -2,
            boxSizing: 'border-box',
            display: bannerLoaded ? 'block' : 'none',
          }}
          onLoad={() => setBannerLoaded(true)}
        />
      )}
      <Container
        sx={{
          display: 'flex',
          alignItems: 'flex-end',
          position: 'absolute',
          bottom: '20px',
          left: 0,
          right: 0,
          justifyContent: 'flex-end',
          height: 'auto',
          zIndex: 1,
          boxSizing: 'border-box',
        }}
      >
        <Box
          sx={{
            color: theme.palette.secondary.dark,
            whiteSpace: 'normal',
            width: {
              desktop: '495px',
              tablet: '400px',
              small: '318px',
            },
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'flex-end',
            gap: '30px',
          }}
        >
          <Typography
            sx={{
              typography: {
                desktop: 'desktop_sustainability',
                tablet: 'desktop_bodyLG',
                small: 'mobile_bodyMD',
              },
              textAlign: 'justify',
            }}
          >
            Stylish sustainability in clothing promotes eco-friendly choices for a greater future
          </Typography>
          <Button
            variant="contained"
            onClick={handleButtonClick}
            sx={{
              typography: {
                desktop: 'desktop_bodyMD',
                tablet: 'mobile_bodyLG',
                small: 'mobile_bodyMD',
              },
              textTransform: 'capitalize',
              backgroundColor: theme.palette.secondary.white,
              color: theme.palette.secondary.dark,
              '&:hover': {
                backgroundColor: theme.palette.primary.gray,
              },
            }}
          >
            Sustainability
          </Button>
        </Box>
      </Container>
    </Container>
  );
}

export default MainBannerSustainability;
