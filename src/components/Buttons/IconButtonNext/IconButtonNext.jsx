/* eslint-disable */
import { IconButton } from '@mui/material';
import PropTypes from 'prop-types';
import ArrowForward from '../../../assets/svg/ArrowForward/ArrowForward';

const IconButtonNext = ({ onClick }) => (
  <IconButton
    aria-label="next"
    onClick={onClick}
    sx={{
      position: 'absolute',
      right: '10px',
      top: '50%',
      transform: 'translateY(-50%)',
      zIndex: 1,
    }}
  >
    <ArrowForward />
  </IconButton>
);

IconButtonNext.propTypes = {
  onClick: PropTypes.func.isRequired,
};

export default IconButtonNext;
