/* eslint-disable */
import { IconButton } from '@mui/material';
import PropTypes from 'prop-types';
import ArrowBackward from '../../../assets/svg/ArrowBackward/ArrowBackward';

const IconButtonPrev = ({ onClick }) => (
  <IconButton
    aria-label="previous"
    onClick={onClick}
    sx={{
      position: 'absolute', left: '10px', top: '50%', transform: 'translateY(-50%)', zIndex: 1,
    }}
  >
    <ArrowBackward />
  </IconButton>
);

IconButtonPrev.propTypes = {
  onClick: PropTypes.func.isRequired,
};

export default IconButtonPrev;
