import { Typography, Button } from '@mui/material';
import PropTypes from 'prop-types';

function FilterButton({
  onClick, text, variant, backgroundColor, width,
}) {
  return (
    <Button
      variant={variant}
      color="primary"
      onClick={onClick}
      size="medium"
      sx={{
        backgroundColor: { backgroundColor },
        width: width || '100%',
      }}
    >
      <Typography
        variant="button_SM"
        sx={{
          textTransform: 'capitalize',
        }}
      >
        {text}
      </Typography>
    </Button>
  );
}

FilterButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired,
  variant: PropTypes.string.isRequired,
  backgroundColor: PropTypes.string.isRequired,
  width: PropTypes.string,
};

export default FilterButton;
