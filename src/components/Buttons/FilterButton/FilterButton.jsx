import { useDispatch } from 'react-redux';
import { Typography, Button, Stack } from '@mui/material';
import FilterSVG from '../../../assets/svg/FilterSVG/FilterSVG';
import theme from '../../../themeStyle';
import { setFilterModalOpen } from '../../../redux/slices/filterSlice.js';

function FilterButton() {
  const dispatch = useDispatch();

  const handleOpenFilterModal = () => {
    dispatch(setFilterModalOpen(true));
  };

  return (
    <Stack direction="row" spacing={2} sx={{ mb: 3 }}>
      <Button onClick={handleOpenFilterModal} startIcon={<FilterSVG />}>
        <Typography
          variant="button_SM"
          color={theme.palette.secondary.dark}
          sx={{ textTransform: 'capitalize' }}
        >
          Filter
        </Typography>
      </Button>
    </Stack>
  );
}

export default FilterButton;
