import { useState } from 'react';
import {
  Container,
  Typography,
  Paper,
  Box,
  TextField,
  Button,
  FormControlLabel,
  Checkbox,
  Alert,
  Snackbar,
} from '@mui/material';
import { Formik, Form } from 'formik';
import MailOutlineIcon from '@mui/icons-material/MailOutline';
import axios from 'axios';
// import BasicSelect from './BasicSelect';
import contactSchema from '../../utils/contactSchema.js';
import BASE_URL from '../../constants/constants.js';

const ContactForm = () => {
  const [openSnackbar, setOpenSnackbar] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');
  const [snackbarStatus, setSnackbarStatus] = useState('');
  const handleSnackbarClose = () => {
    setOpenSnackbar(false);
  };

  return (
    <Container maxWidth="md">
      <Paper elevation={3} style={{ padding: '20px', marginBottom: '20px' }}>
        <Box display="flex">
          <MailOutlineIcon fontSize="large" />
          <Typography
            variant="h6"
            align="left"
            gutterBottom
            sx={{
              fontSize: {
                mobile: '14px',
                large: '20px',
              },
              fontWeight: 'bold',
              ml: '5px',
              mt: 1,
            }}
          >
            Write Us
          </Typography>
        </Box>
        <Typography
          variant="h6"
          align="left"
          gutterBottom
          sx={{
            fontWeight: 'bold',
            fontSize: {
              mobile: '14px',
              large: '20px',
            },
            mt: 2,
          }}
        >
          Your Information
        </Typography>
        <Formik
          initialValues={{
            fullName: '',
            email: '',
            orderNumber: '',
            message: '',
            privacyPolicy: false,
          }}
          validationSchema={contactSchema}
          onSubmit={async (values, { setSubmitting, resetForm }) => {
            try {
              // eslint-disable-next-line no-unused-vars
              const response = await axios.post(`${BASE_URL}/contacts`, values);
              setSnackbarMessage(
                'Your message was sent successfully. We will contact you within 1-2 business das',
              );
              setOpenSnackbar(true);
              setSnackbarStatus('success');
              resetForm();
            } catch (error) {
              console.error('Error: ', error);
              setSnackbarMessage(
                'Failed to send your message. Please try again later.',
              );
              setOpenSnackbar(true);
              setSnackbarStatus('error');
            } finally {
              setSubmitting(false);
            }
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
          }) => (
            <Form onSubmit={handleSubmit} noValidate autoComplete="off">
              <TextField
                fullWidth
                label="Full Name"
                margin="normal"
                name="fullName"
                value={values.fullName}
                onChange={handleChange}
                onBlur={handleBlur}
                error={touched.fullName && Boolean(errors.fullName)}
                helperText={touched.fullName && errors.fullName}
              />
              <TextField
                fullWidth
                label="Email"
                margin="normal"
                name="email"
                value={values.email}
                onChange={handleChange}
                onBlur={handleBlur}
                error={touched.email && Boolean(errors.email)}
                helperText={touched.email && errors.email}
              />
              {/* <BasicSelect /> */}

              <TextField
                fullWidth
                label="Order Number (not necessary)"
                margin="normal"
                name="orderNumber"
                value={values.orderNumber}
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <TextField
                fullWidth
                label="Message"
                multiline
                rows={4}
                margin="normal"
                name="message"
                value={values.message}
                onChange={handleChange}
                onBlur={handleBlur}
                error={touched.message && Boolean(errors.message)}
                helperText={touched.message && errors.message}
              />
              <FormControlLabel
                control={(
                  <Checkbox
                    name="privacyPolicy"
                    checked={values.privacyPolicy}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                )}
                label="I have read and understood the Contact Us Privacy and Policy."
              />
              {touched.privacyPolicy && errors.privacyPolicy && (
                <Typography color="error" variant="body2">
                  {errors.privacyPolicy}
                </Typography>
              )}
              <Box display="flex" justifyContent="flex-end">
                <Button
                  variant="contained"
                  color="primary"
                  type="submit"
                  disabled={isSubmitting}
                  style={{ marginTop: '16px', width: '288px', height: '40px' }}
                >
                  {isSubmitting ? 'Sending...' : 'Send'}
                </Button>
              </Box>

              <Snackbar
                open={openSnackbar}
                autoHideDuration={4000}
                onClose={handleSnackbarClose}
                anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
              >
                <Alert
                  onClose={handleSnackbarClose}
                  severity={snackbarStatus}
                  sx={{ width: '100%' }}
                >
                  {snackbarMessage}
                </Alert>
              </Snackbar>
            </Form>
          )}
        </Formik>
      </Paper>
    </Container>
  );
};

export default ContactForm;
