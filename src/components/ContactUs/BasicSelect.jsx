import React from 'react';
import Box from '@mui/material/Box';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';

export default function BasicSelect() {
  const [subject, setSubject] = React.useState('');

  const handleChange = (event) => {
    setSubject(event.target.value);
  };

  return (
    <Box sx={{ minWidth: 120 }}>
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label">Subject</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={subject}
          label="Subject"
          onChange={handleChange}
          fullWidth
          sx={{
            width: '100%',
            '& .MuiPaper-root': {
              minWidth: '319px',
              backgroundColor: 'lightblue',
              boxShadow: '0px 4px 6px rgba(0, 0, 0, 0.1)',
              position: 'absolute',
              left: '50%',
              transform: 'translateX(-50%)', // Center the dropdown horizontally
              width: '100%', // Make sure the width matches the input
            },
            '& .MuiList-root': {
              position: 'relative',
              width: '100%',
            },
          }}
          MenuProps={{
            PaperProps: {
              sx: {
                position: 'absolute',
                left: '50%',
                transform: 'translateX(-50%)', // Center the dropdown menu horizontally
                width: '70%', // Ensure the dropdown matches the input width
                marginTop: '4px',
              },
            },
            MenuListProps: {
              sx: {
                width: '100%',
              },
            },
            anchorOrigin: {
              vertical: 'bottom',
              horizontal: 'left',
            },
            transformOrigin: {
              vertical: 'top',
              horizontal: 'left',
            },
          }}
        >
          <MenuItem value={10}>General Inquiry</MenuItem>
          <MenuItem value={20}>Support</MenuItem>
          <MenuItem value={30}>Feedback</MenuItem>
        </Select>
      </FormControl>
    </Box>
  );
}
