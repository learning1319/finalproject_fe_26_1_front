import React from 'react';
import {
  Box,
  Button,
  Typography,
  Container,
} from '@mui/material';
import ChatOutlinedIcon from '@mui/icons-material/ChatOutlined';
import ContactPhoneOutlinedIcon from '@mui/icons-material/ContactPhoneOutlined';
import MailOutlineIcon from '@mui/icons-material/MailOutline';

const ContactDetails = () => (
  <Container maxWidth="md">
    <Box
      display="flex"
      justifyContent="space-between"
      padding="20px"
      gap="11px"
    >
      <Box
        display="flex"
        flexDirection="column"
        alignItems="center"
        padding="20px"
        bgcolor="#eff1ef"
        borderRadius="8px"
        textAlign="center"
        width="30%"
      >
        <ChatOutlinedIcon fontSize="large" />
        <Typography
          variant="desktop_h6"
          marginTop="10px"
        >
          Chat With Us
        </Typography>
        <Typography variant="body2" color="textSecondary">
          We Are Here And Ready To Chat
        </Typography>
        <Button
          variant="outlined"
          color="primary"
          style={{ marginTop: '20px', fontFamily: '14px' }}
        >
          Start Chat
        </Button>
      </Box>
      <Box
        display="flex"
        flexDirection="column"
        alignItems="center"
        padding="20px"
        bgcolor="#eff1ef"
        borderRadius="8px"
        textAlign="center"
        width="30%"
      >
        <ContactPhoneOutlinedIcon fontSize="large" />
        <Typography
          variant="desktop_h6"
          marginTop="10px"
        >
          Call Us
        </Typography>
        <Typography variant="body2" color="textSecondary">
          We are Here To Talk To You
        </Typography>
        <Button variant="outlined" color="primary" style={{ marginTop: '20px' }}>
          +1(929)460-3208
        </Button>
      </Box>
      <Box
        display="flex"
        flexDirection="column"
        alignItems="center"
        padding="20px"
        bgcolor="#eff1ef"
        borderRadius="8px"
        textAlign="center"
        width="30%"
      >
        <MailOutlineIcon fontSize="large" />
        <Typography
          variant="desktop_h6"
          marginTop="10px"
        >
          Email Us
        </Typography>
        <Typography variant="body2" color="textSecondary">
          You Are Welcome To Send Us An Email
        </Typography>
        <Button variant="outlined" color="primary" style={{ marginTop: '20px' }}>
          Send Email
        </Button>
      </Box>
    </Box>
  </Container>
);

export default ContactDetails;
