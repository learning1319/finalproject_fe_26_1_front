import PropTypes from 'prop-types';
import {
  Typography,
} from '@mui/material';

const ContactUsSection = ({ title, content }) => (
  <>
    {title && (
    <Typography
      variant="mobile_h1"
      sx={{
        fontFamily: 'Montserrat',
        mb: 2,
        fontSize: {
          small: '16px',
          mobile: '18px',
          tablet: '20px',
        },
      }}
    >
      {title}
    </Typography>
    )}
    <Typography
      sx={{
        fontWeight: 400,
        fontSize: {
          small: '12px',
          mobile: '14px',
          middle: '16px',

        },
        lineHeight: '32px',
        paddingLeft: '10px',
        mb: 3,
      }}
    >
      {content}
    </Typography>
  </>
);

ContactUsSection.propTypes = {
  title: PropTypes.string,
  content: PropTypes.string.isRequired,
};

ContactUsSection.defaultProps = {
  title: '',
};

export default ContactUsSection;
