import React from 'react';
import {
  Box, Button,
  Typography,
  Accordion,
  AccordionSummary,
  AccordionDetails,
} from '@mui/material';
import ChatOutlinedIcon from '@mui/icons-material/ChatOutlined';
import ContactPhoneOutlinedIcon from '@mui/icons-material/ContactPhoneOutlined';
import MailOutlineIcon from '@mui/icons-material/MailOutline';
import AddIcon from '@mui/icons-material/Add';

const ContactDetailsMobil = () => (
  <Box
    padding="16px"
    textAlign="center"
  >
    <Accordion>
      <AccordionSummary
        expandIcon={<AddIcon />}
        sx={{ bgcolor: '#D1D9CF' }}
      >
        <Box
          display="flex"
          alignItems="center"
        >
          <ChatOutlinedIcon style={{ marginRight: '8px', width: '24px', height: '24px' }} />
          <Typography
            variant="mobile_h4"
          >
            Chat With Us
          </Typography>
        </Box>
      </AccordionSummary>
      <AccordionDetails sx={{ bgcolor: '#D1D9CF' }}>
        <Typography variant="body2" color="textSecondary" marginBottom="16px">
          We are here and ready to chat.
        </Typography>
        <Button
          variant="contained"
          color="primary"
          fullWidth
        >
          Start Chat
        </Button>
      </AccordionDetails>
    </Accordion>

    <Accordion>
      <AccordionSummary
        expandIcon={<AddIcon />}
        sx={{ bgcolor: '#D1D9CF' }}
      >
        <Box display="flex" alignItems="center">
          <ContactPhoneOutlinedIcon style={{ marginRight: '8px', width: '24px', height: '24px' }} />
          <Typography
            variant="mobile_h4"
          >
            Call Us
          </Typography>
        </Box>
      </AccordionSummary>
      <AccordionDetails sx={{ bgcolor: '#D1D9CF' }}>
        <Typography variant="body2" color="textSecondary" marginBottom="16px">
          We are here to talk to you.
        </Typography>
        <Button variant="contained" color="primary" fullWidth>
          +1(929)460-3208
        </Button>
      </AccordionDetails>
    </Accordion>

    <Accordion>
      <AccordionSummary
        expandIcon={<AddIcon />}
        sx={{ bgcolor: '#D1D9CF' }}
      >
        <Box display="flex" alignItems="center">
          <MailOutlineIcon style={{ marginRight: '8px', width: '24px', height: '24px' }} />
          <Typography
            variant="mobile_h4"
          >
            Email Us
          </Typography>
        </Box>
      </AccordionSummary>
      <AccordionDetails sx={{ bgcolor: '#D1D9CF' }}>
        <Typography variant="body2" color="textSecondary" marginBottom="16px">
          You are welcome to send us an email.
        </Typography>
        <Button variant="contained" color="primary" fullWidth>
          Send Email
        </Button>
      </AccordionDetails>
    </Accordion>

  </Box>
);

export default ContactDetailsMobil;
