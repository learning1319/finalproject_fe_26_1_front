import React from 'react';
import {
  Box, Button,
  Typography,
  TextField,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  MenuItem,
  Checkbox,
  FormControlLabel,
} from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

const ContactFormsMobil = () => (
  <Box padding="16px" bgcolor="#f5f7f6">
    <Accordion>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        sx={{ bgcolor: '#eff1ef' }}
      >
        <Typography
          variant="mobile_h4"
        >
          Write Us
        </Typography>
      </AccordionSummary>
      <AccordionDetails
        sx={{
          bgcolor: '#eff1ef',
        }}
      >
        <Box component="form" width="100%">
          <TextField
            fullWidth
            label="Full Name"
            margin="normal"
            variant="outlined"
          />
          <TextField
            fullWidth
            label="Email"
            margin="normal"
            variant="outlined"
          />
          <TextField
            fullWidth
            label="Subject"
            margin="normal"
            variant="outlined"
            select
          >
            <MenuItem value="general">General Inquiry</MenuItem>
            <MenuItem value="support">Support</MenuItem>
            <MenuItem value="feedback">Feedback</MenuItem>
          </TextField>
          <TextField
            fullWidth
            label="Order Number"
            margin="normal"
            variant="outlined"
          />
          <TextField
            fullWidth
            label="Message"
            margin="normal"
            variant="outlined"
            multiline
            rows={4}
          />
          <FormControlLabel
            control={<Checkbox />}
            label="I have read and understood the contact us privacy and policy."
            sx={{ marginY: '16px' }}
          />
          <Button variant="contained" color="primary" fullWidth>
            Send
          </Button>
        </Box>
      </AccordionDetails>
    </Accordion>
  </Box>
);

export default ContactFormsMobil;
