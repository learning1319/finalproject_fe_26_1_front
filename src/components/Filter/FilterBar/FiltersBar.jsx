import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import CloseIcon from '@mui/icons-material/Close';
import {
  Container, Box, Typography, IconButton, useMediaQuery,
} from '@mui/material';
import CustomAccordion from '../CustomAccordion/CustomAccordion';
import FilterSubmitButton from '../../Buttons/FilterSubmitButton/FilterSubmitButton';
import {
  sortCategories,
  regularSizeCategories,
  colorCategories,
  fabricCategories,
  plusSizeCategories,
} from '../../../assets/filterCategories.js';
import theme from '../../../themeStyle';
import FilterChips from '../FilterChip/FilterChip';
import {
  selectFilters,
  selectIsExpanded,
  setFilterModalOpen,
  setIsExpanded,
  setFilters,
  clearFilters,
} from '../../../redux/slices/filterSlice.js';

const FiltersBar = ({ isPlusSize = false }) => {
  const isMobile = useMediaQuery(theme.breakpoints.down('tablet'));
  const dispatch = useDispatch();
  const filters = useSelector(selectFilters);
  const isExpanded = useSelector(selectIsExpanded);

  const handleSetIsExpanded = (categoryName) => () => {
    dispatch(setIsExpanded({ categoryName, isExpanded: !isExpanded[categoryName] }));
  };

  const handleFilterChange = (e, categoryName) => {
    const { name, checked } = e.target;
    dispatch(
      setFilters({
        ...filters,
        [categoryName]: {
          ...filters[categoryName],
          [name]: checked,
        },
      }),
    );
  };

  const handleClearFilters = () => {
    dispatch(clearFilters());
  };

  return (
    <Container
      sx={{
        padding: { tablet: 0 },
        minWidth: { small: '318px', tablet: 'unset' },
        maxWidth: '500px',
        minHeight: '60vh',
        position: 'relative',
      }}
    >
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          mb: 2,
        }}
      >
        <Typography sx={{ typography: { small: 'mobile_h2', tablet: 'mobile_h1' } }}>
          Filters
        </Typography>
        {isMobile && (
          <IconButton
            onClick={() => {
              dispatch(setFilterModalOpen(false));
            }}
            sx={{
              color: 'text.primary',
            }}
          >
            <CloseIcon />
          </IconButton>
        )}
      </Box>
      <FilterChips />
      <CustomAccordion
        isExpanded={isExpanded}
        handleSetIsExpanded={handleSetIsExpanded}
        filters={filters}
        handleFilterChange={(e) => handleFilterChange(e, 'sort')}
        categoryName="sort"
        title="Sort by"
        categories={sortCategories}
      />
      <CustomAccordion
        isExpanded={isExpanded}
        handleSetIsExpanded={handleSetIsExpanded}
        filters={filters}
        handleFilterChange={(e) => handleFilterChange(e, 'fabric')}
        categoryName="fabric"
        title="Fabric"
        categories={fabricCategories}
      />
      <CustomAccordion
        isExpanded={isExpanded}
        handleSetIsExpanded={handleSetIsExpanded}
        filters={filters}
        handleFilterChange={(e) => handleFilterChange(e, 'size')}
        categoryName="size"
        title="Size"
        categories={isPlusSize ? plusSizeCategories : regularSizeCategories}
      />
      <CustomAccordion
        isExpanded={isExpanded}
        handleSetIsExpanded={handleSetIsExpanded}
        filters={filters}
        handleFilterChange={(e) => handleFilterChange(e, 'color')}
        categoryName="color"
        title="Color"
        categories={colorCategories}
      />

      {isMobile && (
        <Box
          sx={{
            display: { small: 'flex', tablet: 'none' },
            justifyContent: 'space-between',
          }}
        >
          <FilterSubmitButton
            onClick={handleClearFilters}
            text="Clear filter"
            variant="text"
            backgroundColor="unset"
            width="50%"
          />
          <FilterSubmitButton
            onClick={() => {
              dispatch(setFilterModalOpen(false));
            }}
            text="Apply filters"
            variant="contained"
            backgroundColor={theme.palette.primary.main}
            width="50%"
          />
        </Box>
      )}
    </Container>
  );
};

FiltersBar.propTypes = {
  isPlusSize: PropTypes.bool,
};

export default FiltersBar;
