import { useSelector, useDispatch } from 'react-redux';
import { Box, Chip, useMediaQuery } from '@mui/material';

import theme from '../../../themeStyle';
import {
  selectFilters,
  clearFilters,
  setFilters,
} from '../../../redux/slices/filterSlice.js';
import FilterSubmitButton from '../../Buttons/FilterSubmitButton/FilterSubmitButton';

const validCategories = new Set(['fabric', 'size', 'color', 'sort']);

const filterLabelMap = {
  '+price': 'Price: high to low',
  '-price': 'Price: low to high',
  bestSeller: 'Bestsellers',
};

function FilterChips() {
  const dispatch = useDispatch();
  const filters = useSelector(selectFilters);
  const isMobile = useMediaQuery(theme.breakpoints.down('tablet'));

  const handleDeleteFilter = (category, filter) => {
    dispatch(
      setFilters({
        ...filters,
        [category]: {
          ...filters[category],
          [filter]: false,
        },
      }),
    );
  };

  const handleClearFilters = () => {
    dispatch(clearFilters());
  };

  const hasActiveFilters = () => Object.values(filters)
    .some((filterStates) => Object.values(filterStates)
      .some((isActive) => isActive));

  const getFilterLabel = (filter) => filterLabelMap[filter] || filter;

  return (
    <Box
      sx={{
        display: 'flex',
        flexWrap: 'wrap',
        gap: 1,
        mb: 2,
      }}
    >
      {Object.entries(filters)
        .map(([category, filterStates]) => (validCategories.has(category) ? (
          Object.entries(filterStates)
            // eslint-disable-next-line no-unused-vars
            .filter(([_, isActive]) => isActive)
            .map(([filter]) => (
              <Chip
                key={filter}
                label={getFilterLabel(filter)}
                onDelete={() => handleDeleteFilter(category, filter)}
                sx={{
                  backgroundColor: theme.palette.primary.light,
                  borderRadius: 0,
                  padding: '16px 10px',
                  '& .MuiChip-label': {
                    padding: '0 25px 0 0',
                    typography: 'mobile_bodyLG',
                    color: theme.palette.secondary.dark,
                    textTransform: 'capitalize',
                  },
                }}
              />
            ))
        ) : null))}
      {!isMobile && hasActiveFilters() && (
        <Box
          sx={{
            display: { small: 'none', tablet: 'flex' },
            justifyContent: 'flex-end',
            width: '100%',
            mb: 1,
          }}
        >
          <FilterSubmitButton
            onClick={handleClearFilters}
            text="Clear all"
            variant="text"
            backgroundColor="unset"
            width="50%"
          />
        </Box>
      )}
    </Box>
  );
}

export default FilterChips;
