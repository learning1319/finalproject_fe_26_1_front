import { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  Tabs, Tab, Box, Typography,
} from '@mui/material';
import theme from '../../../themeStyle';
import { selectCategory, setCategory } from '../../../redux/slices/filterSlice.js';

const tabStyles = (value, tabValue) => ({
  backgroundColor: value === tabValue ? theme.palette.primary.main : 'transparent',
  color: value === tabValue ? theme.palette.primary.contrastText : theme.palette.text.primary,
  '&:hover': {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
  },
  textTransform: 'capitalize',
  typography: {
    small: theme.typography.mobile_bodyLG,
    tablet: theme.typography.tablet_bodyMD,
    desktop: theme.typography.desktop_bodyLG,
  },
});

const FilterTabs = () => {
  const dispatch = useDispatch();
  const category = useSelector(selectCategory);
  const [value, setValue] = useState(category);

  useEffect(() => {
    setValue(category);
  }, [category]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
    dispatch(setCategory(newValue));
  };

  return (
    <Box sx={{ width: '100%', display: 'flex', flexWrap: 'wrap' }}>
      <Tabs
        value={value}
        onChange={handleChange}
        сolor="inherit"
        variant="scrollable"
        scrollButtons="on"
        sx={{
          mb: { small: 3, tablet: 6 },
          backgroundColor: 'transparent',
          '& .MuiTabs-indicator': {
            backgroundColor: theme.palette.primary.main,
          },
        }}
        aria-label="scrollable auto tabs example"
      >
        <Tab
          value="shopAll"
          label={<Typography sx={tabStyles(value, 'shopAll')}>shop all</Typography>}
          sx={tabStyles(value, 'shopAll')}
        />
        <Tab
          value="dresses"
          label={<Typography sx={tabStyles(value, 'dresses')}>dresses</Typography>}
          sx={tabStyles(value, 'dresses')}
        />
        <Tab
          value="tops"
          label={<Typography sx={tabStyles(value, 'tops')}>tops</Typography>}
          sx={tabStyles(value, 'tops')}
        />
        <Tab
          value="pants"
          label={<Typography sx={tabStyles(value, 'pants')}>pants</Typography>}
          sx={tabStyles(value, 'pants')}
        />
        <Tab
          value="blouses"
          label={<Typography sx={tabStyles(value, 'blouses')}>blouses</Typography>}
          sx={tabStyles(value, 'blouses')}
        />
        <Tab
          value="pullovers"
          label={<Typography sx={tabStyles(value, 'pullovers')}>pullovers</Typography>}
          sx={tabStyles(value, 'pullovers')}
        />
        <Tab
          value="shorts"
          label={<Typography sx={tabStyles(value, 'shorts')}>shorts</Typography>}
          sx={tabStyles(value, 'shorts')}
        />
      </Tabs>
    </Box>
  );
};

export default FilterTabs;
