import { Box, Typography, Button } from '@mui/material';
import SearchOffIcon from '@mui/icons-material/SearchOff';
import { useDispatch } from 'react-redux';
import { clearFilters } from '../../../redux/slices/filterSlice.js';
import theme from '../../../themeStyle';

const NoSuchProducts = () => {
  const dispatch = useDispatch();

  const handleClearFilters = () => {
    dispatch(clearFilters());
  };

  return (
    <Box
      sx={{
        minHeight: { small: '35vh', tablet: 'unset' },
        minWidth: '320px',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        width: '100%',
        my: { small: 6, tablet: 18 },
      }}
    >
      <SearchOffIcon
        sx={{
          width: '50px',
          height: '50px',
          fill: theme.palette.primary.dark,
        }}
      />
      <Typography variant="mobile_bodyMD" color="black" sx={{ mt: 2 }}>
        No products match selected filters.
      </Typography>
      <Button
        variant="contained"
        color="primary"
        sx={{ mt: 2 }}
        onClick={handleClearFilters}
      >
        Clear Filters
      </Button>
    </Box>
  );
};

export default NoSuchProducts;
