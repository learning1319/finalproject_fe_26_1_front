import PropTypes from 'prop-types';
import {
  Box,
  CircularProgress,
  Button,
  Grid,
} from '@mui/material';
import FilterBarModal from '../FilterBarModal/FilterBarModal';
import FiltersBar from '../FilterBar/FiltersBar';
import NoSuchProducts from '../NoSuchProducts/NoSuchProducts';
import ProductsList from '../ProductsList/ProductsList';

const MainContent = ({
  isMobile, isLoading, products, hasMore, handleLoadMore, isPlusSize = false,
}) => (

  <Grid container spacing={2}>
    <Grid
      item
      small={12}
      mobile={12}
      middle={12}
      sx={{
        display: isMobile ? 'block' : 'none',
      }}
    >
      <Box
        sx={{
          mt: 2,
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <FilterBarModal isPlusSize={isPlusSize} />
      </Box>
    </Grid>
    <Grid
      item
      tablet={4}
      large={4}
      desktop={4}
      sx={{
        display: isMobile ? 'none' : 'block',
      }}
    >
      <FiltersBar isPlusSize={isPlusSize} />
    </Grid>
    <Grid
      item
      small={12}
      mobile={12}
      middle={12}
      tablet={8}
      large={8}
      desktop={8}
    >
      {isLoading ? (
        <Box
          sx={{
            mt: 2,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            minHeight: '45vh',
          }}
        >
          <CircularProgress />
        </Box>
      ) : products.length === 0 ? (
        <NoSuchProducts />
      ) : (
        <>
          <ProductsList />
          {products.length > 0 && hasMore && (
            <Box
              sx={{
                mt: 6,
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Button
                variant="contained"
                color="primary"
                onClick={handleLoadMore}
              >
                Load More
              </Button>
            </Box>
          )}
        </>
      )}
    </Grid>
  </Grid>
);

MainContent.propTypes = {
  isMobile: PropTypes.bool.isRequired,
  isLoading: PropTypes.bool.isRequired,
  products: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
  ).isRequired,
  hasMore: PropTypes.bool.isRequired,
  isPlusSize: PropTypes.bool,
  handleLoadMore: PropTypes.func.isRequired,
};

export default MainContent;
