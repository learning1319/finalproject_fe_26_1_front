import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import { Modal, Box } from '@mui/material';
import theme from '../../../../themeStyle';
import { selectFilterModalOpen, setFilterModalOpen } from '../../../../redux/slices/filterSlice.js';

const customScrollbarStyles = {
  '&::-webkit-scrollbar': {
    width: '4px',
  },
  '&::-webkit-scrollbar-thumb': {
    backgroundColor: theme.palette.primary.main,
    borderRadius: '4px',
  },
  '&::-webkit-scrollbar-thumb:hover': {
    backgroundColor: theme.palette.primary.dark,
  },
  '&::-webkit-scrollbar-track': {
    backgroundColor: theme.palette.primary.light,
  },
};

const FilterModal = ({ children }) => {
  const dispatch = useDispatch();
  const filterModalOpen = useSelector(selectFilterModalOpen);

  const handleClose = () => {
    dispatch(setFilterModalOpen(false));
  };

  return (
    <Modal
      open={filterModalOpen}
      onClose={handleClose}
      aria-labelledby="filter-modal-title"
      aria-describedby="filter-modal-description"
      sx={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden',
      }}
    >
      <Box
        sx={{
          bgcolor: 'background.paper',
          borderRadius: 1,
          boxShadow: 24,
          px: 4,
          py: 8,
          width: '100vw',
          height: '100vh',
          overflowY: 'auto',
          overflowX: 'auto',
          ...customScrollbarStyles,
          position: 'relative',
          boxSizing: 'border-box',
        }}
      >
        {children}
      </Box>
    </Modal>
  );
};

FilterModal.propTypes = {
  children: PropTypes.node.isRequired,
};

export default FilterModal;
