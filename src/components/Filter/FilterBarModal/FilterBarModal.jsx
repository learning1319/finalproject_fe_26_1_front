import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import FilterButton from '../../Buttons/FilterButton/FilterButton';
import FilterModal from './FilterModal/FilterModal';
import FiltersBar from '../FilterBar/FiltersBar';
import { setFilterModalOpen } from '../../../redux/slices/filterSlice.js';

const FilterBarModal = ({ isPlusSize }) => {
  const dispatch = useDispatch();

  const handleFilterModalClose = () => {
    dispatch(setFilterModalOpen(false));
  };

  return (
    <>
      <FilterButton />
      <FilterModal onClose={handleFilterModalClose}>
        <FiltersBar isPlusSize={isPlusSize} />
      </FilterModal>
    </>
  );
};

FilterBarModal.propTypes = {
  isPlusSize: PropTypes.bool,
};

export default FilterBarModal;
