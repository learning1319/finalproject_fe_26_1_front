import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';
import {
  Container,
  useMediaQuery,
} from '@mui/material';

import {
  fetchPlusSizeProducts,
  clearFilters,
  selectIsLoading,
  selectHasMore,
  selectProducts,
  setFilters,
  selectFilters,
  setStartPage,
  setCategory,
} from '../../redux/slices/filterSlice.js';
import theme from '../../themeStyle';
import loadMoreProducts from '../../redux/thunks.js';
import ImageSlider from './ImageSlider/ImageSlider';
import getBannersByCategory from '../../api/getBannersByCategory.js';
import MainContent from './MainContent/MainContent';

const CollectionPlusSize = () => {
  const dispatch = useDispatch();
  const products = useSelector(selectProducts);
  const isLoading = useSelector(selectIsLoading);
  const hasMore = useSelector(selectHasMore);
  const filters = useSelector(selectFilters);
  const location = useLocation();
  const navigate = useNavigate();
  const isMobile = useMediaQuery(theme.breakpoints.down('tablet'));
  const page = 'plusSize';
  const [banners, setBanners] = useState([]);

  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  }, []);

  useEffect(() => {
    const savedFilters = JSON.parse(localStorage.getItem('filters'));
    if (savedFilters) {
      dispatch(setFilters(savedFilters));
    }
  }, [dispatch]);

  useEffect(() => {
    if (location.search === '') {
      dispatch(clearFilters());
      dispatch(setCategory('shopAll'));
    }

    dispatch(fetchPlusSizeProducts());
  }, [dispatch, location.pathname]);

  useEffect(() => {
    const queryString = new URLSearchParams();
    Object.keys(filters).forEach((filterCategory) => {
      const activeFilters = Object.keys(filters[filterCategory])
        .filter((filter) => filters[filterCategory][filter]);
      if (activeFilters.length > 0) {
        queryString.set(filterCategory, activeFilters.join(','));
      }
    });
    queryString.set('plusSize', 'true');
    navigate(`${location.pathname}?${queryString.toString()}`, { replace: true });
    localStorage.setItem('filters', JSON.stringify(filters));
  }, [filters, location.pathname, navigate]);

  useEffect(() => {
    dispatch(setStartPage(1));
    dispatch(fetchPlusSizeProducts());
  }, [filters, dispatch]);

  useEffect(() => {
    const fetchBAnners = async () => {
      try {
        const data = await getBannersByCategory('plusSize');
        setBanners(data);
      } catch (err) {
        console.log(err);
      }
    };
    fetchBAnners();
  }, []);

  const handleLoadMore = () => {
    dispatch(loadMoreProducts(page));
  };

  return (
    <Container sx={{ mb: 6 }}>
      <ImageSlider banners={banners} />
      <MainContent
        isMobile={isMobile}
        isLoading={isLoading}
        products={products}
        hasMore={hasMore}
        handleLoadMore={handleLoadMore}
        isPlusSize
      />
    </Container>
  );
};

export default CollectionPlusSize;
