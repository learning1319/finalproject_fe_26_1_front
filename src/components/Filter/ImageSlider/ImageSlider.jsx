import { useState } from 'react';
import PropTypes from 'prop-types';
import {
  Container,
  ImageList,
  ImageListItem,
  useMediaQuery,
} from '@mui/material';
import theme from '../../../themeStyle';
import IconButtonNext from '../../Buttons/IconButtonNext/IconButtonNext';
import IconButtonPrev from '../../Buttons/IconButtonPrev/IconButtonPrev';

const ImageSlider = ({ banners = [] }) => {
  const isMobile = useMediaQuery(theme.breakpoints.down('tablet'));
  const cols = isMobile ? 1 : banners.length;
  const [currentIndex, setCurrentIndex] = useState(1);

  const handleNextImage = () => {
    setCurrentIndex((prev) => (prev === banners.length - 1 ? 0 : prev + 1));
  };

  const handlePrevImage = () => {
    setCurrentIndex((prev) => (prev === 0 ? banners.length - 1 : prev - 1));
  };

  return (
    <Container
      sx={{
        width: '100%',
        padding: 0,
        height: '35vh',
        display: 'flex',
        alignItems: 'center',
        position: 'relative',
        mb: 3,
      }}
    >
      <ImageList
        sx={{
          width: '100%',
          height: '100%',
          display: isMobile ? 'block' : 'flex',
          overflow: 'hidden',
        }}
        cols={cols}
      >
        {banners.map((item, index) => (
          <ImageListItem
            key={item.id}
            sx={{
              display: isMobile && index !== currentIndex ? 'none' : 'flex',
              flexBasis: isMobile ? '100%' : `${100 / cols}%`,
              flexShrink: 0,
              height: '100%',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <img
              src={item.imageUrl}
              alt={item.name}
              loading="lazy"
              style={{
                objectFit: 'cover',
                objectPosition: '50% 0',
                height: '100%',
                width: '100%',
              }}
            />
          </ImageListItem>
        ))}
        {isMobile && (
          <>
            <IconButtonPrev onClick={handlePrevImage} />
            <IconButtonNext onClick={handleNextImage} />
          </>
        )}
      </ImageList>
    </Container>
  );
};

ImageSlider.propTypes = {
  banners: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      imageUrl: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
    }),
  ).isRequired,
};

export default ImageSlider;
