import { Grid, Box } from '@mui/material';
import { useSelector } from 'react-redux';
import ProductCard from '../../ProductCard/ProductCard';
import { selectProducts } from '../../../redux/slices/filterSlice.js';

const ProductsList = () => {
  const products = useSelector(selectProducts);

  const productArray = Array.isArray(products) ? products : Object.values(products);

  return (
    <Grid container spacing={2}>
      {productArray.map((el) => (
        <Grid
          item
          small={12}
          mobile={6}
          middle={6}
          tablet={6}
          large={6}
          desktop={6}
          key={el.id}
        >
          <Box>
            <ProductCard product={el} />
          </Box>
        </Grid>
      ))}
    </Grid>
  );
};

export default ProductsList;
