import PropTypes from 'prop-types';
import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Typography,
  Checkbox,
  FormGroup,
  FormControlLabel,
  Box,
} from '@mui/material';
import theme from '../../../themeStyle';
import Plus from '../../../assets/svg/Plus/Plus';
import Minus from '../../../assets/svg/Minus/Minus';
import ColorCircle from '../../ColorCircle/ColorCircle';

function CustomAccordion({
  isExpanded,
  handleSetIsExpanded,
  filters,
  handleFilterChange,
  categoryName,
  title,
  categories,
}) {
  return (
    <Accordion
      expanded={isExpanded[categoryName]}
      onChange={handleSetIsExpanded(categoryName)}
      sx={{
        border: `1px solid ${theme.palette.primary.dark}`,
        borderRadius: 0,
        mb: 2,
        '&:last-of-type': {
          borderBottomLeftRadius: 0,
          borderBottomRightRadius: 0,
        },
        '&:first-of-type': {
          borderTopLeftRadius: 0,
          borderTopRightRadius: 0,
        },
      }}
    >
      <AccordionSummary
        expandIcon={isExpanded[categoryName] ? <Minus /> : <Plus />}
        sx={{
          backgroundColor: isExpanded[categoryName]
            ? 'transparent'
            : theme.palette.primary.dark,
          minHeight: '35px',
          height: '40px',
        }}
      >
        <Typography
          variant="mobile_h3"
          sx={{
            color: isExpanded[categoryName]
              ? theme.palette.primary.dark
              : theme.palette.secondary.white,
          }}
        >
          {title}
        </Typography>
      </AccordionSummary>
      <AccordionDetails>
        <FormGroup>
          {categories.map((category) => (
            <FormControlLabel
              key={category.name}
              control={(
                <Checkbox
                  checked={filters[categoryName][category.name] || false}
                  onChange={handleFilterChange}
                  name={category.name}
                  data-category={categoryName}
                />
              )}
              label={(
                <Box
                  sx={{
                    display: 'flex',
                    alignItems: 'center',
                  }}
                >
                  {categoryName === 'color' && (
                    <ColorCircle colorName={category.name} />
                  )}
                  {category.label}
                </Box>
              )}
            />
          ))}
        </FormGroup>
      </AccordionDetails>
    </Accordion>
  );
}

CustomAccordion.propTypes = {
  isExpanded: PropTypes.objectOf(PropTypes.bool).isRequired,
  handleSetIsExpanded: PropTypes.func.isRequired,
  filters: PropTypes.shape({
    color: PropTypes.objectOf(PropTypes.bool).isRequired,
    size: PropTypes.objectOf(PropTypes.bool).isRequired,
    sort: PropTypes.objectOf(PropTypes.bool).isRequired,
  }).isRequired,
  handleFilterChange: PropTypes.func.isRequired,
  categoryName: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  categories: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
      value: PropTypes.string,
    }),
  ).isRequired,
};

export default CustomAccordion;
