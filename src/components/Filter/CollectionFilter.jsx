import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';
import {
  Container,
  useMediaQuery,
} from '@mui/material';

import FilterTabs from './FilterTabs/Tabs';
import {
  fetchProducts,
  clearFilters,
  selectIsLoading,
  selectHasMore,
  selectProducts,
  setFilters,
  selectFilters,
  setStartPage,
  setCategory,
  selectCategory,
} from '../../redux/slices/filterSlice.js';
import theme from '../../themeStyle';
import loadMoreProducts from '../../redux/thunks.js';
import MainContent from './MainContent/MainContent';

const CollectionFilter = () => {
  const dispatch = useDispatch();
  const products = useSelector(selectProducts);
  const isLoading = useSelector(selectIsLoading);
  const hasMore = useSelector(selectHasMore);
  const filters = useSelector(selectFilters);
  const category = useSelector(selectCategory);
  const location = useLocation();
  const navigate = useNavigate();
  const isMobile = useMediaQuery(theme.breakpoints.down('tablet'));

  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  }, []);

  useEffect(() => {
    const savedFilters = JSON.parse(localStorage.getItem('filters'));
    const savedCategory = localStorage.getItem('category');
    if (savedFilters) {
      dispatch(setFilters(savedFilters));
    }
    if (savedCategory) {
      dispatch(setCategory(savedCategory));
    }
  }, [dispatch]);

  useEffect(() => {
    if (location.search === '') {
      dispatch(clearFilters());
      dispatch(setCategory('shopAll'));
    }

    const params = new URLSearchParams(location.search);
    const newCategory = params.get('category') || 'shopAll';
    dispatch(setCategory(newCategory));
  }, [dispatch, location.search]);

  useEffect(() => {
    const queryString = new URLSearchParams();
    Object.keys(filters).forEach((filterCategory) => {
      const activeFilters = Object.keys(filters[filterCategory]).filter(
        (filter) => filters[filterCategory][filter],
      );
      if (activeFilters.length > 0) {
        queryString.set(filterCategory, activeFilters.join(','));
      }
    });
    queryString.set('category', category);
    navigate(`${location.pathname}?${queryString.toString()}`, {
      replace: true,
    });
    localStorage.setItem('filters', JSON.stringify(filters));
    localStorage.setItem('category', category);
  }, [category, filters, location.pathname, navigate]);

  useEffect(() => {
    dispatch(setStartPage(1));
    dispatch(fetchProducts());
  }, [category, filters, dispatch]);

  const handleLoadMore = () => {
    dispatch(loadMoreProducts());
  };

  return (
    <Container sx={{ mb: 6 }}>
      <FilterTabs />

      <MainContent
        isMobile={isMobile}
        isLoading={isLoading}
        products={products}
        hasMore={hasMore}
        handleLoadMore={handleLoadMore}
      />
    </Container>
  );
};

export default CollectionFilter;
