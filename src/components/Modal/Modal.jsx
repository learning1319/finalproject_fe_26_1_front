import {
  Box, IconButton, Typography, Modal,
} from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import PropTypes from 'prop-types';

const style = {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  bgcolor: 'background.paper',
  boxShadow: 24,
  p: 1,
  height: '45vh',
  width: {
    small: 0,
    mobile: 270,
    middle: 400,
    tablet: 550,
    large: 650,
    desktop: 750,
  },
};

const headerStyle = {
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  width: '100%',
};

const ModalWindow = ({
  open, title, main, footer, handleClose,
}) => (
  <Modal
    open={open}
    onClose={handleClose}
    aria-labelledby="modal-title"
    aria-describedby="modal-description"
  >
    <Box sx={style}>
      <Box sx={headerStyle}>
        <IconButton aria-label="close" color="black" onClick={handleClose} data-testid="closeBtn">
          <CloseIcon />
        </IconButton>
        <div />
      </Box>
      <Typography variant="h6" component="div" id="modal-title" sx={{ fontWeight: 700 }}>
        {title}
      </Typography>
      <Typography component="div" id="modal-main">
        {main}
      </Typography>
      <Typography component="div" id="modal-footer">
        {footer}
      </Typography>
    </Box>
  </Modal>
);

ModalWindow.propTypes = {
  open: PropTypes.bool.isRequired,
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.element, PropTypes.node]),
  main: PropTypes.oneOfType([PropTypes.element, PropTypes.node]),
  footer: PropTypes.oneOfType([PropTypes.element, PropTypes.node]),
  handleClose: PropTypes.func.isRequired,
};

export default ModalWindow;
