/* eslint-disable */
import { describe, test, expect } from '@jest/globals';
import {
  fireEvent, render, waitFor, screen,
} from '@testing-library/react';
import { Provider } from 'react-redux';
import userEvent from '@testing-library/user-event';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { BrowserRouter } from 'react-router-dom';
import { openEmailWindow, closeWelcomeWindow, openWelcomeWindow } from '../../redux/slices/modalSlice.js';
import { store } from '../../store.js';
import RegistrationAuthentication from '../RegistrationAuthentication/RegistrationAuthentication.js';
import ModalWindow from './Modal.js';
import Login from '../../pages/Login/Login.js';
import UserProfile from '../../pages/UserProfile/UserProfile.js';

const mock = new MockAdapter(axios);

const matcher = (content, element) => {
  const hasText = (node) => node.textContent.match(/log\s*in/i);
  const nodeHasText = hasText(element);
  const childrenDontHaveText = Array.from(element.children).every(
    (child) => !hasText(child),
  );
  return nodeHasText && childrenDontHaveText;
};
describe('email window opens', () => {
  test('email window opens', async () => {
    const { el } = render(
      <Provider store={store}>
        <RegistrationAuthentication />
        <ModalWindow open={store.getState().modal.showEmailWindow} title="Create Account" />
      </Provider>,
    );
    const btn = screen.getByText('Register Now');
    userEvent.click(btn);
    mock.onAny().reply(200);
    store.dispatch(openEmailWindow());
    await waitFor(() => expect(screen.getByText('Verify Your Email Address')).toBeInTheDocument());
    expect(el).toMatchSnapshot();
  });
  test('welcome window opens', async () => {
    const { l } = render(
      <BrowserRouter>
        <Provider store={store}>
          <Login />
          <UserProfile>
            <ModalWindow open={store.getState().modal.showWelcomeWindow} />
          </UserProfile>
        </Provider>
      </BrowserRouter>,
    );
    const btn = screen.getAllByText(matcher)[1];
    userEvent.click(btn);
    mock.onAny().reply(200);
    store.dispatch(openWelcomeWindow());

    await waitFor(() => {
      const modalElement = screen.getByRole('presentation');
      expect(modalElement).toBeInTheDocument();
    });
    expect(l).toMatchSnapshot();
  });
  test('email window closes', () => {
    const { w } = render(<ModalWindow open />);
    const element = screen.getByRole('presentation');
    const button = screen.getByTestId('closeBtn');
    fireEvent.click(button);
    expect(element).toHaveStyle('z-index: 1300');
    expect(w).toMatchSnapshot();
  });
  test('welcome window closes', async () => {
    store.dispatch(openWelcomeWindow());
    render(<ModalWindow open={store.getState().modal.showWelcomeWindow} />);
    const element = screen.getByRole('presentation');
    store.dispatch(closeWelcomeWindow());
    await expect(element).toHaveStyle('z-index: 1300');
  });
});
