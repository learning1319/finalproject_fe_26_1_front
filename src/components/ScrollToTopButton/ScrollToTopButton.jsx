import { useState, useEffect, useCallback } from 'react';
import { Button } from '@mui/material';
import { useDebounce } from 'use-debounce';
import ArrowUp from '../../assets/svg/ArrowUp/ArrowUp';
import theme from '../../themeStyle';

const ScrollToTopButton = () => {
  const [showButton, setShowButton] = useState(false);
  const [scrollY, setScrollY] = useState(window.scrollY);

  const [debouncedScrollY] = useDebounce(scrollY, 500);

  const handleScroll = useCallback(() => {
    setScrollY(window.scrollY);
  }, []);

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, [handleScroll]);

  useEffect(() => {
    if (debouncedScrollY > window.innerHeight * 2) {
      setShowButton(true);
    } else {
      setShowButton(false);
    }
  }, [debouncedScrollY]);

  const scrollToTop = () => {
    setShowButton(false);
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  };

  return (
    <Button
      onClick={scrollToTop}
      sx={{
        zIndex: 100,
        position: 'fixed',
        bottom: theme.spacing(2),
        right: theme.spacing(2),
        display: showButton ? 'block' : 'none',
        backgroundColor: theme.palette.primary.main,
        '&:hover': {
          backgroundColor: theme.palette.primary.dark,
        },
      }}
    >
      <ArrowUp />
    </Button>
  );
};

export default ScrollToTopButton;
