import {
  Box,
  Container,
  useTheme,
} from '@mui/material';
import EmailSubscription from './FooterComponents/EmailSubscription';
import FooterLinks from './FooterComponents/FooterLinks';
import SocialLinks from './FooterComponents/SocialLinks';

const Footer = () => {
  const theme = useTheme();

  return (
    <Box
      sx={{
        mt: 6,
        bgcolor: theme.palette.secondary.main,
        color: theme.palette.common.white,
        py: 4,
      }}
    >
      <Container>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            [theme.breakpoints.up('tablet')]: {
              flexDirection: 'row',
              justifyContent: 'space-between',
              // gap: '6px',
            },
            [theme.breakpoints.up('desktop')]: {
              gap: 2,
            },
          }}
        >
          <Box
            sx={{
              flex: '1 1 auto',
              [theme.breakpoints.down('tablet')]: {
                mb: 2,
              },
            }}
          >
            <EmailSubscription />
          </Box>
          <Box
            sx={{
              flex: '2 1 auto',
              display: 'flex',
              flexDirection: 'column',
              [theme.breakpoints.up('tablet')]: {
                flexDirection: 'row',
                gap: 2,
              },
              [theme.breakpoints.up('desktop')]: {
                gap: 3,
              },
            }}
          >
            <FooterLinks />
          </Box>
        </Box>
        <Box
          sx={{
            display: 'flex',
            justifyContent: 'center',
            flexDirection: 'column',
            mt: 2,
            [theme.breakpoints.up('tablet')]: {
              mt: 0,
            },
          }}
        >
          <SocialLinks />
        </Box>
      </Container>
    </Box>
  );
};

export default Footer;
