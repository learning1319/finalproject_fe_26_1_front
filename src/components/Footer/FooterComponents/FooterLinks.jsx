import {
  Typography,
  useTheme,
  Box,
} from '@mui/material';
import { Link } from 'react-router-dom';

function FooterLinks() {
  const theme = useTheme();
  const TypographyStyles = {
    ...theme.typography.mobile_bodyLG,
    mb: 1,
    [theme.breakpoints.up('tablet')]: {
      ...theme.typography.desktop_bodyLG,
      display: 'block',
      textAlign: 'left',
      color: '#FFFFFF',
      fontSize: '11px',
    },
    [theme.breakpoints.up('middle')]: {
      fontSize: '14px',
    },
    [theme.breakpoints.up('large')]: {
      fontSize: '13px',
    },
    [theme.breakpoints.up('desktop')]: {
      fontSize: '16px',
    },
    display: 'block',
    textAlign: 'left',
    color: '#FFFFFF',
  };
  const TitleStyles = {
    [theme.breakpoints.up('tablet')]: theme.typography.desktop_h5,
    pl: 0,
    fontSize: {
      small: '14px',
      middle: '15px',
      tablet: '12px',
      large: '15px',
      desktop: '17px',
    },
  };
  return (
    <Box
      sx={{
        display: 'flex',
        flexWrap: 'wrap',
        width: '100%',
        [theme.breakpoints.down('mobile')]: {
          alignItems: 'start',
          gap: 2,
        },
        [theme.breakpoints.up('mobile')]: {
          gap: '22px',
        },
        [theme.breakpoints.up('tablet')]: {
          mt: '13px',
          flexWrap: 'nowrap',
          gap: '9px',
        },
        [theme.breakpoints.up('large')]: {
          justifyContent: 'space-around',
        },
        [theme.breakpoints.up('desktop')]: {
          gap: 0,
        },
      }}
    >
      <Box sx={{ flex: '1 1 auto', mb: 1 }}>
        <Box
          sx={{
            mb: 3,
          }}
        >
          <Typography
            variant="mobile_h4"
            gutterBottom
            sx={TitleStyles}
          >
            About Modimal
          </Typography>
        </Box>
        <Link to="/termsConditions" style={{ textDecoration: 'none' }}>
          <Typography sx={TypographyStyles}>
            Terms & Condition
          </Typography>
        </Link>
        <Link to="/sustainability" style={{ textDecoration: 'none' }}>
          <Typography sx={TypographyStyles}>
            Sustainability
          </Typography>
        </Link>
        <Link to="/policy" style={{ textDecoration: 'none' }}>
          <Typography sx={TypographyStyles}>
            Privacy Policy
          </Typography>
        </Link>
      </Box>
      <Box sx={{ flex: '1 1 auto', mb: 1 }}>
        <Box
          sx={{
            mb: 3,
          }}
        >
          <Typography
            variant="mobile_h4"
            gutterBottom
            sx={TitleStyles}
          >
            Join Up
          </Typography>
        </Box>
        <Link to="/careers" style={{ textDecoration: 'none' }}>
          <Typography sx={TypographyStyles}>
            Careers
          </Typography>
        </Link>
        <Link to="/visitUs" style={{ textDecoration: 'none' }}>
          <Typography sx={TypographyStyles}>
            Visit Us
          </Typography>
        </Link>
      </Box>
      <Box sx={{ flex: '1 1 auto', mb: 1 }}>
        <Box
          sx={{
            mb: 3,
          }}
        >
          <Typography
            variant="mobile_h4"
            gutterBottom
            sx={TitleStyles}
          >
            Help & Support
          </Typography>
        </Box>
        <Link to="/orderShipping" style={{ textDecoration: 'none' }}>
          <Typography sx={TypographyStyles}>
            Orders & Shipping
          </Typography>
        </Link>
        <Link to="/returnsRefunds" style={{ textDecoration: 'none' }}>
          <Typography sx={TypographyStyles}>
            Returns & Refunds
          </Typography>
        </Link>
        <Link to="/contactUs" style={{ textDecoration: 'none' }}>
          <Typography sx={TypographyStyles}>
            Contact Us
          </Typography>
        </Link>
      </Box>
    </Box>
  );
}

export default FooterLinks;
