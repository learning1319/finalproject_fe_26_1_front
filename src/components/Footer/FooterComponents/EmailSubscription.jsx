import React, { useState } from 'react';
import {
  Box,
  TextField,
  Typography,
  Checkbox,
  FormControlLabel,
  InputAdornment,
  useTheme,
  IconButton,
  Alert,
} from '@mui/material';
import {
  Formik, Form, Field, ErrorMessage,
} from 'formik';
import * as Yup from 'yup';
import axios from 'axios';
import { CheckCircle as CheckCircleIcon } from '@mui/icons-material';
import ArrowRight from '../../../assets/svg/ArrowRight/ArrowRight';
import BASE_URL from '../../../constants/constants.js';

function EmailSubscription() {
  const theme = useTheme();
  const [isSubmitted, setIsSubmitted] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const [alertMessage, setAlertMessage] = useState('');
  const validationSchema = Yup.object().shape({
    email: Yup.string()
      .email('Invalid email address'),
    agreed: Yup.boolean()
      .oneOf([true], 'You must agree before submitting'),
  });

  const handleSubmit = async (values, { setSubmitting, resetForm }) => {
    try {
      const response = await axios.post(`${BASE_URL}/subscribers/`, { email: values.email });
      setAlertMessage(response.data.message);
      setIsSubmitted(true);
      resetForm();
    } catch (error) {
      setErrorMessage('Failed to subscribe. Please try again.');
    } finally {
      setSubmitting(false);
    }
  };

  return (
    <Box sx={{ px: { tablet: 1 } }}>
      {isSubmitted ? (
        <Alert
          severity="success"
          icon={<CheckCircleIcon style={{ color: theme.palette.secondary.white }} />}
          sx={{
            my: 2,
            backgroundColor: theme.palette.secondary.light,
            color: theme.palette.secondary.white,
            width: { small: '89%', tablet: '90%', desktop: '87%' },
          }}
        >
          {alertMessage}
        </Alert>
      ) : (
        <Formik
          initialValues={{ email: '', agreed: false }}
          validationSchema={validationSchema}
          onSubmit={handleSubmit}
        >
          {({
            handleChange, handleBlur, values, submitForm, errors, touched,
          }) => (
            <Form>
              <Box
                sx={{
                  width: '100%',
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'start',
                  textAlign: {
                    mobile: 'start',
                    middle: 'center',
                    tablet: 'start',
                  },
                  mb: 1,
                  [theme.breakpoints.up('middle')]: {
                    mb: 2,
                  },
                }}
              >
                <Typography
                  gutterBottom
                  sx={{
                    ...theme.typography.mobile_h4,
                    mt: 2,
                    fontWeight: {
                      mobile: '400',
                      desktop: '500',
                    },
                    fontSize: {
                      mobile: '12px',
                      middle: '14px',
                      tablet: '14px',
                      desktop: '16px',
                    },
                  }}
                >
                  Join Our Club, Get 15% Off For Your Birthday
                </Typography>
                <Field
                  as={TextField}
                  name="email"
                  size="small"
                  variant="outlined"
                  placeholder="Enter Your Email Address"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.email}
                  error={touched.email && Boolean(errors.email)}
                  helperText={<ErrorMessage name="email" component="div" />}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton onClick={submitForm} edge="end">
                          <ArrowRight />
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                  sx={{
                    ...theme.typography.mobile_bodyXS,
                    bgcolor: theme.palette.secondary.main,
                    mt: 2,
                    mb: 2,
                    width: '94%',
                    maxWidth: '100%',
                    '& .MuiInputBase-input::placeholder': {
                      color: 'white',
                      fontSize: '13px',
                      fontFamily: 'Montserrat, sans-serif',
                    },
                    [theme.breakpoints.down('mobile')]: {
                      width: '89%',
                    },
                    [theme.breakpoints.up('mobile')]: {
                      ...theme.typography.desktop_bodyMD,
                    },
                    [theme.breakpoints.up('tablet')]: {
                      ...theme.typography.desktop_bodyLG,
                      width: '93%',
                    },
                    [theme.breakpoints.up('desktop')]: {
                      ...theme.typography.desktop_bodyLG,
                      width: '87%',
                    },
                    '& .MuiOutlinedInput-root': {
                      '& fieldset': {
                        borderColor: theme.palette.secondary.white,
                      },
                      '&:hover fieldset': {
                        borderColor: 'white',
                      },
                      '&.Mui-focused fieldset': {
                        borderColor: 'white',
                      },
                      '&.Mui-error fieldset': {
                        borderColor: theme.palette.error.main,
                      },
                    },
                  }}
                />
                <FormControlLabel
                  control={(
                    <Field
                      as={Checkbox}
                      name="agreed"
                      sx={{
                        color: 'white',
                        '&.Mui-checked': {
                          color: 'white',
                        },
                        '& .MuiSvgIcon-root': {
                          fill: 'white',
                        },
                        '&.MuiCheckbox-indeterminate .MuiSvgIcon-root': {
                          fill: 'white',
                        },
                      }}
                    />
                  )}
                  sx={{
                    width: '100%',
                    display: 'flex',
                    justifyContent: {
                      mobile: 'center',
                      middle: 'flex-start',
                    },
                  }}
                  label={(
                    <Typography
                      variant="mobile_overline"
                      sx={{
                        color: theme.palette.common.white,
                        display: 'block',
                        textAlign: 'left',
                        fontWeight: '500',
                        [theme.breakpoints.up('tablet')]: {
                          ...theme.typography.desktop_overlineSM,
                          fontSize: '8px',
                        },
                        [theme.breakpoints.up('large')]: {
                          fontSize: '10px',
                        },
                      }}
                    >
                      By Submitting Your Email, You Agree To
                      Receive Advertising Emails From Modimal
                    </Typography>
                  )}
                />
                <ErrorMessage
                  name="agreed"
                  component={Typography}
                  variant="mobile_bodySM"
                  sx={{ color: theme.palette.error.main, mt: 1, p: 1 }}
                />
              </Box>
              {errorMessage && (
                <Alert
                  severity="error"
                  sx={{
                    mt: 2,
                    width: { small: '89%', tablet: '75%', desktop: '87%' },
                  }}
                >
                  {errorMessage}
                </Alert>
              )}
            </Form>
          )}
        </Formik>
      )}
    </Box>
  );
}

export default EmailSubscription;
