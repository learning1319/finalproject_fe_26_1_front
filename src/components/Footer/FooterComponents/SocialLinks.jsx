import {
  Box,
  Link as MuiLink,
  Typography,
  useTheme,
} from '@mui/material';
import Instagram from '../../../assets/svg/Instagram/Instagram';
import Facebook from '../../../assets/svg/Facebook/Facebook';
import Pinterest from '../../../assets/svg/Pinterest/Pinterest';
import TikTok from '../../../assets/svg/TikTok/TikTok';
import hoverEffect from '../../../utils/hoverEffect.js';

function SocialLinks() {
  const theme = useTheme();

  return (
    <Box sx={{
      display: 'flex',
      flexDirection: 'column',
      alignItems: {
        small: 'center',
        tablet: 'flex-start',
      },
    }}
    >
      <Box sx={{
        display: 'flex',
        mt: 2,
        mb: 2,
        justifyContent: {
          mobile: 'center',
          desktop: 'flex-start',
        },
      }}
      >
        <MuiLink
          href="https://www.instagram.com"
          target="_blank"
          underline="none"
          sx={{
            p: 1,
            ...hoverEffect(theme),
          }}
        >
          <Instagram />
        </MuiLink>
        <MuiLink
          href="https://www.facebook.com"
          target="_blank"
          underline="none"
          sx={{
            p: 1,
            ...hoverEffect(theme),
          }}
        >
          <Facebook />
        </MuiLink>
        <MuiLink
          href="https://www.pinterest.com"
          target="_blank"
          underline="none"
          sx={{
            p: 1,
            ...hoverEffect(theme),
          }}
        >
          <Pinterest />
        </MuiLink>
        <MuiLink
          href="https://www.tiktok.com"
          target="_blank"
          underline="none"
          sx={{
            p: 1,
            ...hoverEffect(theme),
          }}
        >
          <TikTok />
        </MuiLink>
      </Box>
      <Typography
        variant="mobile_captionSM"
        sx={{
          ...theme.typography.mobile_captionSM,
          color: '#CBCBCB',
          pl: {
            tablet: 1,
          },
        }}
      >
        &copy;
        {' '}
        {new Date().getFullYear()}
        {' '}
        Modimal. All Rights Reserved.
      </Typography>
    </Box>
  );
}

export default SocialLinks;
