import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  Container, Typography, Grid, Box, Button,
} from '@mui/material';
import {
  fetchFavorites, clearAllFavorites, clearFavorites,
} from '../../redux/slices/favoriteSlice.js';
import ProductCard from '../ProductCard/ProductCard';

const Wishlist = () => {
  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  }, []);

  const dispatch = useDispatch();
  const isLogged = useSelector((state) => state.login.isLogged);
  const favoriteProducts = useSelector((state) => state.favorites.items) || [];
  const token = useSelector((state) => state.login.token);
  const user = useSelector((state) => state.login.user);

  useEffect(() => {
    if (isLogged && user) {
      dispatch(fetchFavorites(user.id, token));
    }
  }, [isLogged, user, dispatch]);

  const handleClearAll = () => {
    if (isLogged && user) {
      dispatch(clearAllFavorites(user._id, token));
    } else {
      dispatch(clearFavorites());
    }
  };

  return (
    <Container sx={{ mb: 4, minHeight: '45vh' }}>
      <Typography
        sx={{
          textAlign: 'center',
          typography: 'mobile_h2',
          mt: 2,
          mb: 4,
        }}
      >
        My Wish List
      </Typography>
      <Typography
        sx={{
          mb: 3,
          textAlign: 'center',
          typography: {
            desktop: 'desktop_bodyMD',
            tablet: 'desktop_bodyMD',
            mobile: 'mobile_bodyMD',
          },
        }}
      >
        {`${favoriteProducts.length} item${favoriteProducts.length !== 1 ? 's' : ''}`}
      </Typography>
      {favoriteProducts.length > 3 && (
        <Box
          sx={{
            textAlign: {
              small: 'center',
              middle: 'right',
            },
            mb: 2,
            mr: {
              small: 0,
              middle: 4,
            },
          }}
        >
          <Button variant="text" color="primary" onClick={handleClearAll}>
            Clear All
          </Button>
        </Box>
      )}
      <Box>
        <Grid container spacing={3}>
          {favoriteProducts.map((product) => (
            product ? (
              <Grid
                item
                small={12}
                mobile={6}
                middle={6}
                tablet={4}
                large={4}
                desktop={4}
                key={product.id}
              >
                <Box>
                  <ProductCard product={product} />
                </Box>
              </Grid>
            ) : null
          ))}
        </Grid>
      </Box>
    </Container>
  );
};

export default Wishlist;
