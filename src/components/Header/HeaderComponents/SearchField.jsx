import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useImmer } from 'use-immer';
import { Formik, Field, Form } from 'formik';
import {
  Container, TextField, Box, Typography,
} from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import { useDebounce } from 'use-debounce';
import theme from '../../../themeStyle';
import getSearchedProducts from '../../../api/getSearchedProducts.js';
import ProductsList from './ProductsList';

const SearchField = ({ isOpen, setIsOpen, onOpen }) => {
  const [searchValue, setSearchValue] = useState('');
  const [debouncedSearch] = useDebounce(searchValue, 500);
  const [products, setProducts] = useImmer([]);
  const [hasFetched, setHasFetched] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      if (debouncedSearch) {
        try {
          const { data } = await getSearchedProducts(debouncedSearch);
          setProducts(data);
        } catch (error) {
          console.error('Error fetching search results:', error);
          setProducts([]);
        } finally {
          setHasFetched(true);
        }
      }
    };
    fetchData();
  }, [debouncedSearch, setProducts]);

  useEffect(() => {
    if (!isOpen) {
      setProducts([]);
      setHasFetched(false);
    }
  }, [isOpen]);

  return (
    <Box
      sx={{
        position: 'absolute',
        top: '100%',
        left: 0,
        right: 0,
        width: '100%',
        backgroundColor: 'white',
        transition: 'height 0.3s ease-in-out',
        height: isOpen ? 'auto' : 0,
        overflow: 'hidden',
        zIndex: 11,
        paddingTop: isOpen ? '8px' : 0,
        paddingBottom: isOpen ? '24px' : 0,
        display: 'flex',
        justifyContent: 'center',
        minWidth: '280px',
      }}
    >
      {isOpen && (
        <Container
          sx={{
            padding: '20px',
            minWidth: '280px',
          }}
        >
          <Formik
            initialValues={{ search: '' }}
            onSubmit={() => {
            }}
          >
            {({
              values, handleChange, handleSubmit, setFieldValue,
            }) => (
              <Form
                onSubmit={(e) => {
                  e.preventDefault();
                  handleSubmit();
                }}
              >
                <Field name="search">
                  {({ field }) => (
                    <TextField
                      // eslint-disable-next-line
                      {...field}
                      id="search-input"
                      label={(
                        <Box sx={{ display: 'flex', alignItems: 'center' }}>
                          <SearchIcon sx={{ marginRight: '8px', position: 'relative', top: '2px' }} />
                          <Typography
                            sx={{
                              typography: {
                                desktop: 'desktop_bodyLG',
                                tablet: 'desktop_bodyMD',
                                small: 'mobile_bodyMD',
                              },
                            }}
                          >
                            Search
                          </Typography>
                        </Box>
                      )}
                      variant="outlined"
                      fullWidth
                      autoFocus
                      value={values.search}
                      onFocus={() => {
                        if (onOpen) onOpen();
                      }}
                      onChange={(e) => {
                        handleChange(e);
                        setSearchValue(e.target.value);
                        handleSubmit();
                      }}
                      onBlur={() => {
                        setFieldValue('search', '');
                      }}
                      sx={{
                        '& .MuiOutlinedInput-root': {
                          borderRadius: 0,
                          '& fieldset': {
                            border: 'none',
                          },
                          '&:hover fieldset': {
                            border: 'none',
                          },
                          '&.Mui-focused fieldset': {
                            border: 'none',
                          },
                        },
                        '& .MuiInputBase-root': {
                          borderRadius: 0,
                          borderBottom: '2px solid',
                          borderColor: theme.palette.secondary.gray,
                        },
                        '& .MuiInputLabel-root': {
                          display: 'flex',
                          alignItems: 'center',
                          '&.Mui-focused': {
                            color: theme.palette.secondary.gray,
                          },
                          '& .MuiInputLabel-shrink': {
                            transform: 'translate(0, 1.5px) scale(0.75)',
                          },
                        },
                        '& .MuiInputLabel-outlined': {
                          transform: 'translate(14px, 14px) scale(1)',
                          '&.Mui-focused': {
                            transform: 'translate(14px, -6px) scale(0.75)',
                          },
                        },
                      }}
                    />
                  )}
                </Field>
              </Form>
            )}
          </Formik>
          {isOpen && (
            <Container
              sx={{
                width: '100%',
                backgroundColor: 'white',
                zIndex: 11,
                py: 3,
                px: 0,
                minWidth: 0,
              }}
            >
              <ProductsList products={products} hasFetched={hasFetched} setIsOpen={setIsOpen} />
            </Container>
          )}
        </Container>
      )}
    </Box>
  );
};

SearchField.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  setIsOpen: PropTypes.func.isRequired,
  onOpen: PropTypes.func.isRequired,
};

export default SearchField;
