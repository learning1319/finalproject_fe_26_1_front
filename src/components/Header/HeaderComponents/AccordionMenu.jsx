import {
  Box, Button, Menu, Divider,
} from '@mui/material';
import { Link } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import PersonIcon from '@mui/icons-material/Person';
import LogoutIcon from '@mui/icons-material/Logout';
import LoginIcon from '@mui/icons-material/Login';
import PersonAddIcon from '@mui/icons-material/PersonAdd';
import { loggedOut } from '../../../redux/slices/loginSlice.js';
import { ReactComponent as BurgerIcon } from '../../../assets/svg/menu.svg';
import { ReactComponent as CrossIcon } from '../../../assets/svg/cross.svg';
import CustomLink from '../../CustomLink/CusromLink';

const links = [
  { to: '/collection', children: 'Collection' },
  { to: '/newIn', children: 'New In' },
  { to: '/modiweek', children: 'Modiweek' },
  { to: '/plusSize', children: 'Plus Size' },
  { to: '/sustainability', children: 'Sustainability' },
];

const AccordionMenu = () => {
  const [anchorElNav, setAnchorElNav] = useState(null);

  useEffect(() => {
    if (anchorElNav) {
      document.body.style.overflow = 'hidden';
    } else {
      document.body.style.overflow = '';
    }
    return () => {
      document.body.style.overflow = '';
    };
  }, [anchorElNav]);

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const userIsLogged = useSelector((state) => state.login.isLogged);
  const dispatch = useDispatch();
  const logged = () => {
    dispatch(loggedOut());
    handleCloseNavMenu();
  };

  return (
    <Box
      sx={{
        display: {
          tablet: 'none',
        },
      }}
    >
      <Box onClick={handleOpenNavMenu}>
        {anchorElNav ? (
          <CrossIcon />
        ) : (
          <BurgerIcon />
        )}
      </Box>

      <Menu
        id="menu-appbar"
        anchorEl={anchorElNav}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
        open={Boolean(anchorElNav)}
        onClose={handleCloseNavMenu}
        sx={{
          display: { tablet: 'none' },
          top: '51px',
          gap: '32px',
          background: 'white',
          height: 'fit-content',
          pb: 6,
          zIndex: 1100,
        }}
      >
        <Box
          sx={{
            display: {
              tablet: 'none',
              '& .MuiAccordionDetails-root a': {
                textDecoration: 'none',
                paddingTop: '16px',
                color: '#404040',
                display: 'flex',
                flexDirection: 'column',
              },
              a: {
                textDecoration: 'none',
                color: '#404040',
              },
            },
          }}
        >
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
              gap: '8px',
              a: {
                typography: 'mobile_bodyLG',
              },
            }}
          >
            {links.map((link) => (
              <CustomLink key={link.to} to={link.to} onClick={handleCloseNavMenu}>
                {link.children}
              </CustomLink>
            ))}
          </Box>
        </Box>
        <Box sx={{ mt: 4 }}>
          <Divider />
          <Box
            typography="button_SM"
            sx={{
              display: 'flex',
              justifyContent: 'space-between',
              gap: '4px',
              mt: 4,
            }}
          >
            {userIsLogged ? (
              <>
                <Button
                  component={Link}
                  to="/userProfile"
                  onClick={handleCloseNavMenu}
                  variant="contained"
                  startIcon={<PersonIcon />}
                  sx={{ whiteSpace: 'nowrap', textTransform: 'none', width: '152px' }}
                >
                  User Profile
                </Button>
                <Button
                  component={Link}
                  to="/"
                  variant="contained"
                  onClick={logged}
                  startIcon={<LogoutIcon />}
                  sx={{ whiteSpace: 'nowrap', textTransform: 'none', width: '152px' }}
                >
                  Log Out
                </Button>
              </>
            ) : (
              <>
                <Button
                  component={Link}
                  to="/login"
                  onClick={handleCloseNavMenu}
                  variant="contained"
                  startIcon={<LoginIcon />}
                  sx={{
                    display: 'flex', width: '165px', whiteSpace: 'nowrap', textTransform: 'none',
                  }}
                >
                  Log In
                </Button>
                <Button
                  component={Link}
                  to="/registration"
                  onClick={handleCloseNavMenu}
                  variant="contained"
                  startIcon={<PersonAddIcon />}
                  sx={{ width: '165px', whiteSpace: 'nowrap', textTransform: 'none' }}
                >
                  Create Account
                </Button>
              </>
            )}
          </Box>
        </Box>
      </Menu>
      {anchorElNav && (
        <Box
          sx={{
            position: 'fixed',
            top: '51px',
            left: 0,
            width: '100%',
            height: 'calc(100% - 51px)',
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            backdropFilter: 'blur(8px)',
            zIndex: 1000,
          }}
          onClick={handleCloseNavMenu}
        />
      )}
    </Box>
  );
};

export default AccordionMenu;
