import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Box } from '@mui/material';
import { ReactComponent as Search } from '../../../svg/search.svg';
import { ReactComponent as CrossIcon } from '../../../svg/cross.svg';

const SearchItem = ({ setIsOpen, isOpen }) => {
  const [anchorElNav, setAnchorElNav] = useState(false);

  const handleOpenNavMenu = () => {
    setAnchorElNav((prev) => !prev);
    setIsOpen((prev) => !prev);
  };

  return (
    <Box
      onClick={handleOpenNavMenu}
      id="basic-button"
      aria-controls={anchorElNav ? 'basic-button' : undefined}
      aria-haspopup="true"
      aria-expanded={anchorElNav ? 'true' : undefined}
    >
      {isOpen ? (
        <CrossIcon />
      ) : (
        <Search />
      )}
    </Box>
  );
};

SearchItem.propTypes = {
  setIsOpen: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
};

export default SearchItem;
