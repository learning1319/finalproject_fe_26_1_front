/*eslint-disable */
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import {
  ListItemButton, ListItemText, useTheme, Box, useMediaQuery,
} from '@mui/material';
import { ReactComponent as Logo } from '../../../svg/logo.svg';
import { ReactComponent as Profile } from '../../../svg/profile.svg';
import { ReactComponent as Favorite } from '../../../svg/FavoriteIcon/FavoriteIcon.svg';
import { ReactComponent as Buy } from '../../../svg/buy.svg';
import SearchItem from './SearchItem';
import MiniCart from '../../../pages/Cart/MiniCart';
import Badge from '@mui/material/Badge';
import { useSelector } from 'react-redux';
import { useEffect, useState } from 'react';

const NavigateHeader = ({ isOpen, setIsOpen, isOpenCart, handleOpenCart }) => {
  const [favoriteQuantity, setFavoriteQuantity] = useState([])
  const favorites = useSelector((state) => state.favorites.items)
  useEffect(() => {
    setFavoriteQuantity(favorites);
  }, [favorites]);
  const [cartQuantity, setCartQuantity] = useState([])
  const cart = useSelector((state) => state.cart.items)
  useEffect(() => {
    setCartQuantity(cart || []);
  }, [cart]); 
  const cartLength = Array.isArray(cartQuantity) ? cartQuantity.length : 0; 
  const theme = useTheme();
  const isNotSmall = useMediaQuery(theme.breakpoints.between('tablet', 'large'));

  const linkStyles = {
    color: 'black',
    display: 'flex',
    padding: '0px',
    position: 'relative',
    '&:hover::before, &:hover::after, &.active::before, &.active::after': {
      width: '50%',
    },
    '&::before, &::after': {
      content: '""',
      position: 'absolute',
      bottom: '-2px',
      height: '2px',
      backgroundColor: theme.palette.primary.main,
      width: '0%',
      transition: 'width 0.3s ease',
    },
    '&::before': {
      left: '50%',
    },
    '&::after': {
      right: '50%',
    },
    '&:hover': {
      backgroundColor: 'transparent',
    },
  };

  return (
    <>
      <Box
        component={NavLink}
        to="/"
        alignContent="center"
        sx={{
          width: {
            tablet: '135px',
            desktop: '170px',
          },
          height: {
            tablet: '46px',
          },
          display: {
            small: 'none',
            tablet: 'flex',
          },
          alignItems: 'center',
        }}
      >
        <Logo />
      </Box>

      <Box
        sx={{
          display: {
            small: 'none',
            tablet: 'flex',
          },
          gap: {
            tablet: '16px',
            large: '24px',
            desktop: '32px',
          },
          typography: {
            small: 'mobile_bodyLG',
            tablet: 'mobile_bodyLG',
            desktop: 'desktop_bodyLG',
          },
          alignItems: 'center',
          position: 'relative',
        }}
      >
        <ListItemButton component={NavLink} to="/collection" sx={linkStyles}>
          <ListItemText>
            Collection
          </ListItemText>
        </ListItemButton>
        <ListItemButton component={NavLink} to="/newIn" sx={linkStyles}>
          <ListItemText>New In</ListItemText>
        </ListItemButton>

        <ListItemButton component={NavLink} to="/modiweek" sx={linkStyles}>
          <ListItemText>Modiweek</ListItemText>
        </ListItemButton>

        <ListItemButton component={NavLink} to="/plussize" sx={linkStyles}>
          <ListItemText>Plus Size</ListItemText>
        </ListItemButton>

        { !isNotSmall && (
        <ListItemButton component={NavLink} to="/sustainability" sx={linkStyles}>
          <ListItemText>Sustainability</ListItemText>
        </ListItemButton>
        )}
      </Box>

      <Box
        sx={{
          display: { small: 'none', tablet: 'flex' },
          gap: {
            tablet: '10px',
            desktop: '24px',
          },
          alignItems: 'center',
        }}
      >
        <SearchItem setIsOpen={setIsOpen} isOpen={isOpen} />
        <NavLink to="/userProfile"><Profile /></NavLink>
        <Badge sx={{
          '& .MuiBadge-badge': {
            right: -1,
            top: 4,
            border: `2px solid white`,
            padding: '0 4px',
          },
        }} badgeContent={favoriteQuantity.length} color="secondary">
          <NavLink to="/wishList"><Favorite /></NavLink>
        </Badge>
        <Badge sx={{
          '& .MuiBadge-badge': {
            right: -1,
            top: 4,
            border: `2px solid white`,
            padding: '0 4px',
          },
        }} badgeContent={cartLength} color="secondary">
          <Box
            onClick={handleOpenCart}
            sx={{ cursor: "pointer" }}
          >
            <Buy /></Box> </Badge>
        <MiniCart isOpenCart={isOpenCart} handleOpenCart={handleOpenCart} />
      </Box>
    </>
  );
};

NavigateHeader.propTypes = {
  setIsOpen: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
};

export default NavigateHeader;
