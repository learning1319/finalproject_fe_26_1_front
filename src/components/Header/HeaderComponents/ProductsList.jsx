import PropTypes from 'prop-types';
import {
  Box,
  Typography,
} from '@mui/material';
import SearchProductCard from './SearchProductCard';

const ProductsList = ({ products, hasFetched = false, setIsOpen }) => (
  <Box
    sx={{
      display: 'flex',
      overflowY: 'auto',
      flexDirection: 'column',
      maxHeight: '50vh',
      px: 1,
      '&::-webkit-scrollbar': {
        width: '8px',
      },
      '&::-webkit-scrollbar-thumb': {
        backgroundColor: '#888',
        borderRadius: '4px',
      },
      '&::-webkit-scrollbar-thumb:hover': {
        backgroundColor: '#555',
      },
    }}
  >
    {products.length > 0 && (
      products.map((product) => (
        <Box
          key={product.id}
          sx={{
            py: 1,
          }}
        >
          <SearchProductCard product={product} setIsOpen={setIsOpen} />
        </Box>
      ))
    )}
    {products.length === 0 && hasFetched && (
      <Typography
        variant="body1"
        sx={{
          padding: '8px',
          textAlign: 'center',
          color: 'black',
        }}
      >
        No products found
      </Typography>
    ) }
  </Box>
);

ProductsList.propTypes = {
  hasFetched: PropTypes.bool,
  setIsOpen: PropTypes.func.isRequired,
  products: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      description: PropTypes.string,
      image: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired,
      fabric: PropTypes.string,
      collection: PropTypes.string,
      type: PropTypes.string,
      category: PropTypes.string,
      new: PropTypes.bool,
      favorite: PropTypes.bool,
      products: PropTypes.arrayOf(PropTypes.shape({
        color: PropTypes.string.isRequired,
      })),
      _id: PropTypes.string.isRequired,
    }),
  ).isRequired,
};

export default ProductsList;
