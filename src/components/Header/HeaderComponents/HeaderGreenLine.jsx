import React from 'react';
import { Typography } from '@mui/material';
import theme from '../../../themeStyle';

const HeaderGreenLine = () => (
  <Typography
    color={theme.palette.secondary.white}
    sx={{
      typography: {
        small: 'mobile_bodyXS',
        tablet: 'desktop_overlineSM',
        desktop: 'desktop_overlineSM',
      },
      backgroundColor: theme.palette.primary.main,
      textTransform: 'capitalize',
      display: 'block',
      textAlign: 'center',
      alignContent: 'center',
      height: {
        small: '16px',
        tablet: '23px',
        large: '30px',
      },
      minWidth: '319px',
    }}
  >
    enjoy free shipping on all orders
  </Typography>
);

export default HeaderGreenLine;
