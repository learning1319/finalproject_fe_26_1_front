import PropTypes from 'prop-types';
import {
  Card,
  CardMedia,
  CardContent,
  Typography,
  Button,
  Box,
  useMediaQuery,
} from '@mui/material';
import { useNavigate } from 'react-router-dom';
import theme from '../../../themeStyle';

const SearchProductCard = ({ product, setIsOpen }) => {
  const isMobile = useMediaQuery(theme.breakpoints.down('tablet'));
  const isMiddle = useMediaQuery(theme.breakpoints.up('middle'));
  const navigate = useNavigate();

  const handleButtonClick = () => {
    setIsOpen(false);
    navigate(`/${product.id}`);
  };

  return (
    <Box
      sx={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
      }}
    >
      <Card
        sx={{
          display: 'flex',
          alignItems: 'start',
          overflowX: 'hidden',
          width: '100%',
          maxWidth: { tablet: '700px', desktop: '800px' },
          gap: { small: 1, tablet: 2 },
          position: 'relative',
          '&:hover .hoverButton': {
            opacity: 1,
          },
          height: { middle: 160, tablet: 180 },
        }}
      >
        <CardMedia
          component="img"
          image={product.image}
          alt={product.name}
          sx={{
            width: { small: 100, middle: 160 },
            height: { small: 130, middle: 160, tablet: 180 },
          }}
        />
        <CardContent
          sx={{
            display: 'flex',
            alignItems: 'space-between',
            justifyContent: 'space-between',
            p: 0,
            paddingBottom: 0,
            '&:last-child': {
              paddingBottom: 0,
            },
          }}
        >
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
              gap: 1,
              pr: 2,
            }}
          >
            <Typography
              sx={{
                typography: { small: 'mobile_bodyLG', tablet: 'desktop_bodyMD' },
                whiteSpace: 'wrap',
                overflow: 'hidden',
                textOverflow: 'ellipsis',
              }}
            >
              {product.name}
            </Typography>
            <Typography
              variant="mobile_bodySM"
              color="text.secondary"
              sx={{
                typography: { small: 'mobile_bodyLG', tablet: 'desktop_bodyMD' },
              }}
            >
              {`$${product.price}`}
            </Typography>
            {isMiddle && (
              <Typography
                variant="mobile_bodySM"
                color="text.secondary"
                sx={{
                  typography: { small: 'mobile_bodyLG', tablet: 'desktop_bodyMD' },
                  display: '-webkit-box',
                  WebkitBoxOrient: 'vertical',
                  overflow: 'hidden',
                  WebkitLineClamp: 2,
                  textOverflow: 'ellipsis',
                  textAlign: 'justify',
                }}
              >
                {product.description}
              </Typography>
            )}
          </Box>
        </CardContent>
        <Button
          onClick={handleButtonClick}
          variant="contained"
          color="primary"
          className="hoverButton"
          sx={{
            position: 'absolute',
            right: 0,
            bottom: 0,
            backgroundColor: 'primary.main',
            color: 'white',
            opacity: 1,
            transition: 'opacity 0.3s',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            ml: isMobile ? 0 : 2,
            textTransform: 'capitalize',
          }}
        >
          See More
        </Button>
      </Card>
    </Box>
  );
};

SearchProductCard.propTypes = {
  setIsOpen: PropTypes.func.isRequired,
  product: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string,
    image: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    fabric: PropTypes.string,
    collection: PropTypes.string,
    type: PropTypes.string,
    category: PropTypes.string,
    new: PropTypes.bool,
    favorite: PropTypes.bool,
    products: PropTypes.arrayOf(PropTypes.shape({
      color: PropTypes.string.isRequired,
    })),
    _id: PropTypes.string.isRequired,
  }).isRequired,
};

export default SearchProductCard;
