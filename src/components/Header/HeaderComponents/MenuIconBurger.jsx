import PropTypes from 'prop-types';
import { Box } from '@mui/material';
import { NavLink } from 'react-router-dom';
import { useEffect, useState } from 'react';
import Badge from '@mui/material/Badge';
import { useSelector } from 'react-redux';
import { ReactComponent as Logo } from '../../../svg/logo.svg';
import { ReactComponent as Favorite } from '../../../svg/FavoriteIcon/FavoriteIcon.svg';
import { ReactComponent as Buy } from '../../../svg/buy.svg';
import AccordionMenu from './AccordionMenu';
import MiniCart from '../../../pages/Cart/MiniCart';
import SearchItem from './SearchItem';

const MenuIconBurger = ({
  setIsOpen, isOpen, handleOpenCart, isOpenCart,
}) => {
  const [favoriteQuantity, setFavoriteQuantity] = useState([]);

  const favorites = useSelector((state) => state.favorites.items);
  useEffect(() => {
    setFavoriteQuantity(favorites);
  }, [favorites]);
  const [cartQuantity, setCartQuantity] = useState([]);
  const cart = useSelector((state) => state.cart.items);
  useEffect(() => {
    setCartQuantity(cart || []);
  }, [cart]);
  const cartLength = Array.isArray(cartQuantity) ? cartQuantity.length : 0;
  return (
    <>
      <Box sx={{
        display: {
          small: 'flex',
          tablet: 'none',
        },
        gap: {
          small: '8px',
        },
        alignItems: {
          small: 'center',
        },

      }}
      >
        <AccordionMenu />
        <SearchItem setIsOpen={setIsOpen} isOpen={isOpen} />
      </Box>
      <Box
        component={NavLink}
        to="/"
        sx={{
          width: {
            small: '138px',
            middle: '150px',

          },
          height: {
            small: '40px',
          },
          display: {
            tablet: 'none',
          },
          alignItems: 'center',
        }}
      >
        <Logo />
      </Box>
      <Box sx={{
        display: {
          small: 'flex',
          tablet: 'none',
        },
        alignItems: 'center',
        gap: '8px',
      }}
      >

        <Badge
          sx={{
            '& .MuiBadge-badge': {
              right: -1,
              top: 4,
              border: '2px solid white',
              padding: '0 4px',
            },
          }}
          badgeContent={favoriteQuantity.length}
          color="secondary"
        >
          <NavLink to="/favorites"><Favorite /></NavLink>
        </Badge>

        <Badge
          sx={{
            '& .MuiBadge-badge': {
              right: -1,
              top: 4,
              border: '2px solid white',
              padding: '0 4px',
            },
          }}
          badgeContent={cartLength}
          color="secondary"
        >
          <Box onClick={handleOpenCart}><Buy /></Box>
        </Badge>
        <MiniCart handleOpenCart={handleOpenCart} isOpenCart={isOpenCart} />
      </Box>

    </>
  );
};

MenuIconBurger.propTypes = {
  setIsOpen: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  handleOpenCart: PropTypes.func,
  isOpenCart: PropTypes.bool,

};

export default MenuIconBurger;
