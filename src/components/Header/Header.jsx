import { useState, useEffect, useRef } from 'react';
import { Container, Box } from '@mui/material';
import { useDebounce } from 'use-debounce';
import HeaderGreenLine from './HeaderComponents/HeaderGreenLine';
import MenuIconBurger from './HeaderComponents/MenuIconBurger';
import NavigateHeader from './HeaderComponents/NavigateHeader';
import SearchField from './HeaderComponents/SearchField';

const Header = () => {
  const [scrollY, setScrollY] = useState(window.scrollY);
  const [debouncedScrollY] = useDebounce(scrollY, 100);
  const [elevated, setElevated] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [isOpenCart, setIsOpenCart] = useState(false);

  const handleOpenCart = () => {
    setIsOpenCart(!isOpenCart);
  };
  const openingRef = useRef(false);

  useEffect(() => {
    const handleScroll = () => {
      setScrollY(window.scrollY);
    };

    window.addEventListener('scroll', handleScroll);
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  useEffect(() => {
    if (debouncedScrollY > 150) {
      setElevated(true);
    } else {
      setElevated(false);
    }
  }, [debouncedScrollY]);

  const handleClickOutside = (event) => {
    if (isOpen && !openingRef.current && !event.target.closest('.search-field')) {
      setIsOpen(false);
    }
    openingRef.current = false;
  };

  useEffect(() => {
    window.addEventListener('click', handleClickOutside);
    return () => {
      window.removeEventListener('click', handleClickOutside);
    };
  }, [isOpen]);

  const handleOpenSearch = () => {
    openingRef.current = true;
    setIsOpen(true);
  };

  return (
    <>
      {(isOpen || isOpenCart) && (
        <Box
          sx={{
            position: 'fixed',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            backdropFilter: 'blur(5px)',
            zIndex: 10,
          }}
          onClick={() => {
            // eslint-disable-next-line no-unused-expressions
            setIsOpen(false);
            setIsOpenCart(false);
          }}
        />
      )}
      <Box
        sx={{
          position: 'sticky',
          top: '0px',
          zIndex: '10',
          background: 'white',
          width: '100%',
          boxShadow: elevated ? '0 4px 8px rgba(0, 0, 0, 0.2)' : 'none',
        }}
      >
        <HeaderGreenLine sx={{ width: '100%' }} />
        <Container
          sx={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
            paddingTop: '4px',
            paddingBottom: '4px',
            height: {
              small: '40px',
              tablet: '60px',
              large: '80px',
            },
            // position: 'relative',
          }}
        >
          <MenuIconBurger
            setIsOpen={setIsOpen}
            isOpen={isOpen}
            isOpenCart={isOpenCart}
            handleOpenCart={handleOpenCart}
          />
          <NavigateHeader
            setIsOpen={setIsOpen}
            isOpen={isOpen}
            isOpenCart={isOpenCart}
            handleOpenCart={handleOpenCart}
          />
        </Container>
        <SearchField
          isOpen={isOpen}
          setIsOpen={setIsOpen}
          className="search-field"
          onOpen={handleOpenSearch}
        />
      </Box>
    </>
  );
};

export default Header;
