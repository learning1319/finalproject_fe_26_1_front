import React, { useEffect, useState } from 'react';
import {
  Box,
  Button,
  Typography,
  Checkbox,
  FormControlLabel,
  Grid,
} from '@mui/material';
import { Formik, Form, Field } from 'formik';
import { TextField } from 'formik-mui';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { addAddress, getAddress, setAddress } from '../../redux/slices/addressSlice.js';
import addressSchema from '../../utils/addressSchema.js';

const ShippingForm = () => {
  const isLogged = useSelector((state) => state.login.isLogged);
  const token = useSelector((state) => state.login.token);
  const currentUser = useSelector((state) => state.login.user);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [initialValues, setInitialValues] = useState({
    country: '',
    email: '',
    firstName: '',
    lastName: '',
    company: '',
    address: '',
    apartment: '',
    postalCode: '',
    city: '',
    phone: '',
    saveInfo: false,
    newsOffers: false,
  });

  useEffect(() => {
    const fetchAddress = async (userId, authToken) => {
      const address = await dispatch(getAddress(userId, authToken));
      console.log(address);
      if (address) {
        setInitialValues({
          email: address.email || '',
          firstName: address.firstName || '',
          lastName: address.lastName || '',
          country: address.country || '',
          company: address.company || '',
          address: address.addressLine1 || '',
          apartment: address.addressLine2 || '',
          postalCode: address.zip || '',
          city: address.city || '',
          phone: address.phone || '',
          saveInfo: false,
          newsOffers: false,
        });
      }
    };
    if (isLogged) {
      fetchAddress(currentUser._id, token);
    }
  }, [dispatch, isLogged, currentUser, token]);

  const handleReturnToCart = () => {
    navigate('/cart');
  };

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={addressSchema}
      enableReinitialize
      onSubmit={async (values) => {
        if (isLogged) {
          await addAddress(
            currentUser._id,
            values.email,
            values.firstName,
            values.lastName,
            values.company,
            values.address,
            values.apartment,
            values.postalCode,
            values.country,
            values.city,
            values.phone,
            token,
          );

          dispatch(
            setAddress({
              email: values.email,
              firstName: values.firstName,
              lastName: values.lastName,
              country: values.country,
              company: values.company,
              addressLine1: values.address,
              addressLine2: values.apartment,
              zip: values.postalCode,
              city: values.city,
              phone: values.phone,
            }),
          );

          navigate('/payment');
        } else {
          console.log('Not logged in');
          // Show a message to log in
        }
      }}
    >
      {({
        values, handleChange, touched, errors,
      }) => (
        <Box
          sx={{
            p: 0,
            mb: 5,
            display: 'flex',
            justifyContent: 'center',
          }}
        >
          <Form
            style={{
              maxWidth: '600px',
              width: '100%',
            }}
          >
            <Grid
              container
              spacing={2}
              sx={{
                display: 'flex',
                flexDirection: 'column',
                width: '100%',
                margin: 'auto',
                '& > .MuiGrid-item': {
                  paddingLeft: 0,
                  paddingTop: 0,
                },
              }}
            >
              <Typography
                variant="h5"
                sx={{ marginBottom: '20px', textAlign: 'center' }}
              >
                Contact
              </Typography>
              <Grid item xs={12}>
                <Field
                  component={TextField}
                  fullWidth
                  label="Email"
                  name="email"
                  variant="outlined"
                  value={values.email}
                  onChange={handleChange}
                  error={touched.email && Boolean(errors.email)}
                  helperText={touched.email && errors.email}
                  sx={{ marginBottom: '20px' }}
                />
              </Grid>
              <Grid item xs={12}>
                <FormControlLabel
                  control={(
                    <Checkbox
                      name="newsOffers"
                      checked={values.newsOffers}
                      onChange={handleChange}
                    />
                        )}
                  label="Subscribe To News"
                  sx={{ display: 'flex', textAlign: 'start', alignItems: 'center' }}
                />
              </Grid>
              <Typography
                variant="h5"
                sx={{ margin: '20px 0', textAlign: 'center' }}
              >
                Shipping Address
              </Typography>
              <Grid item xs={12}>
                <Field
                  component={TextField}
                  fullWidth
                  label="Country/Region"
                  name="country"
                  variant="outlined"
                  value={values.country}
                  onChange={handleChange}
                        // InputLabelProps={{
                        //   shrink: true,
                        // }}
                  sx={{ marginBottom: '20px' }}
                />
              </Grid>
              <Grid item xs={12}>
                <Field
                  component={TextField}
                  fullWidth
                  label="First Name"
                  name="firstName"
                  variant="outlined"
                  value={values.firstName}
                  onChange={handleChange}
                  error={touched.firstName && Boolean(errors.firstName)}
                  helperText={touched.firstName && errors.firstName}
                  sx={{ marginBottom: '20px' }}
                />
              </Grid>
              <Grid item xs={12}>
                <Field
                  component={TextField}
                  fullWidth
                  label="Last Name"
                  name="lastName"
                  variant="outlined"
                  value={values.lastName}
                  onChange={handleChange}
                  error={touched.lastName && Boolean(errors.lastName)}
                  helperText={touched.lastName && errors.lastName}
                  sx={{ marginBottom: '20px' }}
                />
              </Grid>
              <Grid item xs={12}>
                <Field
                  component={TextField}
                  fullWidth
                  label="Company (Optional)"
                  name="company"
                  variant="outlined"
                  value={values.company}
                  onChange={handleChange}
                  sx={{ marginBottom: '20px' }}
                />
              </Grid>
              <Grid item xs={12}>
                <Field
                  component={TextField}
                  fullWidth
                  label="Address"
                  name="address"
                  variant="outlined"
                  value={values.address}
                  onChange={handleChange}
                  error={touched.address && Boolean(errors.address)}
                  helperText={touched.address && errors.address}
                  sx={{ marginBottom: '20px' }}
                />
              </Grid>
              <Grid item xs={12}>
                <Field
                  component={TextField}
                  fullWidth
                  label="Apartment, Suite, Etc. (Optional)"
                  name="apartment"
                  variant="outlined"
                  value={values.apartment}
                  onChange={handleChange}
                  sx={{ marginBottom: '20px' }}
                />
              </Grid>
              <Grid item xs={12}>
                <Field
                  component={TextField}
                  fullWidth
                  label="Postal Code"
                  name="postalCode"
                  variant="outlined"
                  value={values.postalCode}
                  onChange={handleChange}
                  error={touched.postalCode && Boolean(errors.postalCode)}
                  helperText={touched.postalCode && errors.postalCode}
                  sx={{ marginBottom: '20px' }}
                />
              </Grid>
              <Grid item xs={12}>
                <Field
                  component={TextField}
                  fullWidth
                  label="City"
                  name="city"
                  variant="outlined"
                  value={values.city}
                  onChange={handleChange}
                  error={touched.city && Boolean(errors.city)}
                  helperText={touched.city && errors.city}
                  sx={{ marginBottom: '20px' }}
                />
              </Grid>
              <Grid item xs={12}>
                <Field
                  component={TextField}
                  fullWidth
                  label="Phone"
                  name="phone"
                  variant="outlined"
                  value={values.phone}
                  onChange={handleChange}
                  error={touched.phone && Boolean(errors.phone)}
                  helperText={touched.phone && errors.phone}
                  sx={{ marginBottom: '20px' }}
                />
              </Grid>
              <Grid item xs={12}>
                <FormControlLabel
                  control={(
                    <Checkbox
                      name="saveInfo"
                      checked={values.saveInfo}
                      onChange={handleChange}
                    />
                        )}
                  label="Save This For Next Time"
                  sx={{ display: 'flex', textAlign: 'start' }}
                />
              </Grid>
              <Grid item xs={12}>
                <Box
                  sx={{
                    display: 'flex',
                    flexDirection: {
                      small: 'column', middle: 'row', tablet: 'column', large: 'row',
                    },
                    gap: 2,
                    justifyContent: 'space-between',
                    marginTop: '20px',
                  }}
                >
                  <Button onClick={handleReturnToCart} variant="outlined">Return To Cart</Button>
                  <Button variant="contained" color="primary" type="submit">
                    Continue Shipping
                  </Button>
                </Box>
              </Grid>
            </Grid>
          </Form>
        </Box>
      )}
    </Formik>
  );
};

export default ShippingForm;
