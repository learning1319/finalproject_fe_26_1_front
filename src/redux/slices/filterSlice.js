import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import getProductsByQuery from '../../api/getProductsByQuery.js';

const initialState = {
  filters: {
    color: {
      blue: false,
      black: false,
      red: false,
      white: false,
      beige: false,
      green: false,
      orange: false,
      gray: false,
      purple: false,
      brown: false,
    },
    size: {
      s: false, m: false, l: false, '1x': false, '2x': false, '3x': false,
    },
    sort: { '+price': false, '-price': false, bestSeller: false },
    fabric: {
      cotton: false,
      linen: false,
      silk: false,
      lace: false,
      chiffon: false,
      denim: false,
    },
  },
  products: [],
  category: 'shopAll',
  prevCategory: 'shopAll',
  isLoading: true,
  hasMore: true,
  startPage: 1,
  perPage: 6,
  isExpanded: {
    fabric: false, size: false, color: false, sort: false,
  },
  filterModalOpen: false,
};

const constructQueryString = (filtersState, isNewIn = false, isPlusSize = false) => {
  const {
    filters, category, startPage, perPage,
  } = filtersState;
  const params = new URLSearchParams();

  if (!isNewIn && category !== 'shopAll') {
    params.set('category', category);
  }

  params.set('perPage', perPage);
  params.set('startPage', startPage);

  if (isNewIn) {
    params.set('new', 'true');
  }

  if (isPlusSize) {
    params.set('plusSize', 'true');
  }

  Object.keys(filters).forEach((filterCategory) => {
    const activeFilters = Object.keys(filters[filterCategory]).filter(
      (filter) => filters[filterCategory][filter],
    );

    if (activeFilters.length > 0) {
      params.set(filterCategory, activeFilters.join(','));
    }
  });

  return params.toString();
};

export const fetchProducts = createAsyncThunk(
  'filters/fetchProducts',
  async (_, { getState }) => {
    const state = getState();
    const queryString = constructQueryString(state.filters);
    const response = await getProductsByQuery(queryString);
    return response;
  },
);

export const fetchNewInProducts = createAsyncThunk(
  'filters/fetchNewInProducts',
  async (_, { getState }) => {
    const state = getState();
    const queryString = constructQueryString(state.filters, true);
    const response = await getProductsByQuery(queryString);
    return response;
  },
);

export const fetchPlusSizeProducts = createAsyncThunk(
  'filters/fetchPlusSizeProducts',
  async (_, { getState }) => {
    const state = getState();
    const queryString = constructQueryString(state.filters, false, true);
    const response = await getProductsByQuery(queryString);
    return response;
  },
);

const filtersSlice = createSlice({
  name: 'filters',
  initialState,
  reducers: {
    setFilters(state, action) {
      state.filters = action.payload;
    },
    setCategory(state, action) {
      state.category = action.payload;
    },
    setStartPage(state, action) {
      state.startPage = action.payload;
    },
    setPrevCategory(state, action) {
      state.prevCategory = action.payload;
    },
    setIsLoading(state, action) {
      state.isLoading = action.payload;
    },
    setHasMore(state, action) {
      state.hasMore = action.payload;
    },
    setProducts(state, action) {
      if (state.startPage === 1) {
        state.products = action.payload;
      } else {
        state.products = [...state.products, ...action.payload];
      }
    },
    setIsExpanded(state, action) {
      state.isExpanded[action.payload.categoryName] = action.payload.isExpanded;
    },
    clearFilters(state) {
      state.filters = initialState.filters;
      state.isExpanded = initialState.isExpanded;
    },
    incrementPage(state) {
      state.startPage += 1;
    },
    setFilterModalOpen(state, action) {
      state.filterModalOpen = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchProducts.pending, (state) => {
        state.isLoading = true;
        state.hasMore = true;
      })
      .addCase(fetchProducts.fulfilled, (state, action) => {
        state.isLoading = false;
        if (action.payload.length < state.perPage) {
          state.hasMore = false;
        }
        filtersSlice.caseReducers.setProducts(state, { payload: action.payload });
      })
      .addCase(fetchProducts.rejected, (state) => {
        state.isLoading = false;
      })
      .addCase(fetchNewInProducts.pending, (state) => {
        state.isLoading = true;
        state.hasMore = true;
      })
      .addCase(fetchNewInProducts.fulfilled, (state, action) => {
        state.isLoading = false;
        if (action.payload.length < state.perPage) {
          state.hasMore = false;
        }
        filtersSlice.caseReducers.setProducts(state, { payload: action.payload });
      })
      .addCase(fetchNewInProducts.rejected, (state) => {
        state.isLoading = false;
      })
      .addCase(fetchPlusSizeProducts.pending, (state) => {
        state.isLoading = true;
        state.hasMore = true;
      })
      .addCase(
        fetchPlusSizeProducts.fulfilled,
        (state, action) => {
          state.isLoading = false;
          if (action.payload.length < state.perPage) {
            state.hasMore = false;
          }
          filtersSlice.caseReducers.setProducts(state, { payload: action.payload });
        },
      )
      .addCase(fetchPlusSizeProducts.rejected, (state) => {
        state.isLoading = false;
      });
  },
});

export const {
  setFilters,
  setCategory,
  setStartPage,
  setPrevCategory,
  setIsLoading,
  setHasMore,
  setProducts,
  setIsExpanded,
  clearFilters,
  incrementPage,
  setFilterModalOpen,
} = filtersSlice.actions;

export const selectFilters = (state) => state.filters.filters;
export const selectCategory = (state) => state.filters.category;
export const selectProducts = (state) => state.filters.products;
export const selectIsLoading = (state) => state.filters.isLoading;
export const selectHasMore = (state) => state.filters.hasMore;
export const selectIsExpanded = (state) => state.filters.isExpanded;
export const selectFilterModalOpen = (state) => state.filters.filterModalOpen;
export const selectIsMobile = (state) => state.filters.isMobile;

export default filtersSlice.reducer;
