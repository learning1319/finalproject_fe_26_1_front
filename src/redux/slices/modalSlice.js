import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  showEmailWindow: false,
  showWelcomeWindow: false,
  showDeleteAccountWindow: false,
  showLogoutWindow: false,
  showPaymentWindow: false,
  showAddressWindow: false,
  showPasswordWindow: false,
  showUserInfoWindow: false,
  showAddressFormWindow: false,
};

const modalSlice = createSlice({
  name: 'modal',
  initialState,
  reducers: {
    openEmailWindow: (state) => ({
      ...state,
      showEmailWindow: true,
    }),
    closeEmailWindow: (state) => ({
      ...state,
      showEmailWindow: false,
    }),
    openWelcomeWindow: (state) => ({
      ...state,
      showWelcomeWindow: true,
    }),
    closeWelcomeWindow: (state) => ({
      ...state,
      showWelcomeWindow: false,
    }),
    openDeleteAccountWindow: (state) => ({
      ...state,
      showDeleteAccountWindow: true,
    }),
    closeDeleteAccountWindow: (state) => ({
      ...state,
      showDeleteAccountWindow: false,
    }),
    openLogoutWindow: (state) => ({
      ...state,
      showLogoutWindow: true,
    }),
    closeLogoutWindow: (state) => ({
      ...state,
      showLogoutWindow: false,
    }),
    openPaymentWindow: (state) => ({
      ...state,
      showPaymentWindow: true,
    }),
    closePaymentWindow: (state) => ({
      ...state,
      showPaymentWindow: false,
    }),
    openAddressWindow: (state) => ({
      ...state,
      showAddressWindow: true,
    }),
    closeAddressWindow: (state) => ({
      ...state,
      showAddressWindow: false,
    }),
    openPasswordWindow: (state) => ({
      ...state,
      showPasswordWindow: true,
    }),
    closePasswordWindow: (state) => ({
      ...state,
      showPasswordWindow: false,
    }),
    openUserInfoWindow: (state) => ({
      ...state,
      showUserInfoWindow: true,
    }),
    closeUserInfoWindow: (state) => ({
      ...state,
      showUserInfoWindow: false,
    }),
    openAddressFormWindow: (state) => ({
      ...state,
      showAddressFormWindow: true,
    }),
    closeAddressFormWindow: (state) => ({
      ...state,
      showAddressFormWindow: false,
    }),
  },
});

export const {
  openEmailWindow,
  closeEmailWindow,
  openWelcomeWindow,
  closeWelcomeWindow,
  openDeleteAccountWindow,
  closeDeleteAccountWindow,
  openLogoutWindow,
  closeLogoutWindow,
  openPaymentWindow,
  closePaymentWindow,
  openAddressWindow,
  closeAddressWindow,
  openPasswordWindow,
  closePasswordWindow,
  openUserInfoWindow,
  closeUserInfoWindow,
  openAddressFormWindow,
  closeAddressFormWindow,
} = modalSlice.actions;

export default modalSlice.reducer;
