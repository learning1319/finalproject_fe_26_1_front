import { createSlice } from '@reduxjs/toolkit';
import getBannersByName from '../../api/getBannersByName.js';

const bannersSlice = createSlice({
  name: 'banners',
  initialState: {
    items: [],
    dayObject: [],
  },
  reducers: {
    setBanners(state, action) {
      return { ...state, items: action.payload };
    },
    setDayObject(state, action) {
      return { ...state, dayObject: action.payload };
    },
  },
});

export const { setBanners, setDayObject } = bannersSlice.actions;

export const fetchBanners = () => async (dispatch) => {
  try {
    const daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const modiweekBanners = await getBannersByName('Modiweek');
    // const modiweekBanners = banners.filter((banner) => daysOfWeek.includes(banner.name));
    modiweekBanners.sort((a, b) => daysOfWeek.indexOf(a.name) - daysOfWeek.indexOf(b.name));
    dispatch(setBanners(modiweekBanners));
  } catch (error) {
    console.error('Error fetching banners:', error);
  }
};

export default bannersSlice.reducer;
