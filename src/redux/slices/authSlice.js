import { createSlice } from '@reduxjs/toolkit';

const initialState = { data: [] };

const usersReducer = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    addUser(state, action) {
      console.log(action.payload);
      state.data.push(action.payload);
    },
    authenticateUser(state, action) {
      const email = action.payload;
      state.data.map((item) => {
        if (item.email !== email) { return item; } item.isAuthenticated = true; return item;
      });
    },
    removeUser(state, action) {
      const email = action.payload;
      const index = state.data.findIndex((item) => item.email === email);
      if (index !== -1) state.data.splice(index, 1);
    },
  },
});

export const { addUser, authenticateUser, removeUser } = usersReducer.actions;
export default usersReducer.reducer;
