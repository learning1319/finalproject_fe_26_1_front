import { createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import BASE_URL from '../../constants/constants.js';

const initialState = {
  card: {},
};

const cardSlice = createSlice({
  name: 'card',
  initialState,
  reducers: {
    setPayment: (state, action) => {
      state.card = action.payload;
    },
    clearPayment: (state) => {
      state.card = {};
    },
  },
});

export const { setPayment, clearPayment } = cardSlice.actions;

export const addCard = (userId, paymentDetails, token, isDefault = false) => async (dispatch) => {
  if (isDefault) {
    dispatch(setPayment(paymentDetails));
    return axios.post(`${BASE_URL}/payment-methods/default`, {
      userId,
      paymentDetails,
      default: true,
    }, {
      headers: {
        Authorization: token,
      },
    });
  }

  return axios.post(`${BASE_URL}/payment-method`, {
    userId,
    paymentDetails,
  }, {
    headers: {
      Authorization: token,
    },
  });
};

export const removeCard = (userId, token) => async (dispatch) => {
  if (userId) {
    await axios.delete(`${BASE_URL}/payment-methods/delete`, {
      headers: {
        Authorization: token,
      },
      data: {
        userId,
      },
    }).then((response) => response);
  }
  dispatch(clearPayment());
  return 'Card was successfully deleted';
};

export const getCardInfo = (userId, token) => async (dispatch) => {
  if (userId) {
    const response = await axios.get(`${BASE_URL}/payment-methods/default`, {
      params: { userId },
      headers: {
        Authorization: token,
      },
    });
    dispatch(setPayment(response.data));
    return response;
  }
  throw new Error('User ID is required to fetch card information');
};
export default cardSlice.reducer;
