import { createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import BASE_URL from '../../constants/constants.js';

const favoriteSlice = createSlice({
  name: 'favorites',
  initialState: {
    items: [],
  },
  reducers: {
    setFavorites(state, action) {
      return {
        ...state,
        items: action.payload,
      };
    },
    addFavorite(state, action) {
      return {
        ...state,
        items: [...state.items, action.payload],
      };
    },
    removeFavorite(state, action) {
      return {
        ...state,
        items: state.items.filter((item) => item.id !== action.payload),
      };
    },
    clearFavorites(state) {
      return {
        ...state,
        items: [],
      };
    },
  },
});

export const {
  setFavorites, addFavorite, removeFavorite, clearFavorites,
} = favoriteSlice.actions;

export const fetchFavorites = (customerId, token) => async (dispatch) => {
  try {
    const customer = await axios.get(`${BASE_URL}/customers/customer`, { headers: { Authorization: token } });
    const ids = customer.data.wishlist;

    const promises = ids.map((item) => axios.get(`${BASE_URL}/products/ids?pid=${item}`));
    const responses = await Promise.all(promises);

    const favoriteProducts = responses.map((response) => response.data[0]);

    dispatch(setFavorites(favoriteProducts));
  } catch (error) {
    console.error('Error fetching favorites:', error);
  }
};

export const saveFavorite = (product, customerId, token) => async (dispatch) => {
  try {
    if (customerId) {
      const config = {
        headers: {
          Authorization: token,
        },
      };

      console.log('Token:', token);
      console.log('Request Config:', config);
      console.log(customerId);

      // saving id of product in db
      await axios.post(
        `${BASE_URL}/customers/wishlist`,
        { productId: product.id, _id: customerId },
        {
          headers: {
            Authorization: token,
          },
        },
      );
    }
    // saving object in redux
    dispatch(addFavorite(product));
  } catch (error) {
    console.error(error);
  }
};

export const deleteFavorite = (productId, customerId, token) => async (dispatch) => {
  try {
    if (customerId) {
      await axios.delete(`${BASE_URL}/customers/wishlist`, {
        data: { productId, _id: customerId },
        headers: {
          Authorization: token,
        },
      });
    }
    dispatch(removeFavorite(productId));
  } catch (error) {
    console.error(error);
  }
};

export const clearAllFavorites = (customerId, token) => async (dispatch) => {
  console.log(customerId);
  try {
    if (customerId) {
      await axios.delete(`${BASE_URL}/customers/clear`, {
        data: {
          _id: customerId,
        },
        headers: {
          Authorization: token,
        },
      });
    }
    dispatch(clearFavorites());
  } catch (error) {
    console.error('Error clearing favorites:', error);
  }
};
export default favoriteSlice.reducer;
