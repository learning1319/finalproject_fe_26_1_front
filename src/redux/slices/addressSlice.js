/* eslint-disable */
import { createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import BASE_URL from '../../constants/constants.js';

const initialState = {
  address: {},
};

const addressSlice = createSlice({
  name: 'address',
  initialState,
  reducers: {
    setAddress: (state, action) => {
      state.address = action.payload;
    },
    removeAddress: (state) => {
      state.address = {};
    },
  },
});

export const { setAddress, removeAddress } = addressSlice.actions;

export const getAddress = (userId, token) => async (dispatch) => {
  try {
    if (userId) {
      const response = await axios.get(`${BASE_URL}/address/default`, {
        params: { userId },
        headers: {
          Authorization: token,
        },
      });
      dispatch(setAddress(response.data));
      return response.data;
    }
  } catch (e) {
    console.error('Error getting address', e);
  }
};

export const addAddress = (
  userId,
  email,
  firstName,
  lastName,
  country,
  city,
  addressLine1,
  addressLine2,
  zip,
  phone,
  token,
  isDefault = false,
) => async (dispatch) => {
  if (userId) {
    try {
      await axios.post(`${BASE_URL}/address`, {
        userId,
        email,
        firstName,
        lastName,
        country,
        city,
        addressLine1,
        addressLine2,
        zip,
        phone,
        default: isDefault,
      }, {
        headers: {
          Authorization: token,
        },
      });
    } catch (e) {
      console.log(e);
    }
  }

  if (isDefault) {
    dispatch(setAddress({
      email,
      firstName,
      lastName,
      country,
      city,
      addressLine1,
      addressLine2,
      zip,
      phone,
    }));
  }
};

export const updateAddress = (
  userId,
  email,
  firstName,
  lastName,
  country,
  city,
  addressLine1,
  addressLine2,
  zip,
  phone,
  token,
) => async (dispatch) => {
  if (userId) {
    try {
      await axios.put(`${BASE_URL}/address`, {
        userId,
        newAddress: {
          email,
          firstName,
          lastName,
          country,
          city,
          addressLine1,
          addressLine2,
          zip,
          phone,
        },
      }, {
        headers: {
          Authorization: token,
        },
      });
    } catch (e) {
      console.error('Error: ', e);
    }
  }
  dispatch(setAddress({
    email,
    firstName,
    lastName,
    country,
    city,
    addressLine1,
    addressLine2,
    zip,
    phone,
  }));
};

// видаляє дефолт адресу
export const deleteAddress = (userId, token) => async (dispatch) => {
  if (userId) {
    await axios.delete('/address/delete', {
      headers: {
        Authorization: token,
      },
      data: {
        userId,
      },
    });
  }
  dispatch(removeAddress());
};

export default addressSlice.reducer;
