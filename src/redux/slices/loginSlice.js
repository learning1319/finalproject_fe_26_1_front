import { createSlice } from '@reduxjs/toolkit';
import { clearFavorites } from './favoriteSlice.js';

const initialState = {
  isLogged: false,
  user: {},
  token: null,
};

const loginSlice = createSlice({
  name: 'login',
  initialState,
  reducers: {
    loggedIn: (state, action) => {
      state.isLogged = true;
      state.token = action.payload.token;
      state.user = action.payload.user;
      localStorage.setItem(action.payload.user.loginOrEmail, JSON.stringify({
        token: action.payload.token,
        isLogged: true,
      }));
    },
    loggedOutState: (state) => {
      const { loginOrEmail } = state.user;
      state.isLogged = false;
      state.token = null;
      state.user = {};
      if (loginOrEmail) {
        localStorage.setItem(loginOrEmail, JSON.stringify({
          token: null,
          isLogged: false,
        }));
      }
    },
  },
});

export const { loggedIn, loggedOutState } = loginSlice.actions;

export const loggedOut = () => (dispatch) => {
  dispatch(loggedOutState());
  dispatch(clearFavorites());

  const cookies = document.cookie.split('; ');
  for (let i = 0; i < cookies.length; i += 1) {
    const cookieName = cookies[i].split('=')[0];
    document.cookie = `${cookieName}=; Max-Age=0; path=/; domain=${window.location.hostname}`;
  }
};

export default loginSlice.reducer;
