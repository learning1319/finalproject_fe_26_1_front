import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import apiCart from '../../api/apiCart.js';
import fetchUserData from '../../api/getUserData.js';

const initialState = {
  newItem: '',
  items: [],
  processedItems: [],
  isLoggedIn: false,
  token: null,
  userId: null,
  orderedItems: [],
};

const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    setCartItems(state, action) {
      return { ...state, items: action.payload };
    },
    addCartItem(state, action) {
      return { ...state, items: [...state.items, action.payload] };
    },
    removeCartItem(state, action) {
      const itemToRemove = action.payload;
      const [id, color, size] = itemToRemove.split('/');

      state.items = state.items.filter((item) => {
        const itemParts = item.split('/');
        return !(itemParts[0] === id && itemParts[1] === color && itemParts[2] === size);
      });

      state.processedItems = state.processedItems.filter(
        (processedItem) => processedItem !== itemToRemove,
      );
    },

    clearLocalCart(state) {
      return { ...state, items: [], processedItems: [] };
    },
    setUserCart(state, action) {
      return {
        ...state,
        isLoggedIn: true,
        token: action.payload.token,
        userId: action.payload.userId,
      };
    },
    clearUserCart(state) {
      return {
        ...state,
        isLoggedIn: false,
        token: null,
        userId: null,
        items: [],
        processedItems: [],
      };
    },
    updateProcessedItems(state, action) {
      return { ...state, processedItems: Array.isArray(action.payload) ? action.payload : [] };
    },
    updateCartItemQuantity(state, action) {
      const { oldItemString, newItemString } = action.payload;
      const index = state.processedItems.findIndex((item) => item
        .startsWith(oldItemString.split('/').slice(0, 3).join('/')));

      if (index !== -1) {
        const updatedItems = [...state.processedItems];
        updatedItems[index] = newItemString;
        return { ...state, processedItems: updatedItems };
      }
      return state;
    },
    setOrderedProducts(state, action) {
      return { ...state, orderedItems: action.payload };
    },
    addOrderedProducts(state, action) {
      return {
        ...state,
        orderedItems: [...state.orderedItems, ...action.payload],
        items: [],
        processedItems: [],
      };
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchUserCartItems.fulfilled, (state, action) => {
      // Update processedItems based on the fetched cart items
      state.processedItems = action.payload;
    });
    builder.addCase(addCartItemAsync.fulfilled, (state, action) => {
      state.processedItems = action.payload;
    });
    builder.addCase(removeCartItemAsync.fulfilled, (state, action) => {
      // Ensure processedItems is set correctly after removing an item
      if (Array.isArray(action.payload)) {
        state.processedItems = action.payload;
      }
    });
    builder.addCase(addOrderedProductsAsync.fulfilled, (state, action) => {
      state.processedItems = action.payload;
    });
  },
});

export const {
  setCartItems,
  addCartItem,
  removeCartItem,
  clearLocalCart,
  setUserCart,
  clearUserCart,
  updateProcessedItems,
  updateCartItemQuantity,
  addOrderedProducts,
  setOrderedProducts,
} = cartSlice.actions;

export const fetchUserCartItems = createAsyncThunk(
  'cart/fetchUserCartItems',
  async (_, { getState, dispatch }) => {
    const { isLogged, user, token } = getState().login;
    if (isLogged && user._id) {
      try {
        const userData = await fetchUserData(token);
        const dbCartItems = userData.cart || [];
        dispatch(setCartItems(dbCartItems));
        return dbCartItems;
      } catch (error) {
        console.error('Error fetching user cart items:', error);
        throw error;
      }
    } else {
      const localCartItems = getState().cart.processedItems;
      dispatch(setCartItems(localCartItems));
      return localCartItems;
    }
  },
);

export const addCartItemAsync = createAsyncThunk(
  'cart/addCartItemAsync',
  async (item, { getState, dispatch }) => {
    const { isLogged, user, token } = getState().login;
    const itemExistsInState = getState().cart.items.includes(item);
    if (!itemExistsInState) {
      dispatch(addCartItem(item));

      if (isLogged && user._id) {
        try {
          await apiCart.addToCart(item, user, token);
          dispatch(fetchUserCartItems());
        } catch (error) {
          console.error('Error adding item to cart in database:', error);
          throw error;
        }
      } else {
        const localCartItems = getState().cart.items;
        dispatch(updateProcessedItems(localCartItems));
        return localCartItems;
      }
    } else {
      console.log('Item already exists in cart:', item);
    }
    return console.log('123');
  },
);
export const removeCartItemAsync = createAsyncThunk(
  'cart/removeCartItemAsync',
  async (item, { getState, dispatch }) => {
    const { isLogged, user, token } = getState().login;

    // Remove the item locally
    dispatch(removeCartItem(item));

    if (isLogged && user._id) {
      try {
        // Remove item from the server
        await apiCart.deleteFromCart(item, user, token);
        // Fetch the updated cart from the server
        const updatedCartItems = await dispatch(fetchUserCartItems()).unwrap();
        return updatedCartItems; // Ensure this returns the updated processedItems
      } catch (error) {
        console.error('Error removing item from cart in database:', error);
        throw error;
      }
    } else {
      // Handle non-authenticated users locally
      const localCartItems = getState().cart.processedItems.filter(
        (cartItem) => cartItem !== item,
      );
      dispatch(updateProcessedItems(localCartItems));
      return localCartItems;
    }
  },
);

export const clearCartAsync = createAsyncThunk(
  'cart/clearCartAsync',
  async (_, { getState, dispatch }) => {
    const { isLogged, user, token } = getState().login;
    if (isLogged && user._id) {
      try {
        await apiCart.clearCart(user._id, user, token);
        dispatch(clearLocalCart());
        return [];
      } catch (error) {
        console.error('Error clearing cart:', error);
        throw error;
      }
    } else {
      dispatch(clearLocalCart());
      return [];
    }
  },
);

export const addOrderedProductsAsync = createAsyncThunk(
  'cart/addOrderedProductsAsync',
  async (products, { dispatch }) => {
    await dispatch(clearCartAsync());
    dispatch(addOrderedProducts(products));
  },
);

export default cartSlice.reducer;
