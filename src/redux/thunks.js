import {
  fetchProducts,
  fetchNewInProducts,
  incrementPage,
  fetchPlusSizeProducts,
} from './slices/filterSlice.js';

const loadMoreProducts = (page) => async (dispatch) => {
  dispatch(incrementPage());

  if (page === 'newIn') {
    await dispatch(fetchNewInProducts());
  } else if (page === 'plusSize') {
    await dispatch(fetchPlusSizeProducts());
  } else {
    await dispatch(fetchProducts());
  }
};

export default loadMoreProducts;
