import { describe, expect, test } from '@jest/globals';
import currentUserReducer, { addCurrentUser, removeCurrentUser } from '../slices/currentUserSlice.js';

describe('currentUserSlice', () => {
  const state = {
    currentUser: null,
  };
  const user = {
    email: 'testemail@gmail.com',
  };

  test('currentUser is true', () => {
    const actual = currentUserReducer(state, addCurrentUser(user));
    expect(actual.currentUser).toEqual({ email: 'testemail@gmail.com' });
  });
  test('currentUser is false', () => {
    const actual = currentUserReducer(state, removeCurrentUser(user));
    expect(actual.currentUser).toEqual(null);
  });
});
