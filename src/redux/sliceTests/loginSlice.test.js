import { expect, test, describe } from '@jest/globals';
import loginReducer, { loggedIn, loggedOut } from '../slices/loginSlice.js';

describe('loginSlice ', () => {
  const state = {
    isLogged: false,
    user: {},
    token: null,
  };
  const newUser = {
    user: {
      loginOrEmail: 'testemail@gmail.com',
      password: 'random',
    },
    success: true,
    token: 'token1234',
  };
  test('handles logging in', () => {
    const actual = loginReducer(state, loggedIn(newUser));
    expect(actual.isLogged).toEqual(true);
    expect(actual.user).toEqual({
      loginOrEmail: 'testemail@gmail.com',
      password: 'random',
    });
    expect(actual.token).toEqual('token1234');
  });
  test('handles logging out', () => {
    const actual = loginReducer(state, loggedOut(newUser));
    expect(actual.isLogged).toEqual(false);
    expect(actual.user).toEqual({});
    expect(actual.token).toEqual(null);
  });
});
