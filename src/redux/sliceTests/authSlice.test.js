import { expect, test, describe } from '@jest/globals';
import authReducer, { addUser, removeUser, authenticateUser } from '../slices/authSlice.js';

describe('authSlice', () => {
  const state = {
    data: [],
  };
  const newUser = {
    email: 'testemail@gmail.com',
    isAuthenticated: false,
  };
  const withAuthenticatedUserState = {
    data: [{
      email: 'testemail@gmail.com',
      isAuthenticated: true,
    }],
  };
  const withNotAuthenticatedUserState = {
    data: [{
      email: 'testemail@gmail.com',
      isAuthenticated: false,
    }],
  };

  test('adds user', () => {
    const actual = authReducer(state, addUser(newUser));
    expect(actual.data).toEqual([{
      email: 'testemail@gmail.com',
      isAuthenticated: false,
    }]);
  });
  test('changes user data', () => {
    const actual = authReducer(withNotAuthenticatedUserState, authenticateUser('testemail@gmail.com'));
    expect(actual.data).toEqual([{
      email: 'testemail@gmail.com',
      isAuthenticated: true,
    }]);
  });
  test('removes user', () => {
    const actual = authReducer(withAuthenticatedUserState, removeUser('testemail@gmail.com'));
    expect(actual.data).toEqual([]);
  });
});
