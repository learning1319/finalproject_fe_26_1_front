/* eslint-disable */
import { describe, expect, test } from '@jest/globals';
import modalSlice from '../slices/modalSlice.js';

describe('modalSlice', () => {
  test('email window is true', () => {
    const state = { showEmailWindow: false };
    const action = { type: 'modal/openEmailWindow' };
    expect(modalSlice(state, action)).toEqual({ showEmailWindow: true });
  });
  test('email window is false', () => {
    const state = { showEmailWindow: true };
    const action = { type: 'modal/closeEmailWindow' };
    expect(modalSlice(state, action)).toEqual({ showEmailWindow: false });
  });
  test('welcome window is true', () => {
    const state = { showWelcomeWindow: false };
    const action = { type: 'modal/openWelcomeWindow' };
    expect(modalSlice(state, action)).toEqual({ showWelcomeWindow: true });
  });
  test('welcome window is false', () => {
    const state = { showWelcomeWindow: true };
    const action = { type: 'modal/closeWelcomeWindow' };
    expect(modalSlice(state, action)).toEqual({ showWelcomeWindow: false });
  });
});
