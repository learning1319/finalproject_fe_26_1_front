import React from 'react';
import { Outlet } from 'react-router-dom';
import { Box } from '@mui/material';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import Breadcrumbs from './components/Breadcrumbs/Breadcrumbs';
import ScrollToTopButton from './components/ScrollToTopButton/ScrollToTopButton';

const Layout = () => (
  (
    <Box sx={{ minHeight: '100vh' }}>
      <Header />
      <Breadcrumbs />
      <Outlet />
      <ScrollToTopButton />
      <Footer />
    </Box>
  ));

export default Layout;
