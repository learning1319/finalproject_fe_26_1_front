import { createTheme } from '@mui/material/styles';
import '@fontsource/montserrat';
import '@fontsource/montserrat/600.css';
import '@fontsource/montserrat/700.css';
import '@fontsource/league-spartan';
import '@fontsource/league-spartan/700.css';
import '@fontsource/neucha/400.css';

const theme = createTheme({
  palette: {
    primary: {
      main: '#5A6D57',
      light: '#D1D9CF',
      dark: '#748C70',
      gray: '#f0f0f0',
    },
    secondary: {
      main: '#404040',
      light: '#606060',
      dark: '#0C0C0C',
      white: '#FFFFFF',
      gray: '#ADADAD',
    },
    success: {
      main: '#00966D',
    },
    error: {
      main: '#FF4858',
    },
  },
  components: {
    MuiButton: {
      variants: [
        {
          props: (props) => props.variant === 'text',
          style: {
            color: '#0C0C0C',
          },
        },
      ],
      styleOverrides: {
        root: {
          borderRadius: '0px',
          height: '2.5rem',
        },
      },
    },
    MuiContainer: {
      styleOverrides: {
        root: {
          maxWidth: '1200px',
          minWidth: '319px',
          padding: '0px 20px',
        },
      },
      variants: [
        {
          props: { variant: 'fullWidth' },
          style: {
            display: 'flex',
            flexDirection: 'column',
            gap: '24px',
            padding: '0px',
            '@media (min-width:500px)': {
              gap: '32px',
            },
            '@media (min-width:768px)': {
              flexDirection: 'row',
              gap: '30px',
            },
          },
        },
        {
          props: { variant: 'formContainer' },
          style: {
            maxWidth: '600px',
            display: 'flex',
            flexDirection: 'column',
            gap: '20px',
            justifyContent: 'center',
            alignItems: 'center',
            padding: '0px 40px',
            '@media (min-width:525px)': {
              padding: '0px 70px',
            },
            '@media (min-width:768px)': {
              padding: '0px 30px',
            },
            '@media (min-width:900px)': {
              padding: '0px 50px',
            },
            '@media (min-width:1024px)': {
              padding: '0px 30px',
            },
          },
        },
      ],
    },
    MuiMenu: {
      styleOverrides: {
        list: {
          padding: '8px 20px',
        },
        paper: {
          minWidth: '319px',
        },
      },
    },
    MuiPaper: {
      styleOverrides: {
        root: {
          boxShadow: 'none',
        },
      },
    },
    MuiPopover: {
      styleOverrides: {
        paper: {
          position: 'unset',
        },
      },
    },
    MuiAccordion: {
      styleOverrides: {
        root: {
          '&.Mui-expanded::before': {
            opacity: '1',
          },
        },
      },
    },
  },
  breakpoints: {
    values: {
      small: 0,
      mobile: 320,
      middle: 500,
      tablet: 768,
      large: 900,
      desktop: 1024,
      bigScreen: 1400,
    },
  },
  typography: {
    fontFamily: 'Montserrat, sans-serif',
    desktop_logoName: {
      fontFamily: 'League Spartan, sans-serif',
      fontWeight: 700,
      fontSize: 32,
      lineHeight: '13px',
      letterSpacing: '3px',
    },
    mobile_logoName: {
      fontFamily: 'League Spartan, sans-serif',
      fontWeight: 700,
      fontSize: 24,
      lineHeight: '10px',
      letterSpacing: '2px',
    },
    desktop_logoSubName: {
      fontFamily: 'League Spartan, sans-serif',
      fontWeight: 400,
      fontSize: 10,
      lineHeight: '36px',
      letterSpacing: '3px',
    },
    mobile_logoSubName: {
      fontFamily: 'League Spartan, sans-serif',
      fontWeight: 400,
      fontSize: 7.5,
      lineHeight: '9.38px',
      letterSpacing: '2px',
    },
    desktop_bodyXL: {
      fontFamily: 'Montserrat, sans-serif',
      fontWeight: 400,
      fontSize: 20,
      lineHeight: '12.5px',
    },
    desktop_bodyLG: {
      fontFamily: 'Montserrat, sans-serif',
      fontWeight: 400,
      fontSize: 18,
      lineHeight: '32px',
    },
    mobile_bodyLG: {
      fontFamily: 'Montserrat, sans-serif',
      fontWeight: 400,
      fontSize: 14,
      lineHeight: '25.2px',
    },
    desktop_bodyMD: {
      fontFamily: 'Montserrat, sans-serif',
      fontWeight: 400,
      fontSize: 16,
      lineHeight: '28.8px',
    },
    tablet_bodyMD: {
      fontFamily: 'Montserrat, sans-serif',
      fontWeight: 400,
      fontSize: 14,
      lineHeight: '26.8px',
    },
    mobile_bodyMD: {
      fontFamily: 'Montserrat, sans-serif',
      fontWeight: 400,
      fontSize: 14,
      lineHeight: '19.6px',
    },
    mobile_bodySM: {
      fontFamily: 'Montserrat, sans-serif',
      fontWeight: 400,
      fontSize: 12,
      lineHeight: '16.8px',
    },
    bodyXS: {
      fontFamily: 'Montserrat, sans-serif',
      fontWeight: 400,
      fontSize: 12,
      lineHeight: '22px',
    },
    mobile_bodyXS: {
      fontFamily: 'Montserrat, sans-serif',
      fontWeight: 400,
      fontSize: 10,
      lineHeight: '14px',
    },
    mobile_h1: {
      fontFamily: 'Montserrat, sans-serif',
      fontWeight: 700,
      fontSize: 24,
      lineHeight: '36.6px',
    },
    mobile_h2: {
      fontFamily: 'Montserrat, sans-serif',
      fontWeight: 700,
      fontSize: 20,
      lineHeight: '28px',
    },
    desktop_h3: {
      fontFamily: 'Montserrat, sans-serif',
      fontWeight: 600,
      fontSize: 32,
      lineHeight: '44.8px',
    },
    mobile_h3: {
      fontFamily: 'Montserrat, sans-serif',
      fontWeight: 700,
      fontSize: 16,
      lineHeight: '22.4px',
    },
    mobile_h4: {
      fontFamily: 'Montserrat, sans-serif',
      fontWeight: 600,
      fontSize: 14,
      lineHeight: '19.6px',
    },
    desktop_h5: {
      fontFamily: 'Montserrat, sans-serif',
      fontWeight: 700,
      fontSize: 20,
      lineHeight: '28px',
    },
    desktop_h6: {
      fontFamily: 'Montserrat, sans-serif',
      fontWeight: 700,
      fontSize: 16,
      lineHeight: '22px',
    },
    desktop_overlineSM: {
      fontFamily: 'Montserrat, sans-serif',
      fontWeight: 600,
      fontSize: 12,
      lineHeight: '15px',
    },
    mobile_overline: {
      fontFamily: 'Montserrat, sans-serif',
      fontWeight: 600,
      fontSize: 10,
      lineHeight: '14px',
    },
    desktop_captionMD: {
      fontFamily: 'Montserrat, sans-serif',
      fontWeight: 400,
      fontSize: 12,
      lineHeight: '22px',
    },
    mobile_captionSM: {
      fontFamily: 'Montserrat, sans-serif',
      fontWeight: 600,
      fontSize: 10,
      lineHeight: '18px',
    },
    button_SM: {
      fontFamily: 'Montserrat, sans-serif',
      fontWeight: 400,
      fontSize: 14,
      lineHeight: '24px',
    },
    desktop_banner: {
      fontFamily: 'Neucha, sans-serif',
      fontWeight: 400,
      fontSize: 34,
      lineHeight: '61.2px',
    },
    mobile_banner: {
      fontFamily: 'Neucha, sans-serif',
      fontWeight: 400,
      fontSize: 20,
      lineHeight: '36px',
    },
    tablet_banner: {
      fontFamily: 'Neucha, sans-serif',
      fontWeight: 400,
      fontSize: 28,
      lineHeight: '50.4px',
    },
    desktop_sustainability: {
      fontFamily: 'Montserrat, sans-serif',
      fontWeight: 400,
      fontSize: 20,
      lineHeight: '36px',
    },
  },
});
export default theme;
