import { useState, useEffect } from 'react';

const usePagination = () => {
  const [page, setPage] = useState(1);
  useEffect(() => {
    console.log('USE-PAGINATION!!!', page);
  }, [page]);

  return [page, setPage];
};

export default usePagination;
