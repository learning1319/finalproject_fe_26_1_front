// src/hooks/useSorting.js

import { useState, useCallback } from 'react';

const useSorting = (onSortChange, initialSort = 'price_asc') => {
  const [sortValue, setSortValue] = useState(initialSort);

  const handleSortChange = useCallback((event) => {
    const { value } = event.target;
    setSortValue(value);
    // Check if onSortChange is a function before calling it
    if (typeof onSortChange === 'function') {
      onSortChange(value);
    }
  }, [onSortChange]);

  return {
    sortValue,
    handleSortChange,
  };
};

export default useSorting;
