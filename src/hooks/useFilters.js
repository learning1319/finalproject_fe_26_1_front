import { useState, useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { useMediaQuery } from '@mui/material';
import axios from 'axios';
import theme from '../themeStyle';
import useQuery from './useQuery.js';
import BASE_URL from '../constants/constants.js';

const useFilters = (
  initialFilters,
  setFilters,
  setFilteredProducts,
  baseURL,
  queryCategory,
  setFilterOn,
  page,
) => {
  const navigate = useNavigate();
  const isMobile = useMediaQuery(theme.breakpoints.down('tablet'));
  const location = useLocation();
  const [queryParams, setQueryParams] = useQuery(location.search);

  const [selectedFilters, setSelectedFilters] = useState(() => {
    const initial = Object.fromEntries(new URLSearchParams(location.search).entries());
    Object.keys(initial).forEach((key) => {
      initial[key] = initial[key].split(',');
    });
    return initial;
  });

  const getQueryString = () => {
    const updatedQueryParams = new URLSearchParams(queryParams);
    if (queryCategory) {
      updatedQueryParams.set('category', queryCategory);
    }
    if (page === 'newIn') {
      updatedQueryParams.set('new', 'true');
    }
    Object.keys(selectedFilters).forEach((key) => {
      if (selectedFilters[key].length > 0) {
        updatedQueryParams.set(key, selectedFilters[key].join(','));
      }
    });
    return updatedQueryParams.toString();
  };

  const fetchProducts = async (queryString) => {
    try {
      const { data } = await axios.get(`${BASE_URL}/products/filter?${queryString}`, {
        params: {
          perPage: 6,
          startPage: 1,
        },
      });
      setFilteredProducts(data);
    } catch (error) {
      console.error('Error fetching products:', error);
    }
  };

  // useEffect(() => {
  //   console.log('call 1');
  //   const queryString = getQueryString();
  //   setQueryParams(new URLSearchParams(queryString));
  //   fetchProducts(queryString);
  // }, [queryCategory, page, selectedFilters]);
  useEffect(() => {
    const queryString = getQueryString();
    // Check if the query string has changed before updating queryParams
    console.log('queryString', queryString);
    console.log('queryParams.toString()', queryParams.toString());
    if (queryString !== queryParams.toString()) {
      setQueryParams(new URLSearchParams(queryString));
    }
  }, [queryCategory, page, selectedFilters]);

  // useEffect(() => {
  //   console.log('call 2');
  //   fetchProducts(getQueryString());
  // }, [queryParams]);
  useEffect(() => {
    fetchProducts(queryParams.toString()); // Fetch products only when queryParams change
  }, [queryParams]);

  const [isExpanded, setIsExpanded] = useState({
    fabric: false,
    size: false,
    color: false,
    sort: false,
  });

  useEffect(() => {
    const newFilters = {};
    Object.keys(selectedFilters).forEach((category) => {
      selectedFilters[category].forEach((filter) => {
        newFilters[filter] = true;
      });
    });
    setFilters(newFilters);
  }, [selectedFilters, setFilters]);

  const updateQueryParams = (filters) => {
    const updatedQueryParams = new URLSearchParams(queryParams);
    Object.keys(filters).forEach((category) => {
      const selectedFiltersForCategory = filters[category] || [];
      if (selectedFiltersForCategory.length > 0) {
        updatedQueryParams.set(category, selectedFiltersForCategory.join(','));
      } else {
        updatedQueryParams.delete(category);
      }
    });
    navigate(`${baseURL}?${updatedQueryParams.toString()}`);
    setQueryParams(updatedQueryParams);
  };

  const handleFilterChange = async (e, categoryName) => {
    const { name, checked } = e.target;

    setFilters((prevFilters) => ({
      ...prevFilters,
      [name]: checked,
    }));

    setSelectedFilters((prevSelectedFilters) => {
      const categoryFilters = prevSelectedFilters[categoryName] || [];
      if (checked) {
        return {
          ...prevSelectedFilters,
          [categoryName]: [...categoryFilters, name],
        };
      }
      return {
        ...prevSelectedFilters,
        [categoryName]: categoryFilters.filter((filter) => filter !== name),
      };
    });

    const updatedFilters = {
      ...selectedFilters,
      [categoryName]: checked
        ? [...(selectedFilters[categoryName] || []), name]
        : (selectedFilters[categoryName] || []).filter((filter) => filter !== name),
    };

    updateQueryParams(updatedFilters);
    console.log('call 3');
    // fetchProducts(getQueryString());
  };

  const handleDeleteFilter = (category, filter) => {
    setFilters((prevFilters) => ({
      ...prevFilters,
      [filter]: false,
    }));
    setSelectedFilters((prevSelectedFilters) => {
      const updatedCategoryFilters = prevSelectedFilters[category].filter(
        (item) => item !== filter,
      );
      const updatedSelectedFilters = {
        ...prevSelectedFilters,
        [category]: updatedCategoryFilters,
      };
      updateQueryParams(updatedSelectedFilters); // Update queryParams after deleting filter
      return updatedSelectedFilters;
    });
  };

  const handleClearFilters = async () => {
    setFilters(initialFilters);
    setSelectedFilters({});
    setIsExpanded({
      fabric: false,
      size: false,
      color: false,
      sort: false,
    });
    navigate({ search: '' });
    setQueryParams('');
    console.log('call 5');
    fetchProducts('');
  };

  const handleSetIsExpanded = (categoryName) => () => {
    setIsExpanded((prev) => ({
      ...prev,
      [categoryName]: !prev[categoryName],
    }));
  };

  const handleApplyFilters = () => {
    updateQueryParams(selectedFilters);
    if (isMobile) {
      setFilterOn(false);
    }
  };

  const hasSelectedFilters = Object.values(selectedFilters).some((filters) => filters.length > 0);

  return {
    isExpanded,
    selectedFilters,
    handleFilterChange,
    handleClearFilters,
    isMobile,
    updateQueryParams,
    handleSetIsExpanded,
    handleDeleteFilter,
    hasSelectedFilters,
    handleApplyFilters,
    getQueryString,
  };
};

export default useFilters;
