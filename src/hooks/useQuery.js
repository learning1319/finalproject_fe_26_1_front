import { useState } from 'react';

const useQuery = (initialQuery) => {
  const [query, setQuery] = useState(new URLSearchParams(initialQuery));

  return [query, setQuery];
};

export default useQuery;
