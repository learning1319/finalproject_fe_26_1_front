const productsNames = [{
  id: '2024-07-19T23:36:08.090Z0.7158577459827218',
  name: 'Elegant Cotton Top',
},
{
  id: '2024-07-19T23:54:48.031Z0.05735388659229779',
  name: 'Modern Linen Top',
},
{
  id: '2024-07-20T08:11:01.306Z0.5532247269542843',
  name: 'Chic Silk Top',
},
{
  id: '2024-07-20T08:16:03.152Z0.24265740435189076',
  name: 'Casual Cotton Tee',
},
{
  id: '2024-07-20T08:21:44.996Z0.47702305346837237',
  name: 'Elegant Lace Blouse',
},
{
  id: '2024-07-20T08:29:37.002Z0.8126600344884802',
  name: 'Sporty Activewear Top',
},
{
  id: '2024-07-20T08:37:06.626Z0.2850558537607',
  name: 'Classic Denim Top',
},
{
  id: '2024-07-20T08:48:02.810Z0.792457028007046',
  name: 'Floral Print Top',
},
{
  id: '2024-07-20T09:00:52.423Z0.27854625589216986',
  name: 'Boho Chic Top',
},
{
  id: '2024-07-20T09:00:52.423Z0.1886577978420012',
  name: 'Vintage Polka Dot Top',
},
{
  id: '2024-07-20T20:32:43.215Z0.32740167343022186',
  name: 'Elegant Evening Dress',
},
{
  id: '2024-07-20T20:35:16.183Z0.22409880540223726',
  name: 'Floral Summer Dress',
},
{
  id: '2024-07-20T20:36:10.648Z0.6559597209402694',
  name: 'Bohemian Maxi Dress',
},
{
  id: '2024-07-20T20:36:52.900Z0.6208784059485368',
  name: 'Classic Little Dress',
},
{
  id: '2024-07-20T20:37:36.700Z0.8162397944444324',
  name: 'Vintage Lace Dress',
},
{
  id: '2024-07-20T20:38:05.978Z0.3931206781145937',
  name: 'Casual Denim Dress',
},
{
  id: '2024-07-20T20:40:26.188Z0.585865129809815',
  name: 'Chiffon Party Dress',
},
{
  id: '2024-07-20T20:51:36.719Z0.21967188952259686',
  name: 'Chiffon Party Dress',
},
{
  id: '2024-07-20T22:34:19.751Z0.27392971545021716',
  name: 'First Love Party Dress',
},
{
  id: '2024-07-20T22:34:19.751Z0.16699801850064855',
  name: 'Night Kiss',
},
{
  id: '2024-07-21T14:19:36.693Z0.4058058093680701',
  name: 'Classic Chinos',
},
{
  id: '2024-07-21T14:20:36.726Z0.5912468514085443',
  name: 'Wide Leg Trousers',
},
{
  id: '2024-07-21T14:20:36.725Z0.7739655332886874',
  name: 'Slim Fit',
},
{
  id: '2024-07-21T14:20:36.726Z0.765889450679611',
  name: 'Cargo Pants',
},
{
  id: '2024-07-21T14:20:36.726Z0.6141816710288213',
  name: 'Jogger Pants',
},
{
  id: '2024-07-21T14:20:36.726Z0.5311588302177725',
  name: 'Formal Dress Pants',
},
{
  id: '2024-07-21T14:20:36.726Z0.08432774786410535',
  name: 'High-Waisted Pants',
},
{
  id: '2024-07-21T14:57:05.302Z0.42372964158264104',
  name: 'Chic Linen Blouse',
},
{
  id: '2024-07-21T14:57:05.302Z0.05521169945324367',
  name: 'Elegant Chiffon Blouse',
},
{
  id: '2024-07-21T14:57:05.302Z0.624488844210126',
  name: 'Floral Print Blouse',
},
{
  id: '2024-07-21T14:57:05.302Z0.5175618669894095',
  name: 'Modern Silk Blouse',
},
{
  id: '2024-07-21T14:57:05.302Z0.009215477861513222',
  name: 'Bohemian Chic Blouse',
},
{
  id: '2024-07-21T14:57:05.302Z0.8530481791163049',
  name: 'Classic Cotton Blouse',
},
{
  id: '2024-07-21T21:02:51.084Z0.7383449802015007',
  name: 'Sporty Mesh Shorts',
},
{
  id: '2024-07-21T21:02:51.084Z0.5004810223428668',
  name: 'Cargo Shorts',
},
{
  id: '2024-07-21T21:02:51.084Z0.7587590102882189',
  name: 'Board Shorts',
},
{
  id: '2024-07-21T21:02:51.084Z0.13401620827718674',
  name: 'Classic Denim Shorts',
},
{
  id: '2024-07-21T21:02:51.084Z0.04662168497805119',
  name: 'Linen Shorts',
},
{
  id: '2024-07-21T21:02:51.084Z0.23000614856716672',
  name: 'Chino Shorts',
},
{
  id: '2024-07-21T21:48:09.988Z0.40642135259018963',
  name: 'Chunky Knit Pullover',
},
{
  id: '2024-07-21T21:48:09.988Z0.6004218442075351',
  name: 'Elegant Mock-Neck Pullover',
},
{
  id: '2024-07-21T21:48:09.988Z0.791210301337971',
  name: 'Cozy Knit Pullover',
},
{
  id: '2024-07-21T21:48:09.988Z0.25220321967942305',
  name: 'Soft Cashmere Pullover',
},
{
  id: '2024-07-21T21:48:09.988Z0.1654022738345975',
  name: 'Classic V-Neck Pullover',
},
{
  id: '2024-07-21T21:48:09.988Z0.7821434059347212',
  name: 'Sporty Zip-Up Pullover',
},
{
  id: '2024-07-21T21:48:09.988Z0.4541807314927364',
  name: 'Lightweight Summer Pullover',
},
{
  id: '2024-08-08T06:21:29.144Z0.4787370370235329',
  name: 'Elegant Plus Size Blouse',
},
{
  id: '2024-08-08T06:21:29.144Z0.6740814726867295',
  name: 'Comfortable Plus Size T-Shirt',
},
{
  id: '2024-08-08T06:21:29.144Z0.5345259729158225',
  name: 'Chic Plus Size Tunic',
},
{
  id: '2024-08-08T06:21:29.144Z0.6101854372456801',
  name: 'Top mouse',
},
{
  id: '2024-08-08T06:21:29.144Z0.623842834646124',
  name: 'Trendy Plus Size Top',
},
{
  id: '2024-08-08T06:21:29.144Z0.38060774356312455',
  name: 'Stylish Plus Size Top',
},
{
  id: '2024-08-08T06:23:13.098Z0.46932342193069765',
  name: 'Sophisticated Plus Size Dress Pants',
},
{
  id: '2024-08-08T06:23:13.098Z0.6791015086884087',
  name: 'Elegant Plus Size Trousers',
},
{
  id: '2024-08-08T06:23:13.098Z0.5314579806482753',
  name: 'Chic Plus Size Pants',
},
{
  id: '2024-08-08T06:23:13.098Z0.46720041011833624',
  name: 'Comfortable Plus Size Slacks',
},
{
  id: '2024-08-08T06:24:49.697Z0.34112559821547483',
  name: 'Elegant Plus Size Pullover',
},
{
  id: '2024-08-08T06:24:49.697Z0.28054734425562056',
  name: 'Classic Plus Size Pullover',
},
{
  id: '2024-08-08T06:24:49.697Z0.6914079860228981',
  name: 'Chic Plus Size Pullover',
},
{
  id: '2024-08-08T06:24:49.697Z0.6048000628171735',
  name: 'Cozy Plus Size Pullover',
},
{
  id: '2024-08-08T06:24:49.697Z0.26691447880669816',
  name: 'Modern Plus Size Pullover',
},
{
  id: '2',
  name: 'Cool Jeans',
},
{
  id: '1',
  name: 'New Blouses Zara',
},
{
  id: '4',
  name: 'Elegant Dress',
},
{
  id: '12',
  name: 'Pants',
},
{
  id: '8',
  name: 'Pants super',
},
{
  id: '3',
  name: 'Elegant Dress',
},
{
  id: '7',
  name: 'Super Blouse',
},
{
  id: '9',
  name: "Line's Blouse",
},
{
  id: '11',
  name: 'Blouse',
},
{
  id: '6',
  name: 'Pants',
},
{
  id: '5',
  name: 'Elegant Blouse',
},
{
  id: '10',
  name: 'Short DER',
}];

const segmentDisplayNames = {
  plussize: 'Plus Size',
  shopAll: 'Shop All',
  newIn: 'New In',
  contactUs: 'Contact Us',
  userProfile: 'User Profile',
  policy: 'Privacy Policy',
  visitUs: 'Visit Us',
  returnsRefunds: 'Returns & Refunds',
  orderShipping: 'Order & Shipping',
  termsConditions: 'Terms & Conditions',
  wishList: 'Wish List',
};

productsNames.forEach((product) => {
  segmentDisplayNames[product.id] = product.name;
});

export default segmentDisplayNames;
