import React from 'react';
import PropTypes from 'prop-types';

const Leaf = ({ width = 20, height = 20, currentColor }) => (
  <svg width={width} height={height} viewBox="0 0 11 10" fill={currentColor} xmlns="http://www.w3.org/2000/svg">
    <path
      d="M4.95057 8.63531C4.78251 8.88741 4.6472 9.16113 4.5413 9.45506L4.54694 9.46093H5.54102C6.59888 9.46093 7.47407 9.18165 8.10687 8.5486C8.74037 7.91485 9.14924 6.90835 9.23547 5.41567V4.84982H7.96672C6.42025 4.84982 5.41419 5.19378 4.79524 5.7307C4.17939 6.26494 3.92744 7.00725 3.92505 7.85V7.85078C3.92505 8.1441 3.92505 8.67374 4.35583 9.23885C4.45986 8.97662 4.58785 8.7287 4.74256 8.49663C5.14757 7.88912 5.73117 7.39704 6.53191 7.04115C6.595 7.01311 6.66887 7.04153 6.69691 7.10461C6.72494 7.1677 6.69653 7.24157 6.63345 7.2696C5.87169 7.60816 5.32682 8.07094 4.95057 8.63531Z"
      stroke="#4A4A4A"
      strokeWidth="0.5"
    />
  </svg>
);

Leaf.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  currentColor: PropTypes.string.isRequired,
};

export default Leaf;
