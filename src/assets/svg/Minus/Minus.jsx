import theme from '../../../themeStyle';

function Minus() {
  return (
    <svg width={20} height={20} viewBox="0 0 24 24">
      <path d="M19,13H5V11H19V13Z" fill={theme.palette.primary.dark} />
    </svg>
  );
}

export default Minus;
