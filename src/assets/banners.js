import getBannersByCategory from '../api/getBannersByCategory.js';

export const mainBanner = await getBannersByCategory('newInBanner');

export const mainBannerSustainability = await getBannersByCategory('sustainability');

// Collection section banners

export const collectionBanners = await getBannersByCategory('collection');

export const mobileBanners = collectionBanners.map((item) => ({
  ...item,
  height: 180,
}));
export const tabletBanners = collectionBanners.map((item) => ({
  ...item,
  height: 300,
}));
export const desktopBanners = collectionBanners.map((item) => ({
  ...item,
  height: 450,
}));
