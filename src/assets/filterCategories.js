export const regularSizeCategories = [
  { name: 's', label: 'S / US (0-4)' },
  { name: 'm', label: 'M / US (6-10)' },
  { name: 'l', label: 'L / US (10-14)' },
];
export const plusSizeCategories = [
  { name: '1x', label: '1X / US (16-18)' },
  { name: '2x', label: '2X / US (20-22)' },
  { name: '3x', label: '3X / US (24-26)' },
];
export const colorCategories = [
  { name: 'blue', label: 'Blue' },
  { name: 'black', label: 'Black' },
  { name: 'red', label: 'Red' },
  { name: 'white', label: 'White' },
  { name: 'beige', label: 'Beige' },
  { name: 'green', label: 'Green' },
  { name: 'orange', label: 'Orange' },
  { name: 'gray', label: 'Gray' },
  { name: 'purple', label: 'Purple' },
  { name: 'brown', label: 'Brown' },
];
export const fabricCategories = [
  { name: 'cotton', label: 'Cotton' },
  { name: 'linen', label: 'Linen' },
  { name: 'silk', label: 'Silk' },
  { name: 'lace', label: 'Lace' },
  { name: 'chiffon', label: 'Chiffon' },
  { name: 'denim', label: 'Denim' },
];

export const sortCategories = [
  { name: '+price', label: 'Price: high to low' },
  { name: '-price', label: 'Price: low to high' },
  { name: 'bestSeller', label: 'Bestsellers' },
];
