import axios from 'axios';
import BASE_URL from '../constants/constants.js';

const fetchUserData = async (token) => {
  try {
    const response = await axios.get(`${BASE_URL}/customers/customer`, {
      headers: {
        Authorization: token,
      },
    });
    return response.data;
  } catch (error) {
    console.log('Error fetching user data:', error);
    throw error;
  }
};

export default fetchUserData;
