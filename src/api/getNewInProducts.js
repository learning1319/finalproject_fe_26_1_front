import axios from 'axios';
import BASE_URL from '../constants/constants.js';

const getNewInProducts = async (perPage = 6, page = 1) => {
  try {
    const { data } = await axios.get(`${BASE_URL}/products/filter?new=true`, {
      params: {
        perPage,
        startPage: page,
      },
    });
    return data;
  } catch (err) {
    console.log(err);
    throw err;
  }
};

export default getNewInProducts;
