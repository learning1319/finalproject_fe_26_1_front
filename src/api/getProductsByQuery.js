import axios from 'axios';
import BASE_URL from '../constants/constants.js';

const getProductsByQuery = async (queryString) => {
  try {
    const { data } = await axios.get(`${BASE_URL}/products/filter?${queryString}`);
    // const { data: products } = data;
    return data;
  } catch (err) {
    console.log(err);
    throw err;
  }
};

export default getProductsByQuery;
