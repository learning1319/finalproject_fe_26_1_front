import axios from 'axios';
import BASE_URL from '../constants/constants.js';

const fetchFeedback = async (token, id) => {
  try {
    const { data } = await axios.get(`${BASE_URL}/comments/product/id?productId=${id}`, {
      headers: {
        Authorization: token,
      },
    });
    return data;
  } catch (error) {
    console.error('Error fetching user data:', error);
    throw error;
  }
};

export default fetchFeedback;
