/*eslint-disable*/
const getProductIdByName = async (productName) => {
    try {
      const response = await fetch(`/api/products?name=${encodeURIComponent(productName)}`);
      const data = await response.json();
      return data.id;
    } catch (error) {
      console.error('Error fetching product ID:', error);
      throw error;
    }
  };

  export default getProductIdByName;