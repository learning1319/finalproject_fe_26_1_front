import axios from 'axios';
import BASE_URL from '../constants/constants.js';

const getUserAddress = async (token) => {
  try {
    console.log('Sending request to get user address');
    const response = await axios.get(`${BASE_URL}/address/default`, {
      headers: {
        Authorization: token,
      },
    });
    console.log('Response received:', response);
    return response.data;
  } catch (error) {
    console.error('Error getting user address:', error);
    throw error;
  }
};

export default getUserAddress;
