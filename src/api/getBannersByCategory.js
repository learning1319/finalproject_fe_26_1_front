import axios from 'axios';
import BASE_URL from '../constants/constants.js';

const getBannersByCategory = async (category) => {
  try {
    const { data } = await axios.get(`${BASE_URL}/banners/${category}`);
    return data;
  } catch (err) {
    console.log(err);
    throw err;
  }
};

export default getBannersByCategory;
