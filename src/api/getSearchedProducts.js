import axios from 'axios';
import BASE_URL from '../constants/constants.js';

const getSearchedProducts = async (query) => {
  try {
    const response = await axios.post(`${BASE_URL}/products/search`, {
      text: query,
    });
    console.log(response);
    return response;
  } catch (err) {
    console.log(err);
    throw err;
  }
};

export default getSearchedProducts;
