import axios from 'axios';
import BASE_URL from '../constants/constants.js';

const getBannersByName = async (name) => {
  try {
    const { data } = await axios.get(`${BASE_URL}/banners/category/${name}`);
    return data;
  } catch (err) {
    console.error('Error fetching banners:', err);
    throw err;
  }
};

export default getBannersByName;
