import axios from 'axios';
import BASE_URL from '../constants/constants.js';

const loginCustomer = async (userData) => {
  try {
    const loginResult = await axios.post(`${BASE_URL}/customers/login`, userData);
    console.log(loginResult);
    return loginResult;
  } catch (err) {
    console.log(err);
    return false;
  }
};
export default loginCustomer;
