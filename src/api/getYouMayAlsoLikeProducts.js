import axios from 'axios';
import BASE_URL from '../constants/constants.js';

const getYouMayAlsoLikeProducts = async (prefer = 'anything') => {
  try {
    const { data } = await axios.get(`${BASE_URL}/products/filter?random=${prefer}`);
    return data;
  } catch (err) {
    console.log(err);
    throw err;
  }
};

export default getYouMayAlsoLikeProducts;
