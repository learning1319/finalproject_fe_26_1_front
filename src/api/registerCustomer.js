import axios from 'axios';
import BASE_URL from '../constants/constants.js';

const registerCustomer = async (payload) => {
  const registrationResult = await axios.post(`${BASE_URL}/customers`, payload);
  return { ...registrationResult.data };
};
export default registerCustomer;
