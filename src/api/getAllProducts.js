import axios from 'axios';
import BASE_URL from '../constants/constants.js';

const getAllProducts = async (perPage, startPage) => {
  try {
    const { data } = await axios.get(`${BASE_URL}/products`, {
      params: {
        perPage,
        startPage,
      },
    });
    const { data: products } = data;
    return products;
  } catch (err) {
    console.log(err);
    throw err;
  }
};

export default getAllProducts;
