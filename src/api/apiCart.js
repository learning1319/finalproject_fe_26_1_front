import axios from 'axios';
import BASE_URL from '../constants/constants.js';

const addToCart = async (productId, user, token) => {
  try {
    const addProductToCart = await axios.post(`${BASE_URL}/customers/cart`, {
      productId,
      _id: user._id,
    }, {
      headers: {
        Authorization: token,
      },
    });
    console.log(addProductToCart);
    return addProductToCart;
  } catch (err) {
    console.log(err);
    return false;
  }
};
const deleteFromCart = async (productId, user, token) => {
  try {
    const deleteProductFromCart = await axios.delete(`${BASE_URL}/customers/cart`, {
      headers: {
        Authorization: token,
      },
      data: {
        productId,
        _id: user._id,
      },
    });
    console.log(deleteProductFromCart);
    return deleteProductFromCart;
  } catch (err) {
    console.log(err);
    return false;
  }
};

const clearCart = async (productId, user, token) => {
  try {
    const clearProductCart = await axios.delete(`${BASE_URL}/customers/clearCart`, {
      headers: {
        Authorization: token,
      },
      data: {
        _id: user._id,
      },
    });
    console.log(clearProductCart);
    return clearProductCart;
  } catch (err) {
    console.log(err);
    return false;
  }
};

export default { addToCart, deleteFromCart, clearCart };
