/* eslint-disable */
import axios from 'axios';
import BASE_URL from '../constants/constants.js';

const getProductById = async (id) => {
  try {
    const { data } = await axios.get(`${BASE_URL}/products?id=${id}`, {
      data: {
        id,
      },
    });
    console.log(data.data);
    if (!data) {
      throw new Error('Product not found');
    }
    return data;
  } catch (err) {
    console.error('Error fetching product:', err);
    throw err;
  }
};

export default getProductById;