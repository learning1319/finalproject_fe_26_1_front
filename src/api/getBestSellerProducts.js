import axios from 'axios';
import BASE_URL from '../constants/constants.js';

const getBestSellerProducts = async (queryString = '') => {
  const queryParams = new URLSearchParams(queryString);

  queryParams.append('sort', 'bestSeller');

  const queryStr = queryParams.toString();
  const URL = `${BASE_URL}/products/filter?${queryStr}`;

  try {
    const response = await axios.get(URL);
    return response.data;
  } catch (err) {
    console.log('Error fetching best seller products:', err.response ? err.response.data : err.message);
    throw err;
  }
};

export default getBestSellerProducts;
